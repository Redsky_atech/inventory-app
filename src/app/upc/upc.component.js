"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_barcodescanner_1 = require("nativescript-barcodescanner");
var router_1 = require("nativescript-angular/router");
var modal_component_1 = require("../modals/modal.component");
var http_1 = require("@angular/common/http");
var router_2 = require("@angular/router");
var user_service_1 = require("../services/user.service");
var UpcComponent = /** @class */ (function () {
    function UpcComponent(activatedRoute, barcodeScanner, routerExtensions, http, userService) {
        var _this = this;
        this.activatedRoute = activatedRoute;
        this.barcodeScanner = barcodeScanner;
        this.routerExtensions = routerExtensions;
        this.http = http;
        this.userService = userService;
        this.id = "";
        this.isFrontCamera = false;
        this.torchOn = false;
        this.isScannedOnce = false;
        this.flashClass = "flash";
        this.cameraClass = "rare";
        this.appointmentId = "";
        this.flashClass = "flash";
        this.cameraClass = "rare";
        this.isFrontCamera = false;
        this.userService.actionBarState(true);
        this.userService.bottomBarDots(2);
        this.userService.bottomBarDotState(true);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.userService.switchState(false);
        this.activatedRoute.queryParams.subscribe(function (params) {
            _this.appointmentId = params.id;
            console.log(_this.appointmentId);
        });
    }
    UpcComponent.prototype.ngOnInit = function () { };
    UpcComponent.prototype.ngAfterViewInit = function () {
        // let view = this.barcodeline.nativeElement as StackLayout;
        // view.animate({
        //   duration: 3000,
        //   curve: AnimationCurve.linear,
        //   translate: { x: 0, y: 50 }
        // });
        // .then(() => {
        //   // Reset animation
        //   setTimeout(() => {
        //     view.translateY = 0;
        //     view.translateX = 0;
        //   }, 3000);
        // });
    };
    UpcComponent.prototype.onFlashSelect = function () {
        if (this.flashClass == "flash") {
            this.flashClass = "flash-focus";
            this.torchOn = true;
        }
        else {
            this.flashClass = "flash";
            this.torchOn = false;
        }
    };
    UpcComponent.prototype.onCameraSelect = function () {
        if (this.cameraClass == "rare") {
            this.cameraClass = "front";
            this.isFrontCamera = true;
            console.log("Tapped");
        }
        else {
            this.cameraClass = "rare";
            this.isFrontCamera = false;
            console.log("Tapped");
        }
    };
    UpcComponent.prototype.onSwipe = function (args) {
        if (args.direction == 1) {
            this.routerExtensions.navigate(["/sku"]);
        }
        if (args.direction == 2) {
            this.routerExtensions.navigate(["/title"]);
        }
        //   (scanResult)="onBarcodeScanningResult($event)
    };
    __decorate([
        core_1.ViewChild("otpDialog"),
        __metadata("design:type", modal_component_1.ModalComponent)
    ], UpcComponent.prototype, "otpModal", void 0);
    __decorate([
        core_1.ViewChild("barcodeline"),
        __metadata("design:type", core_1.ElementRef)
    ], UpcComponent.prototype, "barcodeline", void 0);
    UpcComponent = __decorate([
        core_1.Component({
            selector: "Upc",
            moduleId: module.id,
            templateUrl: "./upc.component.html",
            styleUrls: ["./upc.component.css"]
        }),
        __metadata("design:paramtypes", [router_2.ActivatedRoute,
            nativescript_barcodescanner_1.BarcodeScanner,
            router_1.RouterExtensions,
            http_1.HttpClient,
            user_service_1.UserService])
    ], UpcComponent);
    return UpcComponent;
}());
exports.UpcComponent = UpcComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBjLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInVwYy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FNdUI7QUFDdkIsMkVBQTZEO0FBQzdELHNEQUErRDtBQUMvRCw2REFBMkQ7QUFDM0QsNkNBQStEO0FBQy9ELDBDQUF5RDtBQUl6RCx5REFBdUQ7QUFTdkQ7SUFXRSxzQkFDVSxjQUE4QixFQUM5QixjQUE4QixFQUM5QixnQkFBa0MsRUFDbEMsSUFBZ0IsRUFDaEIsV0FBd0I7UUFMbEMsaUJBb0JDO1FBbkJTLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyxTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2hCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBWmxDLE9BQUUsR0FBRyxFQUFFLENBQUM7UUFDUixrQkFBYSxHQUFZLEtBQUssQ0FBQztRQUMvQixZQUFPLEdBQVksS0FBSyxDQUFDO1FBQ3pCLGtCQUFhLEdBQVksS0FBSyxDQUFDO1FBQy9CLGVBQVUsR0FBRyxPQUFPLENBQUM7UUFDckIsZ0JBQVcsR0FBRyxNQUFNLENBQUM7UUFDckIsa0JBQWEsR0FBRyxFQUFFLENBQUM7UUFRakIsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUM7UUFDMUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUM7UUFDMUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFBLE1BQU07WUFDOUMsS0FBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO1lBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2xDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELCtCQUFRLEdBQVIsY0FBa0IsQ0FBQztJQUVuQixzQ0FBZSxHQUFmO1FBQ0UsNERBQTREO1FBQzVELGlCQUFpQjtRQUNqQixvQkFBb0I7UUFDcEIsa0NBQWtDO1FBQ2xDLCtCQUErQjtRQUMvQixNQUFNO1FBQ04sZ0JBQWdCO1FBQ2hCLHVCQUF1QjtRQUN2Qix1QkFBdUI7UUFDdkIsMkJBQTJCO1FBQzNCLDJCQUEyQjtRQUMzQixjQUFjO1FBQ2QsTUFBTTtJQUNSLENBQUM7SUFFRCxvQ0FBYSxHQUFiO1FBQ0UsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLE9BQU8sRUFBRTtZQUM5QixJQUFJLENBQUMsVUFBVSxHQUFHLGFBQWEsQ0FBQztZQUNoQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztTQUNyQjthQUFNO1lBQ0wsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUM7WUFDMUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDdEI7SUFDSCxDQUFDO0lBRUQscUNBQWMsR0FBZDtRQUNFLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxNQUFNLEVBQUU7WUFDOUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUM7WUFDM0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7WUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUN2QjthQUFNO1lBQ0wsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUM7WUFDMUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUN2QjtJQUNILENBQUM7SUFFRCw4QkFBTyxHQUFQLFVBQVEsSUFBMkI7UUFDakMsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsRUFBRTtZQUN2QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztTQUMxQztRQUNELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7U0FDNUM7UUFDRCxrREFBa0Q7SUFDcEQsQ0FBQztJQWhGdUI7UUFBdkIsZ0JBQVMsQ0FBQyxXQUFXLENBQUM7a0NBQVcsZ0NBQWM7a0RBQUM7SUFDdkI7UUFBekIsZ0JBQVMsQ0FBQyxhQUFhLENBQUM7a0NBQWMsaUJBQVU7cURBQUM7SUFGdkMsWUFBWTtRQU54QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLEtBQUs7WUFDZixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLHNCQUFzQjtZQUNuQyxTQUFTLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQztTQUNuQyxDQUFDO3lDQWEwQix1QkFBYztZQUNkLDRDQUFjO1lBQ1oseUJBQWdCO1lBQzVCLGlCQUFVO1lBQ0gsMEJBQVc7T0FoQnZCLFlBQVksQ0FrRnhCO0lBQUQsbUJBQUM7Q0FBQSxBQWxGRCxJQWtGQztBQWxGWSxvQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIENvbXBvbmVudCxcbiAgT25Jbml0LFxuICBBZnRlclZpZXdJbml0LFxuICBWaWV3Q2hpbGQsXG4gIEVsZW1lbnRSZWZcbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IEJhcmNvZGVTY2FubmVyIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1iYXJjb2Rlc2Nhbm5lclwiO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IE1vZGFsQ29tcG9uZW50IH0gZnJvbSBcIi4uL21vZGFscy9tb2RhbC5jb21wb25lbnRcIjtcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHsgcmVnaXN0ZXJFbGVtZW50IH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2VsZW1lbnQtcmVnaXN0cnlcIjtcbmltcG9ydCB7IEFuaW1hdGlvbkN1cnZlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZW51bXNcIjtcbmltcG9ydCB7IFN0YWNrTGF5b3V0IH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvbGF5b3V0cy9zdGFjay1sYXlvdXRcIjtcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL3VzZXIuc2VydmljZVwiO1xuaW1wb3J0IHsgU3dpcGVHZXN0dXJlRXZlbnREYXRhIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZ2VzdHVyZXNcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcIlVwY1wiLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICB0ZW1wbGF0ZVVybDogXCIuL3VwYy5jb21wb25lbnQuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vdXBjLmNvbXBvbmVudC5jc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgVXBjQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0IHtcbiAgQFZpZXdDaGlsZChcIm90cERpYWxvZ1wiKSBvdHBNb2RhbDogTW9kYWxDb21wb25lbnQ7XG4gIEBWaWV3Q2hpbGQoXCJiYXJjb2RlbGluZVwiKSBiYXJjb2RlbGluZTogRWxlbWVudFJlZjtcblxuICBpZCA9IFwiXCI7XG4gIGlzRnJvbnRDYW1lcmE6IGJvb2xlYW4gPSBmYWxzZTtcbiAgdG9yY2hPbjogYm9vbGVhbiA9IGZhbHNlO1xuICBpc1NjYW5uZWRPbmNlOiBib29sZWFuID0gZmFsc2U7XG4gIGZsYXNoQ2xhc3MgPSBcImZsYXNoXCI7XG4gIGNhbWVyYUNsYXNzID0gXCJyYXJlXCI7XG4gIGFwcG9pbnRtZW50SWQgPSBcIlwiO1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcbiAgICBwcml2YXRlIGJhcmNvZGVTY2FubmVyOiBCYXJjb2RlU2Nhbm5lcixcbiAgICBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsXG4gICAgcHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LFxuICAgIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlXG4gICkge1xuICAgIHRoaXMuZmxhc2hDbGFzcyA9IFwiZmxhc2hcIjtcbiAgICB0aGlzLmNhbWVyYUNsYXNzID0gXCJyYXJlXCI7XG4gICAgdGhpcy5pc0Zyb250Q2FtZXJhID0gZmFsc2U7XG4gICAgdGhpcy51c2VyU2VydmljZS5hY3Rpb25CYXJTdGF0ZSh0cnVlKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdHMoMik7XG4gICAgdGhpcy51c2VyU2VydmljZS5ib3R0b21CYXJEb3RTdGF0ZSh0cnVlKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZSh0cnVlKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZUNoYW5nZXMoMSk7XG4gICAgdGhpcy51c2VyU2VydmljZS5zd2l0Y2hTdGF0ZShmYWxzZSk7XG4gICAgdGhpcy5hY3RpdmF0ZWRSb3V0ZS5xdWVyeVBhcmFtcy5zdWJzY3JpYmUocGFyYW1zID0+IHtcbiAgICAgIHRoaXMuYXBwb2ludG1lbnRJZCA9IHBhcmFtcy5pZDtcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMuYXBwb2ludG1lbnRJZCk7XG4gICAgfSk7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHt9XG5cbiAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgIC8vIGxldCB2aWV3ID0gdGhpcy5iYXJjb2RlbGluZS5uYXRpdmVFbGVtZW50IGFzIFN0YWNrTGF5b3V0O1xuICAgIC8vIHZpZXcuYW5pbWF0ZSh7XG4gICAgLy8gICBkdXJhdGlvbjogMzAwMCxcbiAgICAvLyAgIGN1cnZlOiBBbmltYXRpb25DdXJ2ZS5saW5lYXIsXG4gICAgLy8gICB0cmFuc2xhdGU6IHsgeDogMCwgeTogNTAgfVxuICAgIC8vIH0pO1xuICAgIC8vIC50aGVuKCgpID0+IHtcbiAgICAvLyAgIC8vIFJlc2V0IGFuaW1hdGlvblxuICAgIC8vICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgLy8gICAgIHZpZXcudHJhbnNsYXRlWSA9IDA7XG4gICAgLy8gICAgIHZpZXcudHJhbnNsYXRlWCA9IDA7XG4gICAgLy8gICB9LCAzMDAwKTtcbiAgICAvLyB9KTtcbiAgfVxuXG4gIG9uRmxhc2hTZWxlY3QoKSB7XG4gICAgaWYgKHRoaXMuZmxhc2hDbGFzcyA9PSBcImZsYXNoXCIpIHtcbiAgICAgIHRoaXMuZmxhc2hDbGFzcyA9IFwiZmxhc2gtZm9jdXNcIjtcbiAgICAgIHRoaXMudG9yY2hPbiA9IHRydWU7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuZmxhc2hDbGFzcyA9IFwiZmxhc2hcIjtcbiAgICAgIHRoaXMudG9yY2hPbiA9IGZhbHNlO1xuICAgIH1cbiAgfVxuXG4gIG9uQ2FtZXJhU2VsZWN0KCkge1xuICAgIGlmICh0aGlzLmNhbWVyYUNsYXNzID09IFwicmFyZVwiKSB7XG4gICAgICB0aGlzLmNhbWVyYUNsYXNzID0gXCJmcm9udFwiO1xuICAgICAgdGhpcy5pc0Zyb250Q2FtZXJhID0gdHJ1ZTtcbiAgICAgIGNvbnNvbGUubG9nKFwiVGFwcGVkXCIpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmNhbWVyYUNsYXNzID0gXCJyYXJlXCI7XG4gICAgICB0aGlzLmlzRnJvbnRDYW1lcmEgPSBmYWxzZTtcbiAgICAgIGNvbnNvbGUubG9nKFwiVGFwcGVkXCIpO1xuICAgIH1cbiAgfVxuXG4gIG9uU3dpcGUoYXJnczogU3dpcGVHZXN0dXJlRXZlbnREYXRhKSB7XG4gICAgaWYgKGFyZ3MuZGlyZWN0aW9uID09IDEpIHtcbiAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvc2t1XCJdKTtcbiAgICB9XG4gICAgaWYgKGFyZ3MuZGlyZWN0aW9uID09IDIpIHtcbiAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvdGl0bGVcIl0pO1xuICAgIH1cbiAgICAvLyAgIChzY2FuUmVzdWx0KT1cIm9uQmFyY29kZVNjYW5uaW5nUmVzdWx0KCRldmVudClcbiAgfVxufVxuIl19
import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef
} from "@angular/core";
import { BarcodeScanner } from "nativescript-barcodescanner";
import { RouterExtensions } from "nativescript-angular/router";
import { ModalComponent } from "../modals/modal.component";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";
import { registerElement } from "nativescript-angular/element-registry";
import { AnimationCurve } from "tns-core-modules/ui/enums";
import { StackLayout } from "tns-core-modules/ui/layouts/stack-layout";
import { UserService } from "../services/user.service";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";

@Component({
  selector: "Upc",
  moduleId: module.id,
  templateUrl: "./upc.component.html",
  styleUrls: ["./upc.component.css"]
})
export class UpcComponent implements OnInit, AfterViewInit {
  @ViewChild("otpDialog") otpModal: ModalComponent;
  @ViewChild("barcodeline") barcodeline: ElementRef;

  id = "";
  isFrontCamera: boolean = false;
  torchOn: boolean = false;
  isScannedOnce: boolean = false;
  flashClass = "flash";
  cameraClass = "rare";
  appointmentId = "";
  constructor(
    private activatedRoute: ActivatedRoute,
    private barcodeScanner: BarcodeScanner,
    private routerExtensions: RouterExtensions,
    private http: HttpClient,
    private userService: UserService
  ) {
    this.flashClass = "flash";
    this.cameraClass = "rare";
    this.isFrontCamera = false;
    this.userService.actionBarState(true);
    this.userService.bottomBarDots(2);
    this.userService.bottomBarDotState(true);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
    this.userService.switchState(false);
    this.activatedRoute.queryParams.subscribe(params => {
      this.appointmentId = params.id;
      console.log(this.appointmentId);
    });
  }

  ngOnInit(): void {}

  ngAfterViewInit() {
    // let view = this.barcodeline.nativeElement as StackLayout;
    // view.animate({
    //   duration: 3000,
    //   curve: AnimationCurve.linear,
    //   translate: { x: 0, y: 50 }
    // });
    // .then(() => {
    //   // Reset animation
    //   setTimeout(() => {
    //     view.translateY = 0;
    //     view.translateX = 0;
    //   }, 3000);
    // });
  }

  onFlashSelect() {
    if (this.flashClass == "flash") {
      this.flashClass = "flash-focus";
      this.torchOn = true;
    } else {
      this.flashClass = "flash";
      this.torchOn = false;
    }
  }

  onCameraSelect() {
    if (this.cameraClass == "rare") {
      this.cameraClass = "front";
      this.isFrontCamera = true;
      console.log("Tapped");
    } else {
      this.cameraClass = "rare";
      this.isFrontCamera = false;
      console.log("Tapped");
    }
  }

  onSwipe(args: SwipeGestureEventData) {
    if (args.direction == 1) {
      this.routerExtensions.navigate(["/sku"]);
    }
    if (args.direction == 2) {
      this.routerExtensions.navigate(["/title"]);
    }
    //   (scanResult)="onBarcodeScanningResult($event)
  }
}

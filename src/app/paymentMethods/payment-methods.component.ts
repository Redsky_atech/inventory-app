import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { UserService } from "../services/user.service";

@Component({
  selector: "ns-paymentMethods",
  moduleId: module.id,
  templateUrl: "./payment-methods.component.html",
  styleUrls: ["./payment-methods.component.css"]
})
export class PaymentMethodComponent {
  routerExtensions: RouterExtensions;

  payment1 = true;
  payment2 = false;
  payment3 = false;
  payment4 = false;
  payment5 = false;
  payment6 = false;
  payment7 = false;
  payment8 = false;
  payment9 = false;
  payment10 = false;

  constructor(
    routerExtensions: RouterExtensions,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDots(11);
    this.userService.bottomBarDotState(true);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
  }

  paymentClick1() {
    this.payment1 = true;
    this.payment2 = false;
    this.payment3 = false;
    this.payment4 = false;
    this.payment5 = false;
    this.payment6 = false;
    this.payment7 = false;
    this.payment8 = false;
    this.payment9 = false;
    this.payment10 = false;
  }
  paymentClick2() {
    this.payment1 = false;
    this.payment2 = true;
    this.payment3 = false;
    this.payment4 = false;
    this.payment5 = false;
    this.payment6 = false;
    this.payment7 = false;
    this.payment8 = false;
    this.payment9 = false;
    this.payment10 = false;
  }
  paymentClick3() {
    this.payment1 = false;
    this.payment2 = false;
    this.payment3 = true;
    this.payment4 = false;
    this.payment5 = false;
    this.payment6 = false;
    this.payment7 = false;
    this.payment8 = false;
    this.payment9 = false;
    this.payment10 = false;
  }
  paymentClick4() {
    this.payment1 = false;
    this.payment2 = false;
    this.payment3 = false;
    this.payment4 = true;
    this.payment5 = false;
    this.payment6 = false;
    this.payment7 = false;
    this.payment8 = false;
    this.payment9 = false;
    this.payment10 = false;
  }
  paymentClick5() {
    this.payment1 = false;
    this.payment2 = false;
    this.payment3 = false;
    this.payment4 = false;
    this.payment5 = true;
    this.payment6 = false;
    this.payment7 = false;
    this.payment8 = false;
    this.payment9 = false;
    this.payment10 = false;
  }
  paymentClick6() {
    this.payment1 = false;
    this.payment2 = false;
    this.payment3 = false;
    this.payment4 = false;
    this.payment5 = false;
    this.payment6 = true;
    this.payment7 = false;
    this.payment8 = false;
    this.payment9 = false;
    this.payment10 = false;
  }
  paymentClick7() {
    this.payment1 = false;
    this.payment2 = false;
    this.payment3 = false;
    this.payment4 = false;
    this.payment5 = false;
    this.payment6 = false;
    this.payment7 = true;
    this.payment8 = false;
    this.payment9 = false;
    this.payment10 = false;
  }
  paymentClick8() {
    this.payment1 = false;
    this.payment2 = false;
    this.payment3 = false;
    this.payment4 = false;
    this.payment5 = false;
    this.payment6 = false;
    this.payment7 = false;
    this.payment8 = true;
    this.payment9 = false;
    this.payment10 = false;
  }
  paymentClick9() {
    this.payment1 = false;
    this.payment2 = false;
    this.payment3 = false;
    this.payment4 = false;
    this.payment5 = false;
    this.payment6 = false;
    this.payment7 = false;
    this.payment8 = false;
    this.payment9 = true;
    this.payment10 = false;
  }
  paymentClick10() {
    this.payment1 = false;
    this.payment2 = false;
    this.payment3 = false;
    this.payment4 = false;
    this.payment5 = false;
    this.payment6 = false;
    this.payment7 = false;
    this.payment8 = false;
    this.payment9 = false;
    this.payment10 = true;
  }

  onSwipe(args: SwipeGestureEventData) {
    if (args.direction == 1) {
      this.routerExtensions.navigate(["/itemspecifies"]);
    }
    if (args.direction == 2) {
      this.routerExtensions.navigate(["/returnOptions"]);
    }
  }
}

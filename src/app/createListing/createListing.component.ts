import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { UserService } from "../services/user.service";

@Component({
  selector: "ns-createListing",
  moduleId: module.id,
  templateUrl: "./createListing.component.html",
  styleUrls: ["./createListing.component.css"]
})
export class CreateListingComponent implements OnInit {
  routerExtensions: RouterExtensions;

  constructor(
    routerExtensions: RouterExtensions,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDotState(false);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
    this.userService.switchState(false);
  }
  ngOnInit(): void {}

  public onCreateListing() {
    this.routerExtensions.navigate(["/listingFunctions"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }

  public onManageWarehouse() {
    this.routerExtensions.navigate(["/warehouseFunctions"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var user_service_1 = require("../services/user.service");
var SellingDetailComponent = /** @class */ (function () {
    function SellingDetailComponent(routerExtensions, userService) {
        this.userService = userService;
        this.duration = [
            "5",
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50",
            "55",
            "60"
        ];
        this.acceptOffer = false;
        this.auctionColor = "black";
        this.auctionBackColor = "#2196F3";
        this.fixedPriceColor = "#8C8C8C";
        this.fixedBackColor = "#8C8C8C";
        this.autoAcceptColor = "#8C8C8C";
        this.editableAuctionStatus = true;
        this.editableFixedStatus = false;
        this.dropAuctionEnable = true;
        this.dropFixedEnable = false;
        this.editableAutoStatus = false;
        this.routerExtensions = routerExtensions;
        this.userService.actionBarState(true);
        this.userService.bottomBarDots(8);
        this.userService.bottomBarDotState(true);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
    }
    SellingDetailComponent.prototype.fixedPriceClick = function () {
        this.auctionColor = "#8C8C8C";
        this.auctionBackColor = "#8C8C8C";
        this.fixedPriceColor = "black";
        this.fixedBackColor = "#2196F3";
        this.editableAuctionStatus = false;
        this.editableFixedStatus = true;
        this.dropAuctionEnable = false;
        this.dropFixedEnable = true;
    };
    SellingDetailComponent.prototype.auctionClick = function () {
        this.fixedPriceColor = "#8C8C8C";
        this.fixedBackColor = "#8C8C8C";
        this.auctionColor = "black";
        this.auctionBackColor = "#2196F3";
        this.editableAuctionStatus = true;
        this.editableFixedStatus = false;
        this.dropAuctionEnable = true;
        this.dropFixedEnable = false;
    };
    SellingDetailComponent.prototype.acceptOfferClick = function () {
        this.acceptOffer = !this.acceptOffer;
        if (this.acceptOffer == true) {
            this.autoAcceptColor = "black";
            this.editableAutoStatus = true;
        }
        else {
            this.autoAcceptColor = "#8C8C8C";
            this.editableAutoStatus = false;
        }
    };
    SellingDetailComponent.prototype.onSwipe = function (args) {
        if (args.direction == 1) {
            this.routerExtensions.navigate(["/condition"]);
        }
        if (args.direction == 2) {
            this.routerExtensions.navigate(["/description"]);
        }
    };
    SellingDetailComponent = __decorate([
        core_1.Component({
            selector: "ns-sellingDetail",
            moduleId: module.id,
            templateUrl: "./selling-detail.component.html",
            styleUrls: ["./selling-detail.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            user_service_1.UserService])
    ], SellingDetailComponent);
    return SellingDetailComponent;
}());
exports.SellingDetailComponent = SellingDetailComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsbGluZy1kZXRhaWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2VsbGluZy1kZXRhaWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlFO0FBQ3pFLHNEQUErRDtBQUUvRCx5REFBdUQ7QUFRdkQ7SUE0QkUsZ0NBQ0UsZ0JBQWtDLEVBQzFCLFdBQXdCO1FBQXhCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBNUIzQixhQUFRLEdBQUc7WUFDaEIsR0FBRztZQUNILElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1NBQ0wsQ0FBQztRQUNGLGdCQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLGlCQUFZLEdBQUcsT0FBTyxDQUFDO1FBQ3ZCLHFCQUFnQixHQUFHLFNBQVMsQ0FBQztRQUM3QixvQkFBZSxHQUFHLFNBQVMsQ0FBQztRQUM1QixtQkFBYyxHQUFHLFNBQVMsQ0FBQztRQUMzQixvQkFBZSxHQUFHLFNBQVMsQ0FBQztRQUM1QiwwQkFBcUIsR0FBRyxJQUFJLENBQUM7UUFDN0Isd0JBQW1CLEdBQUcsS0FBSyxDQUFDO1FBQzVCLHNCQUFpQixHQUFHLElBQUksQ0FBQztRQUN6QixvQkFBZSxHQUFHLEtBQUssQ0FBQztRQUN4Qix1QkFBa0IsR0FBRyxLQUFLLENBQUM7UUFNekIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRUQsZ0RBQWUsR0FBZjtRQUNFLElBQUksQ0FBQyxZQUFZLEdBQUcsU0FBUyxDQUFDO1FBQzlCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxTQUFTLENBQUM7UUFDbEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxPQUFPLENBQUM7UUFDL0IsSUFBSSxDQUFDLGNBQWMsR0FBRyxTQUFTLENBQUM7UUFDaEMsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDL0IsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7SUFDOUIsQ0FBQztJQUVELDZDQUFZLEdBQVo7UUFDRSxJQUFJLENBQUMsZUFBZSxHQUFHLFNBQVMsQ0FBQztRQUNqQyxJQUFJLENBQUMsY0FBYyxHQUFHLFNBQVMsQ0FBQztRQUNoQyxJQUFJLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQztRQUM1QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsU0FBUyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7UUFDbEMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQztRQUNqQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQzlCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO0lBQy9CLENBQUM7SUFFRCxpREFBZ0IsR0FBaEI7UUFDRSxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUNyQyxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxFQUFFO1lBQzVCLElBQUksQ0FBQyxlQUFlLEdBQUcsT0FBTyxDQUFDO1lBQy9CLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUM7U0FDaEM7YUFBTTtZQUNMLElBQUksQ0FBQyxlQUFlLEdBQUcsU0FBUyxDQUFDO1lBQ2pDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7U0FDakM7SUFDSCxDQUFDO0lBRUQsd0NBQU8sR0FBUCxVQUFRLElBQTJCO1FBQ2pDLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7U0FDaEQ7UUFDRCxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1NBQ2xEO0lBQ0gsQ0FBQztJQWhGVSxzQkFBc0I7UUFObEMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSxpQ0FBaUM7WUFDOUMsU0FBUyxFQUFFLENBQUMsZ0NBQWdDLENBQUM7U0FDOUMsQ0FBQzt5Q0E4Qm9CLHlCQUFnQjtZQUNiLDBCQUFXO09BOUJ2QixzQkFBc0IsQ0FpRmxDO0lBQUQsNkJBQUM7Q0FBQSxBQWpGRCxJQWlGQztBQWpGWSx3REFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBFbGVtZW50UmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgU3dpcGVHZXN0dXJlRXZlbnREYXRhIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZ2VzdHVyZXNcIjtcclxuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJucy1zZWxsaW5nRGV0YWlsXCIsXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICB0ZW1wbGF0ZVVybDogXCIuL3NlbGxpbmctZGV0YWlsLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL3NlbGxpbmctZGV0YWlsLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIFNlbGxpbmdEZXRhaWxDb21wb25lbnQge1xyXG4gIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnM7XHJcbiAgcHVibGljIGR1cmF0aW9uID0gW1xyXG4gICAgXCI1XCIsXHJcbiAgICBcIjEwXCIsXHJcbiAgICBcIjE1XCIsXHJcbiAgICBcIjIwXCIsXHJcbiAgICBcIjI1XCIsXHJcbiAgICBcIjMwXCIsXHJcbiAgICBcIjM1XCIsXHJcbiAgICBcIjQwXCIsXHJcbiAgICBcIjQ1XCIsXHJcbiAgICBcIjUwXCIsXHJcbiAgICBcIjU1XCIsXHJcbiAgICBcIjYwXCJcclxuICBdO1xyXG4gIGFjY2VwdE9mZmVyID0gZmFsc2U7XHJcbiAgYXVjdGlvbkNvbG9yID0gXCJibGFja1wiO1xyXG4gIGF1Y3Rpb25CYWNrQ29sb3IgPSBcIiMyMTk2RjNcIjtcclxuICBmaXhlZFByaWNlQ29sb3IgPSBcIiM4QzhDOENcIjtcclxuICBmaXhlZEJhY2tDb2xvciA9IFwiIzhDOEM4Q1wiO1xyXG4gIGF1dG9BY2NlcHRDb2xvciA9IFwiIzhDOEM4Q1wiO1xyXG4gIGVkaXRhYmxlQXVjdGlvblN0YXR1cyA9IHRydWU7XHJcbiAgZWRpdGFibGVGaXhlZFN0YXR1cyA9IGZhbHNlO1xyXG4gIGRyb3BBdWN0aW9uRW5hYmxlID0gdHJ1ZTtcclxuICBkcm9wRml4ZWRFbmFibGUgPSBmYWxzZTtcclxuICBlZGl0YWJsZUF1dG9TdGF0dXMgPSBmYWxzZTtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLFxyXG4gICAgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2VcclxuICApIHtcclxuICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucyA9IHJvdXRlckV4dGVuc2lvbnM7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmFjdGlvbkJhclN0YXRlKHRydWUpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5ib3R0b21CYXJEb3RzKDgpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5ib3R0b21CYXJEb3RTdGF0ZSh0cnVlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UudGFiVmlld1N0YXRlKHRydWUpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGVDaGFuZ2VzKDEpO1xyXG4gIH1cclxuXHJcbiAgZml4ZWRQcmljZUNsaWNrKCkge1xyXG4gICAgdGhpcy5hdWN0aW9uQ29sb3IgPSBcIiM4QzhDOENcIjtcclxuICAgIHRoaXMuYXVjdGlvbkJhY2tDb2xvciA9IFwiIzhDOEM4Q1wiO1xyXG4gICAgdGhpcy5maXhlZFByaWNlQ29sb3IgPSBcImJsYWNrXCI7XHJcbiAgICB0aGlzLmZpeGVkQmFja0NvbG9yID0gXCIjMjE5NkYzXCI7XHJcbiAgICB0aGlzLmVkaXRhYmxlQXVjdGlvblN0YXR1cyA9IGZhbHNlO1xyXG4gICAgdGhpcy5lZGl0YWJsZUZpeGVkU3RhdHVzID0gdHJ1ZTtcclxuICAgIHRoaXMuZHJvcEF1Y3Rpb25FbmFibGUgPSBmYWxzZTtcclxuICAgIHRoaXMuZHJvcEZpeGVkRW5hYmxlID0gdHJ1ZTtcclxuICB9XHJcblxyXG4gIGF1Y3Rpb25DbGljaygpIHtcclxuICAgIHRoaXMuZml4ZWRQcmljZUNvbG9yID0gXCIjOEM4QzhDXCI7XHJcbiAgICB0aGlzLmZpeGVkQmFja0NvbG9yID0gXCIjOEM4QzhDXCI7XHJcbiAgICB0aGlzLmF1Y3Rpb25Db2xvciA9IFwiYmxhY2tcIjtcclxuICAgIHRoaXMuYXVjdGlvbkJhY2tDb2xvciA9IFwiIzIxOTZGM1wiO1xyXG4gICAgdGhpcy5lZGl0YWJsZUF1Y3Rpb25TdGF0dXMgPSB0cnVlO1xyXG4gICAgdGhpcy5lZGl0YWJsZUZpeGVkU3RhdHVzID0gZmFsc2U7XHJcbiAgICB0aGlzLmRyb3BBdWN0aW9uRW5hYmxlID0gdHJ1ZTtcclxuICAgIHRoaXMuZHJvcEZpeGVkRW5hYmxlID0gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBhY2NlcHRPZmZlckNsaWNrKCkge1xyXG4gICAgdGhpcy5hY2NlcHRPZmZlciA9ICF0aGlzLmFjY2VwdE9mZmVyO1xyXG4gICAgaWYgKHRoaXMuYWNjZXB0T2ZmZXIgPT0gdHJ1ZSkge1xyXG4gICAgICB0aGlzLmF1dG9BY2NlcHRDb2xvciA9IFwiYmxhY2tcIjtcclxuICAgICAgdGhpcy5lZGl0YWJsZUF1dG9TdGF0dXMgPSB0cnVlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5hdXRvQWNjZXB0Q29sb3IgPSBcIiM4QzhDOENcIjtcclxuICAgICAgdGhpcy5lZGl0YWJsZUF1dG9TdGF0dXMgPSBmYWxzZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uU3dpcGUoYXJnczogU3dpcGVHZXN0dXJlRXZlbnREYXRhKSB7XHJcbiAgICBpZiAoYXJncy5kaXJlY3Rpb24gPT0gMSkge1xyXG4gICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2NvbmRpdGlvblwiXSk7XHJcbiAgICB9XHJcbiAgICBpZiAoYXJncy5kaXJlY3Rpb24gPT0gMikge1xyXG4gICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2Rlc2NyaXB0aW9uXCJdKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19
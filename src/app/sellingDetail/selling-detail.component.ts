import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { UserService } from "../services/user.service";

@Component({
  selector: "ns-sellingDetail",
  moduleId: module.id,
  templateUrl: "./selling-detail.component.html",
  styleUrls: ["./selling-detail.component.css"]
})
export class SellingDetailComponent {
  routerExtensions: RouterExtensions;
  public duration = [
    "5",
    "10",
    "15",
    "20",
    "25",
    "30",
    "35",
    "40",
    "45",
    "50",
    "55",
    "60"
  ];
  acceptOffer = false;
  auctionColor = "black";
  auctionBackColor = "#2196F3";
  fixedPriceColor = "#8C8C8C";
  fixedBackColor = "#8C8C8C";
  autoAcceptColor = "#8C8C8C";
  editableAuctionStatus = true;
  editableFixedStatus = false;
  dropAuctionEnable = true;
  dropFixedEnable = false;
  editableAutoStatus = false;

  constructor(
    routerExtensions: RouterExtensions,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDots(8);
    this.userService.bottomBarDotState(true);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
  }

  fixedPriceClick() {
    this.auctionColor = "#8C8C8C";
    this.auctionBackColor = "#8C8C8C";
    this.fixedPriceColor = "black";
    this.fixedBackColor = "#2196F3";
    this.editableAuctionStatus = false;
    this.editableFixedStatus = true;
    this.dropAuctionEnable = false;
    this.dropFixedEnable = true;
  }

  auctionClick() {
    this.fixedPriceColor = "#8C8C8C";
    this.fixedBackColor = "#8C8C8C";
    this.auctionColor = "black";
    this.auctionBackColor = "#2196F3";
    this.editableAuctionStatus = true;
    this.editableFixedStatus = false;
    this.dropAuctionEnable = true;
    this.dropFixedEnable = false;
  }

  acceptOfferClick() {
    this.acceptOffer = !this.acceptOffer;
    if (this.acceptOffer == true) {
      this.autoAcceptColor = "black";
      this.editableAutoStatus = true;
    } else {
      this.autoAcceptColor = "#8C8C8C";
      this.editableAutoStatus = false;
    }
  }

  onSwipe(args: SwipeGestureEventData) {
    if (args.direction == 1) {
      this.routerExtensions.navigate(["/condition"]);
    }
    if (args.direction == 2) {
      this.routerExtensions.navigate(["/description"]);
    }
  }
}

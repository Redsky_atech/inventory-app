import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

import { AppComponent } from "./app.component";
import { SkuComponent } from "./sku/sku.component";
import { UpcComponent } from "./upc/upc.component";
import { TitleComponent } from "./title/title.component";
import { FindCategoryComponent } from "./findCategory/findCategory.component";
import { SuggestedCategoriesComponent } from "./suggestedcategories/suggestedcategories.component";
import { StoreCategoryComponent } from "./storeCategories/components/store-categories.component";
import { ConditionComponent } from "./condition/condition.component";
import { SellingDetailComponent } from "./sellingDetail/selling-detail.component";
import { DescriptionComponent } from "./description/description.component";
import { ItemSpecifiesComponent } from "./itemspecifies/item-specifies.component";
import { PaymentMethodComponent } from "./paymentMethods/payment-methods.component";
import { ReturnOptionsComponent } from "./returnOptions/return-options.component";
import { BusinessPoliciesComponent } from "./businesspolicies/businesspolicies.component";
import { WeightDimensionComponent } from "./weightDimension/weight-dimension.component";
import { SaveDraftComponent } from "./savedraft/savedraft.component";
import { PicturesComponent } from "./pictures/pictures.component";
import { AddPicturesComponent } from "./addPictures/add-pictures.component";
import { CreateListingComponent } from "./createListing/createListing.component";
import { ListingFunctionsComponent } from "./listingFunctions/listingFunctions.component";
import { SubmitListingComponent } from "./submitListings/submitListings.component";
import { EditListingsComponent } from "./editListings/editListings.component";
import { WarehouseFunctionsComponent } from "./warehouseFunctions/warehouseFunctions.component";
import { ShelveInventoryComponent } from "./shelveInventory/shelveInventory.component";
import { UserAccountComponent } from "./userAccount/userAccount.component";
import { CheckItemStatusComponent } from "./checkItemStatus/checkItemStatus.component";
import { InventoryLocationComponent } from "./inventoryLocation/inventoryLocation.component";
import { ShipItemsComponent } from "./shipItems/shipItems.component";
import { StockErrorsComponent } from "./stockErrors/stockErrors.component";
import { PreferencesDefaultsComponent } from "./preferencesDefaults/preferencesDefaults.component";
import { PrefSkuComponent } from "./preferenceScreens/sku/sku.component";
import { PrefUpcComponent } from "./preferenceScreens/upc/upc.component";
import { PrefTitleComponent } from "./preferenceScreens/title/title.component";
import { PrefSuggestedCategoriesComponent } from "./preferenceScreens/suggestedcategories/suggestedcategories.component";
import { PrefFindCategoryComponent } from "./preferenceScreens/findCategory/findCategory.component";
import { PrefStoreCategoryComponent } from "./preferenceScreens/storeCategories/components/store-categories.component";
import { PrefConditionComponent } from "./preferenceScreens/condition/condition.component";
import { PrefDescriptionComponent } from "./preferenceScreens/description/description.component";
import { PrefItemSpecifiesComponent } from "./preferenceScreens/itemspecifies/item-specifies.component";
import { PrefBusinessPoliciesComponent } from "./preferenceScreens/businesspolicies/businesspolicies.component";
import { PrefWeightDimensionComponent } from "./preferenceScreens/weightDimension/weight-dimension.component";

const routes: Routes = [
  { path: "", redirectTo: "/createListing", pathMatch: "full" },
  { path: "createListing", component: CreateListingComponent },
  { path: "listingFunctions", component: ListingFunctionsComponent },
  { path: "sku", component: SkuComponent },
  { path: "upc", component: UpcComponent },
  { path: "title", component: TitleComponent },
  { path: "suggest", component: SuggestedCategoriesComponent },
  { path: "findCategory", component: FindCategoryComponent },
  { path: "storeCategory", component: StoreCategoryComponent },
  { path: "condition", component: ConditionComponent },
  { path: "sellingDetail", component: SellingDetailComponent },
  { path: "description", component: DescriptionComponent },
  { path: "itemspecifies", component: ItemSpecifiesComponent },
  { path: "paymentMethods", component: PaymentMethodComponent },
  { path: "returnOptions", component: ReturnOptionsComponent },
  { path: "businesspolicies", component: BusinessPoliciesComponent },
  { path: "weightDimension", component: WeightDimensionComponent },
  { path: "savedraft", component: SaveDraftComponent },
  { path: "pictures", component: PicturesComponent },
  { path: "addPictures", component: AddPicturesComponent },
  { path: "submitListings", component: SubmitListingComponent },
  { path: "editListings", component: EditListingsComponent },
  { path: "warehouseFunctions", component: WarehouseFunctionsComponent },
  { path: "shelveInventory", component: ShelveInventoryComponent },
  { path: "userAccount", component: UserAccountComponent },
  { path: "checkItemStatus", component: CheckItemStatusComponent },
  { path: "inventoryLocation", component: InventoryLocationComponent },
  { path: "shipItems", component: ShipItemsComponent },
  { path: "stockErrors", component: StockErrorsComponent },
  { path: "preferencesDefaults", component: PreferencesDefaultsComponent },
  { path: "prefSku", component: PrefSkuComponent },
  { path: "prefUpc", component: PrefUpcComponent },
  { path: "prefTitle", component: PrefTitleComponent },
  { path: "prefSuggest", component: PrefSuggestedCategoriesComponent },
  { path: "prefFindCategory", component: PrefFindCategoryComponent },
  { path: "prefStoreCategory", component: PrefStoreCategoryComponent },
  { path: "prefCondition", component: PrefConditionComponent },
  { path: "prefDescription", component: PrefDescriptionComponent },
  { path: "prefItemspecifies", component: PrefItemSpecifiesComponent },
  { path: "prefBusinesspolicies", component: PrefBusinessPoliciesComponent },
  { path: "prefWeightDimension", component: PrefWeightDimensionComponent }
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {}

import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { UserService } from "../services/user.service";

@Component({
  selector: "ns-warehouseFunctions",
  moduleId: module.id,
  templateUrl: "./warehouseFunctions.component.html",
  styleUrls: ["./warehouseFunctions.component.css"]
})
export class WarehouseFunctionsComponent implements OnInit {
  routerExtensions: RouterExtensions;

  constructor(
    routerExtensions: RouterExtensions,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDotState(false);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
  }
  ngOnInit(): void {}

  public onShelveInventory() {
    this.routerExtensions.navigate(["/shelveInventory"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }

  public onItemDetails() {
    this.routerExtensions.navigate(["/checkItemStatus"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }

  public onFindItems() {
    this.routerExtensions.navigate(["/inventoryLocation"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }

  public onShipItems() {
    this.routerExtensions.navigate(["/shipItems"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }

  public onStockErrors() {
    this.routerExtensions.navigate(["/stockErrors"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }
}

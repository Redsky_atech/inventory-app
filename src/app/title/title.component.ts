import { Component, OnInit } from "@angular/core";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { registerElement } from "nativescript-angular/element-registry";
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page/page";
import { UserService } from "../services/user.service";

registerElement(
  "TextInputLayout",
  () => require("nativescript-textinputlayout").TextInputLayout
);

@Component({
  selector: "ns-title",
  moduleId: module.id,
  templateUrl: "./title.component.html",
  styleUrls: ["./title.component.css"]
})
export class TitleComponent implements OnInit {
  routerExtensions: RouterExtensions;

  constructor(
    routerExtensions: RouterExtensions,
    private activatedRoute: ActivatedRoute,
    private page: Page,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.bottomBarDots(3);
    this.userService.actionBarState(true);
    this.userService.bottomBarDotState(true);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
    this.userService.switchState(false);
  }

  ngOnInit(): void {}

  onSwipe(args: SwipeGestureEventData) {
    if (args.direction == 1) {
      this.routerExtensions.navigate(["/upc"]);
    }
    if (args.direction == 2) {
      this.routerExtensions.navigate(["/suggest"]);
    }
  }
}

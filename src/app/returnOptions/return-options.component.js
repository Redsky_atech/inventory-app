"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var user_service_1 = require("../services/user.service");
var ReturnOptionsComponent = /** @class */ (function () {
    function ReturnOptionsComponent(routerExtensions, userService) {
        this.userService = userService;
        this.return1 = true;
        this.return2 = false;
        this.routerExtensions = routerExtensions;
        this.userService.actionBarState(true);
        this.userService.bottomBarDots(12);
        this.userService.bottomBarDotState(true);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
    }
    ReturnOptionsComponent.prototype.returnOption1 = function () {
        this.return1 = true;
        this.return2 = false;
    };
    ReturnOptionsComponent.prototype.returnOption2 = function () {
        this.return1 = false;
        this.return2 = true;
    };
    ReturnOptionsComponent.prototype.onSwipe = function (args) {
        if (args.direction == 1) {
            this.routerExtensions.navigate(["/paymentMethods"]);
        }
        if (args.direction == 2) {
            this.routerExtensions.navigate(["/businesspolicies"]);
        }
    };
    ReturnOptionsComponent = __decorate([
        core_1.Component({
            selector: "ns-paymentMethods",
            moduleId: module.id,
            templateUrl: "./return-options.component.html",
            styleUrls: ["./return-options.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            user_service_1.UserService])
    ], ReturnOptionsComponent);
    return ReturnOptionsComponent;
}());
exports.ReturnOptionsComponent = ReturnOptionsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmV0dXJuLW9wdGlvbnMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicmV0dXJuLW9wdGlvbnMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlFO0FBQ3pFLHNEQUErRDtBQUUvRCx5REFBdUQ7QUFRdkQ7SUFNRSxnQ0FDRSxnQkFBa0MsRUFDMUIsV0FBd0I7UUFBeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFMbEMsWUFBTyxHQUFHLElBQUksQ0FBQztRQUNmLFlBQU8sR0FBRyxLQUFLLENBQUM7UUFNZCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7UUFDekMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDbkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFFRCw4Q0FBYSxHQUFiO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7SUFDdkIsQ0FBQztJQUNELDhDQUFhLEdBQWI7UUFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztJQUN0QixDQUFDO0lBRUQsd0NBQU8sR0FBUCxVQUFRLElBQTJCO1FBQ2pDLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztTQUNyRDtRQUNELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztTQUN2RDtJQUNILENBQUM7SUFsQ1Usc0JBQXNCO1FBTmxDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsbUJBQW1CO1lBQzdCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsaUNBQWlDO1lBQzlDLFNBQVMsRUFBRSxDQUFDLGdDQUFnQyxDQUFDO1NBQzlDLENBQUM7eUNBUW9CLHlCQUFnQjtZQUNiLDBCQUFXO09BUnZCLHNCQUFzQixDQW1DbEM7SUFBRCw2QkFBQztDQUFBLEFBbkNELElBbUNDO0FBbkNZLHdEQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBTd2lwZUdlc3R1cmVFdmVudERhdGEgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9nZXN0dXJlc1wiO1xyXG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcIm5zLXBheW1lbnRNZXRob2RzXCIsXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICB0ZW1wbGF0ZVVybDogXCIuL3JldHVybi1vcHRpb25zLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL3JldHVybi1vcHRpb25zLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIFJldHVybk9wdGlvbnNDb21wb25lbnQge1xyXG4gIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnM7XHJcblxyXG4gIHJldHVybjEgPSB0cnVlO1xyXG4gIHJldHVybjIgPSBmYWxzZTtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLFxyXG4gICAgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2VcclxuICApIHtcclxuICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucyA9IHJvdXRlckV4dGVuc2lvbnM7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmFjdGlvbkJhclN0YXRlKHRydWUpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5ib3R0b21CYXJEb3RzKDEyKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYm90dG9tQmFyRG90U3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZSh0cnVlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UudGFiVmlld1N0YXRlQ2hhbmdlcygxKTtcclxuICB9XHJcblxyXG4gIHJldHVybk9wdGlvbjEoKSB7XHJcbiAgICB0aGlzLnJldHVybjEgPSB0cnVlO1xyXG4gICAgdGhpcy5yZXR1cm4yID0gZmFsc2U7XHJcbiAgfVxyXG4gIHJldHVybk9wdGlvbjIoKSB7XHJcbiAgICB0aGlzLnJldHVybjEgPSBmYWxzZTtcclxuICAgIHRoaXMucmV0dXJuMiA9IHRydWU7XHJcbiAgfVxyXG5cclxuICBvblN3aXBlKGFyZ3M6IFN3aXBlR2VzdHVyZUV2ZW50RGF0YSkge1xyXG4gICAgaWYgKGFyZ3MuZGlyZWN0aW9uID09IDEpIHtcclxuICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9wYXltZW50TWV0aG9kc1wiXSk7XHJcbiAgICB9XHJcbiAgICBpZiAoYXJncy5kaXJlY3Rpb24gPT0gMikge1xyXG4gICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2J1c2luZXNzcG9saWNpZXNcIl0pO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=
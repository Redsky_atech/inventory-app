import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { UserService } from "../services/user.service";

@Component({
  selector: "ns-paymentMethods",
  moduleId: module.id,
  templateUrl: "./return-options.component.html",
  styleUrls: ["./return-options.component.css"]
})
export class ReturnOptionsComponent {
  routerExtensions: RouterExtensions;

  return1 = true;
  return2 = false;

  constructor(
    routerExtensions: RouterExtensions,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDots(12);
    this.userService.bottomBarDotState(true);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
  }

  returnOption1() {
    this.return1 = true;
    this.return2 = false;
  }
  returnOption2() {
    this.return1 = false;
    this.return2 = true;
  }

  onSwipe(args: SwipeGestureEventData) {
    if (args.direction == 1) {
      this.routerExtensions.navigate(["/paymentMethods"]);
    }
    if (args.direction == 2) {
      this.routerExtensions.navigate(["/businesspolicies"]);
    }
  }
}

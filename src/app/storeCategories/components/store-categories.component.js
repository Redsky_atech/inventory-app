"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_service_1 = require("../../services/user.service");
var router_1 = require("@angular/router");
var StoreCategoryComponent = /** @class */ (function () {
    function StoreCategoryComponent(router, userService) {
        this.router = router;
        this.userService = userService;
        this.category1 = false;
        this.category2 = false;
        this.category3 = false;
        this.category4 = false;
        this.category5 = false;
        this.storeCategories = [
            "Category 1",
            "Category 2",
            "Category 3",
            "Category 4",
            "Category 5"
        ];
        this.storeCategory1 = "";
        this.storeCategory2 = "";
        this.storeCategory3 = "";
        this.storeCategory4 = "";
        this.storeCategory5 = "";
        this.userService.actionBarState(true);
        this.userService.bottomBarDots(6);
        this.userService.bottomBarDotState(true);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.userService.switchState(false);
        this.storeCategory1 = this.storeCategories[0];
        this.storeCategory2 = this.storeCategories[1];
        this.storeCategory3 = this.storeCategories[2];
        this.storeCategory4 = this.storeCategories[3];
        this.storeCategory5 = this.storeCategories[4];
    }
    StoreCategoryComponent.prototype.categoryClick1 = function () {
        if (this.category1 == false) {
            this.category1 = true;
            this.category2 = false;
            this.category3 = false;
            this.category4 = false;
            this.category5 = false;
        }
    };
    StoreCategoryComponent.prototype.categoryClick2 = function () {
        if (this.category2 == false) {
            this.category1 = false;
            this.category2 = true;
            this.category3 = false;
            this.category4 = false;
            this.category5 = false;
        }
    };
    StoreCategoryComponent.prototype.categoryClick3 = function () {
        if (this.category3 == false) {
            this.category1 = false;
            this.category2 = false;
            this.category3 = true;
            this.category4 = false;
            this.category5 = false;
        }
    };
    StoreCategoryComponent.prototype.categoryClick4 = function () {
        if (this.category4 == false) {
            this.category1 = false;
            this.category2 = false;
            this.category3 = false;
            this.category4 = true;
            this.category5 = false;
        }
    };
    StoreCategoryComponent.prototype.categoryClick5 = function () {
        if (this.category5 == false) {
            this.category1 = false;
            this.category2 = false;
            this.category3 = false;
            this.category4 = false;
            this.category5 = true;
        }
    };
    StoreCategoryComponent.prototype.onSwipe = function (args) {
        if (args.direction == 1) {
            this.router.navigate(["/findCategory"]);
        }
        if (args.direction == 2) {
            if (this.category1 == true) {
                var navigationExtras_1 = {
                    queryParams: { storeCategory: " category1" }
                };
                this.router.navigate(["/condition"], navigationExtras_1);
            }
            if (this.category2 == true) {
                var navigationExtras_2 = {
                    queryParams: { storeCategory: " category2" }
                };
                this.router.navigate(["/condition"], navigationExtras_2);
            }
            if (this.category3 == true) {
                var navigationExtras_3 = {
                    queryParams: { storeCategory: " category3" }
                };
                this.router.navigate(["/condition"], navigationExtras_3);
            }
            if (this.category4 == true) {
                var navigationExtras_4 = {
                    queryParams: { storeCategory: " category4" }
                };
                this.router.navigate(["/condition"], navigationExtras_4);
            }
            if (this.category5 == true) {
                var navigationExtras_5 = {
                    queryParams: { storeCategory: " category5" }
                };
                this.router.navigate(["/condition"], navigationExtras_5);
            }
            var navigationExtras = {
                queryParams: { storeCategory: "" }
            };
            this.router.navigate(["/condition"], navigationExtras);
        }
    };
    StoreCategoryComponent = __decorate([
        core_1.Component({
            selector: "app-storeCategory",
            moduleId: module.id,
            templateUrl: "./store-categories.component.html",
            styleUrls: ["./store-categories.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.Router, user_service_1.UserService])
    ], StoreCategoryComponent);
    return StoreCategoryComponent;
}());
exports.StoreCategoryComponent = StoreCategoryComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUtY2F0ZWdvcmllcy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS1jYXRlZ29yaWVzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF5RTtBQUV6RSw0REFBMEQ7QUFDMUQsMENBQTJEO0FBTzNEO0lBa0JFLGdDQUFvQixNQUFjLEVBQVUsV0FBd0I7UUFBaEQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBakJwRSxjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDbEIsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNsQixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDWCxvQkFBZSxHQUFHO1lBQ3ZCLFlBQVk7WUFDWixZQUFZO1lBQ1osWUFBWTtZQUNaLFlBQVk7WUFDWixZQUFZO1NBQ2IsQ0FBQztRQUNGLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBRWxCLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCwrQ0FBYyxHQUFkO1FBQ0UsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBRTtZQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztTQUN4QjtJQUNILENBQUM7SUFDRCwrQ0FBYyxHQUFkO1FBQ0UsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBRTtZQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztTQUN4QjtJQUNILENBQUM7SUFDRCwrQ0FBYyxHQUFkO1FBQ0UsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBRTtZQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztTQUN4QjtJQUNILENBQUM7SUFDRCwrQ0FBYyxHQUFkO1FBQ0UsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBRTtZQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztTQUN4QjtJQUNILENBQUM7SUFDRCwrQ0FBYyxHQUFkO1FBQ0UsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBRTtZQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztTQUN2QjtJQUNILENBQUM7SUFFRCx3Q0FBTyxHQUFQLFVBQVEsSUFBMkI7UUFDakMsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsRUFBRTtZQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7U0FDekM7UUFDRCxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxFQUFFO1lBQ3ZCLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLEVBQUU7Z0JBQzFCLElBQUksa0JBQWdCLEdBQXFCO29CQUN2QyxXQUFXLEVBQUUsRUFBRSxhQUFhLEVBQUUsWUFBWSxFQUFFO2lCQUM3QyxDQUFDO2dCQUNGLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsa0JBQWdCLENBQUMsQ0FBQzthQUN4RDtZQUNELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLEVBQUU7Z0JBQzFCLElBQUksa0JBQWdCLEdBQXFCO29CQUN2QyxXQUFXLEVBQUUsRUFBRSxhQUFhLEVBQUUsWUFBWSxFQUFFO2lCQUM3QyxDQUFDO2dCQUNGLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsa0JBQWdCLENBQUMsQ0FBQzthQUN4RDtZQUNELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLEVBQUU7Z0JBQzFCLElBQUksa0JBQWdCLEdBQXFCO29CQUN2QyxXQUFXLEVBQUUsRUFBRSxhQUFhLEVBQUUsWUFBWSxFQUFFO2lCQUM3QyxDQUFDO2dCQUNGLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsa0JBQWdCLENBQUMsQ0FBQzthQUN4RDtZQUNELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLEVBQUU7Z0JBQzFCLElBQUksa0JBQWdCLEdBQXFCO29CQUN2QyxXQUFXLEVBQUUsRUFBRSxhQUFhLEVBQUUsWUFBWSxFQUFFO2lCQUM3QyxDQUFDO2dCQUNGLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsa0JBQWdCLENBQUMsQ0FBQzthQUN4RDtZQUNELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLEVBQUU7Z0JBQzFCLElBQUksa0JBQWdCLEdBQXFCO29CQUN2QyxXQUFXLEVBQUUsRUFBRSxhQUFhLEVBQUUsWUFBWSxFQUFFO2lCQUM3QyxDQUFDO2dCQUNGLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsa0JBQWdCLENBQUMsQ0FBQzthQUN4RDtZQUNELElBQUksZ0JBQWdCLEdBQXFCO2dCQUN2QyxXQUFXLEVBQUUsRUFBRSxhQUFhLEVBQUUsRUFBRSxFQUFFO2FBQ25DLENBQUM7WUFDRixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxFQUFFLGdCQUFnQixDQUFDLENBQUM7U0FDeEQ7SUFDSCxDQUFDO0lBdEhVLHNCQUFzQjtRQU5sQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLG1CQUFtQjtZQUM3QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLG1DQUFtQztZQUNoRCxTQUFTLEVBQUUsQ0FBQyxrQ0FBa0MsQ0FBQztTQUNoRCxDQUFDO3lDQW1CNEIsZUFBTSxFQUF1QiwwQkFBVztPQWxCekQsc0JBQXNCLENBdUhsQztJQUFELDZCQUFDO0NBQUEsQUF2SEQsSUF1SEM7QUF2SFksd0RBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdDaGlsZCwgRWxlbWVudFJlZiB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFN3aXBlR2VzdHVyZUV2ZW50RGF0YSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2dlc3R1cmVzXCI7XHJcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcIi4uLy4uL3NlcnZpY2VzL3VzZXIuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXIsIE5hdmlnYXRpb25FeHRyYXMgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcImFwcC1zdG9yZUNhdGVnb3J5XCIsXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICB0ZW1wbGF0ZVVybDogXCIuL3N0b3JlLWNhdGVnb3JpZXMuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vc3RvcmUtY2F0ZWdvcmllcy5jb21wb25lbnQuY3NzXCJdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTdG9yZUNhdGVnb3J5Q29tcG9uZW50IHtcclxuICBjYXRlZ29yeTEgPSBmYWxzZTtcclxuICBjYXRlZ29yeTIgPSBmYWxzZTtcclxuICBjYXRlZ29yeTMgPSBmYWxzZTtcclxuICBjYXRlZ29yeTQgPSBmYWxzZTtcclxuICBjYXRlZ29yeTUgPSBmYWxzZTtcclxuICBwdWJsaWMgc3RvcmVDYXRlZ29yaWVzID0gW1xyXG4gICAgXCJDYXRlZ29yeSAxXCIsXHJcbiAgICBcIkNhdGVnb3J5IDJcIixcclxuICAgIFwiQ2F0ZWdvcnkgM1wiLFxyXG4gICAgXCJDYXRlZ29yeSA0XCIsXHJcbiAgICBcIkNhdGVnb3J5IDVcIlxyXG4gIF07XHJcbiAgc3RvcmVDYXRlZ29yeTEgPSBcIlwiO1xyXG4gIHN0b3JlQ2F0ZWdvcnkyID0gXCJcIjtcclxuICBzdG9yZUNhdGVnb3J5MyA9IFwiXCI7XHJcbiAgc3RvcmVDYXRlZ29yeTQgPSBcIlwiO1xyXG4gIHN0b3JlQ2F0ZWdvcnk1ID0gXCJcIjtcclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyLCBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSkge1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5hY3Rpb25CYXJTdGF0ZSh0cnVlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYm90dG9tQmFyRG90cyg2KTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYm90dG9tQmFyRG90U3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZSh0cnVlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UudGFiVmlld1N0YXRlQ2hhbmdlcygxKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2Uuc3dpdGNoU3RhdGUoZmFsc2UpO1xyXG4gICAgdGhpcy5zdG9yZUNhdGVnb3J5MSA9IHRoaXMuc3RvcmVDYXRlZ29yaWVzWzBdO1xyXG4gICAgdGhpcy5zdG9yZUNhdGVnb3J5MiA9IHRoaXMuc3RvcmVDYXRlZ29yaWVzWzFdO1xyXG4gICAgdGhpcy5zdG9yZUNhdGVnb3J5MyA9IHRoaXMuc3RvcmVDYXRlZ29yaWVzWzJdO1xyXG4gICAgdGhpcy5zdG9yZUNhdGVnb3J5NCA9IHRoaXMuc3RvcmVDYXRlZ29yaWVzWzNdO1xyXG4gICAgdGhpcy5zdG9yZUNhdGVnb3J5NSA9IHRoaXMuc3RvcmVDYXRlZ29yaWVzWzRdO1xyXG4gIH1cclxuXHJcbiAgY2F0ZWdvcnlDbGljazEoKSB7XHJcbiAgICBpZiAodGhpcy5jYXRlZ29yeTEgPT0gZmFsc2UpIHtcclxuICAgICAgdGhpcy5jYXRlZ29yeTEgPSB0cnVlO1xyXG4gICAgICB0aGlzLmNhdGVnb3J5MiA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNhdGVnb3J5MyA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNhdGVnb3J5NCA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNhdGVnb3J5NSA9IGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuICBjYXRlZ29yeUNsaWNrMigpIHtcclxuICAgIGlmICh0aGlzLmNhdGVnb3J5MiA9PSBmYWxzZSkge1xyXG4gICAgICB0aGlzLmNhdGVnb3J5MSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNhdGVnb3J5MiA9IHRydWU7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnkzID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnk0ID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnk1ID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGNhdGVnb3J5Q2xpY2szKCkge1xyXG4gICAgaWYgKHRoaXMuY2F0ZWdvcnkzID09IGZhbHNlKSB7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnkxID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnkyID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnkzID0gdHJ1ZTtcclxuICAgICAgdGhpcy5jYXRlZ29yeTQgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jYXRlZ29yeTUgPSBmYWxzZTtcclxuICAgIH1cclxuICB9XHJcbiAgY2F0ZWdvcnlDbGljazQoKSB7XHJcbiAgICBpZiAodGhpcy5jYXRlZ29yeTQgPT0gZmFsc2UpIHtcclxuICAgICAgdGhpcy5jYXRlZ29yeTEgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jYXRlZ29yeTIgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jYXRlZ29yeTMgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jYXRlZ29yeTQgPSB0cnVlO1xyXG4gICAgICB0aGlzLmNhdGVnb3J5NSA9IGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuICBjYXRlZ29yeUNsaWNrNSgpIHtcclxuICAgIGlmICh0aGlzLmNhdGVnb3J5NSA9PSBmYWxzZSkge1xyXG4gICAgICB0aGlzLmNhdGVnb3J5MSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNhdGVnb3J5MiA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNhdGVnb3J5MyA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNhdGVnb3J5NCA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNhdGVnb3J5NSA9IHRydWU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBvblN3aXBlKGFyZ3M6IFN3aXBlR2VzdHVyZUV2ZW50RGF0YSkge1xyXG4gICAgaWYgKGFyZ3MuZGlyZWN0aW9uID09IDEpIHtcclxuICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1wiL2ZpbmRDYXRlZ29yeVwiXSk7XHJcbiAgICB9XHJcbiAgICBpZiAoYXJncy5kaXJlY3Rpb24gPT0gMikge1xyXG4gICAgICBpZiAodGhpcy5jYXRlZ29yeTEgPT0gdHJ1ZSkge1xyXG4gICAgICAgIGxldCBuYXZpZ2F0aW9uRXh0cmFzOiBOYXZpZ2F0aW9uRXh0cmFzID0ge1xyXG4gICAgICAgICAgcXVlcnlQYXJhbXM6IHsgc3RvcmVDYXRlZ29yeTogXCIgY2F0ZWdvcnkxXCIgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1wiL2NvbmRpdGlvblwiXSwgbmF2aWdhdGlvbkV4dHJhcyk7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHRoaXMuY2F0ZWdvcnkyID09IHRydWUpIHtcclxuICAgICAgICBsZXQgbmF2aWdhdGlvbkV4dHJhczogTmF2aWdhdGlvbkV4dHJhcyA9IHtcclxuICAgICAgICAgIHF1ZXJ5UGFyYW1zOiB7IHN0b3JlQ2F0ZWdvcnk6IFwiIGNhdGVnb3J5MlwiIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtcIi9jb25kaXRpb25cIl0sIG5hdmlnYXRpb25FeHRyYXMpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLmNhdGVnb3J5MyA9PSB0cnVlKSB7XHJcbiAgICAgICAgbGV0IG5hdmlnYXRpb25FeHRyYXM6IE5hdmlnYXRpb25FeHRyYXMgPSB7XHJcbiAgICAgICAgICBxdWVyeVBhcmFtczogeyBzdG9yZUNhdGVnb3J5OiBcIiBjYXRlZ29yeTNcIiB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCIvY29uZGl0aW9uXCJdLCBuYXZpZ2F0aW9uRXh0cmFzKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAodGhpcy5jYXRlZ29yeTQgPT0gdHJ1ZSkge1xyXG4gICAgICAgIGxldCBuYXZpZ2F0aW9uRXh0cmFzOiBOYXZpZ2F0aW9uRXh0cmFzID0ge1xyXG4gICAgICAgICAgcXVlcnlQYXJhbXM6IHsgc3RvcmVDYXRlZ29yeTogXCIgY2F0ZWdvcnk0XCIgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1wiL2NvbmRpdGlvblwiXSwgbmF2aWdhdGlvbkV4dHJhcyk7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHRoaXMuY2F0ZWdvcnk1ID09IHRydWUpIHtcclxuICAgICAgICBsZXQgbmF2aWdhdGlvbkV4dHJhczogTmF2aWdhdGlvbkV4dHJhcyA9IHtcclxuICAgICAgICAgIHF1ZXJ5UGFyYW1zOiB7IHN0b3JlQ2F0ZWdvcnk6IFwiIGNhdGVnb3J5NVwiIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtcIi9jb25kaXRpb25cIl0sIG5hdmlnYXRpb25FeHRyYXMpO1xyXG4gICAgICB9XHJcbiAgICAgIGxldCBuYXZpZ2F0aW9uRXh0cmFzOiBOYXZpZ2F0aW9uRXh0cmFzID0ge1xyXG4gICAgICAgIHF1ZXJ5UGFyYW1zOiB7IHN0b3JlQ2F0ZWdvcnk6IFwiXCIgfVxyXG4gICAgICB9O1xyXG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCIvY29uZGl0aW9uXCJdLCBuYXZpZ2F0aW9uRXh0cmFzKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19
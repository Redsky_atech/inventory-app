"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var http_1 = require("@angular/http");
var http_2 = require("nativescript-angular/http");
var forms_1 = require("nativescript-angular/forms");
var http_client_1 = require("nativescript-angular/http-client");
var store_categories_routing_module_1 = require("./store-categories-routing.module");
var store_categories_component_1 = require("./components/store-categories.component");
var StoreCategoryModule = /** @class */ (function () {
    function StoreCategoryModule() {
    }
    StoreCategoryModule = __decorate([
        core_1.NgModule({
            bootstrap: [store_categories_component_1.StoreCategoryComponent],
            imports: [
                nativescript_module_1.NativeScriptModule,
                http_1.HttpModule,
                store_categories_routing_module_1.StoreCategoryRoutingModule,
                http_2.NativeScriptHttpModule,
                forms_1.NativeScriptFormsModule,
                http_client_1.NativeScriptHttpClientModule
            ],
            declarations: [store_categories_component_1.StoreCategoryComponent],
            schemas: [core_1.NO_ERRORS_SCHEMA]
        })
    ], StoreCategoryModule);
    return StoreCategoryModule;
}());
exports.StoreCategoryModule = StoreCategoryModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUtY2F0ZWdvcmllcy5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS1jYXRlZ29yaWVzLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyRTtBQUMzRSxnRkFBOEU7QUFDOUUsc0NBQTJDO0FBQzNDLGtEQUFtRTtBQUNuRSxvREFBcUU7QUFDckUsZ0VBQWdGO0FBQ2hGLHFGQUErRTtBQUMvRSxzRkFBaUY7QUFnQmpGO0lBQUE7SUFBa0MsQ0FBQztJQUF0QixtQkFBbUI7UUFkL0IsZUFBUSxDQUFDO1lBQ1IsU0FBUyxFQUFFLENBQUMsbURBQXNCLENBQUM7WUFDbkMsT0FBTyxFQUFFO2dCQUNQLHdDQUFrQjtnQkFDbEIsaUJBQVU7Z0JBQ1YsNERBQTBCO2dCQUMxQiw2QkFBc0I7Z0JBQ3RCLCtCQUF1QjtnQkFDdkIsMENBQTRCO2FBQzdCO1lBQ0QsWUFBWSxFQUFFLENBQUMsbURBQXNCLENBQUM7WUFFdEMsT0FBTyxFQUFFLENBQUMsdUJBQWdCLENBQUM7U0FDNUIsQ0FBQztPQUNXLG1CQUFtQixDQUFHO0lBQUQsMEJBQUM7Q0FBQSxBQUFuQyxJQUFtQztBQUF0QixrREFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTk9fRVJST1JTX1NDSEVNQSwgSW5qZWN0aW9uVG9rZW4gfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvbmF0aXZlc2NyaXB0Lm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBIdHRwTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2h0dHBcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0SHR0cE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9odHRwXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2Zvcm1zXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvaHR0cC1jbGllbnRcIjtcclxuaW1wb3J0IHsgU3RvcmVDYXRlZ29yeVJvdXRpbmdNb2R1bGUgfSBmcm9tIFwiLi9zdG9yZS1jYXRlZ29yaWVzLXJvdXRpbmcubW9kdWxlXCI7XHJcbmltcG9ydCB7IFN0b3JlQ2F0ZWdvcnlDb21wb25lbnQgfSBmcm9tIFwiLi9jb21wb25lbnRzL3N0b3JlLWNhdGVnb3JpZXMuY29tcG9uZW50XCI7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGJvb3RzdHJhcDogW1N0b3JlQ2F0ZWdvcnlDb21wb25lbnRdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIE5hdGl2ZVNjcmlwdE1vZHVsZSxcclxuICAgIEh0dHBNb2R1bGUsXHJcbiAgICBTdG9yZUNhdGVnb3J5Um91dGluZ01vZHVsZSxcclxuICAgIE5hdGl2ZVNjcmlwdEh0dHBNb2R1bGUsXHJcbiAgICBOYXRpdmVTY3JpcHRGb3Jtc01vZHVsZSxcclxuICAgIE5hdGl2ZVNjcmlwdEh0dHBDbGllbnRNb2R1bGVcclxuICBdLFxyXG4gIGRlY2xhcmF0aW9uczogW1N0b3JlQ2F0ZWdvcnlDb21wb25lbnRdLFxyXG5cclxuICBzY2hlbWFzOiBbTk9fRVJST1JTX1NDSEVNQV1cclxufSlcclxuZXhwb3J0IGNsYXNzIFN0b3JlQ2F0ZWdvcnlNb2R1bGUge31cclxuIl19
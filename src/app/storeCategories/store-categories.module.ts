import { NgModule, NO_ERRORS_SCHEMA, InjectionToken } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { HttpModule } from "@angular/http";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { StoreCategoryRoutingModule } from "./store-categories-routing.module";
import { StoreCategoryComponent } from "./components/store-categories.component";

@NgModule({
  bootstrap: [StoreCategoryComponent],
  imports: [
    NativeScriptModule,
    HttpModule,
    StoreCategoryRoutingModule,
    NativeScriptHttpModule,
    NativeScriptFormsModule,
    NativeScriptHttpClientModule
  ],
  declarations: [StoreCategoryComponent],

  schemas: [NO_ERRORS_SCHEMA]
})
export class StoreCategoryModule {}

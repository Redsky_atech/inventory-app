"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var store_categories_component_1 = require("./components/store-categories.component");
// const routes: Routes = [
//     { path: "", redirectTo: "/register", pathMatch: "full" },
//     { path: "register", component: RegisterComponent },
// ];
var routes = [{ path: "", component: store_categories_component_1.StoreCategoryComponent }];
var StoreCategoryRoutingModule = /** @class */ (function () {
    function StoreCategoryRoutingModule() {
    }
    StoreCategoryRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.NativeScriptRouterModule.forChild(routes)],
            exports: [router_1.NativeScriptRouterModule]
        })
    ], StoreCategoryRoutingModule);
    return StoreCategoryRoutingModule;
}());
exports.StoreCategoryRoutingModule = StoreCategoryRoutingModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUtY2F0ZWdvcmllcy1yb3V0aW5nLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInN0b3JlLWNhdGVnb3JpZXMtcm91dGluZy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBeUM7QUFFekMsc0RBQXVFO0FBQ3ZFLHNGQUFpRjtBQUVqRiwyQkFBMkI7QUFDM0IsZ0VBQWdFO0FBQ2hFLDBEQUEwRDtBQUMxRCxLQUFLO0FBRUwsSUFBTSxNQUFNLEdBQVcsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLG1EQUFzQixFQUFFLENBQUMsQ0FBQztBQU16RTtJQUFBO0lBQXlDLENBQUM7SUFBN0IsMEJBQTBCO1FBSnRDLGVBQVEsQ0FBQztZQUNSLE9BQU8sRUFBRSxDQUFDLGlDQUF3QixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNwRCxPQUFPLEVBQUUsQ0FBQyxpQ0FBd0IsQ0FBQztTQUNwQyxDQUFDO09BQ1csMEJBQTBCLENBQUc7SUFBRCxpQ0FBQztDQUFBLEFBQTFDLElBQTBDO0FBQTdCLGdFQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVzIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IFN0b3JlQ2F0ZWdvcnlDb21wb25lbnQgfSBmcm9tIFwiLi9jb21wb25lbnRzL3N0b3JlLWNhdGVnb3JpZXMuY29tcG9uZW50XCI7XHJcblxyXG4vLyBjb25zdCByb3V0ZXM6IFJvdXRlcyA9IFtcclxuLy8gICAgIHsgcGF0aDogXCJcIiwgcmVkaXJlY3RUbzogXCIvcmVnaXN0ZXJcIiwgcGF0aE1hdGNoOiBcImZ1bGxcIiB9LFxyXG4vLyAgICAgeyBwYXRoOiBcInJlZ2lzdGVyXCIsIGNvbXBvbmVudDogUmVnaXN0ZXJDb21wb25lbnQgfSxcclxuLy8gXTtcclxuXHJcbmNvbnN0IHJvdXRlczogUm91dGVzID0gW3sgcGF0aDogXCJcIiwgY29tcG9uZW50OiBTdG9yZUNhdGVnb3J5Q29tcG9uZW50IH1dO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBpbXBvcnRzOiBbTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlLmZvckNoaWxkKHJvdXRlcyldLFxyXG4gIGV4cG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGVdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTdG9yZUNhdGVnb3J5Um91dGluZ01vZHVsZSB7fVxyXG4iXX0=
import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { UserService } from "../services/user.service";

@Component({
  selector: "ns-shelveInventory",
  moduleId: module.id,
  templateUrl: "./shelveInventory.component.html",
  styleUrls: ["./shelveInventory.component.css"]
})
export class ShelveInventoryComponent implements OnInit {
  routerExtensions: RouterExtensions;

  public brandNames = [
    "Dell",
    "Samsung",
    "Apple",
    "Hp",
    "Nokia",
    "Redmi",
    "Motorola",
    "Linovo",
    "Panasonic"
  ];
  constructor(
    routerExtensions: RouterExtensions,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDotState(false);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
  }
  ngOnInit(): void {}

  public onopen() {
    console.log("Drop Down opened.");
  }

  public onclose() {
    console.log("Drop Down closed.");
  }

  public onScanItem() {
    this.routerExtensions.navigate(["/"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }
}

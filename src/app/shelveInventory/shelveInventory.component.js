"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var user_service_1 = require("../services/user.service");
var ShelveInventoryComponent = /** @class */ (function () {
    function ShelveInventoryComponent(routerExtensions, userService) {
        this.userService = userService;
        this.brandNames = [
            "Dell",
            "Samsung",
            "Apple",
            "Hp",
            "Nokia",
            "Redmi",
            "Motorola",
            "Linovo",
            "Panasonic"
        ];
        this.routerExtensions = routerExtensions;
        this.userService.actionBarState(true);
        this.userService.bottomBarDotState(false);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
    }
    ShelveInventoryComponent.prototype.ngOnInit = function () { };
    ShelveInventoryComponent.prototype.onopen = function () {
        console.log("Drop Down opened.");
    };
    ShelveInventoryComponent.prototype.onclose = function () {
        console.log("Drop Down closed.");
    };
    ShelveInventoryComponent.prototype.onScanItem = function () {
        this.routerExtensions.navigate(["/"], {
            animated: true,
            transition: {
                name: "slide",
                duration: 200,
                curve: "ease"
            }
        });
    };
    ShelveInventoryComponent = __decorate([
        core_1.Component({
            selector: "ns-shelveInventory",
            moduleId: module.id,
            templateUrl: "./shelveInventory.component.html",
            styleUrls: ["./shelveInventory.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            user_service_1.UserService])
    ], ShelveInventoryComponent);
    return ShelveInventoryComponent;
}());
exports.ShelveInventoryComponent = ShelveInventoryComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hlbHZlSW52ZW50b3J5LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNoZWx2ZUludmVudG9yeS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBeUU7QUFDekUsc0RBQStEO0FBQy9ELHlEQUF1RDtBQVF2RDtJQWNFLGtDQUNFLGdCQUFrQyxFQUMxQixXQUF3QjtRQUF4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQWIzQixlQUFVLEdBQUc7WUFDbEIsTUFBTTtZQUNOLFNBQVM7WUFDVCxPQUFPO1lBQ1AsSUFBSTtZQUNKLE9BQU87WUFDUCxPQUFPO1lBQ1AsVUFBVTtZQUNWLFFBQVE7WUFDUixXQUFXO1NBQ1osQ0FBQztRQUtBLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUNELDJDQUFRLEdBQVIsY0FBa0IsQ0FBQztJQUVaLHlDQUFNLEdBQWI7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVNLDBDQUFPLEdBQWQ7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVNLDZDQUFVLEdBQWpCO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ3BDLFFBQVEsRUFBRSxJQUFJO1lBQ2QsVUFBVSxFQUFFO2dCQUNWLElBQUksRUFBRSxPQUFPO2dCQUNiLFFBQVEsRUFBRSxHQUFHO2dCQUNiLEtBQUssRUFBRSxNQUFNO2FBQ2Q7U0FDRixDQUFDLENBQUM7SUFDTCxDQUFDO0lBM0NVLHdCQUF3QjtRQU5wQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLG9CQUFvQjtZQUM5QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLGtDQUFrQztZQUMvQyxTQUFTLEVBQUUsQ0FBQyxpQ0FBaUMsQ0FBQztTQUMvQyxDQUFDO3lDQWdCb0IseUJBQWdCO1lBQ2IsMEJBQVc7T0FoQnZCLHdCQUF3QixDQTRDcEM7SUFBRCwrQkFBQztDQUFBLEFBNUNELElBNENDO0FBNUNZLDREQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcIm5zLXNoZWx2ZUludmVudG9yeVwiLFxyXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9zaGVsdmVJbnZlbnRvcnkuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vc2hlbHZlSW52ZW50b3J5LmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIFNoZWx2ZUludmVudG9yeUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucztcclxuXHJcbiAgcHVibGljIGJyYW5kTmFtZXMgPSBbXHJcbiAgICBcIkRlbGxcIixcclxuICAgIFwiU2Ftc3VuZ1wiLFxyXG4gICAgXCJBcHBsZVwiLFxyXG4gICAgXCJIcFwiLFxyXG4gICAgXCJOb2tpYVwiLFxyXG4gICAgXCJSZWRtaVwiLFxyXG4gICAgXCJNb3Rvcm9sYVwiLFxyXG4gICAgXCJMaW5vdm9cIixcclxuICAgIFwiUGFuYXNvbmljXCJcclxuICBdO1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcclxuICAgIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMgPSByb3V0ZXJFeHRlbnNpb25zO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5hY3Rpb25CYXJTdGF0ZSh0cnVlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYm90dG9tQmFyRG90U3RhdGUoZmFsc2UpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZUNoYW5nZXMoMSk7XHJcbiAgfVxyXG4gIG5nT25Jbml0KCk6IHZvaWQge31cclxuXHJcbiAgcHVibGljIG9ub3BlbigpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiRHJvcCBEb3duIG9wZW5lZC5cIik7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25jbG9zZSgpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiRHJvcCBEb3duIGNsb3NlZC5cIik7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25TY2FuSXRlbSgpIHtcclxuICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvXCJdLCB7XHJcbiAgICAgIGFuaW1hdGVkOiB0cnVlLFxyXG4gICAgICB0cmFuc2l0aW9uOiB7XHJcbiAgICAgICAgbmFtZTogXCJzbGlkZVwiLFxyXG4gICAgICAgIGR1cmF0aW9uOiAyMDAsXHJcbiAgICAgICAgY3VydmU6IFwiZWFzZVwiXHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=
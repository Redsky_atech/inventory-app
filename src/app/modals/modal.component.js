"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_1 = require("tns-core-modules/platform");
var enums_1 = require("tns-core-modules/ui/enums");
var gestures_1 = require("tns-core-modules/ui/gestures");
var page_1 = require("tns-core-modules/ui/page");
var application_1 = require("tns-core-modules/application");
// No support for Array#includes here
function includes(container, value) {
    var returnValue = false;
    var pos = container.indexOf(value);
    if (pos >= 0) {
        returnValue = true;
    }
    return returnValue;
}
var GestureRecognizer, Interop;
if (application_1.ios) {
    GestureRecognizer = NSObject;
    Interop = interop;
}
else {
    GestureRecognizer = /** @class */ (function () {
        function A() {
        }
        return A;
    }());
    Interop = { types: { id: void 0, void: void 0 } };
}
var HideGestureRecognizerImpl = /** @class */ (function (_super) {
    __extends(HideGestureRecognizerImpl, _super);
    function HideGestureRecognizerImpl() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    HideGestureRecognizerImpl.initWithOwner = function (owner) {
        var handler = new HideGestureRecognizerImpl();
        handler._owner = owner;
        return handler;
    };
    HideGestureRecognizerImpl.prototype.tap = function () {
        this._owner.ios.resignFirstResponder();
        if (this.func) {
            this.func();
        }
    };
    HideGestureRecognizerImpl.ObjCExposedMethods = {
        "tap": { returns: Interop.types.void, params: [Interop.types.id] }
    };
    return HideGestureRecognizerImpl;
}(GestureRecognizer));
// Keep external state of views
var targetHandler = null;
var targetHandler2 = null;
var ModalComponent = /** @class */ (function () {
    function ModalComponent(hostEl, page) {
        var _this = this;
        this.hostEl = hostEl;
        this.page = page;
        this.isShowing = false;
        this.durationScale = .75;
        this.data = null; // Optional data parameter
        this.size = "sm"; // sm | md | lg
        this.dismissable = false;
        this.alignment = "center"; // center | stretch | middle | top | bottom
        this.duration = 250; // in milliseconds
        this.open = new core_1.EventEmitter();
        this.close = new core_1.EventEmitter();
        this.onTapHide = function () {
            if (platform_1.isAndroid && _this.dismissable) {
                _this.hide();
            }
        };
        // if (isAndroid) {
        //     this.page.on(Page.loadedEvent, () => {
        //         android.on(AndroidApplication.activityBackPressedEvent, (args: AndroidActivityBackPressedEventData) => {
        //             if (this.isShowing) {
        //                 args.cancel = true;
        //                 this.hide();
        //             }
        //         });
        //     });
        //     this.page.on(Page.unloadedEvent, () => {
        //         android.off(AndroidApplication.activityBackPressedEvent);
        //     });
        // }
    }
    ModalComponent.prototype.ngOnInit = function () {
        this.pageHeight = this.pageHeight ? this.pageHeight : platform_1.screen.mainScreen.heightDIPs;
        this.hostView.style.translateY = this.pageHeight;
    };
    ModalComponent.prototype.show = function (data) {
        var _this = this;
        if (data === void 0) { data = null; }
        if (!this.overlayView) {
            return;
        }
        this.hostView.style.translateY = 0;
        return this.overlayView.animate({
            translate: { x: 0, y: 0 }, duration: 0,
        }).then(function () { return _this.overlayView.animate({
            opacity: 1, duration: _this.timing * _this.durationScale,
        }); }).then(function () { return _this.bodyView.animate({
            translate: { x: 0, y: 0 },
            duration: 0,
            curve: enums_1.AnimationCurve.cubicBezier(.12, .3, .58, .44),
        }); }).then(function () { return _this.bodyView.animate({
            scale: { x: 1, y: 1 },
            opacity: 1,
            duration: _this.timing,
            curve: enums_1.AnimationCurve.cubicBezier(.12, .3, .58, .44),
        }); }).then(function () {
            _this.open.emit(_this.data = data);
            _this.isShowing = true;
        }).catch(function () { return 0; });
    };
    ModalComponent.prototype.hide = function () {
        var _this = this;
        return this.bodyView.animate({
            opacity: 0,
            duration: this.timing * this.durationScale,
            curve: enums_1.AnimationCurve.cubicBezier(.12, .3, .58, .44),
        }).then(function () { return _this.bodyView.animate({
            scale: { x: .6, y: .6 },
            translate: { x: 0, y: _this.pageHeight },
            duration: 0,
            curve: enums_1.AnimationCurve.cubicBezier(.12, .3, .58, .44),
        }); }).then(function () { return _this.overlayView.animate({
            opacity: 0, duration: _this.timing * _this.durationScale,
            curve: enums_1.AnimationCurve.easeInOut,
        }); }).then(function () { return _this.overlayView.animate({
            translate: { x: 0, y: _this.pageHeight },
            duration: 0,
            curve: enums_1.AnimationCurve.easeInOut,
        }); }).then(function (data) {
            _this.hostView.style.translateY = _this.pageHeight;
            _this.close.emit(_this.data);
            _this.isShowing = false;
            return Promise.resolve(_this.data);
        }).catch(function () { return 0; });
    };
    ModalComponent.prototype.onLoad = function (_a) {
        var _this = this;
        var object = _a.object;
        this.overlayView = object;
        this.contentView.off([gestures_1.GestureTypes.touch, gestures_1.GestureTypes.tap].join(","));
        // Event Propagation
        if (application_1.ios) {
            targetHandler = HideGestureRecognizerImpl.initWithOwner(this.overlayView);
            if (this.dismissable) {
                targetHandler.func = function () { return _this.hide(); };
            }
            var gesture = UITapGestureRecognizer.alloc().initWithTargetAction(targetHandler, "tap");
            this.overlayView.ios.addGestureRecognizer(gesture);
            targetHandler2 = HideGestureRecognizerImpl.initWithOwner(this.bodyView);
            var gesture2 = UITapGestureRecognizer.alloc().initWithTargetAction(targetHandler2, "tap");
            gesture2.cancelsTouchesInView = true;
            this.bodyView.ios.addGestureRecognizer(gesture2);
        }
    };
    Object.defineProperty(ModalComponent.prototype, "timing", {
        get: function () {
            return +this.duration;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ModalComponent.prototype, "translateY", {
        get: function () {
            return this.pageHeight;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ModalComponent.prototype, "hostView", {
        get: function () {
            return this.hostEl.nativeElement;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ModalComponent.prototype, "bodyView", {
        get: function () {
            return this.bodyEl.nativeElement;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ModalComponent.prototype, "contentView", {
        get: function () {
            return this.contentEl.nativeElement;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ModalComponent.prototype, "modalWidth", {
        get: function () {
            switch (this.size) {
                case "sm": return "65%";
                case "lg": return "98%";
                case "md":
                default: return "85%";
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ModalComponent.prototype, "modalHeight", {
        get: function () {
            switch (this.size) {
                case "sm": return "50%";
                case "lg": return "98%";
                case "md":
                default: return "65%";
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ModalComponent.prototype, "vAlignment", {
        get: function () {
            if (includes(["center", "stretch", "middle", "top", "bottom"], this.alignment)) {
                return this.alignment;
            }
            return "center";
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], ModalComponent.prototype, "size", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], ModalComponent.prototype, "dismissable", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], ModalComponent.prototype, "alignment", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], ModalComponent.prototype, "duration", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], ModalComponent.prototype, "open", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], ModalComponent.prototype, "close", void 0);
    __decorate([
        core_1.ViewChild("bodyEl"),
        __metadata("design:type", core_1.ElementRef)
    ], ModalComponent.prototype, "bodyEl", void 0);
    __decorate([
        core_1.ViewChild("contentEl"),
        __metadata("design:type", core_1.ElementRef)
    ], ModalComponent.prototype, "contentEl", void 0);
    ModalComponent = __decorate([
        core_1.Component({
            selector: "modal, [modal]",
            template: "\n\t\t<GridLayout (loaded)=\"onLoad($event)\" (tap)=\"onTapHide()\" [translateY]=\"translateY\" opacity=\"0\" class=\"overlay\">\n\t\t\t<GridLayout #bodyEl [verticalAlignment]=\"vAlignment\" [width]=\"modalWidth\" [height]=\"modalHeight\" [translateY]=\"translateY\" scaleY=\".6\" scaleX=\".6\" opacity=\"0\"\n\t\t\tclass=\"overlay-body\">\n\t\t\t\t<StackLayout #contentEl class=\"overlay-content\">\n\t\t\t\t\t<ng-content></ng-content>\n\t\t\t\t</StackLayout>\n\t\t\t</GridLayout>\n\t\t</GridLayout>\n\t",
            styles: ["\n\t\t.overlay {\n\t\t\tbackground-color: rgba(0, 0, 0, 0.8);\n\t\t\tz-index: 999999;\n\t\t}\n\t\t.overlay .overlay-body { }\n\t\t.overlay .overlay-body .overlay-content {\n\t\t\tvertical-align: center;\n\t\t}\n\t\t.overlay .overlay-body >>> .close {\n\t\t\tcolor: red;\n\t\t\tfont-size: 16;\n\t\t}\n\t"]
        }),
        __metadata("design:paramtypes", [core_1.ElementRef,
            page_1.Page])
    ], ModalComponent);
    return ModalComponent;
}());
exports.ModalComponent = ModalComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXNHO0FBRXRHLHNEQUE4RDtBQUM5RCxtREFBMkQ7QUFDM0QseURBQTREO0FBQzVELGlEQUFnRDtBQUNoRCw0REFBcUg7QUFJckgscUNBQXFDO0FBQ3JDLFNBQVMsUUFBUSxDQUFDLFNBQVMsRUFBRSxLQUFLO0lBQzlCLElBQUksV0FBVyxHQUFHLEtBQUssQ0FBQztJQUN4QixJQUFJLEdBQUcsR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ25DLElBQUksR0FBRyxJQUFJLENBQUMsRUFBRTtRQUNWLFdBQVcsR0FBRyxJQUFJLENBQUM7S0FDdEI7SUFDRCxPQUFPLFdBQVcsQ0FBQztBQUN2QixDQUFDO0FBRUQsSUFBSSxpQkFBaUIsRUFBRSxPQUFPLENBQUM7QUFDL0IsSUFBSSxpQkFBRyxFQUFFO0lBQ0wsaUJBQWlCLEdBQUcsUUFBUSxDQUFDO0lBQzdCLE9BQU8sR0FBRyxPQUFPLENBQUM7Q0FDckI7S0FBTTtJQUNILGlCQUFpQjtRQUFHO1FBQVUsQ0FBQztRQUFELFFBQUM7SUFBRCxDQUFDLEFBQVgsR0FBVyxDQUFDO0lBQ2hDLE9BQU8sR0FBRyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxLQUFLLENBQUMsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLEVBQUUsRUFBRSxDQUFDO0NBQ3JEO0FBRUQ7SUFBd0MsNkNBQWlCO0lBQXpEOztJQW1CQSxDQUFDO0lBaEJVLHVDQUFhLEdBQXBCLFVBQXFCLEtBQUs7UUFDdEIsSUFBTSxPQUFPLEdBQUcsSUFBSSx5QkFBeUIsRUFBRSxDQUFDO1FBQ2hELE9BQU8sQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7SUFFRCx1Q0FBRyxHQUFIO1FBQ0ksSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUN2QyxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDWCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDZjtJQUNMLENBQUM7SUFFTSw0Q0FBa0IsR0FBRztRQUN4QixLQUFLLEVBQUUsRUFBRSxPQUFPLEVBQUUsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsRUFBRTtLQUNyRSxDQUFDO0lBQ04sZ0NBQUM7Q0FBQSxBQW5CRCxDQUF3QyxpQkFBaUIsR0FtQnhEO0FBRUQsK0JBQStCO0FBQy9CLElBQUksYUFBYSxHQUFHLElBQUksQ0FBQztBQUN6QixJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUM7QUErQjFCO0lBZUksd0JBQ1ksTUFBa0IsRUFDbEIsSUFBVTtRQUZ0QixpQkFpQkM7UUFoQlcsV0FBTSxHQUFOLE1BQU0sQ0FBWTtRQUNsQixTQUFJLEdBQUosSUFBSSxDQUFNO1FBaEJkLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFFM0Isa0JBQWEsR0FBVyxHQUFHLENBQUM7UUFFNUIsU0FBSSxHQUFRLElBQUksQ0FBQyxDQUFDLDBCQUEwQjtRQUNuQyxTQUFJLEdBQVcsSUFBSSxDQUFDLENBQUMsZUFBZTtRQUNwQyxnQkFBVyxHQUFZLEtBQUssQ0FBQztRQUM3QixjQUFTLEdBQVcsUUFBUSxDQUFDLENBQUMsMkNBQTJDO1FBQ3pFLGFBQVEsR0FBVyxHQUFHLENBQUMsQ0FBQyxrQkFBa0I7UUFDekMsU0FBSSxHQUFHLElBQUksbUJBQVksRUFBTyxDQUFDO1FBQy9CLFVBQUssR0FBRyxJQUFJLG1CQUFZLEVBQU8sQ0FBQztRQTZFbEQsY0FBUyxHQUFHO1lBQ1IsSUFBSSxvQkFBUyxJQUFJLEtBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQy9CLEtBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQzthQUNmO1FBQ0wsQ0FBQyxDQUFBO1FBekVHLG1CQUFtQjtRQUNuQiw2Q0FBNkM7UUFDN0MsbUhBQW1IO1FBQ25ILG9DQUFvQztRQUNwQyxzQ0FBc0M7UUFDdEMsK0JBQStCO1FBQy9CLGdCQUFnQjtRQUNoQixjQUFjO1FBQ2QsVUFBVTtRQUNWLCtDQUErQztRQUMvQyxvRUFBb0U7UUFDcEUsVUFBVTtRQUNWLElBQUk7SUFDUixDQUFDO0lBRUQsaUNBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsaUJBQU0sQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDO1FBQ25GLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ3JELENBQUM7SUFFRCw2QkFBSSxHQUFKLFVBQUssSUFBZ0I7UUFBckIsaUJBc0JDO1FBdEJJLHFCQUFBLEVBQUEsV0FBZ0I7UUFDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDbkIsT0FBTztTQUNWO1FBQ0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQztRQUNuQyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDO1lBQzVCLFNBQVMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxDQUFDO1NBQ3pDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDO1lBQ25DLE9BQU8sRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSSxDQUFDLGFBQWE7U0FDekQsQ0FBQyxFQUZZLENBRVosQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUM7WUFDakMsU0FBUyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFO1lBQ3pCLFFBQVEsRUFBRSxDQUFDO1lBQ1gsS0FBSyxFQUFFLHNCQUFjLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQztTQUN2RCxDQUFDLEVBSmEsQ0FJYixDQUFDLENBQUMsSUFBSSxDQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQztZQUNqQyxLQUFLLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUU7WUFDckIsT0FBTyxFQUFFLENBQUM7WUFDVixRQUFRLEVBQUUsS0FBSSxDQUFDLE1BQU07WUFDckIsS0FBSyxFQUFFLHNCQUFjLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQztTQUN2RCxDQUFDLEVBTGEsQ0FLYixDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ0wsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsQ0FBQztZQUNqQyxLQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsY0FBTSxPQUFBLENBQUMsRUFBRCxDQUFDLENBQUMsQ0FBQztJQUN0QixDQUFDO0lBRUQsNkJBQUksR0FBSjtRQUFBLGlCQXVCQztRQXRCRyxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDO1lBQ3pCLE9BQU8sRUFBRSxDQUFDO1lBQ1YsUUFBUSxFQUFFLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGFBQWE7WUFDMUMsS0FBSyxFQUFFLHNCQUFjLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQztTQUN2RCxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQztZQUNoQyxLQUFLLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUU7WUFDdkIsU0FBUyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsS0FBSSxDQUFDLFVBQVUsRUFBRTtZQUN2QyxRQUFRLEVBQUUsQ0FBQztZQUNYLEtBQUssRUFBRSxzQkFBYyxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUM7U0FDdkQsQ0FBQyxFQUxZLENBS1osQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUM7WUFDcEMsT0FBTyxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFJLENBQUMsYUFBYTtZQUN0RCxLQUFLLEVBQUUsc0JBQWMsQ0FBQyxTQUFTO1NBQ2xDLENBQUMsRUFIYSxDQUdiLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDO1lBQ3BDLFNBQVMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEtBQUksQ0FBQyxVQUFVLEVBQUU7WUFDdkMsUUFBUSxFQUFFLENBQUM7WUFDWCxLQUFLLEVBQUUsc0JBQWMsQ0FBQyxTQUFTO1NBQ2xDLENBQUMsRUFKYSxDQUliLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJO1lBQ1QsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLEtBQUksQ0FBQyxVQUFVLENBQUM7WUFDakQsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNCLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1lBQ3ZCLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLGNBQU0sT0FBQSxDQUFDLEVBQUQsQ0FBQyxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQVFELCtCQUFNLEdBQU4sVUFBTyxFQUFVO1FBQWpCLGlCQW1CQztZQW5CUSxrQkFBTTtRQUNYLElBQUksQ0FBQyxXQUFXLEdBQVMsTUFBTSxDQUFDO1FBRWhDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsdUJBQVksQ0FBQyxLQUFLLEVBQUUsdUJBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUV2RSxvQkFBb0I7UUFDcEIsSUFBSSxpQkFBRyxFQUFFO1lBQ0wsYUFBYSxHQUFHLHlCQUF5QixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDMUUsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNsQixhQUFhLENBQUMsSUFBSSxHQUFHLGNBQU0sT0FBQSxLQUFJLENBQUMsSUFBSSxFQUFFLEVBQVgsQ0FBVyxDQUFDO2FBQzFDO1lBQ0QsSUFBTSxPQUFPLEdBQUcsc0JBQXNCLENBQUMsS0FBSyxFQUFFLENBQUMsb0JBQW9CLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQzFGLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBRW5ELGNBQWMsR0FBRyx5QkFBeUIsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3hFLElBQU0sUUFBUSxHQUFHLHNCQUFzQixDQUFDLEtBQUssRUFBRSxDQUFDLG9CQUFvQixDQUFDLGNBQWMsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUM1RixRQUFRLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO1lBQ3JDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3BEO0lBQ0wsQ0FBQztJQUVELHNCQUFZLGtDQUFNO2FBQWxCO1lBQ0ksT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDMUIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBVyxzQ0FBVTthQUFyQjtZQUNJLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUMzQixDQUFDOzs7T0FBQTtJQUVELHNCQUFZLG9DQUFRO2FBQXBCO1lBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQztRQUNyQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFZLG9DQUFRO2FBQXBCO1lBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQztRQUNyQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFZLHVDQUFXO2FBQXZCO1lBQ0ksT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQztRQUN4QyxDQUFDOzs7T0FBQTtJQUVELHNCQUFXLHNDQUFVO2FBQXJCO1lBQ0ksUUFBUSxJQUFJLENBQUMsSUFBSSxFQUFFO2dCQUNmLEtBQUssSUFBSSxDQUFDLENBQUMsT0FBTyxLQUFLLENBQUM7Z0JBQ3hCLEtBQUssSUFBSSxDQUFDLENBQUMsT0FBTyxLQUFLLENBQUM7Z0JBQ3hCLEtBQUssSUFBSSxDQUFDO2dCQUNWLE9BQU8sQ0FBQyxDQUFDLE9BQU8sS0FBSyxDQUFDO2FBQ3pCO1FBQ0wsQ0FBQzs7O09BQUE7SUFFRCxzQkFBVyx1Q0FBVzthQUF0QjtZQUNJLFFBQVEsSUFBSSxDQUFDLElBQUksRUFBRTtnQkFDZixLQUFLLElBQUksQ0FBQyxDQUFDLE9BQU8sS0FBSyxDQUFDO2dCQUN4QixLQUFLLElBQUksQ0FBQyxDQUFDLE9BQU8sS0FBSyxDQUFDO2dCQUN4QixLQUFLLElBQUksQ0FBQztnQkFDVixPQUFPLENBQUMsQ0FBQyxPQUFPLEtBQUssQ0FBQzthQUN6QjtRQUNMLENBQUM7OztPQUFBO0lBRUQsc0JBQVcsc0NBQVU7YUFBckI7WUFDSSxJQUFJLFFBQVEsQ0FBQyxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxRQUFRLENBQUMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUU7Z0JBQzVFLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQzthQUN6QjtZQUNELE9BQU8sUUFBUSxDQUFDO1FBQ3BCLENBQUM7OztPQUFBO0lBeEpRO1FBQVIsWUFBSyxFQUFFOztnREFBNkI7SUFDNUI7UUFBUixZQUFLLEVBQUU7O3VEQUFzQztJQUNyQztRQUFSLFlBQUssRUFBRTs7cURBQXNDO0lBQ3JDO1FBQVIsWUFBSyxFQUFFOztvREFBZ0M7SUFDOUI7UUFBVCxhQUFNLEVBQUU7O2dEQUF3QztJQUN2QztRQUFULGFBQU0sRUFBRTs7aURBQXlDO0lBQzdCO1FBQXBCLGdCQUFTLENBQUMsUUFBUSxDQUFDO2tDQUFpQixpQkFBVTtrREFBQztJQUN4QjtRQUF2QixnQkFBUyxDQUFDLFdBQVcsQ0FBQztrQ0FBb0IsaUJBQVU7cURBQUM7SUFiN0MsY0FBYztRQTVCMUIsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxnQkFBZ0I7WUFDMUIsUUFBUSxFQUFFLDBmQVNaO1lBQ0UsTUFBTSxFQUFFLENBQUMsZ1RBYVgsQ0FBQztTQUNGLENBQUM7eUNBa0JzQixpQkFBVTtZQUNaLFdBQUk7T0FqQmIsY0FBYyxDQStKMUI7SUFBRCxxQkFBQztDQUFBLEFBL0pELElBK0pDO0FBL0pZLHdDQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYsIElucHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgVmlldyB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2NvcmUvdmlld1wiO1xyXG5pbXBvcnQgeyBzY3JlZW4sIGlzQW5kcm9pZCB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3BsYXRmb3JtXCI7XHJcbmltcG9ydCB7IEFuaW1hdGlvbkN1cnZlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZW51bXNcIjtcclxuaW1wb3J0IHsgR2VzdHVyZVR5cGVzIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZ2VzdHVyZXNcIjtcclxuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2VcIjtcclxuaW1wb3J0IHsgYW5kcm9pZCwgQW5kcm9pZEFwcGxpY2F0aW9uLCBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSwgaW9zIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb25cIjtcclxuXHJcbmRlY2xhcmUgY29uc3QgVUlUYXBHZXN0dXJlUmVjb2duaXplciwgaW50ZXJvcCwgTlNPYmplY3Q7XHJcblxyXG4vLyBObyBzdXBwb3J0IGZvciBBcnJheSNpbmNsdWRlcyBoZXJlXHJcbmZ1bmN0aW9uIGluY2x1ZGVzKGNvbnRhaW5lciwgdmFsdWUpIHtcclxuICAgIHZhciByZXR1cm5WYWx1ZSA9IGZhbHNlO1xyXG4gICAgdmFyIHBvcyA9IGNvbnRhaW5lci5pbmRleE9mKHZhbHVlKTtcclxuICAgIGlmIChwb3MgPj0gMCkge1xyXG4gICAgICAgIHJldHVyblZhbHVlID0gdHJ1ZTtcclxuICAgIH1cclxuICAgIHJldHVybiByZXR1cm5WYWx1ZTtcclxufVxyXG5cclxubGV0IEdlc3R1cmVSZWNvZ25pemVyLCBJbnRlcm9wO1xyXG5pZiAoaW9zKSB7XHJcbiAgICBHZXN0dXJlUmVjb2duaXplciA9IE5TT2JqZWN0O1xyXG4gICAgSW50ZXJvcCA9IGludGVyb3A7XHJcbn0gZWxzZSB7XHJcbiAgICBHZXN0dXJlUmVjb2duaXplciA9IGNsYXNzIEEgeyB9O1xyXG4gICAgSW50ZXJvcCA9IHsgdHlwZXM6IHsgaWQ6IHZvaWQgMCwgdm9pZDogdm9pZCAwIH0gfTtcclxufVxyXG5cclxuY2xhc3MgSGlkZUdlc3R1cmVSZWNvZ25pemVySW1wbCBleHRlbmRzIEdlc3R1cmVSZWNvZ25pemVyIHtcclxuICAgIHB1YmxpYyBmdW5jOiAoKSA9PiB2b2lkO1xyXG5cclxuICAgIHN0YXRpYyBpbml0V2l0aE93bmVyKG93bmVyKSB7XHJcbiAgICAgICAgY29uc3QgaGFuZGxlciA9IG5ldyBIaWRlR2VzdHVyZVJlY29nbml6ZXJJbXBsKCk7XHJcbiAgICAgICAgaGFuZGxlci5fb3duZXIgPSBvd25lcjtcclxuICAgICAgICByZXR1cm4gaGFuZGxlcjtcclxuICAgIH1cclxuXHJcbiAgICB0YXAoKSB7XHJcbiAgICAgICAgdGhpcy5fb3duZXIuaW9zLnJlc2lnbkZpcnN0UmVzcG9uZGVyKCk7XHJcbiAgICAgICAgaWYgKHRoaXMuZnVuYykge1xyXG4gICAgICAgICAgICB0aGlzLmZ1bmMoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIE9iakNFeHBvc2VkTWV0aG9kcyA9IHtcclxuICAgICAgICBcInRhcFwiOiB7IHJldHVybnM6IEludGVyb3AudHlwZXMudm9pZCwgcGFyYW1zOiBbSW50ZXJvcC50eXBlcy5pZF0gfVxyXG4gICAgfTtcclxufVxyXG5cclxuLy8gS2VlcCBleHRlcm5hbCBzdGF0ZSBvZiB2aWV3c1xyXG5sZXQgdGFyZ2V0SGFuZGxlciA9IG51bGw7XHJcbmxldCB0YXJnZXRIYW5kbGVyMiA9IG51bGw7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogXCJtb2RhbCwgW21vZGFsXVwiLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuXHRcdDxHcmlkTGF5b3V0IChsb2FkZWQpPVwib25Mb2FkKCRldmVudClcIiAodGFwKT1cIm9uVGFwSGlkZSgpXCIgW3RyYW5zbGF0ZVldPVwidHJhbnNsYXRlWVwiIG9wYWNpdHk9XCIwXCIgY2xhc3M9XCJvdmVybGF5XCI+XHJcblx0XHRcdDxHcmlkTGF5b3V0ICNib2R5RWwgW3ZlcnRpY2FsQWxpZ25tZW50XT1cInZBbGlnbm1lbnRcIiBbd2lkdGhdPVwibW9kYWxXaWR0aFwiIFtoZWlnaHRdPVwibW9kYWxIZWlnaHRcIiBbdHJhbnNsYXRlWV09XCJ0cmFuc2xhdGVZXCIgc2NhbGVZPVwiLjZcIiBzY2FsZVg9XCIuNlwiIG9wYWNpdHk9XCIwXCJcclxuXHRcdFx0Y2xhc3M9XCJvdmVybGF5LWJvZHlcIj5cclxuXHRcdFx0XHQ8U3RhY2tMYXlvdXQgI2NvbnRlbnRFbCBjbGFzcz1cIm92ZXJsYXktY29udGVudFwiPlxyXG5cdFx0XHRcdFx0PG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxyXG5cdFx0XHRcdDwvU3RhY2tMYXlvdXQ+XHJcblx0XHRcdDwvR3JpZExheW91dD5cclxuXHRcdDwvR3JpZExheW91dD5cclxuXHRgLFxyXG4gICAgc3R5bGVzOiBbYFxyXG5cdFx0Lm92ZXJsYXkge1xyXG5cdFx0XHRiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuOCk7XHJcblx0XHRcdHotaW5kZXg6IDk5OTk5OTtcclxuXHRcdH1cclxuXHRcdC5vdmVybGF5IC5vdmVybGF5LWJvZHkgeyB9XHJcblx0XHQub3ZlcmxheSAub3ZlcmxheS1ib2R5IC5vdmVybGF5LWNvbnRlbnQge1xyXG5cdFx0XHR2ZXJ0aWNhbC1hbGlnbjogY2VudGVyO1xyXG5cdFx0fVxyXG5cdFx0Lm92ZXJsYXkgLm92ZXJsYXktYm9keSA+Pj4gLmNsb3NlIHtcclxuXHRcdFx0Y29sb3I6IHJlZDtcclxuXHRcdFx0Zm9udC1zaXplOiAxNjtcclxuXHRcdH1cclxuXHRgXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIE1vZGFsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIHByaXZhdGUgaXNTaG93aW5nOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBwcml2YXRlIHBhZ2VIZWlnaHQ6IG51bWJlcjtcclxuICAgIHByaXZhdGUgZHVyYXRpb25TY2FsZTogbnVtYmVyID0gLjc1O1xyXG4gICAgcHJpdmF0ZSBvdmVybGF5VmlldzogVmlldztcclxuICAgIHByaXZhdGUgZGF0YTogYW55ID0gbnVsbDsgLy8gT3B0aW9uYWwgZGF0YSBwYXJhbWV0ZXJcclxuICAgIEBJbnB1dCgpIHByaXZhdGUgc2l6ZTogc3RyaW5nID0gXCJzbVwiOyAvLyBzbSB8IG1kIHwgbGdcclxuICAgIEBJbnB1dCgpIHByaXZhdGUgZGlzbWlzc2FibGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIEBJbnB1dCgpIHByaXZhdGUgYWxpZ25tZW50OiBzdHJpbmcgPSBcImNlbnRlclwiOyAvLyBjZW50ZXIgfCBzdHJldGNoIHwgbWlkZGxlIHwgdG9wIHwgYm90dG9tXHJcbiAgICBASW5wdXQoKSBwcml2YXRlIGR1cmF0aW9uOiBudW1iZXIgPSAyNTA7IC8vIGluIG1pbGxpc2Vjb25kc1xyXG4gICAgQE91dHB1dCgpIHByaXZhdGUgb3BlbiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHByaXZhdGUgY2xvc2UgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBWaWV3Q2hpbGQoXCJib2R5RWxcIikgcHJpdmF0ZSBib2R5RWw6IEVsZW1lbnRSZWY7XHJcbiAgICBAVmlld0NoaWxkKFwiY29udGVudEVsXCIpIHByaXZhdGUgY29udGVudEVsOiBFbGVtZW50UmVmO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgaG9zdEVsOiBFbGVtZW50UmVmLFxyXG4gICAgICAgIHByaXZhdGUgcGFnZTogUGFnZVxyXG4gICAgKSB7XHJcbiAgICAgICAgLy8gaWYgKGlzQW5kcm9pZCkge1xyXG4gICAgICAgIC8vICAgICB0aGlzLnBhZ2Uub24oUGFnZS5sb2FkZWRFdmVudCwgKCkgPT4ge1xyXG4gICAgICAgIC8vICAgICAgICAgYW5kcm9pZC5vbihBbmRyb2lkQXBwbGljYXRpb24uYWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50LCAoYXJnczogQW5kcm9pZEFjdGl2aXR5QmFja1ByZXNzZWRFdmVudERhdGEpID0+IHtcclxuICAgICAgICAvLyAgICAgICAgICAgICBpZiAodGhpcy5pc1Nob3dpbmcpIHtcclxuICAgICAgICAvLyAgICAgICAgICAgICAgICAgYXJncy5jYW5jZWwgPSB0cnVlO1xyXG4gICAgICAgIC8vICAgICAgICAgICAgICAgICB0aGlzLmhpZGUoKTtcclxuICAgICAgICAvLyAgICAgICAgICAgICB9XHJcbiAgICAgICAgLy8gICAgICAgICB9KTtcclxuICAgICAgICAvLyAgICAgfSk7XHJcbiAgICAgICAgLy8gICAgIHRoaXMucGFnZS5vbihQYWdlLnVubG9hZGVkRXZlbnQsICgpID0+IHtcclxuICAgICAgICAvLyAgICAgICAgIGFuZHJvaWQub2ZmKEFuZHJvaWRBcHBsaWNhdGlvbi5hY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnQpO1xyXG4gICAgICAgIC8vICAgICB9KTtcclxuICAgICAgICAvLyB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5wYWdlSGVpZ2h0ID0gdGhpcy5wYWdlSGVpZ2h0ID8gdGhpcy5wYWdlSGVpZ2h0IDogc2NyZWVuLm1haW5TY3JlZW4uaGVpZ2h0RElQcztcclxuICAgICAgICB0aGlzLmhvc3RWaWV3LnN0eWxlLnRyYW5zbGF0ZVkgPSB0aGlzLnBhZ2VIZWlnaHQ7XHJcbiAgICB9XHJcblxyXG4gICAgc2hvdyhkYXRhOiBhbnkgPSBudWxsKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLm92ZXJsYXlWaWV3KSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5ob3N0Vmlldy5zdHlsZS50cmFuc2xhdGVZID0gMDtcclxuICAgICAgICByZXR1cm4gdGhpcy5vdmVybGF5Vmlldy5hbmltYXRlKHtcclxuICAgICAgICAgICAgdHJhbnNsYXRlOiB7IHg6IDAsIHk6IDAgfSwgZHVyYXRpb246IDAsXHJcbiAgICAgICAgfSkudGhlbigoKSA9PiB0aGlzLm92ZXJsYXlWaWV3LmFuaW1hdGUoe1xyXG4gICAgICAgICAgICBvcGFjaXR5OiAxLCBkdXJhdGlvbjogdGhpcy50aW1pbmcgKiB0aGlzLmR1cmF0aW9uU2NhbGUsXHJcbiAgICAgICAgfSkpLnRoZW4oKCkgPT4gdGhpcy5ib2R5Vmlldy5hbmltYXRlKHtcclxuICAgICAgICAgICAgdHJhbnNsYXRlOiB7IHg6IDAsIHk6IDAgfSxcclxuICAgICAgICAgICAgZHVyYXRpb246IDAsXHJcbiAgICAgICAgICAgIGN1cnZlOiBBbmltYXRpb25DdXJ2ZS5jdWJpY0JlemllciguMTIsIC4zLCAuNTgsIC40NCksXHJcbiAgICAgICAgfSkpLnRoZW4oKCkgPT4gdGhpcy5ib2R5Vmlldy5hbmltYXRlKHtcclxuICAgICAgICAgICAgc2NhbGU6IHsgeDogMSwgeTogMSB9LFxyXG4gICAgICAgICAgICBvcGFjaXR5OiAxLFxyXG4gICAgICAgICAgICBkdXJhdGlvbjogdGhpcy50aW1pbmcsXHJcbiAgICAgICAgICAgIGN1cnZlOiBBbmltYXRpb25DdXJ2ZS5jdWJpY0JlemllciguMTIsIC4zLCAuNTgsIC40NCksXHJcbiAgICAgICAgfSkpLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm9wZW4uZW1pdCh0aGlzLmRhdGEgPSBkYXRhKTtcclxuICAgICAgICAgICAgdGhpcy5pc1Nob3dpbmcgPSB0cnVlO1xyXG4gICAgICAgIH0pLmNhdGNoKCgpID0+IDApO1xyXG4gICAgfVxyXG5cclxuICAgIGhpZGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYm9keVZpZXcuYW5pbWF0ZSh7XHJcbiAgICAgICAgICAgIG9wYWNpdHk6IDAsXHJcbiAgICAgICAgICAgIGR1cmF0aW9uOiB0aGlzLnRpbWluZyAqIHRoaXMuZHVyYXRpb25TY2FsZSxcclxuICAgICAgICAgICAgY3VydmU6IEFuaW1hdGlvbkN1cnZlLmN1YmljQmV6aWVyKC4xMiwgLjMsIC41OCwgLjQ0KSxcclxuICAgICAgICB9KS50aGVuKCgpID0+IHRoaXMuYm9keVZpZXcuYW5pbWF0ZSh7XHJcbiAgICAgICAgICAgIHNjYWxlOiB7IHg6IC42LCB5OiAuNiB9LFxyXG4gICAgICAgICAgICB0cmFuc2xhdGU6IHsgeDogMCwgeTogdGhpcy5wYWdlSGVpZ2h0IH0sXHJcbiAgICAgICAgICAgIGR1cmF0aW9uOiAwLFxyXG4gICAgICAgICAgICBjdXJ2ZTogQW5pbWF0aW9uQ3VydmUuY3ViaWNCZXppZXIoLjEyLCAuMywgLjU4LCAuNDQpLFxyXG4gICAgICAgIH0pKS50aGVuKCgpID0+IHRoaXMub3ZlcmxheVZpZXcuYW5pbWF0ZSh7XHJcbiAgICAgICAgICAgIG9wYWNpdHk6IDAsIGR1cmF0aW9uOiB0aGlzLnRpbWluZyAqIHRoaXMuZHVyYXRpb25TY2FsZSxcclxuICAgICAgICAgICAgY3VydmU6IEFuaW1hdGlvbkN1cnZlLmVhc2VJbk91dCxcclxuICAgICAgICB9KSkudGhlbigoKSA9PiB0aGlzLm92ZXJsYXlWaWV3LmFuaW1hdGUoe1xyXG4gICAgICAgICAgICB0cmFuc2xhdGU6IHsgeDogMCwgeTogdGhpcy5wYWdlSGVpZ2h0IH0sXHJcbiAgICAgICAgICAgIGR1cmF0aW9uOiAwLFxyXG4gICAgICAgICAgICBjdXJ2ZTogQW5pbWF0aW9uQ3VydmUuZWFzZUluT3V0LFxyXG4gICAgICAgIH0pKS50aGVuKGRhdGEgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmhvc3RWaWV3LnN0eWxlLnRyYW5zbGF0ZVkgPSB0aGlzLnBhZ2VIZWlnaHQ7XHJcbiAgICAgICAgICAgIHRoaXMuY2xvc2UuZW1pdCh0aGlzLmRhdGEpO1xyXG4gICAgICAgICAgICB0aGlzLmlzU2hvd2luZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHRoaXMuZGF0YSk7XHJcbiAgICAgICAgfSkuY2F0Y2goKCkgPT4gMCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25UYXBIaWRlID0gKCkgPT4ge1xyXG4gICAgICAgIGlmIChpc0FuZHJvaWQgJiYgdGhpcy5kaXNtaXNzYWJsZSkge1xyXG4gICAgICAgICAgICB0aGlzLmhpZGUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25Mb2FkKHsgb2JqZWN0IH0pIHtcclxuICAgICAgICB0aGlzLm92ZXJsYXlWaWV3ID0gPFZpZXc+b2JqZWN0O1xyXG5cclxuICAgICAgICB0aGlzLmNvbnRlbnRWaWV3Lm9mZihbR2VzdHVyZVR5cGVzLnRvdWNoLCBHZXN0dXJlVHlwZXMudGFwXS5qb2luKFwiLFwiKSk7XHJcblxyXG4gICAgICAgIC8vIEV2ZW50IFByb3BhZ2F0aW9uXHJcbiAgICAgICAgaWYgKGlvcykge1xyXG4gICAgICAgICAgICB0YXJnZXRIYW5kbGVyID0gSGlkZUdlc3R1cmVSZWNvZ25pemVySW1wbC5pbml0V2l0aE93bmVyKHRoaXMub3ZlcmxheVZpZXcpO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kaXNtaXNzYWJsZSkge1xyXG4gICAgICAgICAgICAgICAgdGFyZ2V0SGFuZGxlci5mdW5jID0gKCkgPT4gdGhpcy5oaWRlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY29uc3QgZ2VzdHVyZSA9IFVJVGFwR2VzdHVyZVJlY29nbml6ZXIuYWxsb2MoKS5pbml0V2l0aFRhcmdldEFjdGlvbih0YXJnZXRIYW5kbGVyLCBcInRhcFwiKTtcclxuICAgICAgICAgICAgdGhpcy5vdmVybGF5Vmlldy5pb3MuYWRkR2VzdHVyZVJlY29nbml6ZXIoZ2VzdHVyZSk7XHJcblxyXG4gICAgICAgICAgICB0YXJnZXRIYW5kbGVyMiA9IEhpZGVHZXN0dXJlUmVjb2duaXplckltcGwuaW5pdFdpdGhPd25lcih0aGlzLmJvZHlWaWV3KTtcclxuICAgICAgICAgICAgY29uc3QgZ2VzdHVyZTIgPSBVSVRhcEdlc3R1cmVSZWNvZ25pemVyLmFsbG9jKCkuaW5pdFdpdGhUYXJnZXRBY3Rpb24odGFyZ2V0SGFuZGxlcjIsIFwidGFwXCIpO1xyXG4gICAgICAgICAgICBnZXN0dXJlMi5jYW5jZWxzVG91Y2hlc0luVmlldyA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuYm9keVZpZXcuaW9zLmFkZEdlc3R1cmVSZWNvZ25pemVyKGdlc3R1cmUyKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXQgdGltaW5nKCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuICt0aGlzLmR1cmF0aW9uO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXQgdHJhbnNsYXRlWSgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnBhZ2VIZWlnaHQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXQgaG9zdFZpZXcoKTogVmlldyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaG9zdEVsLm5hdGl2ZUVsZW1lbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXQgYm9keVZpZXcoKTogVmlldyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYm9keUVsLm5hdGl2ZUVsZW1lbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXQgY29udGVudFZpZXcoKTogVmlldyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY29udGVudEVsLm5hdGl2ZUVsZW1lbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldCBtb2RhbFdpZHRoKCk6IHN0cmluZyB7XHJcbiAgICAgICAgc3dpdGNoICh0aGlzLnNpemUpIHtcclxuICAgICAgICAgICAgY2FzZSBcInNtXCI6IHJldHVybiBcIjY1JVwiO1xyXG4gICAgICAgICAgICBjYXNlIFwibGdcIjogcmV0dXJuIFwiOTglXCI7XHJcbiAgICAgICAgICAgIGNhc2UgXCJtZFwiOlxyXG4gICAgICAgICAgICBkZWZhdWx0OiByZXR1cm4gXCI4NSVcIjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldCBtb2RhbEhlaWdodCgpOiBzdHJpbmcge1xyXG4gICAgICAgIHN3aXRjaCAodGhpcy5zaXplKSB7XHJcbiAgICAgICAgICAgIGNhc2UgXCJzbVwiOiByZXR1cm4gXCI1MCVcIjtcclxuICAgICAgICAgICAgY2FzZSBcImxnXCI6IHJldHVybiBcIjk4JVwiO1xyXG4gICAgICAgICAgICBjYXNlIFwibWRcIjpcclxuICAgICAgICAgICAgZGVmYXVsdDogcmV0dXJuIFwiNjUlXCI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXQgdkFsaWdubWVudCgpOiBzdHJpbmcge1xyXG4gICAgICAgIGlmIChpbmNsdWRlcyhbXCJjZW50ZXJcIiwgXCJzdHJldGNoXCIsIFwibWlkZGxlXCIsIFwidG9wXCIsIFwiYm90dG9tXCJdLCB0aGlzLmFsaWdubWVudCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuYWxpZ25tZW50O1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gXCJjZW50ZXJcIjtcclxuICAgIH1cclxufSJdfQ==
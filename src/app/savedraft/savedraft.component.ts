import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { UserService } from "../services/user.service";

@Component({
  selector: "ns-savedraft",
  moduleId: module.id,
  templateUrl: "./savedraft.component.html",
  styleUrls: ["./savedraft.component.css"]
})
export class SaveDraftComponent implements OnInit {
  routerExtensions: RouterExtensions;

  constructor(
    routerExtensions: RouterExtensions,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDotState(false);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
    this.userService.switchState(false);
  }
  ngOnInit(): void {}

  public onTakePictures() {
    this.routerExtensions.navigate(["/pictures"]);
  }
}

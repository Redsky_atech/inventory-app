import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { DropDownModule } from "nativescript-drop-down/angular";
import { SelectedIndexChangedEventData } from "nativescript-drop-down";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { UserService } from "../services/user.service";

@Component({
  selector: "ns-weightDimension",
  moduleId: module.id,
  templateUrl: "./weight-dimension.component.html",
  styleUrls: ["./weight-dimension.component.css"]
})
export class WeightDimensionComponent implements OnInit {
  @ViewChild("dd") dropDown: ElementRef;

  routerExtensions: RouterExtensions;
  public packageType = [
    "Dell",
    "Samsung",
    "Apple",
    "Hp",
    "Nokia",
    "Redmi",
    "Motorola",
    "Linovo",
    "Panasonic"
  ];
  public dimension = [
    "01",
    "02",
    "03",
    "04",
    "05",
    "06",
    "07",
    "08",
    "09",
    "10"
  ];
  public customWeight = ["10", "20", "30", "40", "50", "60", "70", "80"];
  public weight = ["10", "20", "30", "40", "50", "60", "70", "80", "90", "100"];

  feature1 = "1 TB storage";
  feature2 = "Metal body";
  feature3 = "500 MB SSD";
  constructor(
    routerExtensions: RouterExtensions,
    private dropDownmodule: DropDownModule,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDots(11);
    this.userService.bottomBarDotState(true);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
    this.userService.switchState(false);
  }

  ngOnInit(): void {}

  // public onchange(args: SelectedIndexChangedEventData) {
  //   console.log(
  //     `Drop Down selected index changed from ${args.oldIndex} to ${
  //       args.newIndex
  //     }`
  //   );
  // }

  // public onopen() {
  //   console.log("Drop Down opened.");
  // }

  // public onclose() {
  //   console.log("Drop Down closed.");
  // }

  onSwipe(args: SwipeGestureEventData) {
    if (args.direction == 1) {
      this.routerExtensions.navigate(["/businesspolicies"]);
    }
    if (args.direction == 2) {
      this.routerExtensions.navigate(["/savedraft"]);
    }
  }
}

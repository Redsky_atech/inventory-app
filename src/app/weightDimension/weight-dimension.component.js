"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var angular_1 = require("nativescript-drop-down/angular");
var user_service_1 = require("../services/user.service");
var WeightDimensionComponent = /** @class */ (function () {
    function WeightDimensionComponent(routerExtensions, dropDownmodule, userService) {
        this.dropDownmodule = dropDownmodule;
        this.userService = userService;
        this.packageType = [
            "Dell",
            "Samsung",
            "Apple",
            "Hp",
            "Nokia",
            "Redmi",
            "Motorola",
            "Linovo",
            "Panasonic"
        ];
        this.dimension = [
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10"
        ];
        this.customWeight = ["10", "20", "30", "40", "50", "60", "70", "80"];
        this.weight = ["10", "20", "30", "40", "50", "60", "70", "80", "90", "100"];
        this.feature1 = "1 TB storage";
        this.feature2 = "Metal body";
        this.feature3 = "500 MB SSD";
        this.routerExtensions = routerExtensions;
        this.userService.actionBarState(true);
        this.userService.bottomBarDots(11);
        this.userService.bottomBarDotState(true);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.userService.switchState(false);
    }
    WeightDimensionComponent.prototype.ngOnInit = function () { };
    // public onchange(args: SelectedIndexChangedEventData) {
    //   console.log(
    //     `Drop Down selected index changed from ${args.oldIndex} to ${
    //       args.newIndex
    //     }`
    //   );
    // }
    // public onopen() {
    //   console.log("Drop Down opened.");
    // }
    // public onclose() {
    //   console.log("Drop Down closed.");
    // }
    WeightDimensionComponent.prototype.onSwipe = function (args) {
        if (args.direction == 1) {
            this.routerExtensions.navigate(["/businesspolicies"]);
        }
        if (args.direction == 2) {
            this.routerExtensions.navigate(["/savedraft"]);
        }
    };
    __decorate([
        core_1.ViewChild("dd"),
        __metadata("design:type", core_1.ElementRef)
    ], WeightDimensionComponent.prototype, "dropDown", void 0);
    WeightDimensionComponent = __decorate([
        core_1.Component({
            selector: "ns-weightDimension",
            moduleId: module.id,
            templateUrl: "./weight-dimension.component.html",
            styleUrls: ["./weight-dimension.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            angular_1.DropDownModule,
            user_service_1.UserService])
    ], WeightDimensionComponent);
    return WeightDimensionComponent;
}());
exports.WeightDimensionComponent = WeightDimensionComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2VpZ2h0LWRpbWVuc2lvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ3ZWlnaHQtZGltZW5zaW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF5RTtBQUN6RSxzREFBK0Q7QUFDL0QsMERBQWdFO0FBR2hFLHlEQUF1RDtBQVF2RDtJQWlDRSxrQ0FDRSxnQkFBa0MsRUFDMUIsY0FBOEIsRUFDOUIsV0FBd0I7UUFEeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBaEMzQixnQkFBVyxHQUFHO1lBQ25CLE1BQU07WUFDTixTQUFTO1lBQ1QsT0FBTztZQUNQLElBQUk7WUFDSixPQUFPO1lBQ1AsT0FBTztZQUNQLFVBQVU7WUFDVixRQUFRO1lBQ1IsV0FBVztTQUNaLENBQUM7UUFDSyxjQUFTLEdBQUc7WUFDakIsSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtTQUNMLENBQUM7UUFDSyxpQkFBWSxHQUFHLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2hFLFdBQU0sR0FBRyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBRTlFLGFBQVEsR0FBRyxjQUFjLENBQUM7UUFDMUIsYUFBUSxHQUFHLFlBQVksQ0FBQztRQUN4QixhQUFRLEdBQUcsWUFBWSxDQUFDO1FBTXRCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVELDJDQUFRLEdBQVIsY0FBa0IsQ0FBQztJQUVuQix5REFBeUQ7SUFDekQsaUJBQWlCO0lBQ2pCLG9FQUFvRTtJQUNwRSxzQkFBc0I7SUFDdEIsU0FBUztJQUNULE9BQU87SUFDUCxJQUFJO0lBRUosb0JBQW9CO0lBQ3BCLHNDQUFzQztJQUN0QyxJQUFJO0lBRUoscUJBQXFCO0lBQ3JCLHNDQUFzQztJQUN0QyxJQUFJO0lBRUosMENBQU8sR0FBUCxVQUFRLElBQTJCO1FBQ2pDLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztTQUN2RDtRQUNELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7U0FDaEQ7SUFDSCxDQUFDO0lBdkVnQjtRQUFoQixnQkFBUyxDQUFDLElBQUksQ0FBQztrQ0FBVyxpQkFBVTs4REFBQztJQUQzQix3QkFBd0I7UUFOcEMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxvQkFBb0I7WUFDOUIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSxtQ0FBbUM7WUFDaEQsU0FBUyxFQUFFLENBQUMsa0NBQWtDLENBQUM7U0FDaEQsQ0FBQzt5Q0FtQ29CLHlCQUFnQjtZQUNWLHdCQUFjO1lBQ2pCLDBCQUFXO09BcEN2Qix3QkFBd0IsQ0F5RXBDO0lBQUQsK0JBQUM7Q0FBQSxBQXpFRCxJQXlFQztBQXpFWSw0REFBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBFbGVtZW50UmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgRHJvcERvd25Nb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWRyb3AtZG93bi9hbmd1bGFyXCI7XHJcbmltcG9ydCB7IFNlbGVjdGVkSW5kZXhDaGFuZ2VkRXZlbnREYXRhIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1kcm9wLWRvd25cIjtcclxuaW1wb3J0IHsgU3dpcGVHZXN0dXJlRXZlbnREYXRhIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZ2VzdHVyZXNcIjtcclxuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJucy13ZWlnaHREaW1lbnNpb25cIixcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vd2VpZ2h0LWRpbWVuc2lvbi5jb21wb25lbnQuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wiLi93ZWlnaHQtZGltZW5zaW9uLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIFdlaWdodERpbWVuc2lvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQFZpZXdDaGlsZChcImRkXCIpIGRyb3BEb3duOiBFbGVtZW50UmVmO1xyXG5cclxuICByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zO1xyXG4gIHB1YmxpYyBwYWNrYWdlVHlwZSA9IFtcclxuICAgIFwiRGVsbFwiLFxyXG4gICAgXCJTYW1zdW5nXCIsXHJcbiAgICBcIkFwcGxlXCIsXHJcbiAgICBcIkhwXCIsXHJcbiAgICBcIk5va2lhXCIsXHJcbiAgICBcIlJlZG1pXCIsXHJcbiAgICBcIk1vdG9yb2xhXCIsXHJcbiAgICBcIkxpbm92b1wiLFxyXG4gICAgXCJQYW5hc29uaWNcIlxyXG4gIF07XHJcbiAgcHVibGljIGRpbWVuc2lvbiA9IFtcclxuICAgIFwiMDFcIixcclxuICAgIFwiMDJcIixcclxuICAgIFwiMDNcIixcclxuICAgIFwiMDRcIixcclxuICAgIFwiMDVcIixcclxuICAgIFwiMDZcIixcclxuICAgIFwiMDdcIixcclxuICAgIFwiMDhcIixcclxuICAgIFwiMDlcIixcclxuICAgIFwiMTBcIlxyXG4gIF07XHJcbiAgcHVibGljIGN1c3RvbVdlaWdodCA9IFtcIjEwXCIsIFwiMjBcIiwgXCIzMFwiLCBcIjQwXCIsIFwiNTBcIiwgXCI2MFwiLCBcIjcwXCIsIFwiODBcIl07XHJcbiAgcHVibGljIHdlaWdodCA9IFtcIjEwXCIsIFwiMjBcIiwgXCIzMFwiLCBcIjQwXCIsIFwiNTBcIiwgXCI2MFwiLCBcIjcwXCIsIFwiODBcIiwgXCI5MFwiLCBcIjEwMFwiXTtcclxuXHJcbiAgZmVhdHVyZTEgPSBcIjEgVEIgc3RvcmFnZVwiO1xyXG4gIGZlYXR1cmUyID0gXCJNZXRhbCBib2R5XCI7XHJcbiAgZmVhdHVyZTMgPSBcIjUwMCBNQiBTU0RcIjtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsXHJcbiAgICBwcml2YXRlIGRyb3BEb3dubW9kdWxlOiBEcm9wRG93bk1vZHVsZSxcclxuICAgIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMgPSByb3V0ZXJFeHRlbnNpb25zO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5hY3Rpb25CYXJTdGF0ZSh0cnVlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYm90dG9tQmFyRG90cygxMSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdFN0YXRlKHRydWUpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZUNoYW5nZXMoMSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnN3aXRjaFN0YXRlKGZhbHNlKTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge31cclxuXHJcbiAgLy8gcHVibGljIG9uY2hhbmdlKGFyZ3M6IFNlbGVjdGVkSW5kZXhDaGFuZ2VkRXZlbnREYXRhKSB7XHJcbiAgLy8gICBjb25zb2xlLmxvZyhcclxuICAvLyAgICAgYERyb3AgRG93biBzZWxlY3RlZCBpbmRleCBjaGFuZ2VkIGZyb20gJHthcmdzLm9sZEluZGV4fSB0byAke1xyXG4gIC8vICAgICAgIGFyZ3MubmV3SW5kZXhcclxuICAvLyAgICAgfWBcclxuICAvLyAgICk7XHJcbiAgLy8gfVxyXG5cclxuICAvLyBwdWJsaWMgb25vcGVuKCkge1xyXG4gIC8vICAgY29uc29sZS5sb2coXCJEcm9wIERvd24gb3BlbmVkLlwiKTtcclxuICAvLyB9XHJcblxyXG4gIC8vIHB1YmxpYyBvbmNsb3NlKCkge1xyXG4gIC8vICAgY29uc29sZS5sb2coXCJEcm9wIERvd24gY2xvc2VkLlwiKTtcclxuICAvLyB9XHJcblxyXG4gIG9uU3dpcGUoYXJnczogU3dpcGVHZXN0dXJlRXZlbnREYXRhKSB7XHJcbiAgICBpZiAoYXJncy5kaXJlY3Rpb24gPT0gMSkge1xyXG4gICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2J1c2luZXNzcG9saWNpZXNcIl0pO1xyXG4gICAgfVxyXG4gICAgaWYgKGFyZ3MuZGlyZWN0aW9uID09IDIpIHtcclxuICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9zYXZlZHJhZnRcIl0pO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=
import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { BarcodeScanner } from "nativescript-barcodescanner";
import { RouterExtensions } from "nativescript-angular/router";
import { ModalComponent } from "../modals/modal.component";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { Router, ActivatedRoute } from "@angular/router";
import { Observable, EventData } from "tns-core-modules/data/observable";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { UserService } from "../services/user.service";

class ShipItems {
  title: string;
  quantity: string;
  location: string;

  constructor(title, quantity, location) {
    if (title != null) {
      this.title = title;
    }
    if (quantity != null) {
      this.quantity = quantity;
    }
    if (location != null) {
      this.location = location;
    }
  }
}

let itemTitle = [
  "Title 1",
  "Title 2",
  "Title 3",
  "Title 4",
  "Title 5",
  "Title 6",
  "Title 7",
  "Title 8",
  "Title 9",
  "Title 10"
];

let itemQuantity = ["5", "2", "2", "3", "5", "4", "7", "5", "8", "10"];

let itemLocation = [
  "bjsahfiuwehjdfshjdhskjfhsdkhfkdshfhsdfhfdhgdfhgjhfdhgkfdhg",
  "dkfjhfkahsdkjdshkjhdksj",
  "sdfjhkjadsfhsdfsdfsdfdsf",
  "sajdhkjadhdsfasdfasdfsdfsdfsdfsdfasdfasdfsadfassa",
  "sdhjdshfdfssdfsdf",
  "hgdfgjfgejfsdfsdfsdsdfasdfsafdsadf",
  "ewuyuweuybsdfsd",
  "weyewwedffdsfsdfdsfs",
  "wetufbsdjfdsfsdfdsfdsfsdfsdf",
  "hjsdgfgusyddsfsdfssdafasdfasdfafafasdfsafasdfasdfdsfdsfdsf"
];

@Component({
  selector: "shipItems",
  moduleId: module.id,
  templateUrl: "./shipItems.component.html",
  styleUrls: ["./shipItems.component.css"]
})
export class ShipItemsComponent implements OnInit, AfterViewInit {
  public items: Array<ShipItems>;

  constructor(
    private routerExtensions: RouterExtensions,
    private http: HttpClient,
    private userService: UserService
  ) {
    this.userService.actionBarState(true);
    this.userService.bottomBarDotState(false);
    this.userService.bottomBarDots(5);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
  }

  ngOnInit(): void {
    this.items = [];
    for (let i = 0; i < itemTitle.length; i++) {
      this.items.push(
        new ShipItems(itemTitle[i], itemQuantity[i], itemLocation[i])
      );
    }
    // for (let i = 0; i < itemQuantity.length; i++) {
    //   this.itemsQuantity.push(new ShipItems(itemQuantity[i]));
    // }
    // for (let i = 0; i < itemLocation.length; i++) {
    //   this.itemsLocation.push(new ShipItems(itemLocation[i]));
    // }
  }

  ngAfterViewInit() {}

  // onCategoryTap1(args) {
  //   this.isVisible2 = true;
  //   var i;
  //   i = args.index;
  //   var str = this.categoryList;
  //   var str1 = " > ";
  //   var item = <Category>this.categories1[i];
  //   str = str.concat(item.name);
  //   str = str.concat(str1);
  //   this.categoryList = str;
  // }

  // onCategoryTap2(args) {
  //   var i;
  //   i = args.index;
  //   var str = this.categoryList;
  //   var str1 = " > ";
  //   var item = <Category>this.categories2[i];
  //   str = str.concat(item.name);
  //   str = str.concat(str1);
  //   this.categoryList = str;
  // }
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var http_1 = require("@angular/common/http");
var user_service_1 = require("../services/user.service");
var ShipItems = /** @class */ (function () {
    function ShipItems(title, quantity, location) {
        if (title != null) {
            this.title = title;
        }
        if (quantity != null) {
            this.quantity = quantity;
        }
        if (location != null) {
            this.location = location;
        }
    }
    return ShipItems;
}());
var itemTitle = [
    "Title 1",
    "Title 2",
    "Title 3",
    "Title 4",
    "Title 5",
    "Title 6",
    "Title 7",
    "Title 8",
    "Title 9",
    "Title 10"
];
var itemQuantity = ["5", "2", "2", "3", "5", "4", "7", "5", "8", "10"];
var itemLocation = [
    "bjsahfiuwehjdfshjdhskjfhsdkhfkdshfhsdfhfdhgdfhgjhfdhgkfdhg",
    "dkfjhfkahsdkjdshkjhdksj",
    "sdfjhkjadsfhsdfsdfsdfdsf",
    "sajdhkjadhdsfasdfasdfsdfsdfsdfsdfasdfasdfsadfassa",
    "sdhjdshfdfssdfsdf",
    "hgdfgjfgejfsdfsdfsdsdfasdfsafdsadf",
    "ewuyuweuybsdfsd",
    "weyewwedffdsfsdfdsfs",
    "wetufbsdjfdsfsdfdsfdsfsdfsdf",
    "hjsdgfgusyddsfsdfssdafasdfasdfafafasdfsafasdfasdfdsfdsfdsf"
];
var ShipItemsComponent = /** @class */ (function () {
    function ShipItemsComponent(routerExtensions, http, userService) {
        this.routerExtensions = routerExtensions;
        this.http = http;
        this.userService = userService;
        this.userService.actionBarState(true);
        this.userService.bottomBarDotState(false);
        this.userService.bottomBarDots(5);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
    }
    ShipItemsComponent.prototype.ngOnInit = function () {
        this.items = [];
        for (var i = 0; i < itemTitle.length; i++) {
            this.items.push(new ShipItems(itemTitle[i], itemQuantity[i], itemLocation[i]));
        }
        // for (let i = 0; i < itemQuantity.length; i++) {
        //   this.itemsQuantity.push(new ShipItems(itemQuantity[i]));
        // }
        // for (let i = 0; i < itemLocation.length; i++) {
        //   this.itemsLocation.push(new ShipItems(itemLocation[i]));
        // }
    };
    ShipItemsComponent.prototype.ngAfterViewInit = function () { };
    ShipItemsComponent = __decorate([
        core_1.Component({
            selector: "shipItems",
            moduleId: module.id,
            templateUrl: "./shipItems.component.html",
            styleUrls: ["./shipItems.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            http_1.HttpClient,
            user_service_1.UserService])
    ], ShipItemsComponent);
    return ShipItemsComponent;
}());
exports.ShipItemsComponent = ShipItemsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hpcEl0ZW1zLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNoaXBJdGVtcy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBNEU7QUFFNUUsc0RBQStEO0FBRS9ELDZDQUErRDtBQU0vRCx5REFBdUQ7QUFFdkQ7SUFLRSxtQkFBWSxLQUFLLEVBQUUsUUFBUSxFQUFFLFFBQVE7UUFDbkMsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1NBQ3BCO1FBQ0QsSUFBSSxRQUFRLElBQUksSUFBSSxFQUFFO1lBQ3BCLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1NBQzFCO1FBQ0QsSUFBSSxRQUFRLElBQUksSUFBSSxFQUFFO1lBQ3BCLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1NBQzFCO0lBQ0gsQ0FBQztJQUNILGdCQUFDO0FBQUQsQ0FBQyxBQWhCRCxJQWdCQztBQUVELElBQUksU0FBUyxHQUFHO0lBQ2QsU0FBUztJQUNULFNBQVM7SUFDVCxTQUFTO0lBQ1QsU0FBUztJQUNULFNBQVM7SUFDVCxTQUFTO0lBQ1QsU0FBUztJQUNULFNBQVM7SUFDVCxTQUFTO0lBQ1QsVUFBVTtDQUNYLENBQUM7QUFFRixJQUFJLFlBQVksR0FBRyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO0FBRXZFLElBQUksWUFBWSxHQUFHO0lBQ2pCLDREQUE0RDtJQUM1RCx5QkFBeUI7SUFDekIsMEJBQTBCO0lBQzFCLG1EQUFtRDtJQUNuRCxtQkFBbUI7SUFDbkIsb0NBQW9DO0lBQ3BDLGlCQUFpQjtJQUNqQixzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLDREQUE0RDtDQUM3RCxDQUFDO0FBUUY7SUFHRSw0QkFDVSxnQkFBa0MsRUFDbEMsSUFBZ0IsRUFDaEIsV0FBd0I7UUFGeEIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyxTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2hCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBRWhDLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRUQscUNBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3pDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUNiLElBQUksU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQzlELENBQUM7U0FDSDtRQUNELGtEQUFrRDtRQUNsRCw2REFBNkQ7UUFDN0QsSUFBSTtRQUNKLGtEQUFrRDtRQUNsRCw2REFBNkQ7UUFDN0QsSUFBSTtJQUNOLENBQUM7SUFFRCw0Q0FBZSxHQUFmLGNBQW1CLENBQUM7SUE5QlQsa0JBQWtCO1FBTjlCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsV0FBVztZQUNyQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLDRCQUE0QjtZQUN6QyxTQUFTLEVBQUUsQ0FBQywyQkFBMkIsQ0FBQztTQUN6QyxDQUFDO3lDQUs0Qix5QkFBZ0I7WUFDNUIsaUJBQVU7WUFDSCwwQkFBVztPQU52QixrQkFBa0IsQ0FzRDlCO0lBQUQseUJBQUM7Q0FBQSxBQXRERCxJQXNEQztBQXREWSxnREFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBBZnRlclZpZXdJbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IEJhcmNvZGVTY2FubmVyIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1iYXJjb2Rlc2Nhbm5lclwiO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IE1vZGFsQ29tcG9uZW50IH0gZnJvbSBcIi4uL21vZGFscy9tb2RhbC5jb21wb25lbnRcIjtcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XG5pbXBvcnQgeyBPYnNlcnZhYmxlQXJyYXkgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGUtYXJyYXkvb2JzZXJ2YWJsZS1hcnJheVwiO1xuaW1wb3J0IHsgRXh0ZW5kZWROYXZpZ2F0aW9uRXh0cmFzIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlci9yb3V0ZXItZXh0ZW5zaW9uc1wiO1xuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IE9ic2VydmFibGUsIEV2ZW50RGF0YSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2RhdGEvb2JzZXJ2YWJsZVwiO1xuaW1wb3J0IHsgU3dpcGVHZXN0dXJlRXZlbnREYXRhIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZ2VzdHVyZXNcIjtcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL3VzZXIuc2VydmljZVwiO1xuXG5jbGFzcyBTaGlwSXRlbXMge1xuICB0aXRsZTogc3RyaW5nO1xuICBxdWFudGl0eTogc3RyaW5nO1xuICBsb2NhdGlvbjogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKHRpdGxlLCBxdWFudGl0eSwgbG9jYXRpb24pIHtcbiAgICBpZiAodGl0bGUgIT0gbnVsbCkge1xuICAgICAgdGhpcy50aXRsZSA9IHRpdGxlO1xuICAgIH1cbiAgICBpZiAocXVhbnRpdHkgIT0gbnVsbCkge1xuICAgICAgdGhpcy5xdWFudGl0eSA9IHF1YW50aXR5O1xuICAgIH1cbiAgICBpZiAobG9jYXRpb24gIT0gbnVsbCkge1xuICAgICAgdGhpcy5sb2NhdGlvbiA9IGxvY2F0aW9uO1xuICAgIH1cbiAgfVxufVxuXG5sZXQgaXRlbVRpdGxlID0gW1xuICBcIlRpdGxlIDFcIixcbiAgXCJUaXRsZSAyXCIsXG4gIFwiVGl0bGUgM1wiLFxuICBcIlRpdGxlIDRcIixcbiAgXCJUaXRsZSA1XCIsXG4gIFwiVGl0bGUgNlwiLFxuICBcIlRpdGxlIDdcIixcbiAgXCJUaXRsZSA4XCIsXG4gIFwiVGl0bGUgOVwiLFxuICBcIlRpdGxlIDEwXCJcbl07XG5cbmxldCBpdGVtUXVhbnRpdHkgPSBbXCI1XCIsIFwiMlwiLCBcIjJcIiwgXCIzXCIsIFwiNVwiLCBcIjRcIiwgXCI3XCIsIFwiNVwiLCBcIjhcIiwgXCIxMFwiXTtcblxubGV0IGl0ZW1Mb2NhdGlvbiA9IFtcbiAgXCJianNhaGZpdXdlaGpkZnNoamRoc2tqZmhzZGtoZmtkc2hmaHNkZmhmZGhnZGZoZ2poZmRoZ2tmZGhnXCIsXG4gIFwiZGtmamhma2Foc2RramRzaGtqaGRrc2pcIixcbiAgXCJzZGZqaGtqYWRzZmhzZGZzZGZzZGZkc2ZcIixcbiAgXCJzYWpkaGtqYWRoZHNmYXNkZmFzZGZzZGZzZGZzZGZzZGZhc2RmYXNkZnNhZGZhc3NhXCIsXG4gIFwic2RoamRzaGZkZnNzZGZzZGZcIixcbiAgXCJoZ2RmZ2pmZ2VqZnNkZnNkZnNkc2RmYXNkZnNhZmRzYWRmXCIsXG4gIFwiZXd1eXV3ZXV5YnNkZnNkXCIsXG4gIFwid2V5ZXd3ZWRmZmRzZnNkZmRzZnNcIixcbiAgXCJ3ZXR1ZmJzZGpmZHNmc2RmZHNmZHNmc2Rmc2RmXCIsXG4gIFwiaGpzZGdmZ3VzeWRkc2ZzZGZzc2RhZmFzZGZhc2RmYWZhZmFzZGZzYWZhc2RmYXNkZmRzZmRzZmRzZlwiXG5dO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwic2hpcEl0ZW1zXCIsXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gIHRlbXBsYXRlVXJsOiBcIi4vc2hpcEl0ZW1zLmNvbXBvbmVudC5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9zaGlwSXRlbXMuY29tcG9uZW50LmNzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBTaGlwSXRlbXNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQge1xuICBwdWJsaWMgaXRlbXM6IEFycmF5PFNoaXBJdGVtcz47XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLFxuICAgIHByaXZhdGUgaHR0cDogSHR0cENsaWVudCxcbiAgICBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZVxuICApIHtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmFjdGlvbkJhclN0YXRlKHRydWUpO1xuICAgIHRoaXMudXNlclNlcnZpY2UuYm90dG9tQmFyRG90U3RhdGUoZmFsc2UpO1xuICAgIHRoaXMudXNlclNlcnZpY2UuYm90dG9tQmFyRG90cyg1KTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZSh0cnVlKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZUNoYW5nZXMoMSk7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLml0ZW1zID0gW107XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBpdGVtVGl0bGUubGVuZ3RoOyBpKyspIHtcbiAgICAgIHRoaXMuaXRlbXMucHVzaChcbiAgICAgICAgbmV3IFNoaXBJdGVtcyhpdGVtVGl0bGVbaV0sIGl0ZW1RdWFudGl0eVtpXSwgaXRlbUxvY2F0aW9uW2ldKVxuICAgICAgKTtcbiAgICB9XG4gICAgLy8gZm9yIChsZXQgaSA9IDA7IGkgPCBpdGVtUXVhbnRpdHkubGVuZ3RoOyBpKyspIHtcbiAgICAvLyAgIHRoaXMuaXRlbXNRdWFudGl0eS5wdXNoKG5ldyBTaGlwSXRlbXMoaXRlbVF1YW50aXR5W2ldKSk7XG4gICAgLy8gfVxuICAgIC8vIGZvciAobGV0IGkgPSAwOyBpIDwgaXRlbUxvY2F0aW9uLmxlbmd0aDsgaSsrKSB7XG4gICAgLy8gICB0aGlzLml0ZW1zTG9jYXRpb24ucHVzaChuZXcgU2hpcEl0ZW1zKGl0ZW1Mb2NhdGlvbltpXSkpO1xuICAgIC8vIH1cbiAgfVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHt9XG5cbiAgLy8gb25DYXRlZ29yeVRhcDEoYXJncykge1xuICAvLyAgIHRoaXMuaXNWaXNpYmxlMiA9IHRydWU7XG4gIC8vICAgdmFyIGk7XG4gIC8vICAgaSA9IGFyZ3MuaW5kZXg7XG4gIC8vICAgdmFyIHN0ciA9IHRoaXMuY2F0ZWdvcnlMaXN0O1xuICAvLyAgIHZhciBzdHIxID0gXCIgPiBcIjtcbiAgLy8gICB2YXIgaXRlbSA9IDxDYXRlZ29yeT50aGlzLmNhdGVnb3JpZXMxW2ldO1xuICAvLyAgIHN0ciA9IHN0ci5jb25jYXQoaXRlbS5uYW1lKTtcbiAgLy8gICBzdHIgPSBzdHIuY29uY2F0KHN0cjEpO1xuICAvLyAgIHRoaXMuY2F0ZWdvcnlMaXN0ID0gc3RyO1xuICAvLyB9XG5cbiAgLy8gb25DYXRlZ29yeVRhcDIoYXJncykge1xuICAvLyAgIHZhciBpO1xuICAvLyAgIGkgPSBhcmdzLmluZGV4O1xuICAvLyAgIHZhciBzdHIgPSB0aGlzLmNhdGVnb3J5TGlzdDtcbiAgLy8gICB2YXIgc3RyMSA9IFwiID4gXCI7XG4gIC8vICAgdmFyIGl0ZW0gPSA8Q2F0ZWdvcnk+dGhpcy5jYXRlZ29yaWVzMltpXTtcbiAgLy8gICBzdHIgPSBzdHIuY29uY2F0KGl0ZW0ubmFtZSk7XG4gIC8vICAgc3RyID0gc3RyLmNvbmNhdChzdHIxKTtcbiAgLy8gICB0aGlzLmNhdGVnb3J5TGlzdCA9IHN0cjtcbiAgLy8gfVxufVxuIl19
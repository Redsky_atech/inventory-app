"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var user_service_1 = require("./services/user.service");
var AppComponent = /** @class */ (function () {
    function AppComponent(userService, routerExtensions) {
        var _this = this;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.dotImage1 = "res://dotgrey";
        this.dotImage2 = "res://dotgrey";
        this.dotImage3 = "res://dotgrey";
        this.dotImage4 = "res://dotgrey";
        this.dotImage5 = "res://dotgrey";
        this.dotImage6 = "res://dotgrey";
        this.dotImage7 = "res://dotgrey";
        this.dotImage8 = "res://dotgrey";
        this.dotImage9 = "res://dotgrey";
        this.dotImage10 = "res://dotgrey";
        this.dotImage11 = "res://dotgrey";
        // dotImage12: string = "res://dotgrey";
        // dotImage13: string = "res://dotgrey";
        // dotImage14: string = "res://dotgrey";
        this.tabImage1 = "res://homeblue";
        this.tabImage2 = "res://personblue";
        this.tabImage3 = "res://settingblue";
        this.colorTab1 = "white";
        this.colorTab2 = "white";
        this.colorTab3 = "white";
        this.screenStatusLabel = "Disable *";
        this.screenStatusSwitch = "switchDisable";
        this.switchLabelColor = "black";
        this.userService.tabviewStateChanges.subscribe(function (tab) {
            if (tab != undefined && tab != null && tab != -1) {
                if (tab == 1) {
                    _this.tabImage1 = "res://homewhite";
                    _this.tabImage2 = "res://personblue";
                    _this.tabImage3 = "res://settingblue";
                    _this.colorTab1 = "#2196f3";
                    _this.colorTab2 = "white";
                    _this.colorTab3 = "white";
                }
                if (tab == 2) {
                    _this.tabImage1 = "res://homeblue";
                    _this.tabImage2 = "res://personwhite";
                    _this.tabImage3 = "res://settingblue";
                    _this.colorTab1 = "white";
                    _this.colorTab2 = "#2196f3";
                    _this.colorTab3 = "white";
                }
                if (tab == 3) {
                    _this.tabImage1 = "res://homeblue";
                    _this.tabImage2 = "res://personblue";
                    _this.tabImage3 = "res://settingwhite";
                    _this.colorTab1 = "white";
                    _this.colorTab2 = "white";
                    _this.colorTab3 = "#2196f3";
                }
            }
        });
        this.userService.bottomBarDotsChanges.subscribe(function (dot) {
            if (dot != undefined && dot != null && dot != -1) {
                if (dot == 1) {
                    _this.dotImage1 = "res://dotblue";
                    _this.dotImage2 = "res://dotgrey";
                    _this.dotImage3 = "res://dotgrey";
                    _this.dotImage4 = "res://dotgrey";
                    _this.dotImage5 = "res://dotgrey";
                    _this.dotImage6 = "res://dotgrey";
                    _this.dotImage7 = "res://dotgrey";
                    _this.dotImage8 = "res://dotgrey";
                    _this.dotImage9 = "res://dotgrey";
                    _this.dotImage10 = "res://dotgrey";
                    _this.dotImage11 = "res://dotgrey";
                    // this.dotImage12 = "res://dotgrey";
                    // this.dotImage13 = "res://dotgrey";
                    // this.dotImage14 = "res://dotgrey";
                }
                if (dot == 2) {
                    _this.dotImage1 = "res://dotgreen";
                    _this.dotImage2 = "res://dotblue";
                    _this.dotImage3 = "res://dotgrey";
                    _this.dotImage4 = "res://dotgrey";
                    _this.dotImage5 = "res://dotgrey";
                    _this.dotImage6 = "res://dotgrey";
                    _this.dotImage7 = "res://dotgrey";
                    _this.dotImage8 = "res://dotgrey";
                    _this.dotImage9 = "res://dotgrey";
                    _this.dotImage10 = "res://dotgrey";
                    _this.dotImage11 = "res://dotgrey";
                    // this.dotImage12 = "res://dotgrey";
                    // this.dotImage13 = "res://dotgrey";
                    // this.dotImage14 = "res://dotgrey";
                }
                if (dot == 3) {
                    _this.dotImage1 = "res://dotgreen";
                    _this.dotImage2 = "res://dotgreen";
                    _this.dotImage3 = "res://dotblue";
                    _this.dotImage4 = "res://dotgrey";
                    _this.dotImage5 = "res://dotgrey";
                    _this.dotImage6 = "res://dotgrey";
                    _this.dotImage7 = "res://dotgrey";
                    _this.dotImage8 = "res://dotgrey";
                    _this.dotImage9 = "res://dotgrey";
                    _this.dotImage10 = "res://dotgrey";
                    _this.dotImage11 = "res://dotgrey";
                    // this.dotImage12 = "res://dotgrey";
                    // this.dotImage13 = "res://dotgrey";
                    // this.dotImage14 = "res://dotgrey";
                }
                if (dot == 4) {
                    _this.dotImage1 = "res://dotgreen";
                    _this.dotImage2 = "res://dotgreen";
                    _this.dotImage3 = "res://dotgreen";
                    _this.dotImage4 = "res://dotblue";
                    _this.dotImage5 = "res://dotgrey";
                    _this.dotImage6 = "res://dotgrey";
                    _this.dotImage7 = "res://dotgrey";
                    _this.dotImage8 = "res://dotgrey";
                    _this.dotImage9 = "res://dotgrey";
                    _this.dotImage10 = "res://dotgrey";
                    _this.dotImage11 = "res://dotgrey";
                    // this.dotImage12 = "res://dotgrey";
                    // this.dotImage13 = "res://dotgrey";
                    // this.dotImage14 = "res://dotgrey";
                }
                if (dot == 5) {
                    _this.dotImage1 = "res://dotgreen";
                    _this.dotImage2 = "res://dotgreen";
                    _this.dotImage3 = "res://dotgreen";
                    _this.dotImage4 = "res://dotgreen";
                    _this.dotImage5 = "res://dotblue";
                    _this.dotImage6 = "res://dotgrey";
                    _this.dotImage7 = "res://dotgrey";
                    _this.dotImage8 = "res://dotgrey";
                    _this.dotImage9 = "res://dotgrey";
                    _this.dotImage10 = "res://dotgrey";
                    _this.dotImage11 = "res://dotgrey";
                    // this.dotImage12 = "res://dotgrey";
                    // this.dotImage13 = "res://dotgrey";
                    // this.dotImage14 = "res://dotgrey";
                }
                if (dot == 6) {
                    _this.dotImage1 = "res://dotgreen";
                    _this.dotImage2 = "res://dotgreen";
                    _this.dotImage3 = "res://dotgreen";
                    _this.dotImage4 = "res://dotgreen";
                    _this.dotImage5 = "res://dotgreen";
                    _this.dotImage6 = "res://dotblue";
                    _this.dotImage7 = "res://dotgrey";
                    _this.dotImage8 = "res://dotgrey";
                    _this.dotImage9 = "res://dotgrey";
                    _this.dotImage10 = "res://dotgrey";
                    _this.dotImage11 = "res://dotgrey";
                    // this.dotImage12 = "res://dotgrey";
                    // this.dotImage13 = "res://dotgrey";
                    // this.dotImage14 = "res://dotgrey";
                }
                if (dot == 7) {
                    _this.dotImage1 = "res://dotgreen";
                    _this.dotImage2 = "res://dotgreen";
                    _this.dotImage3 = "res://dotgreen";
                    _this.dotImage4 = "res://dotgreen";
                    _this.dotImage5 = "res://dotgreen";
                    _this.dotImage6 = "res://dotgreen";
                    _this.dotImage7 = "res://dotblue";
                    _this.dotImage8 = "res://dotgrey";
                    _this.dotImage9 = "res://dotgrey";
                    _this.dotImage10 = "res://dotgrey";
                    _this.dotImage11 = "res://dotgrey";
                    // this.dotImage12 = "res://dotgrey";
                    // this.dotImage13 = "res://dotgrey";
                    // this.dotImage14 = "res://dotgrey";
                }
                if (dot == 8) {
                    _this.dotImage1 = "res://dotgreen";
                    _this.dotImage2 = "res://dotgreen";
                    _this.dotImage3 = "res://dotgreen";
                    _this.dotImage4 = "res://dotgreen";
                    _this.dotImage5 = "res://dotgreen";
                    _this.dotImage6 = "res://dotgreen";
                    _this.dotImage7 = "res://dotgreen";
                    _this.dotImage8 = "res://dotblue";
                    _this.dotImage9 = "res://dotgrey";
                    _this.dotImage10 = "res://dotgrey";
                    _this.dotImage11 = "res://dotgrey";
                    // this.dotImage12 = "res://dotgrey";
                    // this.dotImage13 = "res://dotgrey";
                    // this.dotImage14 = "res://dotgrey";
                }
                if (dot == 9) {
                    _this.dotImage1 = "res://dotgreen";
                    _this.dotImage2 = "res://dotgreen";
                    _this.dotImage3 = "res://dotgreen";
                    _this.dotImage4 = "res://dotgreen";
                    _this.dotImage5 = "res://dotgreen";
                    _this.dotImage6 = "res://dotgreen";
                    _this.dotImage7 = "res://dotgreen";
                    _this.dotImage8 = "res://dotgreen";
                    _this.dotImage9 = "res://dotblue";
                    _this.dotImage10 = "res://dotgrey";
                    _this.dotImage11 = "res://dotgrey";
                    // this.dotImage12 = "res://dotgrey";
                    // this.dotImage13 = "res://dotgrey";
                    // this.dotImage14 = "res://dotgrey";
                }
                if (dot == 10) {
                    _this.dotImage1 = "res://dotgreen";
                    _this.dotImage2 = "res://dotgreen";
                    _this.dotImage3 = "res://dotgreen";
                    _this.dotImage4 = "res://dotgreen";
                    _this.dotImage5 = "res://dotgreen";
                    _this.dotImage6 = "res://dotgreen";
                    _this.dotImage7 = "res://dotgreen";
                    _this.dotImage8 = "res://dotgreen";
                    _this.dotImage9 = "res://dotgreen";
                    _this.dotImage10 = "res://dotblue";
                    _this.dotImage11 = "res://dotgrey";
                    // this.dotImage12 = "res://dotgrey";
                    // this.dotImage13 = "res://dotgrey";
                    // this.dotImage14 = "res://dotgrey";
                }
                if (dot == 11) {
                    _this.dotImage1 = "res://dotgreen";
                    _this.dotImage2 = "res://dotgreen";
                    _this.dotImage3 = "res://dotgreen";
                    _this.dotImage4 = "res://dotgreen";
                    _this.dotImage5 = "res://dotgreen";
                    _this.dotImage6 = "res://dotgreen";
                    _this.dotImage7 = "res://dotgreen";
                    _this.dotImage8 = "res://dotgreen";
                    _this.dotImage9 = "res://dotgreen";
                    _this.dotImage10 = "res://dotgreen";
                    _this.dotImage11 = "res://dotblue";
                    // this.dotImage12 = "res://dotgrey";
                    // this.dotImage13 = "res://dotgrey";
                    // this.dotImage14 = "res://dotgrey";
                }
                // if (dot == 12) {
                //   this.dotImage1 = "res://dotgreen";
                //   this.dotImage2 = "res://dotgreen";
                //   this.dotImage3 = "res://dotgreen";
                //   this.dotImage4 = "res://dotgreen";
                //   this.dotImage5 = "res://dotgreen";
                //   this.dotImage6 = "res://dotgreen";
                //   this.dotImage7 = "res://dotgreen";
                //   this.dotImage8 = "res://dotgreen";
                //   this.dotImage9 = "res://dotgreen";
                //   this.dotImage10 = "res://dotgreen";
                //   this.dotImage11 = "res://dotgreen";
                //   this.dotImage12 = "res://dotblue";
                //   this.dotImage13 = "res://dotgrey";
                //   this.dotImage14 = "res://dotgrey";
                // }
                // if (dot == 13) {
                //   this.dotImage1 = "res://dotgreen";
                //   this.dotImage2 = "res://dotgreen";
                //   this.dotImage3 = "res://dotgreen";
                //   this.dotImage4 = "res://dotgreen";
                //   this.dotImage5 = "res://dotgreen";
                //   this.dotImage6 = "res://dotgreen";
                //   this.dotImage7 = "res://dotgreen";
                //   this.dotImage8 = "res://dotgreen";
                //   this.dotImage9 = "res://dotgreen";
                //   this.dotImage10 = "res://dotgreen";
                //   this.dotImage11 = "res://dotgreen";
                //   this.dotImage12 = "res://dotgreen";
                //   this.dotImage13 = "res://dotblue";
                //   this.dotImage14 = "res://dotgrey";
                // }
                // if (dot == 14) {
                //   this.dotImage1 = "res://dotgreen";
                //   this.dotImage2 = "res://dotgreen";
                //   this.dotImage3 = "res://dotgreen";
                //   this.dotImage4 = "res://dotgreen";
                //   this.dotImage5 = "res://dotgreen";
                //   this.dotImage6 = "res://dotgreen";
                //   this.dotImage7 = "res://dotgreen";
                //   this.dotImage8 = "res://dotgreen";
                //   this.dotImage9 = "res://dotgreen";
                //   this.dotImage10 = "res://dotgreen";
                //   this.dotImage11 = "res://dotgreen";
                //   this.dotImage12 = "res://dotgreen";
                //   this.dotImage13 = "res://dotgreen";
                //   this.dotImage14 = "res://dotblue";
                // }
            }
        });
        this.userService.actionBarChanges.subscribe(function (state) {
            if (state != undefined) {
                _this.showActionBar = state;
            }
        });
        this.userService.bottomBarDotsState.subscribe(function (state) {
            if (state != undefined) {
                _this.showDots = state;
            }
        });
        this.userService.tabviewState.subscribe(function (state) {
            if (state != undefined) {
                _this.showTabs = state;
            }
        });
        this.userService.switchstate.subscribe(function (state) {
            if (state != undefined) {
                _this.showSwitch = state;
            }
        });
    }
    AppComponent.prototype.onSwitch = function (args) {
        var checkStatus = args.object;
        if (checkStatus.checked) {
            this.screenStatusSwitch = "switchEnable";
            this.screenStatusLabel = "Disable *";
            this.switchLabelColor = "black";
        }
        else {
            this.screenStatusSwitch = "switchDisable";
            this.screenStatusLabel = "Enable";
            this.switchLabelColor = "#A9A9A9";
        }
    };
    AppComponent.prototype.tabClick1 = function () {
        this.routerExtensions.navigate(["/createListing"]);
    };
    AppComponent.prototype.tabClick2 = function () {
        this.routerExtensions.navigate(["/userAccount"]);
    };
    AppComponent.prototype.tabClick3 = function () {
        this.routerExtensions.navigate(["/preferencesDefaults"]);
    };
    AppComponent.prototype.dotClick1 = function () {
        this.routerExtensions.navigate(["/sku"]);
    };
    AppComponent.prototype.dotClick2 = function () {
        this.routerExtensions.navigate(["/upc"]);
    };
    AppComponent.prototype.dotClick3 = function () {
        this.routerExtensions.navigate(["/title"]);
    };
    AppComponent.prototype.dotClick4 = function () {
        this.routerExtensions.navigate(["/suggest"]);
    };
    AppComponent.prototype.dotClick5 = function () {
        this.routerExtensions.navigate(["/findCategory"]);
    };
    AppComponent.prototype.dotClick6 = function () {
        this.routerExtensions.navigate(["/storeCategory"]);
    };
    AppComponent.prototype.dotClick7 = function () {
        this.routerExtensions.navigate(["/condition"]);
    };
    AppComponent.prototype.dotClick8 = function () {
        this.routerExtensions.navigate(["/description"]);
    };
    AppComponent.prototype.dotClick9 = function () {
        this.routerExtensions.navigate(["/itemspecifies"]);
    };
    AppComponent.prototype.dotClick10 = function () {
        this.routerExtensions.navigate(["/businesspolicies"]);
    };
    AppComponent.prototype.dotClick11 = function () {
        this.routerExtensions.navigate(["/weightDimension"]);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: "ns-app",
            moduleId: module.id,
            templateUrl: "./app.component.html",
            styleUrls: ["./app.component.css"]
        }),
        __metadata("design:paramtypes", [user_service_1.UserService,
            router_1.RouterExtensions])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMEM7QUFFMUMsc0RBQStEO0FBQy9ELHdEQUFzRDtBQVN0RDtJQStCRSxzQkFDVSxXQUF3QixFQUN4QixnQkFBa0M7UUFGNUMsaUJBd1JDO1FBdlJTLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUEzQjVDLGNBQVMsR0FBVyxlQUFlLENBQUM7UUFDcEMsY0FBUyxHQUFXLGVBQWUsQ0FBQztRQUNwQyxjQUFTLEdBQVcsZUFBZSxDQUFDO1FBQ3BDLGNBQVMsR0FBVyxlQUFlLENBQUM7UUFDcEMsY0FBUyxHQUFXLGVBQWUsQ0FBQztRQUNwQyxjQUFTLEdBQVcsZUFBZSxDQUFDO1FBQ3BDLGNBQVMsR0FBVyxlQUFlLENBQUM7UUFDcEMsY0FBUyxHQUFXLGVBQWUsQ0FBQztRQUNwQyxjQUFTLEdBQVcsZUFBZSxDQUFDO1FBQ3BDLGVBQVUsR0FBVyxlQUFlLENBQUM7UUFDckMsZUFBVSxHQUFXLGVBQWUsQ0FBQztRQUNyQyx3Q0FBd0M7UUFDeEMsd0NBQXdDO1FBQ3hDLHdDQUF3QztRQUN4QyxjQUFTLEdBQVcsZ0JBQWdCLENBQUM7UUFDckMsY0FBUyxHQUFXLGtCQUFrQixDQUFDO1FBQ3ZDLGNBQVMsR0FBVyxtQkFBbUIsQ0FBQztRQUN4QyxjQUFTLEdBQVcsT0FBTyxDQUFDO1FBQzVCLGNBQVMsR0FBVyxPQUFPLENBQUM7UUFDNUIsY0FBUyxHQUFXLE9BQU8sQ0FBQztRQUU1QixzQkFBaUIsR0FBRyxXQUFXLENBQUM7UUFDaEMsdUJBQWtCLEdBQUcsZUFBZSxDQUFDO1FBQ3JDLHFCQUFnQixHQUFHLE9BQU8sQ0FBQztRQU16QixJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxVQUFDLEdBQVc7WUFDekQsSUFBSSxHQUFHLElBQUksU0FBUyxJQUFJLEdBQUcsSUFBSSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsQ0FBQyxFQUFFO2dCQUNoRCxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUU7b0JBQ1osS0FBSSxDQUFDLFNBQVMsR0FBRyxpQkFBaUIsQ0FBQztvQkFDbkMsS0FBSSxDQUFDLFNBQVMsR0FBRyxrQkFBa0IsQ0FBQztvQkFDcEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxtQkFBbUIsQ0FBQztvQkFDckMsS0FBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7b0JBQzNCLEtBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDO29CQUN6QixLQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQztpQkFDMUI7Z0JBQ0QsSUFBSSxHQUFHLElBQUksQ0FBQyxFQUFFO29CQUNaLEtBQUksQ0FBQyxTQUFTLEdBQUcsZ0JBQWdCLENBQUM7b0JBQ2xDLEtBQUksQ0FBQyxTQUFTLEdBQUcsbUJBQW1CLENBQUM7b0JBQ3JDLEtBQUksQ0FBQyxTQUFTLEdBQUcsbUJBQW1CLENBQUM7b0JBQ3JDLEtBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDO29CQUN6QixLQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztvQkFDM0IsS0FBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUM7aUJBQzFCO2dCQUNELElBQUksR0FBRyxJQUFJLENBQUMsRUFBRTtvQkFDWixLQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO29CQUNsQyxLQUFJLENBQUMsU0FBUyxHQUFHLGtCQUFrQixDQUFDO29CQUNwQyxLQUFJLENBQUMsU0FBUyxHQUFHLG9CQUFvQixDQUFDO29CQUN0QyxLQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQztvQkFDekIsS0FBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUM7b0JBQ3pCLEtBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO2lCQUM1QjthQUNGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxVQUFDLEdBQVc7WUFDMUQsSUFBSSxHQUFHLElBQUksU0FBUyxJQUFJLEdBQUcsSUFBSSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsQ0FBQyxFQUFFO2dCQUNoRCxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUU7b0JBQ1osS0FBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO29CQUNqQyxLQUFJLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQztvQkFDakMsS0FBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO29CQUNqQyxLQUFJLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQztvQkFDakMsS0FBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO29CQUNqQyxLQUFJLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQztvQkFDakMsS0FBSSxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7b0JBQ2xDLEtBQUksQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDO29CQUNsQyxxQ0FBcUM7b0JBQ3JDLHFDQUFxQztvQkFDckMscUNBQXFDO2lCQUN0QztnQkFDRCxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUU7b0JBQ1osS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO29CQUNqQyxLQUFJLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQztvQkFDakMsS0FBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO29CQUNqQyxLQUFJLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQztvQkFDakMsS0FBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO29CQUNqQyxLQUFJLENBQUMsVUFBVSxHQUFHLGVBQWUsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7b0JBQ2xDLHFDQUFxQztvQkFDckMscUNBQXFDO29CQUNyQyxxQ0FBcUM7aUJBQ3RDO2dCQUNELElBQUksR0FBRyxJQUFJLENBQUMsRUFBRTtvQkFDWixLQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO29CQUNsQyxLQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO29CQUNsQyxLQUFJLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQztvQkFDakMsS0FBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO29CQUNqQyxLQUFJLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQztvQkFDakMsS0FBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO29CQUNqQyxLQUFJLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQztvQkFDakMsS0FBSSxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7b0JBQ2xDLEtBQUksQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDO29CQUNsQyxxQ0FBcUM7b0JBQ3JDLHFDQUFxQztvQkFDckMscUNBQXFDO2lCQUN0QztnQkFDRCxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUU7b0JBQ1osS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO29CQUNqQyxLQUFJLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQztvQkFDakMsS0FBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO29CQUNqQyxLQUFJLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQztvQkFDakMsS0FBSSxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7b0JBQ2xDLEtBQUksQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDO29CQUNsQyxxQ0FBcUM7b0JBQ3JDLHFDQUFxQztvQkFDckMscUNBQXFDO2lCQUN0QztnQkFDRCxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUU7b0JBQ1osS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO29CQUNqQyxLQUFJLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQztvQkFDakMsS0FBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO29CQUNqQyxLQUFJLENBQUMsVUFBVSxHQUFHLGVBQWUsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7b0JBQ2xDLHFDQUFxQztvQkFDckMscUNBQXFDO29CQUNyQyxxQ0FBcUM7aUJBQ3RDO2dCQUNELElBQUksR0FBRyxJQUFJLENBQUMsRUFBRTtvQkFDWixLQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO29CQUNsQyxLQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO29CQUNsQyxLQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO29CQUNsQyxLQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO29CQUNsQyxLQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO29CQUNsQyxLQUFJLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQztvQkFDakMsS0FBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO29CQUNqQyxLQUFJLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQztvQkFDakMsS0FBSSxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7b0JBQ2xDLEtBQUksQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDO29CQUNsQyxxQ0FBcUM7b0JBQ3JDLHFDQUFxQztvQkFDckMscUNBQXFDO2lCQUN0QztnQkFDRCxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUU7b0JBQ1osS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO29CQUNqQyxLQUFJLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQztvQkFDakMsS0FBSSxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7b0JBQ2xDLEtBQUksQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDO29CQUNsQyxxQ0FBcUM7b0JBQ3JDLHFDQUFxQztvQkFDckMscUNBQXFDO2lCQUN0QztnQkFDRCxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUU7b0JBQ1osS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO29CQUNqQyxLQUFJLENBQUMsVUFBVSxHQUFHLGVBQWUsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7b0JBQ2xDLHFDQUFxQztvQkFDckMscUNBQXFDO29CQUNyQyxxQ0FBcUM7aUJBQ3RDO2dCQUNELElBQUksR0FBRyxJQUFJLENBQUMsRUFBRTtvQkFDWixLQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO29CQUNsQyxLQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO29CQUNsQyxLQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO29CQUNsQyxLQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO29CQUNsQyxLQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO29CQUNsQyxLQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO29CQUNsQyxLQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO29CQUNsQyxLQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO29CQUNsQyxLQUFJLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQztvQkFDakMsS0FBSSxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7b0JBQ2xDLEtBQUksQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDO29CQUNsQyxxQ0FBcUM7b0JBQ3JDLHFDQUFxQztvQkFDckMscUNBQXFDO2lCQUN0QztnQkFDRCxJQUFJLEdBQUcsSUFBSSxFQUFFLEVBQUU7b0JBQ2IsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7b0JBQ2xDLEtBQUksQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDO29CQUNsQyxxQ0FBcUM7b0JBQ3JDLHFDQUFxQztvQkFDckMscUNBQXFDO2lCQUN0QztnQkFDRCxJQUFJLEdBQUcsSUFBSSxFQUFFLEVBQUU7b0JBQ2IsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbkMsS0FBSSxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7b0JBQ2xDLHFDQUFxQztvQkFDckMscUNBQXFDO29CQUNyQyxxQ0FBcUM7aUJBQ3RDO2dCQUNELG1CQUFtQjtnQkFDbkIsdUNBQXVDO2dCQUN2Qyx1Q0FBdUM7Z0JBQ3ZDLHVDQUF1QztnQkFDdkMsdUNBQXVDO2dCQUN2Qyx1Q0FBdUM7Z0JBQ3ZDLHVDQUF1QztnQkFDdkMsdUNBQXVDO2dCQUN2Qyx1Q0FBdUM7Z0JBQ3ZDLHVDQUF1QztnQkFDdkMsd0NBQXdDO2dCQUN4Qyx3Q0FBd0M7Z0JBQ3hDLHVDQUF1QztnQkFDdkMsdUNBQXVDO2dCQUN2Qyx1Q0FBdUM7Z0JBQ3ZDLElBQUk7Z0JBQ0osbUJBQW1CO2dCQUNuQix1Q0FBdUM7Z0JBQ3ZDLHVDQUF1QztnQkFDdkMsdUNBQXVDO2dCQUN2Qyx1Q0FBdUM7Z0JBQ3ZDLHVDQUF1QztnQkFDdkMsdUNBQXVDO2dCQUN2Qyx1Q0FBdUM7Z0JBQ3ZDLHVDQUF1QztnQkFDdkMsdUNBQXVDO2dCQUN2Qyx3Q0FBd0M7Z0JBQ3hDLHdDQUF3QztnQkFDeEMsd0NBQXdDO2dCQUN4Qyx1Q0FBdUM7Z0JBQ3ZDLHVDQUF1QztnQkFDdkMsSUFBSTtnQkFDSixtQkFBbUI7Z0JBQ25CLHVDQUF1QztnQkFDdkMsdUNBQXVDO2dCQUN2Qyx1Q0FBdUM7Z0JBQ3ZDLHVDQUF1QztnQkFDdkMsdUNBQXVDO2dCQUN2Qyx1Q0FBdUM7Z0JBQ3ZDLHVDQUF1QztnQkFDdkMsdUNBQXVDO2dCQUN2Qyx1Q0FBdUM7Z0JBQ3ZDLHdDQUF3QztnQkFDeEMsd0NBQXdDO2dCQUN4Qyx3Q0FBd0M7Z0JBQ3hDLHdDQUF3QztnQkFDeEMsdUNBQXVDO2dCQUN2QyxJQUFJO2FBQ0w7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLFVBQUMsS0FBYztZQUN6RCxJQUFJLEtBQUssSUFBSSxTQUFTLEVBQUU7Z0JBQ3RCLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2FBQzVCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxVQUFDLEtBQWM7WUFDM0QsSUFBSSxLQUFLLElBQUksU0FBUyxFQUFFO2dCQUN0QixLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQzthQUN2QjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFVBQUMsS0FBYztZQUNyRCxJQUFJLEtBQUssSUFBSSxTQUFTLEVBQUU7Z0JBQ3RCLEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO2FBQ3ZCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsVUFBQyxLQUFjO1lBQ3BELElBQUksS0FBSyxJQUFJLFNBQVMsRUFBRTtnQkFDdEIsS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7YUFDekI7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSwrQkFBUSxHQUFmLFVBQWdCLElBQUk7UUFDbEIsSUFBSSxXQUFXLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN0QyxJQUFJLFdBQVcsQ0FBQyxPQUFPLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLGNBQWMsQ0FBQztZQUN6QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsV0FBVyxDQUFDO1lBQ3JDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxPQUFPLENBQUM7U0FDakM7YUFBTTtZQUNMLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxlQUFlLENBQUM7WUFDMUMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFFBQVEsQ0FBQztZQUNsQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsU0FBUyxDQUFDO1NBQ25DO0lBQ0gsQ0FBQztJQUVNLGdDQUFTLEdBQWhCO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBQ00sZ0NBQVMsR0FBaEI7UUFDRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBQ00sZ0NBQVMsR0FBaEI7UUFDRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFDTSxnQ0FBUyxHQUFoQjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFDTSxnQ0FBUyxHQUFoQjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFDTSxnQ0FBUyxHQUFoQjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFDTSxnQ0FBUyxHQUFoQjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFDTSxnQ0FBUyxHQUFoQjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFDTSxnQ0FBUyxHQUFoQjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7SUFDckQsQ0FBQztJQUNNLGdDQUFTLEdBQWhCO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUNNLGdDQUFTLEdBQWhCO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQUNNLGdDQUFTLEdBQWhCO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBQ00saUNBQVUsR0FBakI7UUFDRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFDTSxpQ0FBVSxHQUFqQjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQS9XVSxZQUFZO1FBTnhCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsUUFBUTtZQUNsQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLHNCQUFzQjtZQUNuQyxTQUFTLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQztTQUNuQyxDQUFDO3lDQWlDdUIsMEJBQVc7WUFDTix5QkFBZ0I7T0FqQ2pDLFlBQVksQ0F5WHhCO0lBQUQsbUJBQUM7Q0FBQSxBQXpYRCxJQXlYQztBQXpYWSxvQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBCYXJjb2RlU2Nhbm5lciB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYmFyY29kZXNjYW5uZXJcIjtcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2VzL3VzZXIuc2VydmljZVwiO1xuaW1wb3J0IHsgU3dpdGNoIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvc3dpdGNoXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJucy1hcHBcIixcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgdGVtcGxhdGVVcmw6IFwiLi9hcHAuY29tcG9uZW50Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL2FwcC5jb21wb25lbnQuY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIEFwcENvbXBvbmVudCB7XG4gIHNob3dBY3Rpb25CYXI6IGJvb2xlYW47XG4gIHNob3dEb3RzOiBib29sZWFuO1xuICBzaG93VGFiczogYm9vbGVhbjtcbiAgc2hvd1N3aXRjaDogYm9vbGVhbjtcblxuICBkb3RJbWFnZTE6IHN0cmluZyA9IFwicmVzOi8vZG90Z3JleVwiO1xuICBkb3RJbWFnZTI6IHN0cmluZyA9IFwicmVzOi8vZG90Z3JleVwiO1xuICBkb3RJbWFnZTM6IHN0cmluZyA9IFwicmVzOi8vZG90Z3JleVwiO1xuICBkb3RJbWFnZTQ6IHN0cmluZyA9IFwicmVzOi8vZG90Z3JleVwiO1xuICBkb3RJbWFnZTU6IHN0cmluZyA9IFwicmVzOi8vZG90Z3JleVwiO1xuICBkb3RJbWFnZTY6IHN0cmluZyA9IFwicmVzOi8vZG90Z3JleVwiO1xuICBkb3RJbWFnZTc6IHN0cmluZyA9IFwicmVzOi8vZG90Z3JleVwiO1xuICBkb3RJbWFnZTg6IHN0cmluZyA9IFwicmVzOi8vZG90Z3JleVwiO1xuICBkb3RJbWFnZTk6IHN0cmluZyA9IFwicmVzOi8vZG90Z3JleVwiO1xuICBkb3RJbWFnZTEwOiBzdHJpbmcgPSBcInJlczovL2RvdGdyZXlcIjtcbiAgZG90SW1hZ2UxMTogc3RyaW5nID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gIC8vIGRvdEltYWdlMTI6IHN0cmluZyA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAvLyBkb3RJbWFnZTEzOiBzdHJpbmcgPSBcInJlczovL2RvdGdyZXlcIjtcbiAgLy8gZG90SW1hZ2UxNDogc3RyaW5nID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gIHRhYkltYWdlMTogc3RyaW5nID0gXCJyZXM6Ly9ob21lYmx1ZVwiO1xuICB0YWJJbWFnZTI6IHN0cmluZyA9IFwicmVzOi8vcGVyc29uYmx1ZVwiO1xuICB0YWJJbWFnZTM6IHN0cmluZyA9IFwicmVzOi8vc2V0dGluZ2JsdWVcIjtcbiAgY29sb3JUYWIxOiBzdHJpbmcgPSBcIndoaXRlXCI7XG4gIGNvbG9yVGFiMjogc3RyaW5nID0gXCJ3aGl0ZVwiO1xuICBjb2xvclRhYjM6IFN0cmluZyA9IFwid2hpdGVcIjtcblxuICBzY3JlZW5TdGF0dXNMYWJlbCA9IFwiRGlzYWJsZSAqXCI7XG4gIHNjcmVlblN0YXR1c1N3aXRjaCA9IFwic3dpdGNoRGlzYWJsZVwiO1xuICBzd2l0Y2hMYWJlbENvbG9yID0gXCJibGFja1wiO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLFxuICAgIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9uc1xuICApIHtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYnZpZXdTdGF0ZUNoYW5nZXMuc3Vic2NyaWJlKCh0YWI6IG51bWJlcikgPT4ge1xuICAgICAgaWYgKHRhYiAhPSB1bmRlZmluZWQgJiYgdGFiICE9IG51bGwgJiYgdGFiICE9IC0xKSB7XG4gICAgICAgIGlmICh0YWIgPT0gMSkge1xuICAgICAgICAgIHRoaXMudGFiSW1hZ2UxID0gXCJyZXM6Ly9ob21ld2hpdGVcIjtcbiAgICAgICAgICB0aGlzLnRhYkltYWdlMiA9IFwicmVzOi8vcGVyc29uYmx1ZVwiO1xuICAgICAgICAgIHRoaXMudGFiSW1hZ2UzID0gXCJyZXM6Ly9zZXR0aW5nYmx1ZVwiO1xuICAgICAgICAgIHRoaXMuY29sb3JUYWIxID0gXCIjMjE5NmYzXCI7XG4gICAgICAgICAgdGhpcy5jb2xvclRhYjIgPSBcIndoaXRlXCI7XG4gICAgICAgICAgdGhpcy5jb2xvclRhYjMgPSBcIndoaXRlXCI7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRhYiA9PSAyKSB7XG4gICAgICAgICAgdGhpcy50YWJJbWFnZTEgPSBcInJlczovL2hvbWVibHVlXCI7XG4gICAgICAgICAgdGhpcy50YWJJbWFnZTIgPSBcInJlczovL3BlcnNvbndoaXRlXCI7XG4gICAgICAgICAgdGhpcy50YWJJbWFnZTMgPSBcInJlczovL3NldHRpbmdibHVlXCI7XG4gICAgICAgICAgdGhpcy5jb2xvclRhYjEgPSBcIndoaXRlXCI7XG4gICAgICAgICAgdGhpcy5jb2xvclRhYjIgPSBcIiMyMTk2ZjNcIjtcbiAgICAgICAgICB0aGlzLmNvbG9yVGFiMyA9IFwid2hpdGVcIjtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGFiID09IDMpIHtcbiAgICAgICAgICB0aGlzLnRhYkltYWdlMSA9IFwicmVzOi8vaG9tZWJsdWVcIjtcbiAgICAgICAgICB0aGlzLnRhYkltYWdlMiA9IFwicmVzOi8vcGVyc29uYmx1ZVwiO1xuICAgICAgICAgIHRoaXMudGFiSW1hZ2UzID0gXCJyZXM6Ly9zZXR0aW5nd2hpdGVcIjtcbiAgICAgICAgICB0aGlzLmNvbG9yVGFiMSA9IFwid2hpdGVcIjtcbiAgICAgICAgICB0aGlzLmNvbG9yVGFiMiA9IFwid2hpdGVcIjtcbiAgICAgICAgICB0aGlzLmNvbG9yVGFiMyA9IFwiIzIxOTZmM1wiO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG4gICAgdGhpcy51c2VyU2VydmljZS5ib3R0b21CYXJEb3RzQ2hhbmdlcy5zdWJzY3JpYmUoKGRvdDogbnVtYmVyKSA9PiB7XG4gICAgICBpZiAoZG90ICE9IHVuZGVmaW5lZCAmJiBkb3QgIT0gbnVsbCAmJiBkb3QgIT0gLTEpIHtcbiAgICAgICAgaWYgKGRvdCA9PSAxKSB7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTEgPSBcInJlczovL2RvdGJsdWVcIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlMiA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UzID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTQgPSBcInJlczovL2RvdGdyZXlcIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlNSA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U2ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTcgPSBcInJlczovL2RvdGdyZXlcIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlOCA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U5ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTEwID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTExID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTEyID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTEzID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTE0ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGRvdCA9PSAyKSB7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTEgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTIgPSBcInJlczovL2RvdGJsdWVcIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlMyA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U0ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTUgPSBcInJlczovL2RvdGdyZXlcIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlNiA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U3ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTggPSBcInJlczovL2RvdGdyZXlcIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlOSA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UxMCA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UxMSA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIC8vIHRoaXMuZG90SW1hZ2UxMiA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIC8vIHRoaXMuZG90SW1hZ2UxMyA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIC8vIHRoaXMuZG90SW1hZ2UxNCA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICB9XG4gICAgICAgIGlmIChkb3QgPT0gMykge1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UxID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UyID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UzID0gXCJyZXM6Ly9kb3RibHVlXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTQgPSBcInJlczovL2RvdGdyZXlcIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlNSA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U2ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTcgPSBcInJlczovL2RvdGdyZXlcIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlOCA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U5ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTEwID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTExID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTEyID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTEzID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTE0ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGRvdCA9PSA0KSB7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTEgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTIgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTMgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTQgPSBcInJlczovL2RvdGJsdWVcIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlNSA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U2ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTcgPSBcInJlczovL2RvdGdyZXlcIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlOCA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U5ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTEwID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTExID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTEyID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTEzID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTE0ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGRvdCA9PSA1KSB7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTEgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTIgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTMgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTQgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTUgPSBcInJlczovL2RvdGJsdWVcIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlNiA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U3ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTggPSBcInJlczovL2RvdGdyZXlcIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlOSA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UxMCA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UxMSA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIC8vIHRoaXMuZG90SW1hZ2UxMiA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIC8vIHRoaXMuZG90SW1hZ2UxMyA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIC8vIHRoaXMuZG90SW1hZ2UxNCA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICB9XG4gICAgICAgIGlmIChkb3QgPT0gNikge1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UxID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UyID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UzID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U0ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U1ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U2ID0gXCJyZXM6Ly9kb3RibHVlXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTcgPSBcInJlczovL2RvdGdyZXlcIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlOCA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U5ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTEwID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTExID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTEyID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTEzID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTE0ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGRvdCA9PSA3KSB7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTEgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTIgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTMgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTQgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTUgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTYgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTcgPSBcInJlczovL2RvdGJsdWVcIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlOCA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U5ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTEwID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTExID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTEyID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTEzID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTE0ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGRvdCA9PSA4KSB7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTEgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTIgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTMgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTQgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTUgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTYgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTcgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTggPSBcInJlczovL2RvdGJsdWVcIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlOSA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UxMCA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UxMSA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIC8vIHRoaXMuZG90SW1hZ2UxMiA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIC8vIHRoaXMuZG90SW1hZ2UxMyA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIC8vIHRoaXMuZG90SW1hZ2UxNCA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICB9XG4gICAgICAgIGlmIChkb3QgPT0gOSkge1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UxID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UyID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UzID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U0ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U1ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U2ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U3ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U4ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U5ID0gXCJyZXM6Ly9kb3RibHVlXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTEwID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTExID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTEyID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTEzID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTE0ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGRvdCA9PSAxMCkge1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UxID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UyID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UzID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U0ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U1ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U2ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U3ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U4ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2U5ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UxMCA9IFwicmVzOi8vZG90Ymx1ZVwiO1xuICAgICAgICAgIHRoaXMuZG90SW1hZ2UxMSA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIC8vIHRoaXMuZG90SW1hZ2UxMiA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIC8vIHRoaXMuZG90SW1hZ2UxMyA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAgIC8vIHRoaXMuZG90SW1hZ2UxNCA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICB9XG4gICAgICAgIGlmIChkb3QgPT0gMTEpIHtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlMSA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlMiA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlMyA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlNCA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlNSA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlNiA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlNyA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlOCA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlOSA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgICB0aGlzLmRvdEltYWdlMTAgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgICAgdGhpcy5kb3RJbWFnZTExID0gXCJyZXM6Ly9kb3RibHVlXCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTEyID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTEzID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgICAgLy8gdGhpcy5kb3RJbWFnZTE0ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgIH1cbiAgICAgICAgLy8gaWYgKGRvdCA9PSAxMikge1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2UxID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2UyID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2UzID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2U0ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2U1ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2U2ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2U3ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2U4ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2U5ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2UxMCA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgLy8gICB0aGlzLmRvdEltYWdlMTEgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgIC8vICAgdGhpcy5kb3RJbWFnZTEyID0gXCJyZXM6Ly9kb3RibHVlXCI7XG4gICAgICAgIC8vICAgdGhpcy5kb3RJbWFnZTEzID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgIC8vICAgdGhpcy5kb3RJbWFnZTE0ID0gXCJyZXM6Ly9kb3RncmV5XCI7XG4gICAgICAgIC8vIH1cbiAgICAgICAgLy8gaWYgKGRvdCA9PSAxMykge1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2UxID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2UyID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2UzID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2U0ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2U1ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2U2ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2U3ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2U4ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2U5ID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2UxMCA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgLy8gICB0aGlzLmRvdEltYWdlMTEgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgIC8vICAgdGhpcy5kb3RJbWFnZTEyID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2UxMyA9IFwicmVzOi8vZG90Ymx1ZVwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2UxNCA9IFwicmVzOi8vZG90Z3JleVwiO1xuICAgICAgICAvLyB9XG4gICAgICAgIC8vIGlmIChkb3QgPT0gMTQpIHtcbiAgICAgICAgLy8gICB0aGlzLmRvdEltYWdlMSA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgLy8gICB0aGlzLmRvdEltYWdlMiA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgLy8gICB0aGlzLmRvdEltYWdlMyA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgLy8gICB0aGlzLmRvdEltYWdlNCA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgLy8gICB0aGlzLmRvdEltYWdlNSA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgLy8gICB0aGlzLmRvdEltYWdlNiA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgLy8gICB0aGlzLmRvdEltYWdlNyA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgLy8gICB0aGlzLmRvdEltYWdlOCA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgLy8gICB0aGlzLmRvdEltYWdlOSA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgLy8gICB0aGlzLmRvdEltYWdlMTAgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgIC8vICAgdGhpcy5kb3RJbWFnZTExID0gXCJyZXM6Ly9kb3RncmVlblwiO1xuICAgICAgICAvLyAgIHRoaXMuZG90SW1hZ2UxMiA9IFwicmVzOi8vZG90Z3JlZW5cIjtcbiAgICAgICAgLy8gICB0aGlzLmRvdEltYWdlMTMgPSBcInJlczovL2RvdGdyZWVuXCI7XG4gICAgICAgIC8vICAgdGhpcy5kb3RJbWFnZTE0ID0gXCJyZXM6Ly9kb3RibHVlXCI7XG4gICAgICAgIC8vIH1cbiAgICAgIH1cbiAgICB9KTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmFjdGlvbkJhckNoYW5nZXMuc3Vic2NyaWJlKChzdGF0ZTogYm9vbGVhbikgPT4ge1xuICAgICAgaWYgKHN0YXRlICE9IHVuZGVmaW5lZCkge1xuICAgICAgICB0aGlzLnNob3dBY3Rpb25CYXIgPSBzdGF0ZTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdHNTdGF0ZS5zdWJzY3JpYmUoKHN0YXRlOiBib29sZWFuKSA9PiB7XG4gICAgICBpZiAoc3RhdGUgIT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHRoaXMuc2hvd0RvdHMgPSBzdGF0ZTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYnZpZXdTdGF0ZS5zdWJzY3JpYmUoKHN0YXRlOiBib29sZWFuKSA9PiB7XG4gICAgICBpZiAoc3RhdGUgIT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHRoaXMuc2hvd1RhYnMgPSBzdGF0ZTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnN3aXRjaHN0YXRlLnN1YnNjcmliZSgoc3RhdGU6IGJvb2xlYW4pID0+IHtcbiAgICAgIGlmIChzdGF0ZSAhPSB1bmRlZmluZWQpIHtcbiAgICAgICAgdGhpcy5zaG93U3dpdGNoID0gc3RhdGU7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBwdWJsaWMgb25Td2l0Y2goYXJncykge1xuICAgIGxldCBjaGVja1N0YXR1cyA9IDxTd2l0Y2g+YXJncy5vYmplY3Q7XG4gICAgaWYgKGNoZWNrU3RhdHVzLmNoZWNrZWQpIHtcbiAgICAgIHRoaXMuc2NyZWVuU3RhdHVzU3dpdGNoID0gXCJzd2l0Y2hFbmFibGVcIjtcbiAgICAgIHRoaXMuc2NyZWVuU3RhdHVzTGFiZWwgPSBcIkRpc2FibGUgKlwiO1xuICAgICAgdGhpcy5zd2l0Y2hMYWJlbENvbG9yID0gXCJibGFja1wiO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNjcmVlblN0YXR1c1N3aXRjaCA9IFwic3dpdGNoRGlzYWJsZVwiO1xuICAgICAgdGhpcy5zY3JlZW5TdGF0dXNMYWJlbCA9IFwiRW5hYmxlXCI7XG4gICAgICB0aGlzLnN3aXRjaExhYmVsQ29sb3IgPSBcIiNBOUE5QTlcIjtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgdGFiQ2xpY2sxKCkge1xuICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvY3JlYXRlTGlzdGluZ1wiXSk7XG4gIH1cbiAgcHVibGljIHRhYkNsaWNrMigpIHtcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3VzZXJBY2NvdW50XCJdKTtcbiAgfVxuICBwdWJsaWMgdGFiQ2xpY2szKCkge1xuICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvcHJlZmVyZW5jZXNEZWZhdWx0c1wiXSk7XG4gIH1cbiAgcHVibGljIGRvdENsaWNrMSgpIHtcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3NrdVwiXSk7XG4gIH1cbiAgcHVibGljIGRvdENsaWNrMigpIHtcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3VwY1wiXSk7XG4gIH1cbiAgcHVibGljIGRvdENsaWNrMygpIHtcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3RpdGxlXCJdKTtcbiAgfVxuICBwdWJsaWMgZG90Q2xpY2s0KCkge1xuICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvc3VnZ2VzdFwiXSk7XG4gIH1cbiAgcHVibGljIGRvdENsaWNrNSgpIHtcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2ZpbmRDYXRlZ29yeVwiXSk7XG4gIH1cbiAgcHVibGljIGRvdENsaWNrNigpIHtcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3N0b3JlQ2F0ZWdvcnlcIl0pO1xuICB9XG4gIHB1YmxpYyBkb3RDbGljazcoKSB7XG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9jb25kaXRpb25cIl0pO1xuICB9XG4gIHB1YmxpYyBkb3RDbGljazgoKSB7XG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9kZXNjcmlwdGlvblwiXSk7XG4gIH1cbiAgcHVibGljIGRvdENsaWNrOSgpIHtcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2l0ZW1zcGVjaWZpZXNcIl0pO1xuICB9XG4gIHB1YmxpYyBkb3RDbGljazEwKCkge1xuICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvYnVzaW5lc3Nwb2xpY2llc1wiXSk7XG4gIH1cbiAgcHVibGljIGRvdENsaWNrMTEoKSB7XG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi93ZWlnaHREaW1lbnNpb25cIl0pO1xuICB9XG4gIC8vIHB1YmxpYyBkb3RDbGljazEyKCkge1xuICAvLyAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvcmV0dXJuT3B0aW9uc1wiXSk7XG4gIC8vIH1cbiAgLy8gcHVibGljIGRvdENsaWNrMTMoKSB7XG4gIC8vICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9idXNpbmVzc3BvbGljaWVzXCJdKTtcbiAgLy8gfVxuICAvLyBwdWJsaWMgZG90Q2xpY2sxNCgpIHtcbiAgLy8gICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3dlaWdodERpbWVuc2lvblwiXSk7XG4gIC8vIH1cbn1cbiJdfQ==
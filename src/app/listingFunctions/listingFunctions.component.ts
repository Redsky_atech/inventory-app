import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { UserService } from "../services/user.service";

@Component({
  selector: "ns-listingFunctions",
  moduleId: module.id,
  templateUrl: "./listingFunctions.component.html",
  styleUrls: ["./listingFunctions.component.css"]
})
export class ListingFunctionsComponent implements OnInit {
  routerExtensions: RouterExtensions;

  constructor(
    routerExtensions: RouterExtensions,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDotState(false);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
    this.userService.switchState(false);
  }
  ngOnInit(): void {}

  public onNewListing() {
    this.routerExtensions.navigate(["/sku"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }

  public onNewTemplateListing() {
    this.routerExtensions.navigate(["/sku"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }

  public onTakePictures() {
    this.routerExtensions.navigate(["/pictures"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }

  public onSubmitListings() {
    this.routerExtensions.navigate(["/submitListings"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }
  public onEditListings() {
    this.routerExtensions.navigate(["/editListings"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }
}

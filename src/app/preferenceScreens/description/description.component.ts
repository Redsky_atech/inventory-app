import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { RouterExtensions } from "nativescript-angular/router";
import { UserService } from "../../services/user.service";

@Component({
  selector: "prefDescription",
  moduleId: module.id,
  templateUrl: "./description.component.html",
  styleUrls: ["./description.component.css"]
})
export class PrefDescriptionComponent implements OnInit {
  routerExtensions: RouterExtensions;

  constructor(
    routerExtensions: RouterExtensions,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDots(8);
    this.userService.bottomBarDotState(true);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
    this.userService.switchState(true);
  }

  ngOnInit(): void {}

  onSwipe(args: SwipeGestureEventData) {
    if (args.direction == 1) {
      this.routerExtensions.navigate(["/prefCondition"]);
    }
    if (args.direction == 2) {
      this.routerExtensions.navigate(["prefItemspecifies"]);
    }
  }
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_service_1 = require("../../../services/user.service");
var router_1 = require("@angular/router");
var PrefStoreCategoryComponent = /** @class */ (function () {
    function PrefStoreCategoryComponent(router, userService) {
        this.router = router;
        this.userService = userService;
        this.category1 = false;
        this.category2 = false;
        this.category3 = false;
        this.category4 = false;
        this.category5 = false;
        this.storeCategories = [
            "Category 1",
            "Category 2",
            "Category 3",
            "Category 4",
            "Category 5"
        ];
        this.storeCategory1 = "";
        this.storeCategory2 = "";
        this.storeCategory3 = "";
        this.storeCategory4 = "";
        this.storeCategory5 = "";
        this.userService.actionBarState(true);
        this.userService.bottomBarDots(6);
        this.userService.bottomBarDotState(true);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.userService.switchState(true);
        this.storeCategory1 = this.storeCategories[0];
        this.storeCategory2 = this.storeCategories[1];
        this.storeCategory3 = this.storeCategories[2];
        this.storeCategory4 = this.storeCategories[3];
        this.storeCategory5 = this.storeCategories[4];
    }
    PrefStoreCategoryComponent.prototype.categoryClick1 = function () {
        if (this.category1 == false) {
            this.category1 = true;
            this.category2 = false;
            this.category3 = false;
            this.category4 = false;
            this.category5 = false;
        }
    };
    PrefStoreCategoryComponent.prototype.categoryClick2 = function () {
        if (this.category2 == false) {
            this.category1 = false;
            this.category2 = true;
            this.category3 = false;
            this.category4 = false;
            this.category5 = false;
        }
    };
    PrefStoreCategoryComponent.prototype.categoryClick3 = function () {
        if (this.category3 == false) {
            this.category1 = false;
            this.category2 = false;
            this.category3 = true;
            this.category4 = false;
            this.category5 = false;
        }
    };
    PrefStoreCategoryComponent.prototype.categoryClick4 = function () {
        if (this.category4 == false) {
            this.category1 = false;
            this.category2 = false;
            this.category3 = false;
            this.category4 = true;
            this.category5 = false;
        }
    };
    PrefStoreCategoryComponent.prototype.categoryClick5 = function () {
        if (this.category5 == false) {
            this.category1 = false;
            this.category2 = false;
            this.category3 = false;
            this.category4 = false;
            this.category5 = true;
        }
    };
    PrefStoreCategoryComponent.prototype.onSwipe = function (args) {
        if (args.direction == 1) {
            this.router.navigate(["/prefFindCategory"]);
        }
        if (args.direction == 2) {
            if (this.category1 == true) {
                var navigationExtras_1 = {
                    queryParams: { storeCategory: " category1" }
                };
                this.router.navigate(["/prefCondition"], navigationExtras_1);
            }
            if (this.category2 == true) {
                var navigationExtras_2 = {
                    queryParams: { storeCategory: " category2" }
                };
                this.router.navigate(["/prefCondition"], navigationExtras_2);
            }
            if (this.category3 == true) {
                var navigationExtras_3 = {
                    queryParams: { storeCategory: " category3" }
                };
                this.router.navigate(["/prefCondition"], navigationExtras_3);
            }
            if (this.category4 == true) {
                var navigationExtras_4 = {
                    queryParams: { storeCategory: " category4" }
                };
                this.router.navigate(["/prefCondition"], navigationExtras_4);
            }
            if (this.category5 == true) {
                var navigationExtras_5 = {
                    queryParams: { storeCategory: " category5" }
                };
                this.router.navigate(["/prefCondition"], navigationExtras_5);
            }
            var navigationExtras = {
                queryParams: { storeCategory: "" }
            };
            this.router.navigate(["/prefCondition"], navigationExtras);
        }
    };
    PrefStoreCategoryComponent = __decorate([
        core_1.Component({
            selector: "prefStoreCategory",
            moduleId: module.id,
            templateUrl: "./store-categories.component.html",
            styleUrls: ["./store-categories.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.Router, user_service_1.UserService])
    ], PrefStoreCategoryComponent);
    return PrefStoreCategoryComponent;
}());
exports.PrefStoreCategoryComponent = PrefStoreCategoryComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUtY2F0ZWdvcmllcy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS1jYXRlZ29yaWVzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF5RTtBQUV6RSwrREFBNkQ7QUFDN0QsMENBQTJEO0FBTzNEO0lBa0JFLG9DQUFvQixNQUFjLEVBQVUsV0FBd0I7UUFBaEQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBakJwRSxjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDbEIsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNsQixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDWCxvQkFBZSxHQUFHO1lBQ3ZCLFlBQVk7WUFDWixZQUFZO1lBQ1osWUFBWTtZQUNaLFlBQVk7WUFDWixZQUFZO1NBQ2IsQ0FBQztRQUNGLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBRWxCLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxtREFBYyxHQUFkO1FBQ0UsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBRTtZQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztTQUN4QjtJQUNILENBQUM7SUFDRCxtREFBYyxHQUFkO1FBQ0UsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBRTtZQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztTQUN4QjtJQUNILENBQUM7SUFDRCxtREFBYyxHQUFkO1FBQ0UsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBRTtZQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztTQUN4QjtJQUNILENBQUM7SUFDRCxtREFBYyxHQUFkO1FBQ0UsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBRTtZQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztTQUN4QjtJQUNILENBQUM7SUFDRCxtREFBYyxHQUFkO1FBQ0UsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBRTtZQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztTQUN2QjtJQUNILENBQUM7SUFFRCw0Q0FBTyxHQUFQLFVBQVEsSUFBMkI7UUFDakMsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsRUFBRTtZQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztTQUM3QztRQUNELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksRUFBRTtnQkFDMUIsSUFBSSxrQkFBZ0IsR0FBcUI7b0JBQ3ZDLFdBQVcsRUFBRSxFQUFFLGFBQWEsRUFBRSxZQUFZLEVBQUU7aUJBQzdDLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLGtCQUFnQixDQUFDLENBQUM7YUFDNUQ7WUFDRCxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxFQUFFO2dCQUMxQixJQUFJLGtCQUFnQixHQUFxQjtvQkFDdkMsV0FBVyxFQUFFLEVBQUUsYUFBYSxFQUFFLFlBQVksRUFBRTtpQkFDN0MsQ0FBQztnQkFDRixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEVBQUUsa0JBQWdCLENBQUMsQ0FBQzthQUM1RDtZQUNELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLEVBQUU7Z0JBQzFCLElBQUksa0JBQWdCLEdBQXFCO29CQUN2QyxXQUFXLEVBQUUsRUFBRSxhQUFhLEVBQUUsWUFBWSxFQUFFO2lCQUM3QyxDQUFDO2dCQUNGLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxrQkFBZ0IsQ0FBQyxDQUFDO2FBQzVEO1lBQ0QsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksRUFBRTtnQkFDMUIsSUFBSSxrQkFBZ0IsR0FBcUI7b0JBQ3ZDLFdBQVcsRUFBRSxFQUFFLGFBQWEsRUFBRSxZQUFZLEVBQUU7aUJBQzdDLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLGtCQUFnQixDQUFDLENBQUM7YUFDNUQ7WUFDRCxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxFQUFFO2dCQUMxQixJQUFJLGtCQUFnQixHQUFxQjtvQkFDdkMsV0FBVyxFQUFFLEVBQUUsYUFBYSxFQUFFLFlBQVksRUFBRTtpQkFDN0MsQ0FBQztnQkFDRixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEVBQUUsa0JBQWdCLENBQUMsQ0FBQzthQUM1RDtZQUNELElBQUksZ0JBQWdCLEdBQXFCO2dCQUN2QyxXQUFXLEVBQUUsRUFBRSxhQUFhLEVBQUUsRUFBRSxFQUFFO2FBQ25DLENBQUM7WUFDRixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztTQUM1RDtJQUNILENBQUM7SUF0SFUsMEJBQTBCO1FBTnRDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsbUJBQW1CO1lBQzdCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsbUNBQW1DO1lBQ2hELFNBQVMsRUFBRSxDQUFDLGtDQUFrQyxDQUFDO1NBQ2hELENBQUM7eUNBbUI0QixlQUFNLEVBQXVCLDBCQUFXO09BbEJ6RCwwQkFBMEIsQ0F1SHRDO0lBQUQsaUNBQUM7Q0FBQSxBQXZIRCxJQXVIQztBQXZIWSxnRUFBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBFbGVtZW50UmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgU3dpcGVHZXN0dXJlRXZlbnREYXRhIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZ2VzdHVyZXNcIjtcclxuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vLi4vLi4vc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IFJvdXRlciwgTmF2aWdhdGlvbkV4dHJhcyB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwicHJlZlN0b3JlQ2F0ZWdvcnlcIixcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vc3RvcmUtY2F0ZWdvcmllcy5jb21wb25lbnQuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wiLi9zdG9yZS1jYXRlZ29yaWVzLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIFByZWZTdG9yZUNhdGVnb3J5Q29tcG9uZW50IHtcclxuICBjYXRlZ29yeTEgPSBmYWxzZTtcclxuICBjYXRlZ29yeTIgPSBmYWxzZTtcclxuICBjYXRlZ29yeTMgPSBmYWxzZTtcclxuICBjYXRlZ29yeTQgPSBmYWxzZTtcclxuICBjYXRlZ29yeTUgPSBmYWxzZTtcclxuICBwdWJsaWMgc3RvcmVDYXRlZ29yaWVzID0gW1xyXG4gICAgXCJDYXRlZ29yeSAxXCIsXHJcbiAgICBcIkNhdGVnb3J5IDJcIixcclxuICAgIFwiQ2F0ZWdvcnkgM1wiLFxyXG4gICAgXCJDYXRlZ29yeSA0XCIsXHJcbiAgICBcIkNhdGVnb3J5IDVcIlxyXG4gIF07XHJcbiAgc3RvcmVDYXRlZ29yeTEgPSBcIlwiO1xyXG4gIHN0b3JlQ2F0ZWdvcnkyID0gXCJcIjtcclxuICBzdG9yZUNhdGVnb3J5MyA9IFwiXCI7XHJcbiAgc3RvcmVDYXRlZ29yeTQgPSBcIlwiO1xyXG4gIHN0b3JlQ2F0ZWdvcnk1ID0gXCJcIjtcclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyLCBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSkge1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5hY3Rpb25CYXJTdGF0ZSh0cnVlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYm90dG9tQmFyRG90cyg2KTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYm90dG9tQmFyRG90U3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZSh0cnVlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UudGFiVmlld1N0YXRlQ2hhbmdlcygxKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2Uuc3dpdGNoU3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnN0b3JlQ2F0ZWdvcnkxID0gdGhpcy5zdG9yZUNhdGVnb3JpZXNbMF07XHJcbiAgICB0aGlzLnN0b3JlQ2F0ZWdvcnkyID0gdGhpcy5zdG9yZUNhdGVnb3JpZXNbMV07XHJcbiAgICB0aGlzLnN0b3JlQ2F0ZWdvcnkzID0gdGhpcy5zdG9yZUNhdGVnb3JpZXNbMl07XHJcbiAgICB0aGlzLnN0b3JlQ2F0ZWdvcnk0ID0gdGhpcy5zdG9yZUNhdGVnb3JpZXNbM107XHJcbiAgICB0aGlzLnN0b3JlQ2F0ZWdvcnk1ID0gdGhpcy5zdG9yZUNhdGVnb3JpZXNbNF07XHJcbiAgfVxyXG5cclxuICBjYXRlZ29yeUNsaWNrMSgpIHtcclxuICAgIGlmICh0aGlzLmNhdGVnb3J5MSA9PSBmYWxzZSkge1xyXG4gICAgICB0aGlzLmNhdGVnb3J5MSA9IHRydWU7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnkyID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnkzID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnk0ID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnk1ID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGNhdGVnb3J5Q2xpY2syKCkge1xyXG4gICAgaWYgKHRoaXMuY2F0ZWdvcnkyID09IGZhbHNlKSB7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnkxID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnkyID0gdHJ1ZTtcclxuICAgICAgdGhpcy5jYXRlZ29yeTMgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jYXRlZ29yeTQgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jYXRlZ29yeTUgPSBmYWxzZTtcclxuICAgIH1cclxuICB9XHJcbiAgY2F0ZWdvcnlDbGljazMoKSB7XHJcbiAgICBpZiAodGhpcy5jYXRlZ29yeTMgPT0gZmFsc2UpIHtcclxuICAgICAgdGhpcy5jYXRlZ29yeTEgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jYXRlZ29yeTIgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jYXRlZ29yeTMgPSB0cnVlO1xyXG4gICAgICB0aGlzLmNhdGVnb3J5NCA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNhdGVnb3J5NSA9IGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuICBjYXRlZ29yeUNsaWNrNCgpIHtcclxuICAgIGlmICh0aGlzLmNhdGVnb3J5NCA9PSBmYWxzZSkge1xyXG4gICAgICB0aGlzLmNhdGVnb3J5MSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNhdGVnb3J5MiA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNhdGVnb3J5MyA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNhdGVnb3J5NCA9IHRydWU7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnk1ID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGNhdGVnb3J5Q2xpY2s1KCkge1xyXG4gICAgaWYgKHRoaXMuY2F0ZWdvcnk1ID09IGZhbHNlKSB7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnkxID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnkyID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnkzID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnk0ID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY2F0ZWdvcnk1ID0gdHJ1ZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uU3dpcGUoYXJnczogU3dpcGVHZXN0dXJlRXZlbnREYXRhKSB7XHJcbiAgICBpZiAoYXJncy5kaXJlY3Rpb24gPT0gMSkge1xyXG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCIvcHJlZkZpbmRDYXRlZ29yeVwiXSk7XHJcbiAgICB9XHJcbiAgICBpZiAoYXJncy5kaXJlY3Rpb24gPT0gMikge1xyXG4gICAgICBpZiAodGhpcy5jYXRlZ29yeTEgPT0gdHJ1ZSkge1xyXG4gICAgICAgIGxldCBuYXZpZ2F0aW9uRXh0cmFzOiBOYXZpZ2F0aW9uRXh0cmFzID0ge1xyXG4gICAgICAgICAgcXVlcnlQYXJhbXM6IHsgc3RvcmVDYXRlZ29yeTogXCIgY2F0ZWdvcnkxXCIgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1wiL3ByZWZDb25kaXRpb25cIl0sIG5hdmlnYXRpb25FeHRyYXMpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLmNhdGVnb3J5MiA9PSB0cnVlKSB7XHJcbiAgICAgICAgbGV0IG5hdmlnYXRpb25FeHRyYXM6IE5hdmlnYXRpb25FeHRyYXMgPSB7XHJcbiAgICAgICAgICBxdWVyeVBhcmFtczogeyBzdG9yZUNhdGVnb3J5OiBcIiBjYXRlZ29yeTJcIiB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCIvcHJlZkNvbmRpdGlvblwiXSwgbmF2aWdhdGlvbkV4dHJhcyk7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHRoaXMuY2F0ZWdvcnkzID09IHRydWUpIHtcclxuICAgICAgICBsZXQgbmF2aWdhdGlvbkV4dHJhczogTmF2aWdhdGlvbkV4dHJhcyA9IHtcclxuICAgICAgICAgIHF1ZXJ5UGFyYW1zOiB7IHN0b3JlQ2F0ZWdvcnk6IFwiIGNhdGVnb3J5M1wiIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtcIi9wcmVmQ29uZGl0aW9uXCJdLCBuYXZpZ2F0aW9uRXh0cmFzKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAodGhpcy5jYXRlZ29yeTQgPT0gdHJ1ZSkge1xyXG4gICAgICAgIGxldCBuYXZpZ2F0aW9uRXh0cmFzOiBOYXZpZ2F0aW9uRXh0cmFzID0ge1xyXG4gICAgICAgICAgcXVlcnlQYXJhbXM6IHsgc3RvcmVDYXRlZ29yeTogXCIgY2F0ZWdvcnk0XCIgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1wiL3ByZWZDb25kaXRpb25cIl0sIG5hdmlnYXRpb25FeHRyYXMpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLmNhdGVnb3J5NSA9PSB0cnVlKSB7XHJcbiAgICAgICAgbGV0IG5hdmlnYXRpb25FeHRyYXM6IE5hdmlnYXRpb25FeHRyYXMgPSB7XHJcbiAgICAgICAgICBxdWVyeVBhcmFtczogeyBzdG9yZUNhdGVnb3J5OiBcIiBjYXRlZ29yeTVcIiB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCIvcHJlZkNvbmRpdGlvblwiXSwgbmF2aWdhdGlvbkV4dHJhcyk7XHJcbiAgICAgIH1cclxuICAgICAgbGV0IG5hdmlnYXRpb25FeHRyYXM6IE5hdmlnYXRpb25FeHRyYXMgPSB7XHJcbiAgICAgICAgcXVlcnlQYXJhbXM6IHsgc3RvcmVDYXRlZ29yeTogXCJcIiB9XHJcbiAgICAgIH07XHJcbiAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtcIi9wcmVmQ29uZGl0aW9uXCJdLCBuYXZpZ2F0aW9uRXh0cmFzKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19
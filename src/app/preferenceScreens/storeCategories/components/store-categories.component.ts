import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { UserService } from "../../../services/user.service";
import { Router, NavigationExtras } from "@angular/router";
@Component({
  selector: "prefStoreCategory",
  moduleId: module.id,
  templateUrl: "./store-categories.component.html",
  styleUrls: ["./store-categories.component.css"]
})
export class PrefStoreCategoryComponent {
  category1 = false;
  category2 = false;
  category3 = false;
  category4 = false;
  category5 = false;
  public storeCategories = [
    "Category 1",
    "Category 2",
    "Category 3",
    "Category 4",
    "Category 5"
  ];
  storeCategory1 = "";
  storeCategory2 = "";
  storeCategory3 = "";
  storeCategory4 = "";
  storeCategory5 = "";
  constructor(private router: Router, private userService: UserService) {
    this.userService.actionBarState(true);
    this.userService.bottomBarDots(6);
    this.userService.bottomBarDotState(true);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
    this.userService.switchState(true);
    this.storeCategory1 = this.storeCategories[0];
    this.storeCategory2 = this.storeCategories[1];
    this.storeCategory3 = this.storeCategories[2];
    this.storeCategory4 = this.storeCategories[3];
    this.storeCategory5 = this.storeCategories[4];
  }

  categoryClick1() {
    if (this.category1 == false) {
      this.category1 = true;
      this.category2 = false;
      this.category3 = false;
      this.category4 = false;
      this.category5 = false;
    }
  }
  categoryClick2() {
    if (this.category2 == false) {
      this.category1 = false;
      this.category2 = true;
      this.category3 = false;
      this.category4 = false;
      this.category5 = false;
    }
  }
  categoryClick3() {
    if (this.category3 == false) {
      this.category1 = false;
      this.category2 = false;
      this.category3 = true;
      this.category4 = false;
      this.category5 = false;
    }
  }
  categoryClick4() {
    if (this.category4 == false) {
      this.category1 = false;
      this.category2 = false;
      this.category3 = false;
      this.category4 = true;
      this.category5 = false;
    }
  }
  categoryClick5() {
    if (this.category5 == false) {
      this.category1 = false;
      this.category2 = false;
      this.category3 = false;
      this.category4 = false;
      this.category5 = true;
    }
  }

  onSwipe(args: SwipeGestureEventData) {
    if (args.direction == 1) {
      this.router.navigate(["/prefFindCategory"]);
    }
    if (args.direction == 2) {
      if (this.category1 == true) {
        let navigationExtras: NavigationExtras = {
          queryParams: { storeCategory: " category1" }
        };
        this.router.navigate(["/prefCondition"], navigationExtras);
      }
      if (this.category2 == true) {
        let navigationExtras: NavigationExtras = {
          queryParams: { storeCategory: " category2" }
        };
        this.router.navigate(["/prefCondition"], navigationExtras);
      }
      if (this.category3 == true) {
        let navigationExtras: NavigationExtras = {
          queryParams: { storeCategory: " category3" }
        };
        this.router.navigate(["/prefCondition"], navigationExtras);
      }
      if (this.category4 == true) {
        let navigationExtras: NavigationExtras = {
          queryParams: { storeCategory: " category4" }
        };
        this.router.navigate(["/prefCondition"], navigationExtras);
      }
      if (this.category5 == true) {
        let navigationExtras: NavigationExtras = {
          queryParams: { storeCategory: " category5" }
        };
        this.router.navigate(["/prefCondition"], navigationExtras);
      }
      let navigationExtras: NavigationExtras = {
        queryParams: { storeCategory: "" }
      };
      this.router.navigate(["/prefCondition"], navigationExtras);
    }
  }
}

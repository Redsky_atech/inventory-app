import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { registerElement } from "nativescript-angular/element-registry";
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page/page";
import { UserService } from "../../services/user.service";
import { View } from "tns-core-modules/ui/core/view";

@Component({
  selector: "prefTitle",
  moduleId: module.id,
  templateUrl: "./title.component.html",
  styleUrls: ["./title.component.css"]
})
export class PrefTitleComponent implements OnInit {
  // @ViewChild("mainContainer")
  // MainGrid: ElementRef;

  routerExtensions: RouterExtensions;

  constructor(
    routerExtensions: RouterExtensions,
    private activatedRoute: ActivatedRoute,
    private page: Page,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.bottomBarDots(3);
    this.userService.actionBarState(true);
    this.userService.bottomBarDotState(true);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
    this.userService.switchState(true);

    // view: <View>this.MainGrid.nativeElement;
    // view.isUserInteractionEnabled = false;
  }

  ngOnInit(): void {}

  onSwipe(args: SwipeGestureEventData) {
    if (args.direction == 1) {
      this.routerExtensions.navigate(["/prefTitle"]);
    }
    if (args.direction == 2) {
      this.routerExtensions.navigate(["/prefSuggest"]);
    }
  }
}

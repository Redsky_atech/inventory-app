"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var router_2 = require("@angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var user_service_1 = require("../../services/user.service");
var PrefTitleComponent = /** @class */ (function () {
    function PrefTitleComponent(routerExtensions, activatedRoute, page, userService) {
        this.activatedRoute = activatedRoute;
        this.page = page;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.userService.bottomBarDots(3);
        this.userService.actionBarState(true);
        this.userService.bottomBarDotState(true);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.userService.switchState(true);
        // view: <View>this.MainGrid.nativeElement;
        // view.isUserInteractionEnabled = false;
    }
    PrefTitleComponent.prototype.ngOnInit = function () { };
    PrefTitleComponent.prototype.onSwipe = function (args) {
        if (args.direction == 1) {
            this.routerExtensions.navigate(["/prefTitle"]);
        }
        if (args.direction == 2) {
            this.routerExtensions.navigate(["/prefSuggest"]);
        }
    };
    PrefTitleComponent = __decorate([
        core_1.Component({
            selector: "prefTitle",
            moduleId: module.id,
            templateUrl: "./title.component.html",
            styleUrls: ["./title.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            router_2.ActivatedRoute,
            page_1.Page,
            user_service_1.UserService])
    ], PrefTitleComponent);
    return PrefTitleComponent;
}());
exports.PrefTitleComponent = PrefTitleComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGl0bGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidGl0bGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlFO0FBR3pFLHNEQUErRDtBQUMvRCwwQ0FBaUQ7QUFDakQsc0RBQXFEO0FBQ3JELDREQUEwRDtBQVMxRDtJQU1FLDRCQUNFLGdCQUFrQyxFQUMxQixjQUE4QixFQUM5QixJQUFVLEVBQ1YsV0FBd0I7UUFGeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLFNBQUksR0FBSixJQUFJLENBQU07UUFDVixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUVoQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7UUFDekMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRW5DLDJDQUEyQztRQUMzQyx5Q0FBeUM7SUFDM0MsQ0FBQztJQUVELHFDQUFRLEdBQVIsY0FBa0IsQ0FBQztJQUVuQixvQ0FBTyxHQUFQLFVBQVEsSUFBMkI7UUFDakMsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsRUFBRTtZQUN2QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztTQUNoRDtRQUNELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7U0FDbEQ7SUFDSCxDQUFDO0lBakNVLGtCQUFrQjtRQU45QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFdBQVc7WUFDckIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSx3QkFBd0I7WUFDckMsU0FBUyxFQUFFLENBQUMsdUJBQXVCLENBQUM7U0FDckMsQ0FBQzt5Q0FRb0IseUJBQWdCO1lBQ1YsdUJBQWM7WUFDeEIsV0FBSTtZQUNHLDBCQUFXO09BVnZCLGtCQUFrQixDQWtDOUI7SUFBRCx5QkFBQztDQUFBLEFBbENELElBa0NDO0FBbENZLGdEQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBTd2lwZUdlc3R1cmVFdmVudERhdGEgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9nZXN0dXJlc1wiO1xyXG5pbXBvcnQgeyByZWdpc3RlckVsZW1lbnQgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZWxlbWVudC1yZWdpc3RyeVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2UvcGFnZVwiO1xyXG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuLi8uLi9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgVmlldyB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2NvcmUvdmlld1wiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwicHJlZlRpdGxlXCIsXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICB0ZW1wbGF0ZVVybDogXCIuL3RpdGxlLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL3RpdGxlLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIFByZWZUaXRsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgLy8gQFZpZXdDaGlsZChcIm1haW5Db250YWluZXJcIilcclxuICAvLyBNYWluR3JpZDogRWxlbWVudFJlZjtcclxuXHJcbiAgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucztcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLFxyXG4gICAgcHJpdmF0ZSBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICBwcml2YXRlIHBhZ2U6IFBhZ2UsXHJcbiAgICBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZVxyXG4gICkge1xyXG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zID0gcm91dGVyRXh0ZW5zaW9ucztcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYm90dG9tQmFyRG90cygzKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYWN0aW9uQmFyU3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdFN0YXRlKHRydWUpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZUNoYW5nZXMoMSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnN3aXRjaFN0YXRlKHRydWUpO1xyXG5cclxuICAgIC8vIHZpZXc6IDxWaWV3PnRoaXMuTWFpbkdyaWQubmF0aXZlRWxlbWVudDtcclxuICAgIC8vIHZpZXcuaXNVc2VySW50ZXJhY3Rpb25FbmFibGVkID0gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHt9XHJcblxyXG4gIG9uU3dpcGUoYXJnczogU3dpcGVHZXN0dXJlRXZlbnREYXRhKSB7XHJcbiAgICBpZiAoYXJncy5kaXJlY3Rpb24gPT0gMSkge1xyXG4gICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3ByZWZUaXRsZVwiXSk7XHJcbiAgICB9XHJcbiAgICBpZiAoYXJncy5kaXJlY3Rpb24gPT0gMikge1xyXG4gICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3ByZWZTdWdnZXN0XCJdKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var angular_1 = require("nativescript-drop-down/angular");
var router_1 = require("nativescript-angular/router");
var user_service_1 = require("../../services/user.service");
var PrefSuggestedCategoriesComponent = /** @class */ (function () {
    function PrefSuggestedCategoriesComponent(dropDownmodule, routerExtensions, userService) {
        this.dropDownmodule = dropDownmodule;
        this.userService = userService;
        this.categoriesArray = ["Home", "Mindful", "Kitchen"];
        this.categories = "";
        this.category = "";
        this.routerExtensions = routerExtensions;
        this.userService.actionBarState(true);
        this.userService.bottomBarDots(4);
        this.userService.bottomBarDotState(true);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.userService.switchState(true);
        var str = this.categories;
        var str1 = " > ";
        var arrLength = this.categoriesArray.length;
        for (var i = 0; i < arrLength; i++) {
            str = str.concat(this.categoriesArray[i]);
            if (i < arrLength - 1) {
                str = str.concat(str1);
            }
            this.categories = str;
        }
        this.category = this.categories;
    }
    PrefSuggestedCategoriesComponent.prototype.ngOnInit = function () { };
    PrefSuggestedCategoriesComponent.prototype.ngAfterViewInit = function () { };
    PrefSuggestedCategoriesComponent.prototype.onSwipe = function (args) {
        if (args.direction == 1) {
            this.routerExtensions.navigate(["/prefTitle"]);
        }
        if (args.direction == 2) {
            this.routerExtensions.navigate(["/prefFindCategory"]);
        }
    };
    PrefSuggestedCategoriesComponent = __decorate([
        core_1.Component({
            selector: "Prefsuggest",
            moduleId: module.id,
            templateUrl: "./suggestedcategories.component.html",
            styleUrls: ["./suggestedcategories.component.css"]
        }),
        __metadata("design:paramtypes", [angular_1.DropDownModule,
            router_1.RouterExtensions,
            user_service_1.UserService])
    ], PrefSuggestedCategoriesComponent);
    return PrefSuggestedCategoriesComponent;
}());
exports.PrefSuggestedCategoriesComponent = PrefSuggestedCategoriesComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3VnZ2VzdGVkY2F0ZWdvcmllcy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdWdnZXN0ZWRjYXRlZ29yaWVzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFpRTtBQUVqRSwwREFBZ0U7QUFFaEUsc0RBQStEO0FBQy9ELDREQUEwRDtBQVExRDtJQVFFLDBDQUNVLGNBQThCLEVBQ3RDLGdCQUFrQyxFQUMxQixXQUF3QjtRQUZ4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFFOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFWbEMsb0JBQWUsR0FBRyxDQUFDLE1BQU0sRUFBRSxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFFakQsZUFBVSxHQUFXLEVBQUUsQ0FBQztRQUV4QixhQUFRLEdBQUcsRUFBRSxDQUFDO1FBUVosSUFBSSxDQUFDLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUVuQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQzFCLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQztRQUNqQixJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQztRQUM1QyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ2xDLEdBQUcsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsR0FBRyxTQUFTLEdBQUcsQ0FBQyxFQUFFO2dCQUNyQixHQUFHLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN4QjtZQUNELElBQUksQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDO1NBQ3ZCO1FBRUQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ2xDLENBQUM7SUFFRCxtREFBUSxHQUFSLGNBQWtCLENBQUM7SUFFbkIsMERBQWUsR0FBZixjQUFtQixDQUFDO0lBRXBCLGtEQUFPLEdBQVAsVUFBUSxJQUEyQjtRQUNqQyxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1NBQ2hEO1FBQ0QsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsRUFBRTtZQUN2QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDO1NBQ3ZEO0lBQ0gsQ0FBQztJQTlDVSxnQ0FBZ0M7UUFONUMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxhQUFhO1lBQ3ZCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsc0NBQXNDO1lBQ25ELFNBQVMsRUFBRSxDQUFDLHFDQUFxQyxDQUFDO1NBQ25ELENBQUM7eUNBVTBCLHdCQUFjO1lBQ3BCLHlCQUFnQjtZQUNiLDBCQUFXO09BWHZCLGdDQUFnQyxDQStDNUM7SUFBRCx1Q0FBQztDQUFBLEFBL0NELElBK0NDO0FBL0NZLDRFQUFnQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBBZnRlclZpZXdJbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgU3dpcGVHZXN0dXJlRXZlbnREYXRhIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZ2VzdHVyZXNcIjtcclxuaW1wb3J0IHsgRHJvcERvd25Nb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWRyb3AtZG93bi9hbmd1bGFyXCI7XHJcbmltcG9ydCB7IFNlbGVjdGVkSW5kZXhDaGFuZ2VkRXZlbnREYXRhIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1kcm9wLWRvd25cIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vLi4vc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJQcmVmc3VnZ2VzdFwiLFxyXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9zdWdnZXN0ZWRjYXRlZ29yaWVzLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL3N1Z2dlc3RlZGNhdGVnb3JpZXMuY29tcG9uZW50LmNzc1wiXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUHJlZlN1Z2dlc3RlZENhdGVnb3JpZXNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQge1xyXG4gIGNhdGVnb3JpZXNBcnJheSA9IFtcIkhvbWVcIiwgXCJNaW5kZnVsXCIsIFwiS2l0Y2hlblwiXTtcclxuXHJcbiAgY2F0ZWdvcmllczogc3RyaW5nID0gXCJcIjtcclxuXHJcbiAgY2F0ZWdvcnkgPSBcIlwiO1xyXG4gIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnM7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBkcm9wRG93bm1vZHVsZTogRHJvcERvd25Nb2R1bGUsXHJcbiAgICByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLFxyXG4gICAgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2VcclxuICApIHtcclxuICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucyA9IHJvdXRlckV4dGVuc2lvbnM7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmFjdGlvbkJhclN0YXRlKHRydWUpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5ib3R0b21CYXJEb3RzKDQpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5ib3R0b21CYXJEb3RTdGF0ZSh0cnVlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UudGFiVmlld1N0YXRlKHRydWUpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGVDaGFuZ2VzKDEpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5zd2l0Y2hTdGF0ZSh0cnVlKTtcclxuICAgIFxyXG4gICAgdmFyIHN0ciA9IHRoaXMuY2F0ZWdvcmllcztcclxuICAgIHZhciBzdHIxID0gXCIgPiBcIjtcclxuICAgIHZhciBhcnJMZW5ndGggPSB0aGlzLmNhdGVnb3JpZXNBcnJheS5sZW5ndGg7XHJcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGFyckxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIHN0ciA9IHN0ci5jb25jYXQodGhpcy5jYXRlZ29yaWVzQXJyYXlbaV0pO1xyXG4gICAgICBpZiAoaSA8IGFyckxlbmd0aCAtIDEpIHtcclxuICAgICAgICBzdHIgPSBzdHIuY29uY2F0KHN0cjEpO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuY2F0ZWdvcmllcyA9IHN0cjtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmNhdGVnb3J5ID0gdGhpcy5jYXRlZ29yaWVzO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7fVxyXG5cclxuICBuZ0FmdGVyVmlld0luaXQoKSB7fVxyXG5cclxuICBvblN3aXBlKGFyZ3M6IFN3aXBlR2VzdHVyZUV2ZW50RGF0YSkge1xyXG4gICAgaWYgKGFyZ3MuZGlyZWN0aW9uID09IDEpIHtcclxuICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9wcmVmVGl0bGVcIl0pO1xyXG4gICAgfVxyXG4gICAgaWYgKGFyZ3MuZGlyZWN0aW9uID09IDIpIHtcclxuICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9wcmVmRmluZENhdGVnb3J5XCJdKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19
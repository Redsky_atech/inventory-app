"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_service_1 = require("../../services/user.service");
var router_1 = require("@angular/router");
var PrefConditionComponent = /** @class */ (function () {
    function PrefConditionComponent(route, router, userService) {
        var _this = this;
        this.route = route;
        this.router = router;
        this.userService = userService;
        this.condition1 = true;
        this.condition2 = false;
        this.condition3 = false;
        this.condition4 = false;
        this.condition5 = false;
        this.condition6 = false;
        this.storeCategory = "";
        this.isVisibleConditions = false;
        this.userService.actionBarState(true);
        this.userService.bottomBarDots(7);
        this.userService.bottomBarDotState(true);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.userService.switchState(true);
        route.queryParams.subscribe(function (params) {
            _this.storeCategory = params["storeCategory"];
        });
        console.log(this.storeCategory);
    }
    PrefConditionComponent.prototype.ngOnInit = function () { };
    PrefConditionComponent.prototype.conditionClick1 = function () {
        if (this.condition1 == false) {
            this.condition1 = true;
            this.condition2 = false;
            this.condition3 = false;
            this.condition4 = false;
            this.condition5 = false;
            this.condition6 = false;
        }
    };
    PrefConditionComponent.prototype.conditionClick2 = function () {
        if (this.condition2 == false) {
            this.condition1 = false;
            this.condition2 = true;
            this.condition3 = false;
            this.condition4 = false;
            this.condition5 = false;
            this.condition6 = false;
        }
    };
    PrefConditionComponent.prototype.conditionClick3 = function () {
        if (this.condition3 == false) {
            this.condition1 = false;
            this.condition2 = false;
            this.condition3 = true;
            this.condition4 = false;
            this.condition5 = false;
            this.condition6 = false;
        }
    };
    PrefConditionComponent.prototype.conditionClick4 = function () {
        if (this.condition4 == false) {
            this.condition1 = false;
            this.condition2 = false;
            this.condition3 = false;
            this.condition4 = true;
            this.condition5 = false;
            this.condition6 = false;
        }
    };
    PrefConditionComponent.prototype.conditionClick5 = function () {
        if (this.condition5 == false) {
            this.condition1 = false;
            this.condition2 = false;
            this.condition3 = false;
            this.condition4 = false;
            this.condition5 = true;
            this.condition6 = false;
        }
    };
    PrefConditionComponent.prototype.conditionClick6 = function () {
        if (this.condition6 == false) {
            this.condition1 = false;
            this.condition2 = false;
            this.condition3 = false;
            this.condition4 = false;
            this.condition5 = false;
            this.condition6 = true;
        }
    };
    PrefConditionComponent.prototype.onSwipe = function (args) {
        if (args.direction == 1) {
            this.router.navigate(["/prefStoreCategory"]);
        }
        if (args.direction == 2) {
            this.router.navigate(["/prefDescription"]);
        }
    };
    PrefConditionComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: "./condition.component.html",
            styleUrls: ["./condition.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            router_1.Router,
            user_service_1.UserService])
    ], PrefConditionComponent);
    return PrefConditionComponent;
}());
exports.PrefConditionComponent = PrefConditionComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZGl0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNvbmRpdGlvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBeUU7QUFDekUsNERBQTBEO0FBQzFELDBDQUF5RDtBQVF6RDtJQVNFLGdDQUNVLEtBQXFCLEVBQ3JCLE1BQWMsRUFDZCxXQUF3QjtRQUhsQyxpQkFlQztRQWRTLFVBQUssR0FBTCxLQUFLLENBQWdCO1FBQ3JCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQVhsQyxlQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixlQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ25CLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQUNuQix3QkFBbUIsR0FBWSxLQUFLLENBQUM7UUFNbkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25DLEtBQUssQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtZQUNoQyxLQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUMvQyxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFFRCx5Q0FBUSxHQUFSLGNBQWtCLENBQUM7SUFFbkIsZ0RBQWUsR0FBZjtRQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUU7WUFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7U0FDekI7SUFDSCxDQUFDO0lBQ0QsZ0RBQWUsR0FBZjtRQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUU7WUFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7U0FDekI7SUFDSCxDQUFDO0lBQ0QsZ0RBQWUsR0FBZjtRQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUU7WUFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7U0FDekI7SUFDSCxDQUFDO0lBQ0QsZ0RBQWUsR0FBZjtRQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUU7WUFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7U0FDekI7SUFDSCxDQUFDO0lBQ0QsZ0RBQWUsR0FBZjtRQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUU7WUFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7U0FDekI7SUFDSCxDQUFDO0lBQ0QsZ0RBQWUsR0FBZjtRQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUU7WUFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7U0FDeEI7SUFDSCxDQUFDO0lBQ0Qsd0NBQU8sR0FBUCxVQUFRLElBQTJCO1FBQ2pDLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUM7U0FDOUM7UUFDRCxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO1NBQzVDO0lBQ0gsQ0FBQztJQS9GVSxzQkFBc0I7UUFMbEMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsNEJBQTRCO1lBQ3pDLFNBQVMsRUFBRSxDQUFDLDJCQUEyQixDQUFDO1NBQ3pDLENBQUM7eUNBV2lCLHVCQUFjO1lBQ2IsZUFBTTtZQUNELDBCQUFXO09BWnZCLHNCQUFzQixDQWdHbEM7SUFBRCw2QkFBQztDQUFBLEFBaEdELElBZ0dDO0FBaEdZLHdEQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuLi8uLi9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFJvdXRlciB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgU3dpcGVHZXN0dXJlRXZlbnREYXRhIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZ2VzdHVyZXNcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9jb25kaXRpb24uY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vY29uZGl0aW9uLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIFByZWZDb25kaXRpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIGNvbmRpdGlvbjEgPSB0cnVlO1xyXG4gIGNvbmRpdGlvbjIgPSBmYWxzZTtcclxuICBjb25kaXRpb24zID0gZmFsc2U7XHJcbiAgY29uZGl0aW9uNCA9IGZhbHNlO1xyXG4gIGNvbmRpdGlvbjUgPSBmYWxzZTtcclxuICBjb25kaXRpb242ID0gZmFsc2U7XHJcbiAgc3RvcmVDYXRlZ29yeSA9IFwiXCI7XHJcbiAgaXNWaXNpYmxlQ29uZGl0aW9uczogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSByb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxyXG4gICAgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2VcclxuICApIHtcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYWN0aW9uQmFyU3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdHMoNyk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdFN0YXRlKHRydWUpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZUNoYW5nZXMoMSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnN3aXRjaFN0YXRlKHRydWUpO1xyXG4gICAgcm91dGUucXVlcnlQYXJhbXMuc3Vic2NyaWJlKHBhcmFtcyA9PiB7XHJcbiAgICAgIHRoaXMuc3RvcmVDYXRlZ29yeSA9IHBhcmFtc1tcInN0b3JlQ2F0ZWdvcnlcIl07XHJcbiAgICB9KTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuc3RvcmVDYXRlZ29yeSk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHt9XHJcblxyXG4gIGNvbmRpdGlvbkNsaWNrMSgpIHtcclxuICAgIGlmICh0aGlzLmNvbmRpdGlvbjEgPT0gZmFsc2UpIHtcclxuICAgICAgdGhpcy5jb25kaXRpb24xID0gdHJ1ZTtcclxuICAgICAgdGhpcy5jb25kaXRpb24yID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uMyA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjQgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jb25kaXRpb241ID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uNiA9IGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuICBjb25kaXRpb25DbGljazIoKSB7XHJcbiAgICBpZiAodGhpcy5jb25kaXRpb24yID09IGZhbHNlKSB7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uMSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjIgPSB0cnVlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjMgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jb25kaXRpb240ID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uNSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjYgPSBmYWxzZTtcclxuICAgIH1cclxuICB9XHJcbiAgY29uZGl0aW9uQ2xpY2szKCkge1xyXG4gICAgaWYgKHRoaXMuY29uZGl0aW9uMyA9PSBmYWxzZSkge1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjEgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jb25kaXRpb24yID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uMyA9IHRydWU7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uNCA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjUgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jb25kaXRpb242ID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGNvbmRpdGlvbkNsaWNrNCgpIHtcclxuICAgIGlmICh0aGlzLmNvbmRpdGlvbjQgPT0gZmFsc2UpIHtcclxuICAgICAgdGhpcy5jb25kaXRpb24xID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uMiA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjMgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jb25kaXRpb240ID0gdHJ1ZTtcclxuICAgICAgdGhpcy5jb25kaXRpb241ID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uNiA9IGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuICBjb25kaXRpb25DbGljazUoKSB7XHJcbiAgICBpZiAodGhpcy5jb25kaXRpb241ID09IGZhbHNlKSB7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uMSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjIgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jb25kaXRpb24zID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uNCA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjUgPSB0cnVlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjYgPSBmYWxzZTtcclxuICAgIH1cclxuICB9XHJcbiAgY29uZGl0aW9uQ2xpY2s2KCkge1xyXG4gICAgaWYgKHRoaXMuY29uZGl0aW9uNiA9PSBmYWxzZSkge1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjEgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jb25kaXRpb24yID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uMyA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjQgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jb25kaXRpb241ID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uNiA9IHRydWU7XHJcbiAgICB9XHJcbiAgfVxyXG4gIG9uU3dpcGUoYXJnczogU3dpcGVHZXN0dXJlRXZlbnREYXRhKSB7XHJcbiAgICBpZiAoYXJncy5kaXJlY3Rpb24gPT0gMSkge1xyXG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCIvcHJlZlN0b3JlQ2F0ZWdvcnlcIl0pO1xyXG4gICAgfVxyXG4gICAgaWYgKGFyZ3MuZGlyZWN0aW9uID09IDIpIHtcclxuICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1wiL3ByZWZEZXNjcmlwdGlvblwiXSk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==
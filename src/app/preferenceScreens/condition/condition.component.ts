import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { UserService } from "../../services/user.service";
import { ActivatedRoute, Router } from "@angular/router";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";

@Component({
  moduleId: module.id,
  templateUrl: "./condition.component.html",
  styleUrls: ["./condition.component.css"]
})
export class PrefConditionComponent implements OnInit {
  condition1 = true;
  condition2 = false;
  condition3 = false;
  condition4 = false;
  condition5 = false;
  condition6 = false;
  storeCategory = "";
  isVisibleConditions: boolean = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) {
    this.userService.actionBarState(true);
    this.userService.bottomBarDots(7);
    this.userService.bottomBarDotState(true);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
    this.userService.switchState(true);
    route.queryParams.subscribe(params => {
      this.storeCategory = params["storeCategory"];
    });
    console.log(this.storeCategory);
  }

  ngOnInit(): void {}

  conditionClick1() {
    if (this.condition1 == false) {
      this.condition1 = true;
      this.condition2 = false;
      this.condition3 = false;
      this.condition4 = false;
      this.condition5 = false;
      this.condition6 = false;
    }
  }
  conditionClick2() {
    if (this.condition2 == false) {
      this.condition1 = false;
      this.condition2 = true;
      this.condition3 = false;
      this.condition4 = false;
      this.condition5 = false;
      this.condition6 = false;
    }
  }
  conditionClick3() {
    if (this.condition3 == false) {
      this.condition1 = false;
      this.condition2 = false;
      this.condition3 = true;
      this.condition4 = false;
      this.condition5 = false;
      this.condition6 = false;
    }
  }
  conditionClick4() {
    if (this.condition4 == false) {
      this.condition1 = false;
      this.condition2 = false;
      this.condition3 = false;
      this.condition4 = true;
      this.condition5 = false;
      this.condition6 = false;
    }
  }
  conditionClick5() {
    if (this.condition5 == false) {
      this.condition1 = false;
      this.condition2 = false;
      this.condition3 = false;
      this.condition4 = false;
      this.condition5 = true;
      this.condition6 = false;
    }
  }
  conditionClick6() {
    if (this.condition6 == false) {
      this.condition1 = false;
      this.condition2 = false;
      this.condition3 = false;
      this.condition4 = false;
      this.condition5 = false;
      this.condition6 = true;
    }
  }
  onSwipe(args: SwipeGestureEventData) {
    if (args.direction == 1) {
      this.router.navigate(["/prefStoreCategory"]);
    }
    if (args.direction == 2) {
      this.router.navigate(["/prefDescription"]);
    }
  }
}

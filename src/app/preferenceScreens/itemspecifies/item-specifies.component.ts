import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { DropDownModule } from "nativescript-drop-down/angular";
import { SelectedIndexChangedEventData } from "nativescript-drop-down";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { UserService } from "../../services/user.service";

@Component({
  selector: "prefItemspecifies",
  moduleId: module.id,
  templateUrl: "./item-specifies.component.html",
  styleUrls: ["./item-specifies.component.css"]
})
export class PrefItemSpecifiesComponent implements OnInit {
  @ViewChild("dd") dropDown: ElementRef;

  routerExtensions: RouterExtensions;
  // public selectedIndex = 0;
  public brandNames = [
    "Dell",
    "Samsung",
    "Apple",
    "Hp",
    "Nokia",
    "Redmi",
    "Motorola",
    "Linovo",
    "Panasonic"
  ];
  public mpn = [
    "23421313123",
    "3242312132",
    "123134123",
    "44234234",
    "3242332423",
    "32434232",
    "342343432",
    "4323423423"
  ];
  public type = [
    "dsghdsd",
    "sdmnfgd",
    "dshgjhd",
    "sadjghja",
    "dsfjgashfdh",
    "gjsagdja",
    "sdfdsaad",
    "sadasdsad",
    "sadaass"
  ];
  public system = [
    "sdsaa",
    "sadasd",
    "dsasdas",
    "sdfdas",
    "sdfadsdd",
    "sdasdsad",
    "dSasdsad",
    "daasdsad",
    "dsasdasd"
  ];
  public model = [
    "dsghdsd",
    "sdmnfgd",
    "dshgjhd",
    "sadjghja",
    "dsfjgashfdh",
    "gjsagdja",
    "sdfdsaad",
    "sadasdsad",
    "sadaass"
  ];
  public displayType = [
    "sdsaa",
    "sadasd",
    "dsasdas",
    "sdfdas",
    "sdfadsdd",
    "sdasdsad",
    "dSasdsad",
    "daasdsad",
    "dsasdasd"
  ];
  feature1 = "1 TB storage";
  feature2 = "Metal body";
  feature3 = "500 MB SSD";
  myCheckColor = "#2196f3";
  constructor(
    routerExtensions: RouterExtensions,
    private dropDownmodule: DropDownModule,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDots(9);
    this.userService.bottomBarDotState(true);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
    this.userService.switchState(true);
  }

  ngOnInit(): void {}

  // public onchange(args: SelectedIndexChangedEventData) {
  //   console.log(
  //     `Drop Down selected index changed from ${args.oldIndex} to ${
  //       args.newIndex
  //     }`
  //   );
  // }

  public onopen() {
    console.log("Drop Down opened.");
  }

  public onclose() {
    console.log("Drop Down closed.");
  }

  onSwipe(args: SwipeGestureEventData) {
    if (args.direction == 1) {
      this.routerExtensions.navigate(["/prefDescription"]);
    }
    if (args.direction == 2) {
      this.routerExtensions.navigate(["/prefBusinesspolicies"]);
    }
  }
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var angular_1 = require("nativescript-drop-down/angular");
var user_service_1 = require("../../services/user.service");
var PrefItemSpecifiesComponent = /** @class */ (function () {
    function PrefItemSpecifiesComponent(routerExtensions, dropDownmodule, userService) {
        this.dropDownmodule = dropDownmodule;
        this.userService = userService;
        // public selectedIndex = 0;
        this.brandNames = [
            "Dell",
            "Samsung",
            "Apple",
            "Hp",
            "Nokia",
            "Redmi",
            "Motorola",
            "Linovo",
            "Panasonic"
        ];
        this.mpn = [
            "23421313123",
            "3242312132",
            "123134123",
            "44234234",
            "3242332423",
            "32434232",
            "342343432",
            "4323423423"
        ];
        this.type = [
            "dsghdsd",
            "sdmnfgd",
            "dshgjhd",
            "sadjghja",
            "dsfjgashfdh",
            "gjsagdja",
            "sdfdsaad",
            "sadasdsad",
            "sadaass"
        ];
        this.system = [
            "sdsaa",
            "sadasd",
            "dsasdas",
            "sdfdas",
            "sdfadsdd",
            "sdasdsad",
            "dSasdsad",
            "daasdsad",
            "dsasdasd"
        ];
        this.model = [
            "dsghdsd",
            "sdmnfgd",
            "dshgjhd",
            "sadjghja",
            "dsfjgashfdh",
            "gjsagdja",
            "sdfdsaad",
            "sadasdsad",
            "sadaass"
        ];
        this.displayType = [
            "sdsaa",
            "sadasd",
            "dsasdas",
            "sdfdas",
            "sdfadsdd",
            "sdasdsad",
            "dSasdsad",
            "daasdsad",
            "dsasdasd"
        ];
        this.feature1 = "1 TB storage";
        this.feature2 = "Metal body";
        this.feature3 = "500 MB SSD";
        this.myCheckColor = "#2196f3";
        this.routerExtensions = routerExtensions;
        this.userService.actionBarState(true);
        this.userService.bottomBarDots(9);
        this.userService.bottomBarDotState(true);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.userService.switchState(true);
    }
    PrefItemSpecifiesComponent.prototype.ngOnInit = function () { };
    // public onchange(args: SelectedIndexChangedEventData) {
    //   console.log(
    //     `Drop Down selected index changed from ${args.oldIndex} to ${
    //       args.newIndex
    //     }`
    //   );
    // }
    PrefItemSpecifiesComponent.prototype.onopen = function () {
        console.log("Drop Down opened.");
    };
    PrefItemSpecifiesComponent.prototype.onclose = function () {
        console.log("Drop Down closed.");
    };
    PrefItemSpecifiesComponent.prototype.onSwipe = function (args) {
        if (args.direction == 1) {
            this.routerExtensions.navigate(["/prefDescription"]);
        }
        if (args.direction == 2) {
            this.routerExtensions.navigate(["/prefBusinesspolicies"]);
        }
    };
    __decorate([
        core_1.ViewChild("dd"),
        __metadata("design:type", core_1.ElementRef)
    ], PrefItemSpecifiesComponent.prototype, "dropDown", void 0);
    PrefItemSpecifiesComponent = __decorate([
        core_1.Component({
            selector: "prefItemspecifies",
            moduleId: module.id,
            templateUrl: "./item-specifies.component.html",
            styleUrls: ["./item-specifies.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            angular_1.DropDownModule,
            user_service_1.UserService])
    ], PrefItemSpecifiesComponent);
    return PrefItemSpecifiesComponent;
}());
exports.PrefItemSpecifiesComponent = PrefItemSpecifiesComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbS1zcGVjaWZpZXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaXRlbS1zcGVjaWZpZXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlFO0FBQ3pFLHNEQUErRDtBQUMvRCwwREFBZ0U7QUFHaEUsNERBQTBEO0FBUTFEO0lBMEVFLG9DQUNFLGdCQUFrQyxFQUMxQixjQUE4QixFQUM5QixXQUF3QjtRQUR4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUF6RWxDLDRCQUE0QjtRQUNyQixlQUFVLEdBQUc7WUFDbEIsTUFBTTtZQUNOLFNBQVM7WUFDVCxPQUFPO1lBQ1AsSUFBSTtZQUNKLE9BQU87WUFDUCxPQUFPO1lBQ1AsVUFBVTtZQUNWLFFBQVE7WUFDUixXQUFXO1NBQ1osQ0FBQztRQUNLLFFBQUcsR0FBRztZQUNYLGFBQWE7WUFDYixZQUFZO1lBQ1osV0FBVztZQUNYLFVBQVU7WUFDVixZQUFZO1lBQ1osVUFBVTtZQUNWLFdBQVc7WUFDWCxZQUFZO1NBQ2IsQ0FBQztRQUNLLFNBQUksR0FBRztZQUNaLFNBQVM7WUFDVCxTQUFTO1lBQ1QsU0FBUztZQUNULFVBQVU7WUFDVixhQUFhO1lBQ2IsVUFBVTtZQUNWLFVBQVU7WUFDVixXQUFXO1lBQ1gsU0FBUztTQUNWLENBQUM7UUFDSyxXQUFNLEdBQUc7WUFDZCxPQUFPO1lBQ1AsUUFBUTtZQUNSLFNBQVM7WUFDVCxRQUFRO1lBQ1IsVUFBVTtZQUNWLFVBQVU7WUFDVixVQUFVO1lBQ1YsVUFBVTtZQUNWLFVBQVU7U0FDWCxDQUFDO1FBQ0ssVUFBSyxHQUFHO1lBQ2IsU0FBUztZQUNULFNBQVM7WUFDVCxTQUFTO1lBQ1QsVUFBVTtZQUNWLGFBQWE7WUFDYixVQUFVO1lBQ1YsVUFBVTtZQUNWLFdBQVc7WUFDWCxTQUFTO1NBQ1YsQ0FBQztRQUNLLGdCQUFXLEdBQUc7WUFDbkIsT0FBTztZQUNQLFFBQVE7WUFDUixTQUFTO1lBQ1QsUUFBUTtZQUNSLFVBQVU7WUFDVixVQUFVO1lBQ1YsVUFBVTtZQUNWLFVBQVU7WUFDVixVQUFVO1NBQ1gsQ0FBQztRQUNGLGFBQVEsR0FBRyxjQUFjLENBQUM7UUFDMUIsYUFBUSxHQUFHLFlBQVksQ0FBQztRQUN4QixhQUFRLEdBQUcsWUFBWSxDQUFDO1FBQ3hCLGlCQUFZLEdBQUcsU0FBUyxDQUFDO1FBTXZCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNsQyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVELDZDQUFRLEdBQVIsY0FBa0IsQ0FBQztJQUVuQix5REFBeUQ7SUFDekQsaUJBQWlCO0lBQ2pCLG9FQUFvRTtJQUNwRSxzQkFBc0I7SUFDdEIsU0FBUztJQUNULE9BQU87SUFDUCxJQUFJO0lBRUcsMkNBQU0sR0FBYjtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRU0sNENBQU8sR0FBZDtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsNENBQU8sR0FBUCxVQUFRLElBQTJCO1FBQ2pDLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztTQUN0RDtRQUNELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQztTQUMzRDtJQUNILENBQUM7SUFoSGdCO1FBQWhCLGdCQUFTLENBQUMsSUFBSSxDQUFDO2tDQUFXLGlCQUFVO2dFQUFDO0lBRDNCLDBCQUEwQjtRQU50QyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLG1CQUFtQjtZQUM3QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLGlDQUFpQztZQUM5QyxTQUFTLEVBQUUsQ0FBQyxnQ0FBZ0MsQ0FBQztTQUM5QyxDQUFDO3lDQTRFb0IseUJBQWdCO1lBQ1Ysd0JBQWM7WUFDakIsMEJBQVc7T0E3RXZCLDBCQUEwQixDQWtIdEM7SUFBRCxpQ0FBQztDQUFBLEFBbEhELElBa0hDO0FBbEhZLGdFQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBEcm9wRG93bk1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtZHJvcC1kb3duL2FuZ3VsYXJcIjtcclxuaW1wb3J0IHsgU2VsZWN0ZWRJbmRleENoYW5nZWRFdmVudERhdGEgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWRyb3AtZG93blwiO1xyXG5pbXBvcnQgeyBTd2lwZUdlc3R1cmVFdmVudERhdGEgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9nZXN0dXJlc1wiO1xyXG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuLi8uLi9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcInByZWZJdGVtc3BlY2lmaWVzXCIsXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICB0ZW1wbGF0ZVVybDogXCIuL2l0ZW0tc3BlY2lmaWVzLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL2l0ZW0tc3BlY2lmaWVzLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIFByZWZJdGVtU3BlY2lmaWVzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBAVmlld0NoaWxkKFwiZGRcIikgZHJvcERvd246IEVsZW1lbnRSZWY7XHJcblxyXG4gIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnM7XHJcbiAgLy8gcHVibGljIHNlbGVjdGVkSW5kZXggPSAwO1xyXG4gIHB1YmxpYyBicmFuZE5hbWVzID0gW1xyXG4gICAgXCJEZWxsXCIsXHJcbiAgICBcIlNhbXN1bmdcIixcclxuICAgIFwiQXBwbGVcIixcclxuICAgIFwiSHBcIixcclxuICAgIFwiTm9raWFcIixcclxuICAgIFwiUmVkbWlcIixcclxuICAgIFwiTW90b3JvbGFcIixcclxuICAgIFwiTGlub3ZvXCIsXHJcbiAgICBcIlBhbmFzb25pY1wiXHJcbiAgXTtcclxuICBwdWJsaWMgbXBuID0gW1xyXG4gICAgXCIyMzQyMTMxMzEyM1wiLFxyXG4gICAgXCIzMjQyMzEyMTMyXCIsXHJcbiAgICBcIjEyMzEzNDEyM1wiLFxyXG4gICAgXCI0NDIzNDIzNFwiLFxyXG4gICAgXCIzMjQyMzMyNDIzXCIsXHJcbiAgICBcIjMyNDM0MjMyXCIsXHJcbiAgICBcIjM0MjM0MzQzMlwiLFxyXG4gICAgXCI0MzIzNDIzNDIzXCJcclxuICBdO1xyXG4gIHB1YmxpYyB0eXBlID0gW1xyXG4gICAgXCJkc2doZHNkXCIsXHJcbiAgICBcInNkbW5mZ2RcIixcclxuICAgIFwiZHNoZ2poZFwiLFxyXG4gICAgXCJzYWRqZ2hqYVwiLFxyXG4gICAgXCJkc2ZqZ2FzaGZkaFwiLFxyXG4gICAgXCJnanNhZ2RqYVwiLFxyXG4gICAgXCJzZGZkc2FhZFwiLFxyXG4gICAgXCJzYWRhc2RzYWRcIixcclxuICAgIFwic2FkYWFzc1wiXHJcbiAgXTtcclxuICBwdWJsaWMgc3lzdGVtID0gW1xyXG4gICAgXCJzZHNhYVwiLFxyXG4gICAgXCJzYWRhc2RcIixcclxuICAgIFwiZHNhc2Rhc1wiLFxyXG4gICAgXCJzZGZkYXNcIixcclxuICAgIFwic2RmYWRzZGRcIixcclxuICAgIFwic2Rhc2RzYWRcIixcclxuICAgIFwiZFNhc2RzYWRcIixcclxuICAgIFwiZGFhc2RzYWRcIixcclxuICAgIFwiZHNhc2Rhc2RcIlxyXG4gIF07XHJcbiAgcHVibGljIG1vZGVsID0gW1xyXG4gICAgXCJkc2doZHNkXCIsXHJcbiAgICBcInNkbW5mZ2RcIixcclxuICAgIFwiZHNoZ2poZFwiLFxyXG4gICAgXCJzYWRqZ2hqYVwiLFxyXG4gICAgXCJkc2ZqZ2FzaGZkaFwiLFxyXG4gICAgXCJnanNhZ2RqYVwiLFxyXG4gICAgXCJzZGZkc2FhZFwiLFxyXG4gICAgXCJzYWRhc2RzYWRcIixcclxuICAgIFwic2FkYWFzc1wiXHJcbiAgXTtcclxuICBwdWJsaWMgZGlzcGxheVR5cGUgPSBbXHJcbiAgICBcInNkc2FhXCIsXHJcbiAgICBcInNhZGFzZFwiLFxyXG4gICAgXCJkc2FzZGFzXCIsXHJcbiAgICBcInNkZmRhc1wiLFxyXG4gICAgXCJzZGZhZHNkZFwiLFxyXG4gICAgXCJzZGFzZHNhZFwiLFxyXG4gICAgXCJkU2FzZHNhZFwiLFxyXG4gICAgXCJkYWFzZHNhZFwiLFxyXG4gICAgXCJkc2FzZGFzZFwiXHJcbiAgXTtcclxuICBmZWF0dXJlMSA9IFwiMSBUQiBzdG9yYWdlXCI7XHJcbiAgZmVhdHVyZTIgPSBcIk1ldGFsIGJvZHlcIjtcclxuICBmZWF0dXJlMyA9IFwiNTAwIE1CIFNTRFwiO1xyXG4gIG15Q2hlY2tDb2xvciA9IFwiIzIxOTZmM1wiO1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcclxuICAgIHByaXZhdGUgZHJvcERvd25tb2R1bGU6IERyb3BEb3duTW9kdWxlLFxyXG4gICAgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2VcclxuICApIHtcclxuICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucyA9IHJvdXRlckV4dGVuc2lvbnM7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmFjdGlvbkJhclN0YXRlKHRydWUpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5ib3R0b21CYXJEb3RzKDkpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5ib3R0b21CYXJEb3RTdGF0ZSh0cnVlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UudGFiVmlld1N0YXRlKHRydWUpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGVDaGFuZ2VzKDEpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5zd2l0Y2hTdGF0ZSh0cnVlKTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge31cclxuXHJcbiAgLy8gcHVibGljIG9uY2hhbmdlKGFyZ3M6IFNlbGVjdGVkSW5kZXhDaGFuZ2VkRXZlbnREYXRhKSB7XHJcbiAgLy8gICBjb25zb2xlLmxvZyhcclxuICAvLyAgICAgYERyb3AgRG93biBzZWxlY3RlZCBpbmRleCBjaGFuZ2VkIGZyb20gJHthcmdzLm9sZEluZGV4fSB0byAke1xyXG4gIC8vICAgICAgIGFyZ3MubmV3SW5kZXhcclxuICAvLyAgICAgfWBcclxuICAvLyAgICk7XHJcbiAgLy8gfVxyXG5cclxuICBwdWJsaWMgb25vcGVuKCkge1xyXG4gICAgY29uc29sZS5sb2coXCJEcm9wIERvd24gb3BlbmVkLlwiKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbmNsb3NlKCkge1xyXG4gICAgY29uc29sZS5sb2coXCJEcm9wIERvd24gY2xvc2VkLlwiKTtcclxuICB9XHJcblxyXG4gIG9uU3dpcGUoYXJnczogU3dpcGVHZXN0dXJlRXZlbnREYXRhKSB7XHJcbiAgICBpZiAoYXJncy5kaXJlY3Rpb24gPT0gMSkge1xyXG4gICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3ByZWZEZXNjcmlwdGlvblwiXSk7XHJcbiAgICB9XHJcbiAgICBpZiAoYXJncy5kaXJlY3Rpb24gPT0gMikge1xyXG4gICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3ByZWZCdXNpbmVzc3BvbGljaWVzXCJdKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19
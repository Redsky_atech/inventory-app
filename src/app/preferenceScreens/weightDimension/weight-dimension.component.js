"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var angular_1 = require("nativescript-drop-down/angular");
var user_service_1 = require("../../services/user.service");
var PrefWeightDimensionComponent = /** @class */ (function () {
    function PrefWeightDimensionComponent(routerExtensions, dropDownmodule, userService) {
        this.dropDownmodule = dropDownmodule;
        this.userService = userService;
        this.packageType = [
            "Dell",
            "Samsung",
            "Apple",
            "Hp",
            "Nokia",
            "Redmi",
            "Motorola",
            "Linovo",
            "Panasonic"
        ];
        this.dimension = [
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10"
        ];
        this.customWeight = ["10", "20", "30", "40", "50", "60", "70", "80"];
        this.weight = ["10", "20", "30", "40", "50", "60", "70", "80", "90", "100"];
        this.feature1 = "1 TB storage";
        this.feature2 = "Metal body";
        this.feature3 = "500 MB SSD";
        this.routerExtensions = routerExtensions;
        this.userService.actionBarState(true);
        this.userService.bottomBarDots(11);
        this.userService.bottomBarDotState(true);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.userService.switchState(true);
    }
    PrefWeightDimensionComponent.prototype.ngOnInit = function () { };
    // public onchange(args: SelectedIndexChangedEventData) {
    //   console.log(
    //     `Drop Down selected index changed from ${args.oldIndex} to ${
    //       args.newIndex
    //     }`
    //   );
    // }
    // public onopen() {
    //   console.log("Drop Down opened.");
    // }
    // public onclose() {
    //   console.log("Drop Down closed.");
    // }
    PrefWeightDimensionComponent.prototype.onSwipe = function (args) {
        if (args.direction == 1) {
            this.routerExtensions.navigate(["/prefBusinesspolicies"]);
        }
        // if (args.direction == 2) {
        //   this.routerExtensions.navigate(["/prefsavedraft"]);
        // }
    };
    __decorate([
        core_1.ViewChild("dd"),
        __metadata("design:type", core_1.ElementRef)
    ], PrefWeightDimensionComponent.prototype, "dropDown", void 0);
    PrefWeightDimensionComponent = __decorate([
        core_1.Component({
            selector: "prefWeightDimension",
            moduleId: module.id,
            templateUrl: "./weight-dimension.component.html",
            styleUrls: ["./weight-dimension.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            angular_1.DropDownModule,
            user_service_1.UserService])
    ], PrefWeightDimensionComponent);
    return PrefWeightDimensionComponent;
}());
exports.PrefWeightDimensionComponent = PrefWeightDimensionComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2VpZ2h0LWRpbWVuc2lvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ3ZWlnaHQtZGltZW5zaW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF5RTtBQUN6RSxzREFBK0Q7QUFDL0QsMERBQWdFO0FBR2hFLDREQUEwRDtBQVExRDtJQWlDRSxzQ0FDRSxnQkFBa0MsRUFDMUIsY0FBOEIsRUFDOUIsV0FBd0I7UUFEeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBaEMzQixnQkFBVyxHQUFHO1lBQ25CLE1BQU07WUFDTixTQUFTO1lBQ1QsT0FBTztZQUNQLElBQUk7WUFDSixPQUFPO1lBQ1AsT0FBTztZQUNQLFVBQVU7WUFDVixRQUFRO1lBQ1IsV0FBVztTQUNaLENBQUM7UUFDSyxjQUFTLEdBQUc7WUFDakIsSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtTQUNMLENBQUM7UUFDSyxpQkFBWSxHQUFHLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2hFLFdBQU0sR0FBRyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBRTlFLGFBQVEsR0FBRyxjQUFjLENBQUM7UUFDMUIsYUFBUSxHQUFHLFlBQVksQ0FBQztRQUN4QixhQUFRLEdBQUcsWUFBWSxDQUFDO1FBTXRCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVELCtDQUFRLEdBQVIsY0FBa0IsQ0FBQztJQUVuQix5REFBeUQ7SUFDekQsaUJBQWlCO0lBQ2pCLG9FQUFvRTtJQUNwRSxzQkFBc0I7SUFDdEIsU0FBUztJQUNULE9BQU87SUFDUCxJQUFJO0lBRUosb0JBQW9CO0lBQ3BCLHNDQUFzQztJQUN0QyxJQUFJO0lBRUoscUJBQXFCO0lBQ3JCLHNDQUFzQztJQUN0QyxJQUFJO0lBRUosOENBQU8sR0FBUCxVQUFRLElBQTJCO1FBQ2pDLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQztTQUMzRDtRQUNELDZCQUE2QjtRQUM3Qix3REFBd0Q7UUFDeEQsSUFBSTtJQUNOLENBQUM7SUF2RWdCO1FBQWhCLGdCQUFTLENBQUMsSUFBSSxDQUFDO2tDQUFXLGlCQUFVO2tFQUFDO0lBRDNCLDRCQUE0QjtRQU54QyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLHFCQUFxQjtZQUMvQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLG1DQUFtQztZQUNoRCxTQUFTLEVBQUUsQ0FBQyxrQ0FBa0MsQ0FBQztTQUNoRCxDQUFDO3lDQW1Db0IseUJBQWdCO1lBQ1Ysd0JBQWM7WUFDakIsMEJBQVc7T0FwQ3ZCLDRCQUE0QixDQXlFeEM7SUFBRCxtQ0FBQztDQUFBLEFBekVELElBeUVDO0FBekVZLG9FQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBEcm9wRG93bk1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtZHJvcC1kb3duL2FuZ3VsYXJcIjtcclxuaW1wb3J0IHsgU2VsZWN0ZWRJbmRleENoYW5nZWRFdmVudERhdGEgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWRyb3AtZG93blwiO1xyXG5pbXBvcnQgeyBTd2lwZUdlc3R1cmVFdmVudERhdGEgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9nZXN0dXJlc1wiO1xyXG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuLi8uLi9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcInByZWZXZWlnaHREaW1lbnNpb25cIixcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vd2VpZ2h0LWRpbWVuc2lvbi5jb21wb25lbnQuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wiLi93ZWlnaHQtZGltZW5zaW9uLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIFByZWZXZWlnaHREaW1lbnNpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBWaWV3Q2hpbGQoXCJkZFwiKSBkcm9wRG93bjogRWxlbWVudFJlZjtcclxuXHJcbiAgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucztcclxuICBwdWJsaWMgcGFja2FnZVR5cGUgPSBbXHJcbiAgICBcIkRlbGxcIixcclxuICAgIFwiU2Ftc3VuZ1wiLFxyXG4gICAgXCJBcHBsZVwiLFxyXG4gICAgXCJIcFwiLFxyXG4gICAgXCJOb2tpYVwiLFxyXG4gICAgXCJSZWRtaVwiLFxyXG4gICAgXCJNb3Rvcm9sYVwiLFxyXG4gICAgXCJMaW5vdm9cIixcclxuICAgIFwiUGFuYXNvbmljXCJcclxuICBdO1xyXG4gIHB1YmxpYyBkaW1lbnNpb24gPSBbXHJcbiAgICBcIjAxXCIsXHJcbiAgICBcIjAyXCIsXHJcbiAgICBcIjAzXCIsXHJcbiAgICBcIjA0XCIsXHJcbiAgICBcIjA1XCIsXHJcbiAgICBcIjA2XCIsXHJcbiAgICBcIjA3XCIsXHJcbiAgICBcIjA4XCIsXHJcbiAgICBcIjA5XCIsXHJcbiAgICBcIjEwXCJcclxuICBdO1xyXG4gIHB1YmxpYyBjdXN0b21XZWlnaHQgPSBbXCIxMFwiLCBcIjIwXCIsIFwiMzBcIiwgXCI0MFwiLCBcIjUwXCIsIFwiNjBcIiwgXCI3MFwiLCBcIjgwXCJdO1xyXG4gIHB1YmxpYyB3ZWlnaHQgPSBbXCIxMFwiLCBcIjIwXCIsIFwiMzBcIiwgXCI0MFwiLCBcIjUwXCIsIFwiNjBcIiwgXCI3MFwiLCBcIjgwXCIsIFwiOTBcIiwgXCIxMDBcIl07XHJcblxyXG4gIGZlYXR1cmUxID0gXCIxIFRCIHN0b3JhZ2VcIjtcclxuICBmZWF0dXJlMiA9IFwiTWV0YWwgYm9keVwiO1xyXG4gIGZlYXR1cmUzID0gXCI1MDAgTUIgU1NEXCI7XHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLFxyXG4gICAgcHJpdmF0ZSBkcm9wRG93bm1vZHVsZTogRHJvcERvd25Nb2R1bGUsXHJcbiAgICBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZVxyXG4gICkge1xyXG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zID0gcm91dGVyRXh0ZW5zaW9ucztcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYWN0aW9uQmFyU3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdHMoMTEpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5ib3R0b21CYXJEb3RTdGF0ZSh0cnVlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UudGFiVmlld1N0YXRlKHRydWUpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGVDaGFuZ2VzKDEpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5zd2l0Y2hTdGF0ZSh0cnVlKTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge31cclxuXHJcbiAgLy8gcHVibGljIG9uY2hhbmdlKGFyZ3M6IFNlbGVjdGVkSW5kZXhDaGFuZ2VkRXZlbnREYXRhKSB7XHJcbiAgLy8gICBjb25zb2xlLmxvZyhcclxuICAvLyAgICAgYERyb3AgRG93biBzZWxlY3RlZCBpbmRleCBjaGFuZ2VkIGZyb20gJHthcmdzLm9sZEluZGV4fSB0byAke1xyXG4gIC8vICAgICAgIGFyZ3MubmV3SW5kZXhcclxuICAvLyAgICAgfWBcclxuICAvLyAgICk7XHJcbiAgLy8gfVxyXG5cclxuICAvLyBwdWJsaWMgb25vcGVuKCkge1xyXG4gIC8vICAgY29uc29sZS5sb2coXCJEcm9wIERvd24gb3BlbmVkLlwiKTtcclxuICAvLyB9XHJcblxyXG4gIC8vIHB1YmxpYyBvbmNsb3NlKCkge1xyXG4gIC8vICAgY29uc29sZS5sb2coXCJEcm9wIERvd24gY2xvc2VkLlwiKTtcclxuICAvLyB9XHJcblxyXG4gIG9uU3dpcGUoYXJnczogU3dpcGVHZXN0dXJlRXZlbnREYXRhKSB7XHJcbiAgICBpZiAoYXJncy5kaXJlY3Rpb24gPT0gMSkge1xyXG4gICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3ByZWZCdXNpbmVzc3BvbGljaWVzXCJdKTtcclxuICAgIH1cclxuICAgIC8vIGlmIChhcmdzLmRpcmVjdGlvbiA9PSAyKSB7XHJcbiAgICAvLyAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvcHJlZnNhdmVkcmFmdFwiXSk7XHJcbiAgICAvLyB9XHJcbiAgfVxyXG59XHJcbiJdfQ==
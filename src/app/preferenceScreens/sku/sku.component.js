"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_barcodescanner_1 = require("nativescript-barcodescanner");
var router_1 = require("nativescript-angular/router");
var modal_component_1 = require("../../modals/modal.component");
var http_1 = require("@angular/common/http");
var router_2 = require("@angular/router");
var user_service_1 = require("../../services/user.service");
var PrefSkuComponent = /** @class */ (function () {
    function PrefSkuComponent(activatedRoute, barcodeScanner, routerExtensions, http, userService) {
        var _this = this;
        this.activatedRoute = activatedRoute;
        this.barcodeScanner = barcodeScanner;
        this.routerExtensions = routerExtensions;
        this.http = http;
        this.userService = userService;
        this.id = "";
        this.isFrontCamera = false;
        this.torchOn = false;
        this.isScannedOnce = false;
        this.flashClass = "flash";
        this.cameraClass = "rare";
        this.appointmentId = "";
        this.flashClass = "flash";
        this.cameraClass = "rare";
        this.isFrontCamera = false;
        this.userService.actionBarState(true);
        this.userService.bottomBarDots(1);
        this.userService.bottomBarDotState(true);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.userService.switchState(true);
        this.activatedRoute.queryParams.subscribe(function (params) {
            _this.appointmentId = params.id;
            console.log(_this.appointmentId);
        });
    }
    PrefSkuComponent.prototype.ngOnInit = function () { };
    PrefSkuComponent.prototype.ngAfterViewInit = function () {
        // let view = this.barcodeline.nativeElement as StackLayout;
        // view.animate({
        //   duration: 3000,
        //   curve: AnimationCurve.linear,
        //   translate: { x: 0, y: 50 }
        // });
        // .then(() => {
        //   // Reset animation
        //   setTimeout(() => {
        //     view.translateY = 0;
        //     view.translateX = 0;
        //   }, 3000);
        // });
    };
    // putData() {
    //     let headers = new HttpHeaders({
    //         "Content-Type": "application/json",
    //         "x-role-key": "b312f1f9-1f51-c060-11b5-d3e3d9019b8c"
    //     });
    //     var body = {
    //         "role": {
    //             "id": "5afbb4bfdfab9716aab75fa7"
    //         },
    //         "appointment": {
    //             "id": this.appointmentId
    //         }
    //     }
    //     console.log('Reached fun', this.appointmentId)
    //     this.http.put("http://welcome-api-dev.m-sas.com/api/sessions/" + this.id, body, { headers: headers }).subscribe((res: any) => {
    //         if (res.isSuccess) {
    //             let result: any
    //             result = res.data
    //             this.isScannedOnce = true;
    //             // this.routerExtensions.navigate(['/login']);
    //     console.log(res)
    //             this.otpModal.show();
    //         }
    //         else {
    //                     console.log(res)
    //             // alert(res.error)
    //         }
    //     },
    //         error => {
    //                     console.log(error )
    //             // alert(error)
    //         })
    // }
    // onDone() {
    //     this.otpModal.hide();
    //     this.routerExtensions.navigate(['/login']);
    // }
    // onBarcodeScanningResult(args) {
    //     this.id = args.value.barcodes[0].value;
    //     if (this.id != undefined && !this.isScannedOnce) {
    //         this.putData();
    //         console.log('RSS:', this.id)
    //     }
    // }
    PrefSkuComponent.prototype.onFlashSelect = function () {
        if (this.flashClass == "flash") {
            this.flashClass = "flash-focus";
            this.torchOn = true;
        }
        else {
            this.flashClass = "flash";
            this.torchOn = false;
        }
    };
    PrefSkuComponent.prototype.onCameraSelect = function () {
        if (this.cameraClass == "rare") {
            this.cameraClass = "front";
            this.isFrontCamera = true;
            console.log("Tapped");
        }
        else {
            this.cameraClass = "rare";
            this.isFrontCamera = false;
            console.log("Tapped");
        }
    };
    PrefSkuComponent.prototype.onSwipe = function (args) {
        if (args.direction == 2) {
            this.routerExtensions.navigate(["/prefUpc"]);
        }
    };
    __decorate([
        core_1.ViewChild("otpDialog"),
        __metadata("design:type", modal_component_1.ModalComponent)
    ], PrefSkuComponent.prototype, "otpModal", void 0);
    __decorate([
        core_1.ViewChild("barcodeline"),
        __metadata("design:type", core_1.ElementRef)
    ], PrefSkuComponent.prototype, "barcodeline", void 0);
    PrefSkuComponent = __decorate([
        core_1.Component({
            selector: "prefSku",
            moduleId: module.id,
            templateUrl: "./sku.component.html",
            styleUrls: ["./sku.component.css"]
        }),
        __metadata("design:paramtypes", [router_2.ActivatedRoute,
            nativescript_barcodescanner_1.BarcodeScanner,
            router_1.RouterExtensions,
            http_1.HttpClient,
            user_service_1.UserService])
    ], PrefSkuComponent);
    return PrefSkuComponent;
}());
exports.PrefSkuComponent = PrefSkuComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2t1LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNrdS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FNdUI7QUFDdkIsMkVBQTZEO0FBQzdELHNEQUErRDtBQUMvRCxnRUFBOEQ7QUFDOUQsNkNBQStEO0FBQy9ELDBDQUF5RDtBQUl6RCw0REFBMEQ7QUFTMUQ7SUFXRSwwQkFDVSxjQUE4QixFQUM5QixjQUE4QixFQUM5QixnQkFBa0MsRUFDbEMsSUFBZ0IsRUFDaEIsV0FBd0I7UUFMbEMsaUJBcUJDO1FBcEJTLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyxTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2hCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBWmxDLE9BQUUsR0FBRyxFQUFFLENBQUM7UUFDUixrQkFBYSxHQUFZLEtBQUssQ0FBQztRQUMvQixZQUFPLEdBQVksS0FBSyxDQUFDO1FBQ3pCLGtCQUFhLEdBQVksS0FBSyxDQUFDO1FBQy9CLGVBQVUsR0FBRyxPQUFPLENBQUM7UUFDckIsZ0JBQVcsR0FBRyxNQUFNLENBQUM7UUFDckIsa0JBQWEsR0FBRyxFQUFFLENBQUM7UUFRakIsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUM7UUFDMUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUM7UUFDMUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRW5DLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFBLE1BQU07WUFDOUMsS0FBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO1lBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2xDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELG1DQUFRLEdBQVIsY0FBa0IsQ0FBQztJQUVuQiwwQ0FBZSxHQUFmO1FBQ0UsNERBQTREO1FBQzVELGlCQUFpQjtRQUNqQixvQkFBb0I7UUFDcEIsa0NBQWtDO1FBQ2xDLCtCQUErQjtRQUMvQixNQUFNO1FBQ04sZ0JBQWdCO1FBQ2hCLHVCQUF1QjtRQUN2Qix1QkFBdUI7UUFDdkIsMkJBQTJCO1FBQzNCLDJCQUEyQjtRQUMzQixjQUFjO1FBQ2QsTUFBTTtJQUNSLENBQUM7SUFDRCxjQUFjO0lBQ2Qsc0NBQXNDO0lBQ3RDLDhDQUE4QztJQUM5QywrREFBK0Q7SUFDL0QsVUFBVTtJQUVWLG1CQUFtQjtJQUNuQixvQkFBb0I7SUFDcEIsK0NBQStDO0lBQy9DLGFBQWE7SUFDYiwyQkFBMkI7SUFDM0IsdUNBQXVDO0lBQ3ZDLFlBQVk7SUFDWixRQUFRO0lBRVIscURBQXFEO0lBRXJELHNJQUFzSTtJQUV0SSwrQkFBK0I7SUFDL0IsOEJBQThCO0lBQzlCLGdDQUFnQztJQUNoQyx5Q0FBeUM7SUFDekMsNkRBQTZEO0lBQzdELHVCQUF1QjtJQUV2QixvQ0FBb0M7SUFDcEMsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQix1Q0FBdUM7SUFFdkMsa0NBQWtDO0lBQ2xDLFlBQVk7SUFDWixTQUFTO0lBQ1QscUJBQXFCO0lBQ3JCLDBDQUEwQztJQUUxQyw4QkFBOEI7SUFDOUIsYUFBYTtJQUNiLElBQUk7SUFFSixhQUFhO0lBQ2IsNEJBQTRCO0lBQzVCLGtEQUFrRDtJQUNsRCxJQUFJO0lBRUosa0NBQWtDO0lBQ2xDLDhDQUE4QztJQUM5Qyx5REFBeUQ7SUFDekQsMEJBQTBCO0lBQzFCLHVDQUF1QztJQUN2QyxRQUFRO0lBQ1IsSUFBSTtJQUVKLHdDQUFhLEdBQWI7UUFDRSxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksT0FBTyxFQUFFO1lBQzlCLElBQUksQ0FBQyxVQUFVLEdBQUcsYUFBYSxDQUFDO1lBQ2hDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1NBQ3JCO2FBQU07WUFDTCxJQUFJLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQztZQUMxQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUN0QjtJQUNILENBQUM7SUFFRCx5Q0FBYyxHQUFkO1FBQ0UsSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLE1BQU0sRUFBRTtZQUM5QixJQUFJLENBQUMsV0FBVyxHQUFHLE9BQU8sQ0FBQztZQUMzQixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztZQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3ZCO2FBQU07WUFDTCxJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQztZQUMxQixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztZQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3ZCO0lBQ0gsQ0FBQztJQUVELGtDQUFPLEdBQVAsVUFBUSxJQUEyQjtRQUNqQyxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1NBQzlDO0lBQ0gsQ0FBQztJQWxJdUI7UUFBdkIsZ0JBQVMsQ0FBQyxXQUFXLENBQUM7a0NBQVcsZ0NBQWM7c0RBQUM7SUFDdkI7UUFBekIsZ0JBQVMsQ0FBQyxhQUFhLENBQUM7a0NBQWMsaUJBQVU7eURBQUM7SUFGdkMsZ0JBQWdCO1FBTjVCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsU0FBUztZQUNuQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLHNCQUFzQjtZQUNuQyxTQUFTLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQztTQUNuQyxDQUFDO3lDQWEwQix1QkFBYztZQUNkLDRDQUFjO1lBQ1oseUJBQWdCO1lBQzVCLGlCQUFVO1lBQ0gsMEJBQVc7T0FoQnZCLGdCQUFnQixDQXNJNUI7SUFBRCx1QkFBQztDQUFBLEFBdElELElBc0lDO0FBdElZLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIENvbXBvbmVudCxcbiAgT25Jbml0LFxuICBBZnRlclZpZXdJbml0LFxuICBWaWV3Q2hpbGQsXG4gIEVsZW1lbnRSZWZcbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IEJhcmNvZGVTY2FubmVyIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1iYXJjb2Rlc2Nhbm5lclwiO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IE1vZGFsQ29tcG9uZW50IH0gZnJvbSBcIi4uLy4uL21vZGFscy9tb2RhbC5jb21wb25lbnRcIjtcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHsgcmVnaXN0ZXJFbGVtZW50IH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2VsZW1lbnQtcmVnaXN0cnlcIjtcbmltcG9ydCB7IEFuaW1hdGlvbkN1cnZlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZW51bXNcIjtcbmltcG9ydCB7IFN0YWNrTGF5b3V0IH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvbGF5b3V0cy9zdGFjay1sYXlvdXRcIjtcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcIi4uLy4uL3NlcnZpY2VzL3VzZXIuc2VydmljZVwiO1xuaW1wb3J0IHsgU3dpcGVHZXN0dXJlRXZlbnREYXRhIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZ2VzdHVyZXNcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInByZWZTa3VcIixcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgdGVtcGxhdGVVcmw6IFwiLi9za3UuY29tcG9uZW50Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL3NrdS5jb21wb25lbnQuY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIFByZWZTa3VDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQge1xuICBAVmlld0NoaWxkKFwib3RwRGlhbG9nXCIpIG90cE1vZGFsOiBNb2RhbENvbXBvbmVudDtcbiAgQFZpZXdDaGlsZChcImJhcmNvZGVsaW5lXCIpIGJhcmNvZGVsaW5lOiBFbGVtZW50UmVmO1xuXG4gIGlkID0gXCJcIjtcbiAgaXNGcm9udENhbWVyYTogYm9vbGVhbiA9IGZhbHNlO1xuICB0b3JjaE9uOiBib29sZWFuID0gZmFsc2U7XG4gIGlzU2Nhbm5lZE9uY2U6IGJvb2xlYW4gPSBmYWxzZTtcbiAgZmxhc2hDbGFzcyA9IFwiZmxhc2hcIjtcbiAgY2FtZXJhQ2xhc3MgPSBcInJhcmVcIjtcbiAgYXBwb2ludG1lbnRJZCA9IFwiXCI7XG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlLFxuICAgIHByaXZhdGUgYmFyY29kZVNjYW5uZXI6IEJhcmNvZGVTY2FubmVyLFxuICAgIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcbiAgICBwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQsXG4gICAgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2VcbiAgKSB7XG4gICAgdGhpcy5mbGFzaENsYXNzID0gXCJmbGFzaFwiO1xuICAgIHRoaXMuY2FtZXJhQ2xhc3MgPSBcInJhcmVcIjtcbiAgICB0aGlzLmlzRnJvbnRDYW1lcmEgPSBmYWxzZTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmFjdGlvbkJhclN0YXRlKHRydWUpO1xuICAgIHRoaXMudXNlclNlcnZpY2UuYm90dG9tQmFyRG90cygxKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdFN0YXRlKHRydWUpO1xuICAgIHRoaXMudXNlclNlcnZpY2UudGFiVmlld1N0YXRlKHRydWUpO1xuICAgIHRoaXMudXNlclNlcnZpY2UudGFiVmlld1N0YXRlQ2hhbmdlcygxKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnN3aXRjaFN0YXRlKHRydWUpO1xuICAgIFxuICAgIHRoaXMuYWN0aXZhdGVkUm91dGUucXVlcnlQYXJhbXMuc3Vic2NyaWJlKHBhcmFtcyA9PiB7XG4gICAgICB0aGlzLmFwcG9pbnRtZW50SWQgPSBwYXJhbXMuaWQ7XG4gICAgICBjb25zb2xlLmxvZyh0aGlzLmFwcG9pbnRtZW50SWQpO1xuICAgIH0pO1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7fVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcbiAgICAvLyBsZXQgdmlldyA9IHRoaXMuYmFyY29kZWxpbmUubmF0aXZlRWxlbWVudCBhcyBTdGFja0xheW91dDtcbiAgICAvLyB2aWV3LmFuaW1hdGUoe1xuICAgIC8vICAgZHVyYXRpb246IDMwMDAsXG4gICAgLy8gICBjdXJ2ZTogQW5pbWF0aW9uQ3VydmUubGluZWFyLFxuICAgIC8vICAgdHJhbnNsYXRlOiB7IHg6IDAsIHk6IDUwIH1cbiAgICAvLyB9KTtcbiAgICAvLyAudGhlbigoKSA9PiB7XG4gICAgLy8gICAvLyBSZXNldCBhbmltYXRpb25cbiAgICAvLyAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgIC8vICAgICB2aWV3LnRyYW5zbGF0ZVkgPSAwO1xuICAgIC8vICAgICB2aWV3LnRyYW5zbGF0ZVggPSAwO1xuICAgIC8vICAgfSwgMzAwMCk7XG4gICAgLy8gfSk7XG4gIH1cbiAgLy8gcHV0RGF0YSgpIHtcbiAgLy8gICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgLy8gICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcbiAgLy8gICAgICAgICBcIngtcm9sZS1rZXlcIjogXCJiMzEyZjFmOS0xZjUxLWMwNjAtMTFiNS1kM2UzZDkwMTliOGNcIlxuICAvLyAgICAgfSk7XG5cbiAgLy8gICAgIHZhciBib2R5ID0ge1xuICAvLyAgICAgICAgIFwicm9sZVwiOiB7XG4gIC8vICAgICAgICAgICAgIFwiaWRcIjogXCI1YWZiYjRiZmRmYWI5NzE2YWFiNzVmYTdcIlxuICAvLyAgICAgICAgIH0sXG4gIC8vICAgICAgICAgXCJhcHBvaW50bWVudFwiOiB7XG4gIC8vICAgICAgICAgICAgIFwiaWRcIjogdGhpcy5hcHBvaW50bWVudElkXG4gIC8vICAgICAgICAgfVxuICAvLyAgICAgfVxuXG4gIC8vICAgICBjb25zb2xlLmxvZygnUmVhY2hlZCBmdW4nLCB0aGlzLmFwcG9pbnRtZW50SWQpXG5cbiAgLy8gICAgIHRoaXMuaHR0cC5wdXQoXCJodHRwOi8vd2VsY29tZS1hcGktZGV2Lm0tc2FzLmNvbS9hcGkvc2Vzc2lvbnMvXCIgKyB0aGlzLmlkLCBib2R5LCB7IGhlYWRlcnM6IGhlYWRlcnMgfSkuc3Vic2NyaWJlKChyZXM6IGFueSkgPT4ge1xuXG4gIC8vICAgICAgICAgaWYgKHJlcy5pc1N1Y2Nlc3MpIHtcbiAgLy8gICAgICAgICAgICAgbGV0IHJlc3VsdDogYW55XG4gIC8vICAgICAgICAgICAgIHJlc3VsdCA9IHJlcy5kYXRhXG4gIC8vICAgICAgICAgICAgIHRoaXMuaXNTY2FubmVkT25jZSA9IHRydWU7XG4gIC8vICAgICAgICAgICAgIC8vIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbJy9sb2dpbiddKTtcbiAgLy8gICAgIGNvbnNvbGUubG9nKHJlcylcblxuICAvLyAgICAgICAgICAgICB0aGlzLm90cE1vZGFsLnNob3coKTtcbiAgLy8gICAgICAgICB9XG4gIC8vICAgICAgICAgZWxzZSB7XG4gIC8vICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzKVxuXG4gIC8vICAgICAgICAgICAgIC8vIGFsZXJ0KHJlcy5lcnJvcilcbiAgLy8gICAgICAgICB9XG4gIC8vICAgICB9LFxuICAvLyAgICAgICAgIGVycm9yID0+IHtcbiAgLy8gICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvciApXG5cbiAgLy8gICAgICAgICAgICAgLy8gYWxlcnQoZXJyb3IpXG4gIC8vICAgICAgICAgfSlcbiAgLy8gfVxuXG4gIC8vIG9uRG9uZSgpIHtcbiAgLy8gICAgIHRoaXMub3RwTW9kYWwuaGlkZSgpO1xuICAvLyAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFsnL2xvZ2luJ10pO1xuICAvLyB9XG5cbiAgLy8gb25CYXJjb2RlU2Nhbm5pbmdSZXN1bHQoYXJncykge1xuICAvLyAgICAgdGhpcy5pZCA9IGFyZ3MudmFsdWUuYmFyY29kZXNbMF0udmFsdWU7XG4gIC8vICAgICBpZiAodGhpcy5pZCAhPSB1bmRlZmluZWQgJiYgIXRoaXMuaXNTY2FubmVkT25jZSkge1xuICAvLyAgICAgICAgIHRoaXMucHV0RGF0YSgpO1xuICAvLyAgICAgICAgIGNvbnNvbGUubG9nKCdSU1M6JywgdGhpcy5pZClcbiAgLy8gICAgIH1cbiAgLy8gfVxuXG4gIG9uRmxhc2hTZWxlY3QoKSB7XG4gICAgaWYgKHRoaXMuZmxhc2hDbGFzcyA9PSBcImZsYXNoXCIpIHtcbiAgICAgIHRoaXMuZmxhc2hDbGFzcyA9IFwiZmxhc2gtZm9jdXNcIjtcbiAgICAgIHRoaXMudG9yY2hPbiA9IHRydWU7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuZmxhc2hDbGFzcyA9IFwiZmxhc2hcIjtcbiAgICAgIHRoaXMudG9yY2hPbiA9IGZhbHNlO1xuICAgIH1cbiAgfVxuXG4gIG9uQ2FtZXJhU2VsZWN0KCkge1xuICAgIGlmICh0aGlzLmNhbWVyYUNsYXNzID09IFwicmFyZVwiKSB7XG4gICAgICB0aGlzLmNhbWVyYUNsYXNzID0gXCJmcm9udFwiO1xuICAgICAgdGhpcy5pc0Zyb250Q2FtZXJhID0gdHJ1ZTtcbiAgICAgIGNvbnNvbGUubG9nKFwiVGFwcGVkXCIpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmNhbWVyYUNsYXNzID0gXCJyYXJlXCI7XG4gICAgICB0aGlzLmlzRnJvbnRDYW1lcmEgPSBmYWxzZTtcbiAgICAgIGNvbnNvbGUubG9nKFwiVGFwcGVkXCIpO1xuICAgIH1cbiAgfVxuXG4gIG9uU3dpcGUoYXJnczogU3dpcGVHZXN0dXJlRXZlbnREYXRhKSB7XG4gICAgaWYgKGFyZ3MuZGlyZWN0aW9uID09IDIpIHtcbiAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvcHJlZlVwY1wiXSk7XG4gICAgfVxuICB9XG5cbiAgLy8gICAoc2NhblJlc3VsdCk9XCJvbkJhcmNvZGVTY2FubmluZ1Jlc3VsdCgkZXZlbnQpXG59XG4iXX0=
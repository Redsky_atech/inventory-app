"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var http_1 = require("@angular/common/http");
var user_service_1 = require("../../services/user.service");
var Category = /** @class */ (function () {
    function Category(name) {
        if (name != null) {
            this.name = name;
        }
    }
    return Category;
}());
var categoryNames = [
    "Dolls & Bears",
    "Electronics",
    "Gift Cards & Vouchers",
    "Health & Beauty",
    "Home & Garden",
    "Home Appliances",
    "Industrial"
];
var categoryNames1 = [
    "GPS Accessories",
    "GPS",
    "Other Electronics",
    "Smart Glasses",
    "Torches",
    "Virtual Reality",
    "Wholesale,Bulk Lots"
];
var PrefFindCategoryComponent = /** @class */ (function () {
    function PrefFindCategoryComponent(routerExtensions, http, userService) {
        this.routerExtensions = routerExtensions;
        this.http = http;
        this.userService = userService;
        this.id = "";
        // rowNumber = "1";
        this.categoryList = "";
        this.isVisible1 = true;
        this.userService.actionBarState(true);
        this.userService.bottomBarDotState(true);
        this.userService.bottomBarDots(5);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.userService.switchState(true);
    }
    PrefFindCategoryComponent.prototype.ngOnInit = function () {
        this.categories1 = [];
        this.categories2 = [];
        for (var i = 0; i < categoryNames.length; i++) {
            this.categories1.push(new Category(categoryNames[i]));
        }
        for (var i = 0; i < categoryNames1.length; i++) {
            this.categories2.push(new Category(categoryNames1[i]));
        }
    };
    PrefFindCategoryComponent.prototype.ngAfterViewInit = function () {
        // this.getAppointments();
    };
    //   getAppointments() {
    //     let headers = new HttpHeaders({
    //       "Content-Type": "application/json",
    //       "x-role-key": "3e1d070d-1847-57d7-36e5-d6f8a166241b"
    //     });
    //     console.log("Reached fun");
    //     this.http
    //       .get("http://welcome-api-dev.m-sas.com/api/appointments/visitor/my", {
    //         headers: headers
    //       })
    //       .subscribe(
    //         (res: any) => {
    //           if (res.isSuccess) {
    //             let result: any;
    //             result = res.data;
    //             if (result.old != null && result.old != undefined) {
    //               for (var i = 0; i < result.old.length; i++) {
    //                 this.appointments.push(
    //                   new Appointment(result.old[i].id, result.old[i].status)
    //                 );
    //               }
    //             }
    //             this.viewModel = new Observable();
    //             this.viewModel.set("items", this.appointments);
    //             this.page.bindingContext = this.viewModel;
    //             console.log(this.appointments);
    //           } else {
    //             console.log(res);
    //           }
    //         },
    //         error => {
    //           console.log(error);
    //         }
    //       );
    //   }
    PrefFindCategoryComponent.prototype.onCategoryTap1 = function (args) {
        this.isVisible2 = true;
        var i;
        i = args.index;
        var str = this.categoryList;
        var str1 = " > ";
        var item = this.categories1[i];
        str = str.concat(item.name);
        str = str.concat(str1);
        this.categoryList = str;
    };
    PrefFindCategoryComponent.prototype.onCategoryTap2 = function (args) {
        var i;
        i = args.index;
        var str = this.categoryList;
        var str1 = " > ";
        var item = this.categories2[i];
        str = str.concat(item.name);
        str = str.concat(str1);
        this.categoryList = str;
    };
    PrefFindCategoryComponent.prototype.onSwipe = function (args) {
        if (args.direction == 1) {
            this.routerExtensions.navigate(["/prefSuggest"]);
        }
        if (args.direction == 2) {
            this.routerExtensions.navigate(["/prefStoreCategory"]);
        }
    };
    PrefFindCategoryComponent = __decorate([
        core_1.Component({
            selector: "prefFindCategory",
            moduleId: module.id,
            templateUrl: "./findCategory.component.html",
            styleUrls: ["./findCategory.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            http_1.HttpClient,
            user_service_1.UserService])
    ], PrefFindCategoryComponent);
    return PrefFindCategoryComponent;
}());
exports.PrefFindCategoryComponent = PrefFindCategoryComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmluZENhdGVnb3J5LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZpbmRDYXRlZ29yeS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBNEU7QUFFNUUsc0RBQStEO0FBRS9ELDZDQUErRDtBQU0vRCw0REFBMEQ7QUFFMUQ7SUFHRSxrQkFBWSxJQUFJO1FBQ2QsSUFBSSxJQUFJLElBQUksSUFBSSxFQUFFO1lBQ2hCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1NBQ2xCO0lBQ0gsQ0FBQztJQUNILGVBQUM7QUFBRCxDQUFDLEFBUkQsSUFRQztBQUVELElBQUksYUFBYSxHQUFHO0lBQ2xCLGVBQWU7SUFDZixhQUFhO0lBQ2IsdUJBQXVCO0lBQ3ZCLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLFlBQVk7Q0FDYixDQUFDO0FBQ0YsSUFBSSxjQUFjLEdBQUc7SUFDbkIsaUJBQWlCO0lBQ2pCLEtBQUs7SUFDTCxtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLFNBQVM7SUFDVCxpQkFBaUI7SUFDakIscUJBQXFCO0NBQ3RCLENBQUM7QUFRRjtJQVlFLG1DQUNVLGdCQUFrQyxFQUNsQyxJQUFnQixFQUNoQixXQUF3QjtRQUZ4QixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLFNBQUksR0FBSixJQUFJLENBQVk7UUFDaEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFibEMsT0FBRSxHQUFHLEVBQUUsQ0FBQztRQUlSLG1CQUFtQjtRQUNuQixpQkFBWSxHQUFXLEVBQUUsQ0FBQztRQUMxQixlQUFVLEdBQVksSUFBSSxDQUFDO1FBU3pCLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRUQsNENBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzdDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDdkQ7UUFDRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsY0FBYyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM5QyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3hEO0lBQ0gsQ0FBQztJQUVELG1EQUFlLEdBQWY7UUFDRSwwQkFBMEI7SUFDNUIsQ0FBQztJQUVELHdCQUF3QjtJQUN4QixzQ0FBc0M7SUFDdEMsNENBQTRDO0lBQzVDLDZEQUE2RDtJQUM3RCxVQUFVO0lBRVYsa0NBQWtDO0lBRWxDLGdCQUFnQjtJQUNoQiwrRUFBK0U7SUFDL0UsMkJBQTJCO0lBQzNCLFdBQVc7SUFDWCxvQkFBb0I7SUFDcEIsMEJBQTBCO0lBQzFCLGlDQUFpQztJQUNqQywrQkFBK0I7SUFDL0IsaUNBQWlDO0lBRWpDLG1FQUFtRTtJQUNuRSw4REFBOEQ7SUFDOUQsMENBQTBDO0lBQzFDLDRFQUE0RTtJQUM1RSxxQkFBcUI7SUFDckIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixpREFBaUQ7SUFDakQsOERBQThEO0lBRTlELHlEQUF5RDtJQUN6RCw4Q0FBOEM7SUFDOUMscUJBQXFCO0lBQ3JCLGdDQUFnQztJQUNoQyxjQUFjO0lBQ2QsYUFBYTtJQUNiLHFCQUFxQjtJQUNyQixnQ0FBZ0M7SUFDaEMsWUFBWTtJQUNaLFdBQVc7SUFDWCxNQUFNO0lBRU4sa0RBQWMsR0FBZCxVQUFlLElBQUk7UUFDakIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLENBQUM7UUFDTixDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUNmLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDNUIsSUFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLElBQUksSUFBSSxHQUFhLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDekMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLEdBQUcsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxZQUFZLEdBQUcsR0FBRyxDQUFDO0lBQzFCLENBQUM7SUFFRCxrREFBYyxHQUFkLFVBQWUsSUFBSTtRQUNqQixJQUFJLENBQUMsQ0FBQztRQUNOLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ2YsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztRQUM1QixJQUFJLElBQUksR0FBRyxLQUFLLENBQUM7UUFDakIsSUFBSSxJQUFJLEdBQWEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN6QyxHQUFHLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsR0FBRyxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLFlBQVksR0FBRyxHQUFHLENBQUM7SUFDMUIsQ0FBQztJQUVELDJDQUFPLEdBQVAsVUFBUSxJQUEyQjtRQUNqQyxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1NBQ2xEO1FBQ0QsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsRUFBRTtZQUN2QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDO1NBQ3hEO0lBQ0gsQ0FBQztJQTlHVSx5QkFBeUI7UUFOckMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSwrQkFBK0I7WUFDNUMsU0FBUyxFQUFFLENBQUMsOEJBQThCLENBQUM7U0FDNUMsQ0FBQzt5Q0FjNEIseUJBQWdCO1lBQzVCLGlCQUFVO1lBQ0gsMEJBQVc7T0FmdkIseUJBQXlCLENBK0dyQztJQUFELGdDQUFDO0NBQUEsQUEvR0QsSUErR0M7QUEvR1ksOERBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdDaGlsZCwgQWZ0ZXJWaWV3SW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBCYXJjb2RlU2Nhbm5lciB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYmFyY29kZXNjYW5uZXJcIjtcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBNb2RhbENvbXBvbmVudCB9IGZyb20gXCIuLi8uLi9tb2RhbHMvbW9kYWwuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycyB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZUFycmF5IH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvZGF0YS9vYnNlcnZhYmxlLWFycmF5L29ic2VydmFibGUtYXJyYXlcIjtcbmltcG9ydCB7IEV4dGVuZGVkTmF2aWdhdGlvbkV4dHJhcyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXIvcm91dGVyLWV4dGVuc2lvbnNcIjtcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBFdmVudERhdGEgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGVcIjtcbmltcG9ydCB7IFN3aXBlR2VzdHVyZUV2ZW50RGF0YSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2dlc3R1cmVzXCI7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuLi8uLi9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcblxuY2xhc3MgQ2F0ZWdvcnkge1xuICBuYW1lOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IobmFtZSkge1xuICAgIGlmIChuYW1lICE9IG51bGwpIHtcbiAgICAgIHRoaXMubmFtZSA9IG5hbWU7XG4gICAgfVxuICB9XG59XG5cbmxldCBjYXRlZ29yeU5hbWVzID0gW1xuICBcIkRvbGxzICYgQmVhcnNcIixcbiAgXCJFbGVjdHJvbmljc1wiLFxuICBcIkdpZnQgQ2FyZHMgJiBWb3VjaGVyc1wiLFxuICBcIkhlYWx0aCAmIEJlYXV0eVwiLFxuICBcIkhvbWUgJiBHYXJkZW5cIixcbiAgXCJIb21lIEFwcGxpYW5jZXNcIixcbiAgXCJJbmR1c3RyaWFsXCJcbl07XG5sZXQgY2F0ZWdvcnlOYW1lczEgPSBbXG4gIFwiR1BTIEFjY2Vzc29yaWVzXCIsXG4gIFwiR1BTXCIsXG4gIFwiT3RoZXIgRWxlY3Ryb25pY3NcIixcbiAgXCJTbWFydCBHbGFzc2VzXCIsXG4gIFwiVG9yY2hlc1wiLFxuICBcIlZpcnR1YWwgUmVhbGl0eVwiLFxuICBcIldob2xlc2FsZSxCdWxrIExvdHNcIlxuXTtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInByZWZGaW5kQ2F0ZWdvcnlcIixcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgdGVtcGxhdGVVcmw6IFwiLi9maW5kQ2F0ZWdvcnkuY29tcG9uZW50Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL2ZpbmRDYXRlZ29yeS5jb21wb25lbnQuY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIFByZWZGaW5kQ2F0ZWdvcnlDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQge1xuICB2aWV3TW9kZWw7XG4gIGlkID0gXCJcIjtcbiAgLy8gICBjYXRlZ29yaWVzID0gbmV3IE9ic2VydmFibGVBcnJheSgpO1xuICBwdWJsaWMgY2F0ZWdvcmllczE6IEFycmF5PENhdGVnb3J5PjtcbiAgcHVibGljIGNhdGVnb3JpZXMyOiBBcnJheTxDYXRlZ29yeT47XG4gIC8vIHJvd051bWJlciA9IFwiMVwiO1xuICBjYXRlZ29yeUxpc3Q6IHN0cmluZyA9IFwiXCI7XG4gIGlzVmlzaWJsZTE6IGJvb2xlYW4gPSB0cnVlO1xuICBpc1Zpc2libGUyOiBib29sZWFuO1xuICBpc1Zpc2libGUzOiBib29sZWFuO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcbiAgICBwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQsXG4gICAgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2VcbiAgKSB7XG4gICAgdGhpcy51c2VyU2VydmljZS5hY3Rpb25CYXJTdGF0ZSh0cnVlKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdFN0YXRlKHRydWUpO1xuICAgIHRoaXMudXNlclNlcnZpY2UuYm90dG9tQmFyRG90cyg1KTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZSh0cnVlKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZUNoYW5nZXMoMSk7XG4gICAgdGhpcy51c2VyU2VydmljZS5zd2l0Y2hTdGF0ZSh0cnVlKTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuY2F0ZWdvcmllczEgPSBbXTtcbiAgICB0aGlzLmNhdGVnb3JpZXMyID0gW107XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjYXRlZ29yeU5hbWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB0aGlzLmNhdGVnb3JpZXMxLnB1c2gobmV3IENhdGVnb3J5KGNhdGVnb3J5TmFtZXNbaV0pKTtcbiAgICB9XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjYXRlZ29yeU5hbWVzMS5sZW5ndGg7IGkrKykge1xuICAgICAgdGhpcy5jYXRlZ29yaWVzMi5wdXNoKG5ldyBDYXRlZ29yeShjYXRlZ29yeU5hbWVzMVtpXSkpO1xuICAgIH1cbiAgfVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcbiAgICAvLyB0aGlzLmdldEFwcG9pbnRtZW50cygpO1xuICB9XG5cbiAgLy8gICBnZXRBcHBvaW50bWVudHMoKSB7XG4gIC8vICAgICBsZXQgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gIC8vICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiLFxuICAvLyAgICAgICBcIngtcm9sZS1rZXlcIjogXCIzZTFkMDcwZC0xODQ3LTU3ZDctMzZlNS1kNmY4YTE2NjI0MWJcIlxuICAvLyAgICAgfSk7XG5cbiAgLy8gICAgIGNvbnNvbGUubG9nKFwiUmVhY2hlZCBmdW5cIik7XG5cbiAgLy8gICAgIHRoaXMuaHR0cFxuICAvLyAgICAgICAuZ2V0KFwiaHR0cDovL3dlbGNvbWUtYXBpLWRldi5tLXNhcy5jb20vYXBpL2FwcG9pbnRtZW50cy92aXNpdG9yL215XCIsIHtcbiAgLy8gICAgICAgICBoZWFkZXJzOiBoZWFkZXJzXG4gIC8vICAgICAgIH0pXG4gIC8vICAgICAgIC5zdWJzY3JpYmUoXG4gIC8vICAgICAgICAgKHJlczogYW55KSA9PiB7XG4gIC8vICAgICAgICAgICBpZiAocmVzLmlzU3VjY2Vzcykge1xuICAvLyAgICAgICAgICAgICBsZXQgcmVzdWx0OiBhbnk7XG4gIC8vICAgICAgICAgICAgIHJlc3VsdCA9IHJlcy5kYXRhO1xuXG4gIC8vICAgICAgICAgICAgIGlmIChyZXN1bHQub2xkICE9IG51bGwgJiYgcmVzdWx0Lm9sZCAhPSB1bmRlZmluZWQpIHtcbiAgLy8gICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHJlc3VsdC5vbGQubGVuZ3RoOyBpKyspIHtcbiAgLy8gICAgICAgICAgICAgICAgIHRoaXMuYXBwb2ludG1lbnRzLnB1c2goXG4gIC8vICAgICAgICAgICAgICAgICAgIG5ldyBBcHBvaW50bWVudChyZXN1bHQub2xkW2ldLmlkLCByZXN1bHQub2xkW2ldLnN0YXR1cylcbiAgLy8gICAgICAgICAgICAgICAgICk7XG4gIC8vICAgICAgICAgICAgICAgfVxuICAvLyAgICAgICAgICAgICB9XG4gIC8vICAgICAgICAgICAgIHRoaXMudmlld01vZGVsID0gbmV3IE9ic2VydmFibGUoKTtcbiAgLy8gICAgICAgICAgICAgdGhpcy52aWV3TW9kZWwuc2V0KFwiaXRlbXNcIiwgdGhpcy5hcHBvaW50bWVudHMpO1xuXG4gIC8vICAgICAgICAgICAgIHRoaXMucGFnZS5iaW5kaW5nQ29udGV4dCA9IHRoaXMudmlld01vZGVsO1xuICAvLyAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmFwcG9pbnRtZW50cyk7XG4gIC8vICAgICAgICAgICB9IGVsc2Uge1xuICAvLyAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXMpO1xuICAvLyAgICAgICAgICAgfVxuICAvLyAgICAgICAgIH0sXG4gIC8vICAgICAgICAgZXJyb3IgPT4ge1xuICAvLyAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAvLyAgICAgICAgIH1cbiAgLy8gICAgICAgKTtcbiAgLy8gICB9XG5cbiAgb25DYXRlZ29yeVRhcDEoYXJncykge1xuICAgIHRoaXMuaXNWaXNpYmxlMiA9IHRydWU7XG4gICAgdmFyIGk7XG4gICAgaSA9IGFyZ3MuaW5kZXg7XG4gICAgdmFyIHN0ciA9IHRoaXMuY2F0ZWdvcnlMaXN0O1xuICAgIHZhciBzdHIxID0gXCIgPiBcIjtcbiAgICB2YXIgaXRlbSA9IDxDYXRlZ29yeT50aGlzLmNhdGVnb3JpZXMxW2ldO1xuICAgIHN0ciA9IHN0ci5jb25jYXQoaXRlbS5uYW1lKTtcbiAgICBzdHIgPSBzdHIuY29uY2F0KHN0cjEpO1xuICAgIHRoaXMuY2F0ZWdvcnlMaXN0ID0gc3RyO1xuICB9XG5cbiAgb25DYXRlZ29yeVRhcDIoYXJncykge1xuICAgIHZhciBpO1xuICAgIGkgPSBhcmdzLmluZGV4O1xuICAgIHZhciBzdHIgPSB0aGlzLmNhdGVnb3J5TGlzdDtcbiAgICB2YXIgc3RyMSA9IFwiID4gXCI7XG4gICAgdmFyIGl0ZW0gPSA8Q2F0ZWdvcnk+dGhpcy5jYXRlZ29yaWVzMltpXTtcbiAgICBzdHIgPSBzdHIuY29uY2F0KGl0ZW0ubmFtZSk7XG4gICAgc3RyID0gc3RyLmNvbmNhdChzdHIxKTtcbiAgICB0aGlzLmNhdGVnb3J5TGlzdCA9IHN0cjtcbiAgfVxuXG4gIG9uU3dpcGUoYXJnczogU3dpcGVHZXN0dXJlRXZlbnREYXRhKSB7XG4gICAgaWYgKGFyZ3MuZGlyZWN0aW9uID09IDEpIHtcbiAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvcHJlZlN1Z2dlc3RcIl0pO1xuICAgIH1cbiAgICBpZiAoYXJncy5kaXJlY3Rpb24gPT0gMikge1xuICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9wcmVmU3RvcmVDYXRlZ29yeVwiXSk7XG4gICAgfVxuICB9XG59XG4iXX0=
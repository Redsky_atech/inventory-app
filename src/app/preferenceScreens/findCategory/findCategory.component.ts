import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { BarcodeScanner } from "nativescript-barcodescanner";
import { RouterExtensions } from "nativescript-angular/router";
import { ModalComponent } from "../../modals/modal.component";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { Router, ActivatedRoute } from "@angular/router";
import { Observable, EventData } from "tns-core-modules/data/observable";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { UserService } from "../../services/user.service";

class Category {
  name: string;

  constructor(name) {
    if (name != null) {
      this.name = name;
    }
  }
}

let categoryNames = [
  "Dolls & Bears",
  "Electronics",
  "Gift Cards & Vouchers",
  "Health & Beauty",
  "Home & Garden",
  "Home Appliances",
  "Industrial"
];
let categoryNames1 = [
  "GPS Accessories",
  "GPS",
  "Other Electronics",
  "Smart Glasses",
  "Torches",
  "Virtual Reality",
  "Wholesale,Bulk Lots"
];

@Component({
  selector: "prefFindCategory",
  moduleId: module.id,
  templateUrl: "./findCategory.component.html",
  styleUrls: ["./findCategory.component.css"]
})
export class PrefFindCategoryComponent implements OnInit, AfterViewInit {
  viewModel;
  id = "";
  //   categories = new ObservableArray();
  public categories1: Array<Category>;
  public categories2: Array<Category>;
  // rowNumber = "1";
  categoryList: string = "";
  isVisible1: boolean = true;
  isVisible2: boolean;
  isVisible3: boolean;

  constructor(
    private routerExtensions: RouterExtensions,
    private http: HttpClient,
    private userService: UserService
  ) {
    this.userService.actionBarState(true);
    this.userService.bottomBarDotState(true);
    this.userService.bottomBarDots(5);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
    this.userService.switchState(true);
  }

  ngOnInit(): void {
    this.categories1 = [];
    this.categories2 = [];
    for (let i = 0; i < categoryNames.length; i++) {
      this.categories1.push(new Category(categoryNames[i]));
    }
    for (let i = 0; i < categoryNames1.length; i++) {
      this.categories2.push(new Category(categoryNames1[i]));
    }
  }

  ngAfterViewInit() {
    // this.getAppointments();
  }

  //   getAppointments() {
  //     let headers = new HttpHeaders({
  //       "Content-Type": "application/json",
  //       "x-role-key": "3e1d070d-1847-57d7-36e5-d6f8a166241b"
  //     });

  //     console.log("Reached fun");

  //     this.http
  //       .get("http://welcome-api-dev.m-sas.com/api/appointments/visitor/my", {
  //         headers: headers
  //       })
  //       .subscribe(
  //         (res: any) => {
  //           if (res.isSuccess) {
  //             let result: any;
  //             result = res.data;

  //             if (result.old != null && result.old != undefined) {
  //               for (var i = 0; i < result.old.length; i++) {
  //                 this.appointments.push(
  //                   new Appointment(result.old[i].id, result.old[i].status)
  //                 );
  //               }
  //             }
  //             this.viewModel = new Observable();
  //             this.viewModel.set("items", this.appointments);

  //             this.page.bindingContext = this.viewModel;
  //             console.log(this.appointments);
  //           } else {
  //             console.log(res);
  //           }
  //         },
  //         error => {
  //           console.log(error);
  //         }
  //       );
  //   }

  onCategoryTap1(args) {
    this.isVisible2 = true;
    var i;
    i = args.index;
    var str = this.categoryList;
    var str1 = " > ";
    var item = <Category>this.categories1[i];
    str = str.concat(item.name);
    str = str.concat(str1);
    this.categoryList = str;
  }

  onCategoryTap2(args) {
    var i;
    i = args.index;
    var str = this.categoryList;
    var str1 = " > ";
    var item = <Category>this.categories2[i];
    str = str.concat(item.name);
    str = str.concat(str1);
    this.categoryList = str;
  }

  onSwipe(args: SwipeGestureEventData) {
    if (args.direction == 1) {
      this.routerExtensions.navigate(["/prefSuggest"]);
    }
    if (args.direction == 2) {
      this.routerExtensions.navigate(["/prefStoreCategory"]);
    }
  }
}

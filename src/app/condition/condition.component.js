"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_service_1 = require("../services/user.service");
var router_1 = require("@angular/router");
var ConditionComponent = /** @class */ (function () {
    function ConditionComponent(route, router, userService) {
        var _this = this;
        this.route = route;
        this.router = router;
        this.userService = userService;
        this.condition1 = true;
        this.condition2 = false;
        this.condition3 = false;
        this.condition4 = false;
        this.condition5 = false;
        this.condition6 = false;
        this.storeCategory = "";
        this.isVisibleConditions = false;
        this.userService.actionBarState(true);
        this.userService.bottomBarDots(7);
        this.userService.bottomBarDotState(true);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.userService.switchState(false);
        route.queryParams.subscribe(function (params) {
            _this.storeCategory = params["storeCategory"];
        });
        console.log(this.storeCategory);
    }
    ConditionComponent.prototype.ngOnInit = function () { };
    ConditionComponent.prototype.conditionClick1 = function () {
        if (this.condition1 == false) {
            this.condition1 = true;
            this.condition2 = false;
            this.condition3 = false;
            this.condition4 = false;
            this.condition5 = false;
            this.condition6 = false;
        }
    };
    ConditionComponent.prototype.conditionClick2 = function () {
        if (this.condition2 == false) {
            this.condition1 = false;
            this.condition2 = true;
            this.condition3 = false;
            this.condition4 = false;
            this.condition5 = false;
            this.condition6 = false;
        }
    };
    ConditionComponent.prototype.conditionClick3 = function () {
        if (this.condition3 == false) {
            this.condition1 = false;
            this.condition2 = false;
            this.condition3 = true;
            this.condition4 = false;
            this.condition5 = false;
            this.condition6 = false;
        }
    };
    ConditionComponent.prototype.conditionClick4 = function () {
        if (this.condition4 == false) {
            this.condition1 = false;
            this.condition2 = false;
            this.condition3 = false;
            this.condition4 = true;
            this.condition5 = false;
            this.condition6 = false;
        }
    };
    ConditionComponent.prototype.conditionClick5 = function () {
        if (this.condition5 == false) {
            this.condition1 = false;
            this.condition2 = false;
            this.condition3 = false;
            this.condition4 = false;
            this.condition5 = true;
            this.condition6 = false;
        }
    };
    ConditionComponent.prototype.conditionClick6 = function () {
        if (this.condition6 == false) {
            this.condition1 = false;
            this.condition2 = false;
            this.condition3 = false;
            this.condition4 = false;
            this.condition5 = false;
            this.condition6 = true;
        }
    };
    ConditionComponent.prototype.onSwipe = function (args) {
        if (args.direction == 1) {
            this.router.navigate(["/storeCategory"]);
        }
        if (args.direction == 2) {
            this.router.navigate(["/description"]);
        }
    };
    ConditionComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: "./condition.component.html",
            styleUrls: ["./condition.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            router_1.Router,
            user_service_1.UserService])
    ], ConditionComponent);
    return ConditionComponent;
}());
exports.ConditionComponent = ConditionComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZGl0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNvbmRpdGlvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBeUU7QUFDekUseURBQXVEO0FBQ3ZELDBDQUF5RDtBQVF6RDtJQVNFLDRCQUNVLEtBQXFCLEVBQ3JCLE1BQWMsRUFDZCxXQUF3QjtRQUhsQyxpQkFlQztRQWRTLFVBQUssR0FBTCxLQUFLLENBQWdCO1FBQ3JCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQVhsQyxlQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixlQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ25CLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQUNuQix3QkFBbUIsR0FBWSxLQUFLLENBQUM7UUFNbkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3BDLEtBQUssQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtZQUNoQyxLQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUMvQyxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFFRCxxQ0FBUSxHQUFSLGNBQWtCLENBQUM7SUFFbkIsNENBQWUsR0FBZjtRQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUU7WUFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7U0FDekI7SUFDSCxDQUFDO0lBQ0QsNENBQWUsR0FBZjtRQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUU7WUFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7U0FDekI7SUFDSCxDQUFDO0lBQ0QsNENBQWUsR0FBZjtRQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUU7WUFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7U0FDekI7SUFDSCxDQUFDO0lBQ0QsNENBQWUsR0FBZjtRQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUU7WUFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7U0FDekI7SUFDSCxDQUFDO0lBQ0QsNENBQWUsR0FBZjtRQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUU7WUFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7U0FDekI7SUFDSCxDQUFDO0lBQ0QsNENBQWUsR0FBZjtRQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUU7WUFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7U0FDeEI7SUFDSCxDQUFDO0lBQ0Qsb0NBQU8sR0FBUCxVQUFRLElBQTJCO1FBQ2pDLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7U0FDMUM7UUFDRCxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztTQUN4QztJQUNILENBQUM7SUEvRlUsa0JBQWtCO1FBTDlCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLDRCQUE0QjtZQUN6QyxTQUFTLEVBQUUsQ0FBQywyQkFBMkIsQ0FBQztTQUN6QyxDQUFDO3lDQVdpQix1QkFBYztZQUNiLGVBQU07WUFDRCwwQkFBVztPQVp2QixrQkFBa0IsQ0FnRzlCO0lBQUQseUJBQUM7Q0FBQSxBQWhHRCxJQWdHQztBQWhHWSxnREFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBFbGVtZW50UmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlLCBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IFN3aXBlR2VzdHVyZUV2ZW50RGF0YSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2dlc3R1cmVzXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vY29uZGl0aW9uLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL2NvbmRpdGlvbi5jb21wb25lbnQuY3NzXCJdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb25kaXRpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIGNvbmRpdGlvbjEgPSB0cnVlO1xyXG4gIGNvbmRpdGlvbjIgPSBmYWxzZTtcclxuICBjb25kaXRpb24zID0gZmFsc2U7XHJcbiAgY29uZGl0aW9uNCA9IGZhbHNlO1xyXG4gIGNvbmRpdGlvbjUgPSBmYWxzZTtcclxuICBjb25kaXRpb242ID0gZmFsc2U7XHJcbiAgc3RvcmVDYXRlZ29yeSA9IFwiXCI7XHJcbiAgaXNWaXNpYmxlQ29uZGl0aW9uczogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSByb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxyXG4gICAgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2VcclxuICApIHtcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYWN0aW9uQmFyU3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdHMoNyk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdFN0YXRlKHRydWUpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZUNoYW5nZXMoMSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnN3aXRjaFN0YXRlKGZhbHNlKTtcclxuICAgIHJvdXRlLnF1ZXJ5UGFyYW1zLnN1YnNjcmliZShwYXJhbXMgPT4ge1xyXG4gICAgICB0aGlzLnN0b3JlQ2F0ZWdvcnkgPSBwYXJhbXNbXCJzdG9yZUNhdGVnb3J5XCJdO1xyXG4gICAgfSk7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLnN0b3JlQ2F0ZWdvcnkpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7fVxyXG5cclxuICBjb25kaXRpb25DbGljazEoKSB7XHJcbiAgICBpZiAodGhpcy5jb25kaXRpb24xID09IGZhbHNlKSB7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uMSA9IHRydWU7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uMiA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjMgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jb25kaXRpb240ID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uNSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjYgPSBmYWxzZTtcclxuICAgIH1cclxuICB9XHJcbiAgY29uZGl0aW9uQ2xpY2syKCkge1xyXG4gICAgaWYgKHRoaXMuY29uZGl0aW9uMiA9PSBmYWxzZSkge1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjEgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jb25kaXRpb24yID0gdHJ1ZTtcclxuICAgICAgdGhpcy5jb25kaXRpb24zID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uNCA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjUgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jb25kaXRpb242ID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGNvbmRpdGlvbkNsaWNrMygpIHtcclxuICAgIGlmICh0aGlzLmNvbmRpdGlvbjMgPT0gZmFsc2UpIHtcclxuICAgICAgdGhpcy5jb25kaXRpb24xID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uMiA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjMgPSB0cnVlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjQgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jb25kaXRpb241ID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uNiA9IGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuICBjb25kaXRpb25DbGljazQoKSB7XHJcbiAgICBpZiAodGhpcy5jb25kaXRpb240ID09IGZhbHNlKSB7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uMSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjIgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jb25kaXRpb24zID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uNCA9IHRydWU7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uNSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjYgPSBmYWxzZTtcclxuICAgIH1cclxuICB9XHJcbiAgY29uZGl0aW9uQ2xpY2s1KCkge1xyXG4gICAgaWYgKHRoaXMuY29uZGl0aW9uNSA9PSBmYWxzZSkge1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjEgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jb25kaXRpb24yID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uMyA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjQgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jb25kaXRpb241ID0gdHJ1ZTtcclxuICAgICAgdGhpcy5jb25kaXRpb242ID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGNvbmRpdGlvbkNsaWNrNigpIHtcclxuICAgIGlmICh0aGlzLmNvbmRpdGlvbjYgPT0gZmFsc2UpIHtcclxuICAgICAgdGhpcy5jb25kaXRpb24xID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uMiA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjMgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jb25kaXRpb240ID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uNSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmNvbmRpdGlvbjYgPSB0cnVlO1xyXG4gICAgfVxyXG4gIH1cclxuICBvblN3aXBlKGFyZ3M6IFN3aXBlR2VzdHVyZUV2ZW50RGF0YSkge1xyXG4gICAgaWYgKGFyZ3MuZGlyZWN0aW9uID09IDEpIHtcclxuICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1wiL3N0b3JlQ2F0ZWdvcnlcIl0pO1xyXG4gICAgfVxyXG4gICAgaWYgKGFyZ3MuZGlyZWN0aW9uID09IDIpIHtcclxuICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1wiL2Rlc2NyaXB0aW9uXCJdKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var user_service_1 = require("../services/user.service");
var EditListingsComponent = /** @class */ (function () {
    function EditListingsComponent(routerExtensions, userService) {
        this.userService = userService;
        this.listing1 = true;
        this.listing2 = false;
        this.listing3 = false;
        this.listing4 = false;
        this.listing5 = false;
        this.listing6 = false;
        this.listing7 = false;
        this.listing8 = false;
        this.listing9 = false;
        this.listing10 = false;
        this.routerExtensions = routerExtensions;
        this.userService.actionBarState(true);
        this.userService.bottomBarDots(11);
        this.userService.bottomBarDotState(false);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
    }
    EditListingsComponent.prototype.listingClick1 = function () {
        this.listing1 = true;
        this.listing2 = false;
        this.listing3 = false;
        this.listing4 = false;
        this.listing5 = false;
        this.listing6 = false;
        this.listing7 = false;
        this.listing8 = false;
        this.listing9 = false;
        this.listing10 = false;
    };
    EditListingsComponent.prototype.listingClick2 = function () {
        this.listing1 = false;
        this.listing2 = true;
        this.listing3 = false;
        this.listing4 = false;
        this.listing5 = false;
        this.listing6 = false;
        this.listing7 = false;
        this.listing8 = false;
        this.listing9 = false;
        this.listing10 = false;
    };
    EditListingsComponent.prototype.listingClick3 = function () {
        this.listing1 = false;
        this.listing2 = false;
        this.listing3 = true;
        this.listing4 = false;
        this.listing5 = false;
        this.listing6 = false;
        this.listing7 = false;
        this.listing8 = false;
        this.listing9 = false;
        this.listing10 = false;
    };
    EditListingsComponent.prototype.listingClick4 = function () {
        this.listing1 = false;
        this.listing2 = false;
        this.listing3 = false;
        this.listing4 = true;
        this.listing5 = false;
        this.listing6 = false;
        this.listing7 = false;
        this.listing8 = false;
        this.listing9 = false;
        this.listing10 = false;
    };
    EditListingsComponent.prototype.listingClick5 = function () {
        this.listing1 = false;
        this.listing2 = false;
        this.listing3 = false;
        this.listing4 = false;
        this.listing5 = true;
        this.listing6 = false;
        this.listing7 = false;
        this.listing8 = false;
        this.listing9 = false;
        this.listing10 = false;
    };
    EditListingsComponent.prototype.listingClick6 = function () {
        this.listing1 = false;
        this.listing2 = false;
        this.listing3 = false;
        this.listing4 = false;
        this.listing5 = false;
        this.listing6 = true;
        this.listing7 = false;
        this.listing8 = false;
        this.listing9 = false;
        this.listing10 = false;
    };
    EditListingsComponent.prototype.listingClick7 = function () {
        this.listing1 = false;
        this.listing2 = false;
        this.listing3 = false;
        this.listing4 = false;
        this.listing5 = false;
        this.listing6 = false;
        this.listing7 = true;
        this.listing8 = false;
        this.listing9 = false;
        this.listing10 = false;
    };
    EditListingsComponent.prototype.listingClick8 = function () {
        this.listing1 = false;
        this.listing2 = false;
        this.listing3 = false;
        this.listing4 = false;
        this.listing5 = false;
        this.listing6 = false;
        this.listing7 = false;
        this.listing8 = true;
        this.listing9 = false;
        this.listing10 = false;
    };
    EditListingsComponent.prototype.listingClick9 = function () {
        this.listing1 = false;
        this.listing2 = false;
        this.listing3 = false;
        this.listing4 = false;
        this.listing5 = false;
        this.listing6 = false;
        this.listing7 = false;
        this.listing8 = false;
        this.listing9 = true;
        this.listing10 = false;
    };
    EditListingsComponent.prototype.listingClick10 = function () {
        this.listing1 = false;
        this.listing2 = false;
        this.listing3 = false;
        this.listing4 = false;
        this.listing5 = false;
        this.listing6 = false;
        this.listing7 = false;
        this.listing8 = false;
        this.listing9 = false;
        this.listing10 = true;
    };
    EditListingsComponent.prototype.onProceed = function () {
        this.routerExtensions.navigate(["/listingFunctions"], {
            animated: true,
            transition: {
                name: "slide",
                duration: 200,
                curve: "ease"
            }
        });
    };
    EditListingsComponent = __decorate([
        core_1.Component({
            selector: "ns-editListings",
            moduleId: module.id,
            templateUrl: "./editListings.component.html",
            styleUrls: ["./editListings.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            user_service_1.UserService])
    ], EditListingsComponent);
    return EditListingsComponent;
}());
exports.EditListingsComponent = EditListingsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWRpdExpc3RpbmdzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImVkaXRMaXN0aW5ncy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBeUU7QUFDekUsc0RBQStEO0FBRS9ELHlEQUF1RDtBQVF2RDtJQWNFLCtCQUNFLGdCQUFrQyxFQUMxQixXQUF3QjtRQUF4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQWJsQyxhQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBTWhCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVELDZDQUFhLEdBQWI7UUFDRSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNyQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUN6QixDQUFDO0lBQ0QsNkNBQWEsR0FBYjtRQUNFLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO0lBQ3pCLENBQUM7SUFDRCw2Q0FBYSxHQUFiO1FBQ0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7SUFDekIsQ0FBQztJQUNELDZDQUFhLEdBQWI7UUFDRSxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNyQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUN6QixDQUFDO0lBQ0QsNkNBQWEsR0FBYjtRQUNFLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO0lBQ3pCLENBQUM7SUFDRCw2Q0FBYSxHQUFiO1FBQ0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7SUFDekIsQ0FBQztJQUNELDZDQUFhLEdBQWI7UUFDRSxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNyQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUN6QixDQUFDO0lBQ0QsNkNBQWEsR0FBYjtRQUNFLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO0lBQ3pCLENBQUM7SUFDRCw2Q0FBYSxHQUFiO1FBQ0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7SUFDekIsQ0FBQztJQUNELDhDQUFjLEdBQWQ7UUFDRSxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztJQUN4QixDQUFDO0lBRU0seUNBQVMsR0FBaEI7UUFDRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsbUJBQW1CLENBQUMsRUFBRTtZQUNwRCxRQUFRLEVBQUUsSUFBSTtZQUNkLFVBQVUsRUFBRTtnQkFDVixJQUFJLEVBQUUsT0FBTztnQkFDYixRQUFRLEVBQUUsR0FBRztnQkFDYixLQUFLLEVBQUUsTUFBTTthQUNkO1NBQ0YsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQTVKVSxxQkFBcUI7UUFOakMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxpQkFBaUI7WUFDM0IsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSwrQkFBK0I7WUFDNUMsU0FBUyxFQUFFLENBQUMsOEJBQThCLENBQUM7U0FDNUMsQ0FBQzt5Q0FnQm9CLHlCQUFnQjtZQUNiLDBCQUFXO09BaEJ2QixxQkFBcUIsQ0E2SmpDO0lBQUQsNEJBQUM7Q0FBQSxBQTdKRCxJQTZKQztBQTdKWSxzREFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBFbGVtZW50UmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgU3dpcGVHZXN0dXJlRXZlbnREYXRhIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZ2VzdHVyZXNcIjtcclxuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJucy1lZGl0TGlzdGluZ3NcIixcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vZWRpdExpc3RpbmdzLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL2VkaXRMaXN0aW5ncy5jb21wb25lbnQuY3NzXCJdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBFZGl0TGlzdGluZ3NDb21wb25lbnQge1xyXG4gIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnM7XHJcblxyXG4gIGxpc3RpbmcxID0gdHJ1ZTtcclxuICBsaXN0aW5nMiA9IGZhbHNlO1xyXG4gIGxpc3RpbmczID0gZmFsc2U7XHJcbiAgbGlzdGluZzQgPSBmYWxzZTtcclxuICBsaXN0aW5nNSA9IGZhbHNlO1xyXG4gIGxpc3Rpbmc2ID0gZmFsc2U7XHJcbiAgbGlzdGluZzcgPSBmYWxzZTtcclxuICBsaXN0aW5nOCA9IGZhbHNlO1xyXG4gIGxpc3Rpbmc5ID0gZmFsc2U7XHJcbiAgbGlzdGluZzEwID0gZmFsc2U7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcclxuICAgIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMgPSByb3V0ZXJFeHRlbnNpb25zO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5hY3Rpb25CYXJTdGF0ZSh0cnVlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYm90dG9tQmFyRG90cygxMSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdFN0YXRlKGZhbHNlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UudGFiVmlld1N0YXRlKHRydWUpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGVDaGFuZ2VzKDEpO1xyXG4gIH1cclxuXHJcbiAgbGlzdGluZ0NsaWNrMSgpIHtcclxuICAgIHRoaXMubGlzdGluZzEgPSB0cnVlO1xyXG4gICAgdGhpcy5saXN0aW5nMiA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nMyA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nNCA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nNSA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nNiA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nNyA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nOCA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nOSA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nMTAgPSBmYWxzZTtcclxuICB9XHJcbiAgbGlzdGluZ0NsaWNrMigpIHtcclxuICAgIHRoaXMubGlzdGluZzEgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzIgPSB0cnVlO1xyXG4gICAgdGhpcy5saXN0aW5nMyA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nNCA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nNSA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nNiA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nNyA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nOCA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nOSA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nMTAgPSBmYWxzZTtcclxuICB9XHJcbiAgbGlzdGluZ0NsaWNrMygpIHtcclxuICAgIHRoaXMubGlzdGluZzEgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzIgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzMgPSB0cnVlO1xyXG4gICAgdGhpcy5saXN0aW5nNCA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nNSA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nNiA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nNyA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nOCA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nOSA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nMTAgPSBmYWxzZTtcclxuICB9XHJcbiAgbGlzdGluZ0NsaWNrNCgpIHtcclxuICAgIHRoaXMubGlzdGluZzEgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzIgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzMgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzQgPSB0cnVlO1xyXG4gICAgdGhpcy5saXN0aW5nNSA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nNiA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nNyA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nOCA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nOSA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nMTAgPSBmYWxzZTtcclxuICB9XHJcbiAgbGlzdGluZ0NsaWNrNSgpIHtcclxuICAgIHRoaXMubGlzdGluZzEgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzIgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzMgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzQgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzUgPSB0cnVlO1xyXG4gICAgdGhpcy5saXN0aW5nNiA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nNyA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nOCA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nOSA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nMTAgPSBmYWxzZTtcclxuICB9XHJcbiAgbGlzdGluZ0NsaWNrNigpIHtcclxuICAgIHRoaXMubGlzdGluZzEgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzIgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzMgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzQgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzUgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzYgPSB0cnVlO1xyXG4gICAgdGhpcy5saXN0aW5nNyA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nOCA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nOSA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nMTAgPSBmYWxzZTtcclxuICB9XHJcbiAgbGlzdGluZ0NsaWNrNygpIHtcclxuICAgIHRoaXMubGlzdGluZzEgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzIgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzMgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzQgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzUgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzYgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzcgPSB0cnVlO1xyXG4gICAgdGhpcy5saXN0aW5nOCA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nOSA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nMTAgPSBmYWxzZTtcclxuICB9XHJcbiAgbGlzdGluZ0NsaWNrOCgpIHtcclxuICAgIHRoaXMubGlzdGluZzEgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzIgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzMgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzQgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzUgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzYgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzcgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzggPSB0cnVlO1xyXG4gICAgdGhpcy5saXN0aW5nOSA9IGZhbHNlO1xyXG4gICAgdGhpcy5saXN0aW5nMTAgPSBmYWxzZTtcclxuICB9XHJcbiAgbGlzdGluZ0NsaWNrOSgpIHtcclxuICAgIHRoaXMubGlzdGluZzEgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzIgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzMgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzQgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzUgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzYgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzcgPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzggPSBmYWxzZTtcclxuICAgIHRoaXMubGlzdGluZzkgPSB0cnVlO1xyXG4gICAgdGhpcy5saXN0aW5nMTAgPSBmYWxzZTtcclxuICB9XHJcbiAgbGlzdGluZ0NsaWNrMTAoKSB7XHJcbiAgICB0aGlzLmxpc3RpbmcxID0gZmFsc2U7XHJcbiAgICB0aGlzLmxpc3RpbmcyID0gZmFsc2U7XHJcbiAgICB0aGlzLmxpc3RpbmczID0gZmFsc2U7XHJcbiAgICB0aGlzLmxpc3Rpbmc0ID0gZmFsc2U7XHJcbiAgICB0aGlzLmxpc3Rpbmc1ID0gZmFsc2U7XHJcbiAgICB0aGlzLmxpc3Rpbmc2ID0gZmFsc2U7XHJcbiAgICB0aGlzLmxpc3Rpbmc3ID0gZmFsc2U7XHJcbiAgICB0aGlzLmxpc3Rpbmc4ID0gZmFsc2U7XHJcbiAgICB0aGlzLmxpc3Rpbmc5ID0gZmFsc2U7XHJcbiAgICB0aGlzLmxpc3RpbmcxMCA9IHRydWU7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25Qcm9jZWVkKCkge1xyXG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9saXN0aW5nRnVuY3Rpb25zXCJdLCB7XHJcbiAgICAgIGFuaW1hdGVkOiB0cnVlLFxyXG4gICAgICB0cmFuc2l0aW9uOiB7XHJcbiAgICAgICAgbmFtZTogXCJzbGlkZVwiLFxyXG4gICAgICAgIGR1cmF0aW9uOiAyMDAsXHJcbiAgICAgICAgY3VydmU6IFwiZWFzZVwiXHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=
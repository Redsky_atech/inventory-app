import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { UserService } from "../services/user.service";

@Component({
  selector: "ns-editListings",
  moduleId: module.id,
  templateUrl: "./editListings.component.html",
  styleUrls: ["./editListings.component.css"]
})
export class EditListingsComponent {
  routerExtensions: RouterExtensions;

  listing1 = true;
  listing2 = false;
  listing3 = false;
  listing4 = false;
  listing5 = false;
  listing6 = false;
  listing7 = false;
  listing8 = false;
  listing9 = false;
  listing10 = false;

  constructor(
    routerExtensions: RouterExtensions,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDots(11);
    this.userService.bottomBarDotState(false);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
  }

  listingClick1() {
    this.listing1 = true;
    this.listing2 = false;
    this.listing3 = false;
    this.listing4 = false;
    this.listing5 = false;
    this.listing6 = false;
    this.listing7 = false;
    this.listing8 = false;
    this.listing9 = false;
    this.listing10 = false;
  }
  listingClick2() {
    this.listing1 = false;
    this.listing2 = true;
    this.listing3 = false;
    this.listing4 = false;
    this.listing5 = false;
    this.listing6 = false;
    this.listing7 = false;
    this.listing8 = false;
    this.listing9 = false;
    this.listing10 = false;
  }
  listingClick3() {
    this.listing1 = false;
    this.listing2 = false;
    this.listing3 = true;
    this.listing4 = false;
    this.listing5 = false;
    this.listing6 = false;
    this.listing7 = false;
    this.listing8 = false;
    this.listing9 = false;
    this.listing10 = false;
  }
  listingClick4() {
    this.listing1 = false;
    this.listing2 = false;
    this.listing3 = false;
    this.listing4 = true;
    this.listing5 = false;
    this.listing6 = false;
    this.listing7 = false;
    this.listing8 = false;
    this.listing9 = false;
    this.listing10 = false;
  }
  listingClick5() {
    this.listing1 = false;
    this.listing2 = false;
    this.listing3 = false;
    this.listing4 = false;
    this.listing5 = true;
    this.listing6 = false;
    this.listing7 = false;
    this.listing8 = false;
    this.listing9 = false;
    this.listing10 = false;
  }
  listingClick6() {
    this.listing1 = false;
    this.listing2 = false;
    this.listing3 = false;
    this.listing4 = false;
    this.listing5 = false;
    this.listing6 = true;
    this.listing7 = false;
    this.listing8 = false;
    this.listing9 = false;
    this.listing10 = false;
  }
  listingClick7() {
    this.listing1 = false;
    this.listing2 = false;
    this.listing3 = false;
    this.listing4 = false;
    this.listing5 = false;
    this.listing6 = false;
    this.listing7 = true;
    this.listing8 = false;
    this.listing9 = false;
    this.listing10 = false;
  }
  listingClick8() {
    this.listing1 = false;
    this.listing2 = false;
    this.listing3 = false;
    this.listing4 = false;
    this.listing5 = false;
    this.listing6 = false;
    this.listing7 = false;
    this.listing8 = true;
    this.listing9 = false;
    this.listing10 = false;
  }
  listingClick9() {
    this.listing1 = false;
    this.listing2 = false;
    this.listing3 = false;
    this.listing4 = false;
    this.listing5 = false;
    this.listing6 = false;
    this.listing7 = false;
    this.listing8 = false;
    this.listing9 = true;
    this.listing10 = false;
  }
  listingClick10() {
    this.listing1 = false;
    this.listing2 = false;
    this.listing3 = false;
    this.listing4 = false;
    this.listing5 = false;
    this.listing6 = false;
    this.listing7 = false;
    this.listing8 = false;
    this.listing9 = false;
    this.listing10 = true;
  }

  public onProceed() {
    this.routerExtensions.navigate(["/listingFunctions"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var angular_1 = require("nativescript-checkbox/angular");
var element_registry_1 = require("nativescript-angular/element-registry");
var modal_component_1 = require("./modals/modal.component");
var nativescript_barcodescanner_1 = require("nativescript-barcodescanner");
var sku_component_1 = require("./sku/sku.component");
var upc_component_1 = require("./upc/upc.component");
var http_1 = require("@angular/http");
var http_2 = require("nativescript-angular/http");
var forms_1 = require("nativescript-angular/forms");
var http_client_1 = require("nativescript-angular/http-client");
var http_3 = require("@angular/common/http");
var title_component_1 = require("./title/title.component");
var findCategory_component_1 = require("./findCategory/findCategory.component");
var user_service_1 = require("./services/user.service");
var suggestedcategories_component_1 = require("./suggestedcategories/suggestedcategories.component");
var angular_2 = require("nativescript-drop-down/angular");
var store_categories_component_1 = require("./storeCategories/components/store-categories.component");
var condition_component_1 = require("./condition/condition.component");
var selling_detail_component_1 = require("./sellingDetail/selling-detail.component");
var description_component_1 = require("./description/description.component");
var item_specifies_component_1 = require("./itemspecifies/item-specifies.component");
var payment_methods_component_1 = require("./paymentMethods/payment-methods.component");
var return_options_component_1 = require("./returnOptions/return-options.component");
var businesspolicies_component_1 = require("./businesspolicies/businesspolicies.component");
var weight_dimension_component_1 = require("./weightDimension/weight-dimension.component");
var savedraft_component_1 = require("./savedraft/savedraft.component");
var pictures_component_1 = require("./pictures/pictures.component");
var add_pictures_component_1 = require("./addPictures/add-pictures.component");
var createListing_component_1 = require("./createListing/createListing.component");
var listingFunctions_component_1 = require("./listingFunctions/listingFunctions.component");
var submitListings_component_1 = require("./submitListings/submitListings.component");
var editListings_component_1 = require("./editListings/editListings.component");
var warehouseFunctions_component_1 = require("./warehouseFunctions/warehouseFunctions.component");
var shelveInventory_component_1 = require("./shelveInventory/shelveInventory.component");
var userAccount_component_1 = require("./userAccount/userAccount.component");
var checkItemStatus_component_1 = require("./checkItemStatus/checkItemStatus.component");
var inventoryLocation_component_1 = require("./inventoryLocation/inventoryLocation.component");
var shipItems_component_1 = require("./shipItems/shipItems.component");
var stockErrors_component_1 = require("./stockErrors/stockErrors.component");
var preferencesDefaults_component_1 = require("./preferencesDefaults/preferencesDefaults.component");
var sku_component_2 = require("./preferenceScreens/sku/sku.component");
var upc_component_2 = require("./preferenceScreens/upc/upc.component");
var title_component_2 = require("./preferenceScreens/title/title.component");
var suggestedcategories_component_2 = require("./preferenceScreens/suggestedcategories/suggestedcategories.component");
var findCategory_component_2 = require("./preferenceScreens/findCategory/findCategory.component");
var store_categories_component_2 = require("./preferenceScreens/storeCategories/components/store-categories.component");
var condition_component_2 = require("./preferenceScreens/condition/condition.component");
var description_component_2 = require("./preferenceScreens/description/description.component");
var item_specifies_component_2 = require("./preferenceScreens/itemspecifies/item-specifies.component");
var businesspolicies_component_2 = require("./preferenceScreens/businesspolicies/businesspolicies.component");
var weight_dimension_component_2 = require("./preferenceScreens/weightDimension/weight-dimension.component");
element_registry_1.registerElement("MLKitBarcodeScanner", function () {
    return require("nativescript-plugin-firebase/mlkit/barcodescanning")
        .MLKitBarcodeScanner;
});
element_registry_1.registerElement("BarcodeScanner", function () { return require("nativescript-barcodescanner").BarcodeScannerView; });
// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";
// Uncomment and add to NgModule imports if you need to use the HttpClient wrapper
// import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
var AppModule = /** @class */ (function () {
    /*
    Pass your application module to the bootstrapModule function located in main.ts to start your app
    */
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            bootstrap: [app_component_1.AppComponent],
            imports: [
                nativescript_module_1.NativeScriptModule,
                app_routing_module_1.AppRoutingModule,
                http_1.HttpModule,
                http_3.HttpClientModule,
                http_2.NativeScriptHttpModule,
                forms_1.NativeScriptFormsModule,
                http_client_1.NativeScriptHttpClientModule,
                angular_2.DropDownModule,
                angular_1.TNSCheckBoxModule
            ],
            declarations: [
                app_component_1.AppComponent,
                modal_component_1.ModalComponent,
                sku_component_1.SkuComponent,
                upc_component_1.UpcComponent,
                title_component_1.TitleComponent,
                suggestedcategories_component_1.SuggestedCategoriesComponent,
                findCategory_component_1.FindCategoryComponent,
                store_categories_component_1.StoreCategoryComponent,
                condition_component_1.ConditionComponent,
                selling_detail_component_1.SellingDetailComponent,
                description_component_1.DescriptionComponent,
                item_specifies_component_1.ItemSpecifiesComponent,
                payment_methods_component_1.PaymentMethodComponent,
                return_options_component_1.ReturnOptionsComponent,
                businesspolicies_component_1.BusinessPoliciesComponent,
                weight_dimension_component_1.WeightDimensionComponent,
                savedraft_component_1.SaveDraftComponent,
                pictures_component_1.PicturesComponent,
                add_pictures_component_1.AddPicturesComponent,
                createListing_component_1.CreateListingComponent,
                listingFunctions_component_1.ListingFunctionsComponent,
                submitListings_component_1.SubmitListingComponent,
                editListings_component_1.EditListingsComponent,
                warehouseFunctions_component_1.WarehouseFunctionsComponent,
                shelveInventory_component_1.ShelveInventoryComponent,
                userAccount_component_1.UserAccountComponent,
                checkItemStatus_component_1.CheckItemStatusComponent,
                inventoryLocation_component_1.InventoryLocationComponent,
                shipItems_component_1.ShipItemsComponent,
                stockErrors_component_1.StockErrorsComponent,
                preferencesDefaults_component_1.PreferencesDefaultsComponent,
                sku_component_2.PrefSkuComponent,
                upc_component_2.PrefUpcComponent,
                title_component_2.PrefTitleComponent,
                suggestedcategories_component_2.PrefSuggestedCategoriesComponent,
                findCategory_component_2.PrefFindCategoryComponent,
                store_categories_component_2.PrefStoreCategoryComponent,
                condition_component_2.PrefConditionComponent,
                description_component_2.PrefDescriptionComponent,
                item_specifies_component_2.PrefItemSpecifiesComponent,
                businesspolicies_component_2.PrefBusinessPoliciesComponent,
                weight_dimension_component_2.PrefWeightDimensionComponent
            ],
            providers: [nativescript_barcodescanner_1.BarcodeScanner, user_service_1.UserService],
            schemas: [core_1.NO_ERRORS_SCHEMA]
        })
        /*
        Pass your application module to the bootstrapModule function located in main.ts to start your app
        */
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkQ7QUFDM0QsZ0ZBQThFO0FBRTlFLDJEQUF3RDtBQUN4RCxpREFBK0M7QUFFL0MseURBQWtFO0FBQ2xFLDBFQUF3RTtBQUN4RSw0REFBMEQ7QUFDMUQsMkVBQTZEO0FBQzdELHFEQUFtRDtBQUNuRCxxREFBbUQ7QUFDbkQsc0NBQTJDO0FBQzNDLGtEQUFtRTtBQUNuRSxvREFBcUU7QUFDckUsZ0VBQWdGO0FBQ2hGLDZDQUF3RDtBQUN4RCwyREFBeUQ7QUFDekQsZ0ZBQThFO0FBQzlFLHdEQUFzRDtBQUN0RCxxR0FBbUc7QUFDbkcsMERBQWdFO0FBQ2hFLHNHQUFpRztBQUNqRyx1RUFBcUU7QUFDckUscUZBQWtGO0FBQ2xGLDZFQUEyRTtBQUMzRSxxRkFBa0Y7QUFDbEYsd0ZBQW9GO0FBQ3BGLHFGQUFrRjtBQUNsRiw0RkFBMEY7QUFDMUYsMkZBQXdGO0FBQ3hGLHVFQUFxRTtBQUNyRSxvRUFBa0U7QUFDbEUsK0VBQTRFO0FBQzVFLG1GQUFpRjtBQUNqRiw0RkFBMEY7QUFDMUYsc0ZBQW1GO0FBQ25GLGdGQUE4RTtBQUM5RSxrR0FBZ0c7QUFDaEcseUZBQXVGO0FBQ3ZGLDZFQUEyRTtBQUMzRSx5RkFBdUY7QUFDdkYsK0ZBQTZGO0FBQzdGLHVFQUFxRTtBQUNyRSw2RUFBMkU7QUFDM0UscUdBQW1HO0FBQ25HLHVFQUF5RTtBQUN6RSx1RUFBeUU7QUFDekUsNkVBQStFO0FBQy9FLHVIQUF5SDtBQUN6SCxrR0FBb0c7QUFDcEcsd0hBQXVIO0FBQ3ZILHlGQUEyRjtBQUMzRiwrRkFBaUc7QUFDakcsdUdBQXdHO0FBQ3hHLDhHQUFnSDtBQUNoSCw2R0FBOEc7QUFFOUcsa0NBQWUsQ0FDYixxQkFBcUIsRUFDckI7SUFDRSxPQUFBLE9BQU8sQ0FBQyxvREFBb0QsQ0FBQztTQUMxRCxtQkFBbUI7QUFEdEIsQ0FDc0IsQ0FDekIsQ0FBQztBQUVGLGtDQUFlLENBQ2IsZ0JBQWdCLEVBQ2hCLGNBQU0sT0FBQSxPQUFPLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxrQkFBa0IsRUFBekQsQ0FBeUQsQ0FDaEUsQ0FBQztBQUVGLDJFQUEyRTtBQUMzRSx3RUFBd0U7QUFFeEUsa0ZBQWtGO0FBQ2xGLG1GQUFtRjtBQWlFbkY7SUFIQTs7TUFFRTtJQUNGO0lBQXdCLENBQUM7SUFBWixTQUFTO1FBL0RyQixlQUFRLENBQUM7WUFDUixTQUFTLEVBQUUsQ0FBQyw0QkFBWSxDQUFDO1lBQ3pCLE9BQU8sRUFBRTtnQkFDUCx3Q0FBa0I7Z0JBQ2xCLHFDQUFnQjtnQkFDaEIsaUJBQVU7Z0JBQ1YsdUJBQWdCO2dCQUNoQiw2QkFBc0I7Z0JBQ3RCLCtCQUF1QjtnQkFDdkIsMENBQTRCO2dCQUM1Qix3QkFBYztnQkFDZCwyQkFBaUI7YUFDbEI7WUFDRCxZQUFZLEVBQUU7Z0JBQ1osNEJBQVk7Z0JBQ1osZ0NBQWM7Z0JBQ2QsNEJBQVk7Z0JBQ1osNEJBQVk7Z0JBQ1osZ0NBQWM7Z0JBQ2QsNERBQTRCO2dCQUM1Qiw4Q0FBcUI7Z0JBQ3JCLG1EQUFzQjtnQkFDdEIsd0NBQWtCO2dCQUNsQixpREFBc0I7Z0JBQ3RCLDRDQUFvQjtnQkFDcEIsaURBQXNCO2dCQUN0QixrREFBc0I7Z0JBQ3RCLGlEQUFzQjtnQkFDdEIsc0RBQXlCO2dCQUN6QixxREFBd0I7Z0JBQ3hCLHdDQUFrQjtnQkFDbEIsc0NBQWlCO2dCQUNqQiw2Q0FBb0I7Z0JBQ3BCLGdEQUFzQjtnQkFDdEIsc0RBQXlCO2dCQUN6QixpREFBc0I7Z0JBQ3RCLDhDQUFxQjtnQkFDckIsMERBQTJCO2dCQUMzQixvREFBd0I7Z0JBQ3hCLDRDQUFvQjtnQkFDcEIsb0RBQXdCO2dCQUN4Qix3REFBMEI7Z0JBQzFCLHdDQUFrQjtnQkFDbEIsNENBQW9CO2dCQUNwQiw0REFBNEI7Z0JBQzVCLGdDQUFnQjtnQkFDaEIsZ0NBQWdCO2dCQUNoQixvQ0FBa0I7Z0JBQ2xCLGdFQUFnQztnQkFDaEMsa0RBQXlCO2dCQUN6Qix1REFBMEI7Z0JBQzFCLDRDQUFzQjtnQkFDdEIsZ0RBQXdCO2dCQUN4QixxREFBMEI7Z0JBQzFCLDBEQUE2QjtnQkFDN0IseURBQTRCO2FBQzdCO1lBQ0QsU0FBUyxFQUFFLENBQUMsNENBQWMsRUFBRSwwQkFBVyxDQUFDO1lBQ3hDLE9BQU8sRUFBRSxDQUFDLHVCQUFnQixDQUFDO1NBQzVCLENBQUM7UUFDRjs7VUFFRTtPQUNXLFNBQVMsQ0FBRztJQUFELGdCQUFDO0NBQUEsQUFBekIsSUFBeUI7QUFBWiw4QkFBUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9uYXRpdmVzY3JpcHQubW9kdWxlXCI7XG5cbmltcG9ydCB7IEFwcFJvdXRpbmdNb2R1bGUgfSBmcm9tIFwiLi9hcHAtcm91dGluZy5tb2R1bGVcIjtcbmltcG9ydCB7IEFwcENvbXBvbmVudCB9IGZyb20gXCIuL2FwcC5jb21wb25lbnRcIjtcblxuaW1wb3J0IHsgVE5TQ2hlY2tCb3hNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWNoZWNrYm94L2FuZ3VsYXJcIjtcbmltcG9ydCB7IHJlZ2lzdGVyRWxlbWVudCB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9lbGVtZW50LXJlZ2lzdHJ5XCI7XG5pbXBvcnQgeyBNb2RhbENvbXBvbmVudCB9IGZyb20gXCIuL21vZGFscy9tb2RhbC5jb21wb25lbnRcIjtcbmltcG9ydCB7IEJhcmNvZGVTY2FubmVyIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1iYXJjb2Rlc2Nhbm5lclwiO1xuaW1wb3J0IHsgU2t1Q29tcG9uZW50IH0gZnJvbSBcIi4vc2t1L3NrdS5jb21wb25lbnRcIjtcbmltcG9ydCB7IFVwY0NvbXBvbmVudCB9IGZyb20gXCIuL3VwYy91cGMuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBIdHRwTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2h0dHBcIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdEh0dHBNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvaHR0cFwiO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZm9ybXNcIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvaHR0cC1jbGllbnRcIjtcbmltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uL2h0dHBcIjtcbmltcG9ydCB7IFRpdGxlQ29tcG9uZW50IH0gZnJvbSBcIi4vdGl0bGUvdGl0bGUuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBGaW5kQ2F0ZWdvcnlDb21wb25lbnQgfSBmcm9tIFwiLi9maW5kQ2F0ZWdvcnkvZmluZENhdGVnb3J5LmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcbmltcG9ydCB7IFN1Z2dlc3RlZENhdGVnb3JpZXNDb21wb25lbnQgfSBmcm9tIFwiLi9zdWdnZXN0ZWRjYXRlZ29yaWVzL3N1Z2dlc3RlZGNhdGVnb3JpZXMuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBEcm9wRG93bk1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtZHJvcC1kb3duL2FuZ3VsYXJcIjtcbmltcG9ydCB7IFN0b3JlQ2F0ZWdvcnlDb21wb25lbnQgfSBmcm9tIFwiLi9zdG9yZUNhdGVnb3JpZXMvY29tcG9uZW50cy9zdG9yZS1jYXRlZ29yaWVzLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQ29uZGl0aW9uQ29tcG9uZW50IH0gZnJvbSBcIi4vY29uZGl0aW9uL2NvbmRpdGlvbi5jb21wb25lbnRcIjtcbmltcG9ydCB7IFNlbGxpbmdEZXRhaWxDb21wb25lbnQgfSBmcm9tIFwiLi9zZWxsaW5nRGV0YWlsL3NlbGxpbmctZGV0YWlsLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgRGVzY3JpcHRpb25Db21wb25lbnQgfSBmcm9tIFwiLi9kZXNjcmlwdGlvbi9kZXNjcmlwdGlvbi5jb21wb25lbnRcIjtcbmltcG9ydCB7IEl0ZW1TcGVjaWZpZXNDb21wb25lbnQgfSBmcm9tIFwiLi9pdGVtc3BlY2lmaWVzL2l0ZW0tc3BlY2lmaWVzLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgUGF5bWVudE1ldGhvZENvbXBvbmVudCB9IGZyb20gXCIuL3BheW1lbnRNZXRob2RzL3BheW1lbnQtbWV0aG9kcy5jb21wb25lbnRcIjtcbmltcG9ydCB7IFJldHVybk9wdGlvbnNDb21wb25lbnQgfSBmcm9tIFwiLi9yZXR1cm5PcHRpb25zL3JldHVybi1vcHRpb25zLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQnVzaW5lc3NQb2xpY2llc0NvbXBvbmVudCB9IGZyb20gXCIuL2J1c2luZXNzcG9saWNpZXMvYnVzaW5lc3Nwb2xpY2llcy5jb21wb25lbnRcIjtcbmltcG9ydCB7IFdlaWdodERpbWVuc2lvbkNvbXBvbmVudCB9IGZyb20gXCIuL3dlaWdodERpbWVuc2lvbi93ZWlnaHQtZGltZW5zaW9uLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgU2F2ZURyYWZ0Q29tcG9uZW50IH0gZnJvbSBcIi4vc2F2ZWRyYWZ0L3NhdmVkcmFmdC5jb21wb25lbnRcIjtcbmltcG9ydCB7IFBpY3R1cmVzQ29tcG9uZW50IH0gZnJvbSBcIi4vcGljdHVyZXMvcGljdHVyZXMuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBBZGRQaWN0dXJlc0NvbXBvbmVudCB9IGZyb20gXCIuL2FkZFBpY3R1cmVzL2FkZC1waWN0dXJlcy5jb21wb25lbnRcIjtcbmltcG9ydCB7IENyZWF0ZUxpc3RpbmdDb21wb25lbnQgfSBmcm9tIFwiLi9jcmVhdGVMaXN0aW5nL2NyZWF0ZUxpc3RpbmcuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBMaXN0aW5nRnVuY3Rpb25zQ29tcG9uZW50IH0gZnJvbSBcIi4vbGlzdGluZ0Z1bmN0aW9ucy9saXN0aW5nRnVuY3Rpb25zLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgU3VibWl0TGlzdGluZ0NvbXBvbmVudCB9IGZyb20gXCIuL3N1Ym1pdExpc3RpbmdzL3N1Ym1pdExpc3RpbmdzLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgRWRpdExpc3RpbmdzQ29tcG9uZW50IH0gZnJvbSBcIi4vZWRpdExpc3RpbmdzL2VkaXRMaXN0aW5ncy5jb21wb25lbnRcIjtcbmltcG9ydCB7IFdhcmVob3VzZUZ1bmN0aW9uc0NvbXBvbmVudCB9IGZyb20gXCIuL3dhcmVob3VzZUZ1bmN0aW9ucy93YXJlaG91c2VGdW5jdGlvbnMuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBTaGVsdmVJbnZlbnRvcnlDb21wb25lbnQgfSBmcm9tIFwiLi9zaGVsdmVJbnZlbnRvcnkvc2hlbHZlSW52ZW50b3J5LmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgVXNlckFjY291bnRDb21wb25lbnQgfSBmcm9tIFwiLi91c2VyQWNjb3VudC91c2VyQWNjb3VudC5jb21wb25lbnRcIjtcbmltcG9ydCB7IENoZWNrSXRlbVN0YXR1c0NvbXBvbmVudCB9IGZyb20gXCIuL2NoZWNrSXRlbVN0YXR1cy9jaGVja0l0ZW1TdGF0dXMuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBJbnZlbnRvcnlMb2NhdGlvbkNvbXBvbmVudCB9IGZyb20gXCIuL2ludmVudG9yeUxvY2F0aW9uL2ludmVudG9yeUxvY2F0aW9uLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgU2hpcEl0ZW1zQ29tcG9uZW50IH0gZnJvbSBcIi4vc2hpcEl0ZW1zL3NoaXBJdGVtcy5jb21wb25lbnRcIjtcbmltcG9ydCB7IFN0b2NrRXJyb3JzQ29tcG9uZW50IH0gZnJvbSBcIi4vc3RvY2tFcnJvcnMvc3RvY2tFcnJvcnMuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBQcmVmZXJlbmNlc0RlZmF1bHRzQ29tcG9uZW50IH0gZnJvbSBcIi4vcHJlZmVyZW5jZXNEZWZhdWx0cy9wcmVmZXJlbmNlc0RlZmF1bHRzLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgUHJlZlNrdUNvbXBvbmVudCB9IGZyb20gXCIuL3ByZWZlcmVuY2VTY3JlZW5zL3NrdS9za3UuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBQcmVmVXBjQ29tcG9uZW50IH0gZnJvbSBcIi4vcHJlZmVyZW5jZVNjcmVlbnMvdXBjL3VwYy5jb21wb25lbnRcIjtcbmltcG9ydCB7IFByZWZUaXRsZUNvbXBvbmVudCB9IGZyb20gXCIuL3ByZWZlcmVuY2VTY3JlZW5zL3RpdGxlL3RpdGxlLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgUHJlZlN1Z2dlc3RlZENhdGVnb3JpZXNDb21wb25lbnQgfSBmcm9tIFwiLi9wcmVmZXJlbmNlU2NyZWVucy9zdWdnZXN0ZWRjYXRlZ29yaWVzL3N1Z2dlc3RlZGNhdGVnb3JpZXMuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBQcmVmRmluZENhdGVnb3J5Q29tcG9uZW50IH0gZnJvbSBcIi4vcHJlZmVyZW5jZVNjcmVlbnMvZmluZENhdGVnb3J5L2ZpbmRDYXRlZ29yeS5jb21wb25lbnRcIjtcbmltcG9ydCB7IFByZWZTdG9yZUNhdGVnb3J5Q29tcG9uZW50IH0gZnJvbSBcIi4vcHJlZmVyZW5jZVNjcmVlbnMvc3RvcmVDYXRlZ29yaWVzL2NvbXBvbmVudHMvc3RvcmUtY2F0ZWdvcmllcy5jb21wb25lbnRcIjtcbmltcG9ydCB7IFByZWZDb25kaXRpb25Db21wb25lbnQgfSBmcm9tIFwiLi9wcmVmZXJlbmNlU2NyZWVucy9jb25kaXRpb24vY29uZGl0aW9uLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgUHJlZkRlc2NyaXB0aW9uQ29tcG9uZW50IH0gZnJvbSBcIi4vcHJlZmVyZW5jZVNjcmVlbnMvZGVzY3JpcHRpb24vZGVzY3JpcHRpb24uY29tcG9uZW50XCI7XG5pbXBvcnQgeyBQcmVmSXRlbVNwZWNpZmllc0NvbXBvbmVudCB9IGZyb20gXCIuL3ByZWZlcmVuY2VTY3JlZW5zL2l0ZW1zcGVjaWZpZXMvaXRlbS1zcGVjaWZpZXMuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBQcmVmQnVzaW5lc3NQb2xpY2llc0NvbXBvbmVudCB9IGZyb20gXCIuL3ByZWZlcmVuY2VTY3JlZW5zL2J1c2luZXNzcG9saWNpZXMvYnVzaW5lc3Nwb2xpY2llcy5jb21wb25lbnRcIjtcbmltcG9ydCB7IFByZWZXZWlnaHREaW1lbnNpb25Db21wb25lbnQgfSBmcm9tIFwiLi9wcmVmZXJlbmNlU2NyZWVucy93ZWlnaHREaW1lbnNpb24vd2VpZ2h0LWRpbWVuc2lvbi5jb21wb25lbnRcIjtcblxucmVnaXN0ZXJFbGVtZW50KFxuICBcIk1MS2l0QmFyY29kZVNjYW5uZXJcIixcbiAgKCkgPT5cbiAgICByZXF1aXJlKFwibmF0aXZlc2NyaXB0LXBsdWdpbi1maXJlYmFzZS9tbGtpdC9iYXJjb2Rlc2Nhbm5pbmdcIilcbiAgICAgIC5NTEtpdEJhcmNvZGVTY2FubmVyXG4pO1xuXG5yZWdpc3RlckVsZW1lbnQoXG4gIFwiQmFyY29kZVNjYW5uZXJcIixcbiAgKCkgPT4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1iYXJjb2Rlc2Nhbm5lclwiKS5CYXJjb2RlU2Nhbm5lclZpZXdcbik7XG5cbi8vIFVuY29tbWVudCBhbmQgYWRkIHRvIE5nTW9kdWxlIGltcG9ydHMgaWYgeW91IG5lZWQgdG8gdXNlIHR3by13YXkgYmluZGluZ1xuLy8gaW1wb3J0IHsgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZm9ybXNcIjtcblxuLy8gVW5jb21tZW50IGFuZCBhZGQgdG8gTmdNb2R1bGUgaW1wb3J0cyBpZiB5b3UgbmVlZCB0byB1c2UgdGhlIEh0dHBDbGllbnQgd3JhcHBlclxuLy8gaW1wb3J0IHsgTmF0aXZlU2NyaXB0SHR0cENsaWVudE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9odHRwLWNsaWVudFwiO1xuXG5ATmdNb2R1bGUoe1xuICBib290c3RyYXA6IFtBcHBDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbXG4gICAgTmF0aXZlU2NyaXB0TW9kdWxlLFxuICAgIEFwcFJvdXRpbmdNb2R1bGUsXG4gICAgSHR0cE1vZHVsZSxcbiAgICBIdHRwQ2xpZW50TW9kdWxlLFxuICAgIE5hdGl2ZVNjcmlwdEh0dHBNb2R1bGUsXG4gICAgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUsXG4gICAgTmF0aXZlU2NyaXB0SHR0cENsaWVudE1vZHVsZSxcbiAgICBEcm9wRG93bk1vZHVsZSxcbiAgICBUTlNDaGVja0JveE1vZHVsZVxuICBdLFxuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBBcHBDb21wb25lbnQsXG4gICAgTW9kYWxDb21wb25lbnQsXG4gICAgU2t1Q29tcG9uZW50LFxuICAgIFVwY0NvbXBvbmVudCxcbiAgICBUaXRsZUNvbXBvbmVudCxcbiAgICBTdWdnZXN0ZWRDYXRlZ29yaWVzQ29tcG9uZW50LFxuICAgIEZpbmRDYXRlZ29yeUNvbXBvbmVudCxcbiAgICBTdG9yZUNhdGVnb3J5Q29tcG9uZW50LFxuICAgIENvbmRpdGlvbkNvbXBvbmVudCxcbiAgICBTZWxsaW5nRGV0YWlsQ29tcG9uZW50LFxuICAgIERlc2NyaXB0aW9uQ29tcG9uZW50LFxuICAgIEl0ZW1TcGVjaWZpZXNDb21wb25lbnQsXG4gICAgUGF5bWVudE1ldGhvZENvbXBvbmVudCxcbiAgICBSZXR1cm5PcHRpb25zQ29tcG9uZW50LFxuICAgIEJ1c2luZXNzUG9saWNpZXNDb21wb25lbnQsXG4gICAgV2VpZ2h0RGltZW5zaW9uQ29tcG9uZW50LFxuICAgIFNhdmVEcmFmdENvbXBvbmVudCxcbiAgICBQaWN0dXJlc0NvbXBvbmVudCxcbiAgICBBZGRQaWN0dXJlc0NvbXBvbmVudCxcbiAgICBDcmVhdGVMaXN0aW5nQ29tcG9uZW50LFxuICAgIExpc3RpbmdGdW5jdGlvbnNDb21wb25lbnQsXG4gICAgU3VibWl0TGlzdGluZ0NvbXBvbmVudCxcbiAgICBFZGl0TGlzdGluZ3NDb21wb25lbnQsXG4gICAgV2FyZWhvdXNlRnVuY3Rpb25zQ29tcG9uZW50LFxuICAgIFNoZWx2ZUludmVudG9yeUNvbXBvbmVudCxcbiAgICBVc2VyQWNjb3VudENvbXBvbmVudCxcbiAgICBDaGVja0l0ZW1TdGF0dXNDb21wb25lbnQsXG4gICAgSW52ZW50b3J5TG9jYXRpb25Db21wb25lbnQsXG4gICAgU2hpcEl0ZW1zQ29tcG9uZW50LFxuICAgIFN0b2NrRXJyb3JzQ29tcG9uZW50LFxuICAgIFByZWZlcmVuY2VzRGVmYXVsdHNDb21wb25lbnQsXG4gICAgUHJlZlNrdUNvbXBvbmVudCxcbiAgICBQcmVmVXBjQ29tcG9uZW50LFxuICAgIFByZWZUaXRsZUNvbXBvbmVudCxcbiAgICBQcmVmU3VnZ2VzdGVkQ2F0ZWdvcmllc0NvbXBvbmVudCxcbiAgICBQcmVmRmluZENhdGVnb3J5Q29tcG9uZW50LFxuICAgIFByZWZTdG9yZUNhdGVnb3J5Q29tcG9uZW50LFxuICAgIFByZWZDb25kaXRpb25Db21wb25lbnQsXG4gICAgUHJlZkRlc2NyaXB0aW9uQ29tcG9uZW50LFxuICAgIFByZWZJdGVtU3BlY2lmaWVzQ29tcG9uZW50LFxuICAgIFByZWZCdXNpbmVzc1BvbGljaWVzQ29tcG9uZW50LFxuICAgIFByZWZXZWlnaHREaW1lbnNpb25Db21wb25lbnRcbiAgXSxcbiAgcHJvdmlkZXJzOiBbQmFyY29kZVNjYW5uZXIsIFVzZXJTZXJ2aWNlXSxcbiAgc2NoZW1hczogW05PX0VSUk9SU19TQ0hFTUFdXG59KVxuLypcblBhc3MgeW91ciBhcHBsaWNhdGlvbiBtb2R1bGUgdG8gdGhlIGJvb3RzdHJhcE1vZHVsZSBmdW5jdGlvbiBsb2NhdGVkIGluIG1haW4udHMgdG8gc3RhcnQgeW91ciBhcHBcbiovXG5leHBvcnQgY2xhc3MgQXBwTW9kdWxlIHt9XG4iXX0=
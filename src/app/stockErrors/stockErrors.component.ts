import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { UserService } from "../services/user.service";

@Component({
  selector: "stockErrors",
  moduleId: module.id,
  templateUrl: "./stockErrors.component.html",
  styleUrls: ["./stockErrors.component.css"]
})
export class StockErrorsComponent implements OnInit {
  routerExtensions: RouterExtensions;

  constructor(
    routerExtensions: RouterExtensions,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDots(8);
    this.userService.bottomBarDotState(false);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
  }

  ngOnInit(): void {}

}

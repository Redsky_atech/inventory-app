import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { TNSCheckBoxModule } from "nativescript-checkbox/angular";
import { registerElement } from "nativescript-angular/element-registry";
import { ModalComponent } from "./modals/modal.component";
import { BarcodeScanner } from "nativescript-barcodescanner";
import { SkuComponent } from "./sku/sku.component";
import { UpcComponent } from "./upc/upc.component";
import { HttpModule } from "@angular/http";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { HttpClientModule } from "@angular/common/http";
import { TitleComponent } from "./title/title.component";
import { FindCategoryComponent } from "./findCategory/findCategory.component";
import { UserService } from "./services/user.service";
import { SuggestedCategoriesComponent } from "./suggestedcategories/suggestedcategories.component";
import { DropDownModule } from "nativescript-drop-down/angular";
import { StoreCategoryComponent } from "./storeCategories/components/store-categories.component";
import { ConditionComponent } from "./condition/condition.component";
import { SellingDetailComponent } from "./sellingDetail/selling-detail.component";
import { DescriptionComponent } from "./description/description.component";
import { ItemSpecifiesComponent } from "./itemspecifies/item-specifies.component";
import { PaymentMethodComponent } from "./paymentMethods/payment-methods.component";
import { ReturnOptionsComponent } from "./returnOptions/return-options.component";
import { BusinessPoliciesComponent } from "./businesspolicies/businesspolicies.component";
import { WeightDimensionComponent } from "./weightDimension/weight-dimension.component";
import { SaveDraftComponent } from "./savedraft/savedraft.component";
import { PicturesComponent } from "./pictures/pictures.component";
import { AddPicturesComponent } from "./addPictures/add-pictures.component";
import { CreateListingComponent } from "./createListing/createListing.component";
import { ListingFunctionsComponent } from "./listingFunctions/listingFunctions.component";
import { SubmitListingComponent } from "./submitListings/submitListings.component";
import { EditListingsComponent } from "./editListings/editListings.component";
import { WarehouseFunctionsComponent } from "./warehouseFunctions/warehouseFunctions.component";
import { ShelveInventoryComponent } from "./shelveInventory/shelveInventory.component";
import { UserAccountComponent } from "./userAccount/userAccount.component";
import { CheckItemStatusComponent } from "./checkItemStatus/checkItemStatus.component";
import { InventoryLocationComponent } from "./inventoryLocation/inventoryLocation.component";
import { ShipItemsComponent } from "./shipItems/shipItems.component";
import { StockErrorsComponent } from "./stockErrors/stockErrors.component";
import { PreferencesDefaultsComponent } from "./preferencesDefaults/preferencesDefaults.component";
import { PrefSkuComponent } from "./preferenceScreens/sku/sku.component";
import { PrefUpcComponent } from "./preferenceScreens/upc/upc.component";
import { PrefTitleComponent } from "./preferenceScreens/title/title.component";
import { PrefSuggestedCategoriesComponent } from "./preferenceScreens/suggestedcategories/suggestedcategories.component";
import { PrefFindCategoryComponent } from "./preferenceScreens/findCategory/findCategory.component";
import { PrefStoreCategoryComponent } from "./preferenceScreens/storeCategories/components/store-categories.component";
import { PrefConditionComponent } from "./preferenceScreens/condition/condition.component";
import { PrefDescriptionComponent } from "./preferenceScreens/description/description.component";
import { PrefItemSpecifiesComponent } from "./preferenceScreens/itemspecifies/item-specifies.component";
import { PrefBusinessPoliciesComponent } from "./preferenceScreens/businesspolicies/businesspolicies.component";
import { PrefWeightDimensionComponent } from "./preferenceScreens/weightDimension/weight-dimension.component";

registerElement(
  "MLKitBarcodeScanner",
  () =>
    require("nativescript-plugin-firebase/mlkit/barcodescanning")
      .MLKitBarcodeScanner
);

registerElement(
  "BarcodeScanner",
  () => require("nativescript-barcodescanner").BarcodeScannerView
);

// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";

// Uncomment and add to NgModule imports if you need to use the HttpClient wrapper
// import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

@NgModule({
  bootstrap: [AppComponent],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    NativeScriptHttpModule,
    NativeScriptFormsModule,
    NativeScriptHttpClientModule,
    DropDownModule,
    TNSCheckBoxModule
  ],
  declarations: [
    AppComponent,
    ModalComponent,
    SkuComponent,
    UpcComponent,
    TitleComponent,
    SuggestedCategoriesComponent,
    FindCategoryComponent,
    StoreCategoryComponent,
    ConditionComponent,
    SellingDetailComponent,
    DescriptionComponent,
    ItemSpecifiesComponent,
    PaymentMethodComponent,
    ReturnOptionsComponent,
    BusinessPoliciesComponent,
    WeightDimensionComponent,
    SaveDraftComponent,
    PicturesComponent,
    AddPicturesComponent,
    CreateListingComponent,
    ListingFunctionsComponent,
    SubmitListingComponent,
    EditListingsComponent,
    WarehouseFunctionsComponent,
    ShelveInventoryComponent,
    UserAccountComponent,
    CheckItemStatusComponent,
    InventoryLocationComponent,
    ShipItemsComponent,
    StockErrorsComponent,
    PreferencesDefaultsComponent,
    PrefSkuComponent,
    PrefUpcComponent,
    PrefTitleComponent,
    PrefSuggestedCategoriesComponent,
    PrefFindCategoryComponent,
    PrefStoreCategoryComponent,
    PrefConditionComponent,
    PrefDescriptionComponent,
    PrefItemSpecifiesComponent,
    PrefBusinessPoliciesComponent,
    PrefWeightDimensionComponent
  ],
  providers: [BarcodeScanner, UserService],
  schemas: [NO_ERRORS_SCHEMA]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule {}

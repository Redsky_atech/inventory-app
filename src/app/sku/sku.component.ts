import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef
} from "@angular/core";
import { BarcodeScanner } from "nativescript-barcodescanner";
import { RouterExtensions } from "nativescript-angular/router";
import { ModalComponent } from "../modals/modal.component";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";
import { registerElement } from "nativescript-angular/element-registry";
import { AnimationCurve } from "tns-core-modules/ui/enums";
import { StackLayout } from "tns-core-modules/ui/layouts/stack-layout";
import { UserService } from "../services/user.service";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";

@Component({
  selector: "Sku",
  moduleId: module.id,
  templateUrl: "./sku.component.html",
  styleUrls: ["./sku.component.css"]
})
export class SkuComponent implements OnInit, AfterViewInit {
  @ViewChild("otpDialog") otpModal: ModalComponent;
  @ViewChild("barcodeline") barcodeline: ElementRef;

  id = "";
  isFrontCamera: boolean = false;
  torchOn: boolean = false;
  isScannedOnce: boolean = false;
  flashClass = "flash";
  cameraClass = "rare";
  appointmentId = "";
  constructor(
    private activatedRoute: ActivatedRoute,
    private barcodeScanner: BarcodeScanner,
    private routerExtensions: RouterExtensions,
    private http: HttpClient,
    private userService: UserService
  ) {
    this.flashClass = "flash";
    this.cameraClass = "rare";
    this.isFrontCamera = false;
    this.userService.actionBarState(true);
    this.userService.bottomBarDots(1);
    this.userService.bottomBarDotState(true);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
    this.userService.switchState(false);
    this.activatedRoute.queryParams.subscribe(params => {
      this.appointmentId = params.id;
      console.log(this.appointmentId);
    });
  }

  ngOnInit(): void {}

  ngAfterViewInit() {
    // let view = this.barcodeline.nativeElement as StackLayout;
    // view.animate({
    //   duration: 3000,
    //   curve: AnimationCurve.linear,
    //   translate: { x: 0, y: 50 }
    // });
    // .then(() => {
    //   // Reset animation
    //   setTimeout(() => {
    //     view.translateY = 0;
    //     view.translateX = 0;
    //   }, 3000);
    // });
  }
  // putData() {
  //     let headers = new HttpHeaders({
  //         "Content-Type": "application/json",
  //         "x-role-key": "b312f1f9-1f51-c060-11b5-d3e3d9019b8c"
  //     });

  //     var body = {
  //         "role": {
  //             "id": "5afbb4bfdfab9716aab75fa7"
  //         },
  //         "appointment": {
  //             "id": this.appointmentId
  //         }
  //     }

  //     console.log('Reached fun', this.appointmentId)

  //     this.http.put("http://welcome-api-dev.m-sas.com/api/sessions/" + this.id, body, { headers: headers }).subscribe((res: any) => {

  //         if (res.isSuccess) {
  //             let result: any
  //             result = res.data
  //             this.isScannedOnce = true;
  //             // this.routerExtensions.navigate(['/login']);
  //     console.log(res)

  //             this.otpModal.show();
  //         }
  //         else {
  //                     console.log(res)

  //             // alert(res.error)
  //         }
  //     },
  //         error => {
  //                     console.log(error )

  //             // alert(error)
  //         })
  // }

  // onDone() {
  //     this.otpModal.hide();
  //     this.routerExtensions.navigate(['/login']);
  // }

  // onBarcodeScanningResult(args) {
  //   // this.id = args.value.barcodes[0].value;
  //   // if (this.id != undefined && !this.isScannedOnce) {
  //   //   // this.putData();
  //   //   console.log("RSS:", this.id);
  //   // }
  //   console.log(args);
  // }

  onFlashSelect() {
    if (this.flashClass == "flash") {
      this.flashClass = "flash-focus";
      this.torchOn = true;
    } else {
      this.flashClass = "flash";
      this.torchOn = false;
    }
  }

  onCameraSelect() {
    if (this.cameraClass == "rare") {
      this.cameraClass = "front";
      this.isFrontCamera = true;
      console.log("Tapped");
    } else {
      this.cameraClass = "rare";
      this.isFrontCamera = false;
      console.log("Tapped");
    }
  }

  onSwipe(args: SwipeGestureEventData) {
    if (args.direction == 2) {
      this.routerExtensions.navigate(["/upc"]);
    }
  }

  //   (scanResult)="onBarcodeScanningResult($event)
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_barcodescanner_1 = require("nativescript-barcodescanner");
var router_1 = require("nativescript-angular/router");
var modal_component_1 = require("../modals/modal.component");
var http_1 = require("@angular/common/http");
var router_2 = require("@angular/router");
var user_service_1 = require("../services/user.service");
var SkuComponent = /** @class */ (function () {
    function SkuComponent(activatedRoute, barcodeScanner, routerExtensions, http, userService) {
        var _this = this;
        this.activatedRoute = activatedRoute;
        this.barcodeScanner = barcodeScanner;
        this.routerExtensions = routerExtensions;
        this.http = http;
        this.userService = userService;
        this.id = "";
        this.isFrontCamera = false;
        this.torchOn = false;
        this.isScannedOnce = false;
        this.flashClass = "flash";
        this.cameraClass = "rare";
        this.appointmentId = "";
        this.flashClass = "flash";
        this.cameraClass = "rare";
        this.isFrontCamera = false;
        this.userService.actionBarState(true);
        this.userService.bottomBarDots(1);
        this.userService.bottomBarDotState(true);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.userService.switchState(false);
        this.activatedRoute.queryParams.subscribe(function (params) {
            _this.appointmentId = params.id;
            console.log(_this.appointmentId);
        });
    }
    SkuComponent.prototype.ngOnInit = function () { };
    SkuComponent.prototype.ngAfterViewInit = function () {
        // let view = this.barcodeline.nativeElement as StackLayout;
        // view.animate({
        //   duration: 3000,
        //   curve: AnimationCurve.linear,
        //   translate: { x: 0, y: 50 }
        // });
        // .then(() => {
        //   // Reset animation
        //   setTimeout(() => {
        //     view.translateY = 0;
        //     view.translateX = 0;
        //   }, 3000);
        // });
    };
    // putData() {
    //     let headers = new HttpHeaders({
    //         "Content-Type": "application/json",
    //         "x-role-key": "b312f1f9-1f51-c060-11b5-d3e3d9019b8c"
    //     });
    //     var body = {
    //         "role": {
    //             "id": "5afbb4bfdfab9716aab75fa7"
    //         },
    //         "appointment": {
    //             "id": this.appointmentId
    //         }
    //     }
    //     console.log('Reached fun', this.appointmentId)
    //     this.http.put("http://welcome-api-dev.m-sas.com/api/sessions/" + this.id, body, { headers: headers }).subscribe((res: any) => {
    //         if (res.isSuccess) {
    //             let result: any
    //             result = res.data
    //             this.isScannedOnce = true;
    //             // this.routerExtensions.navigate(['/login']);
    //     console.log(res)
    //             this.otpModal.show();
    //         }
    //         else {
    //                     console.log(res)
    //             // alert(res.error)
    //         }
    //     },
    //         error => {
    //                     console.log(error )
    //             // alert(error)
    //         })
    // }
    // onDone() {
    //     this.otpModal.hide();
    //     this.routerExtensions.navigate(['/login']);
    // }
    // onBarcodeScanningResult(args) {
    //   // this.id = args.value.barcodes[0].value;
    //   // if (this.id != undefined && !this.isScannedOnce) {
    //   //   // this.putData();
    //   //   console.log("RSS:", this.id);
    //   // }
    //   console.log(args);
    // }
    SkuComponent.prototype.onFlashSelect = function () {
        if (this.flashClass == "flash") {
            this.flashClass = "flash-focus";
            this.torchOn = true;
        }
        else {
            this.flashClass = "flash";
            this.torchOn = false;
        }
    };
    SkuComponent.prototype.onCameraSelect = function () {
        if (this.cameraClass == "rare") {
            this.cameraClass = "front";
            this.isFrontCamera = true;
            console.log("Tapped");
        }
        else {
            this.cameraClass = "rare";
            this.isFrontCamera = false;
            console.log("Tapped");
        }
    };
    SkuComponent.prototype.onSwipe = function (args) {
        if (args.direction == 2) {
            this.routerExtensions.navigate(["/upc"]);
        }
    };
    __decorate([
        core_1.ViewChild("otpDialog"),
        __metadata("design:type", modal_component_1.ModalComponent)
    ], SkuComponent.prototype, "otpModal", void 0);
    __decorate([
        core_1.ViewChild("barcodeline"),
        __metadata("design:type", core_1.ElementRef)
    ], SkuComponent.prototype, "barcodeline", void 0);
    SkuComponent = __decorate([
        core_1.Component({
            selector: "Sku",
            moduleId: module.id,
            templateUrl: "./sku.component.html",
            styleUrls: ["./sku.component.css"]
        }),
        __metadata("design:paramtypes", [router_2.ActivatedRoute,
            nativescript_barcodescanner_1.BarcodeScanner,
            router_1.RouterExtensions,
            http_1.HttpClient,
            user_service_1.UserService])
    ], SkuComponent);
    return SkuComponent;
}());
exports.SkuComponent = SkuComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2t1LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNrdS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FNdUI7QUFDdkIsMkVBQTZEO0FBQzdELHNEQUErRDtBQUMvRCw2REFBMkQ7QUFDM0QsNkNBQStEO0FBQy9ELDBDQUF5RDtBQUl6RCx5REFBdUQ7QUFTdkQ7SUFXRSxzQkFDVSxjQUE4QixFQUM5QixjQUE4QixFQUM5QixnQkFBa0MsRUFDbEMsSUFBZ0IsRUFDaEIsV0FBd0I7UUFMbEMsaUJBb0JDO1FBbkJTLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyxTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2hCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBWmxDLE9BQUUsR0FBRyxFQUFFLENBQUM7UUFDUixrQkFBYSxHQUFZLEtBQUssQ0FBQztRQUMvQixZQUFPLEdBQVksS0FBSyxDQUFDO1FBQ3pCLGtCQUFhLEdBQVksS0FBSyxDQUFDO1FBQy9CLGVBQVUsR0FBRyxPQUFPLENBQUM7UUFDckIsZ0JBQVcsR0FBRyxNQUFNLENBQUM7UUFDckIsa0JBQWEsR0FBRyxFQUFFLENBQUM7UUFRakIsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUM7UUFDMUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUM7UUFDMUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFBLE1BQU07WUFDOUMsS0FBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO1lBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2xDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELCtCQUFRLEdBQVIsY0FBa0IsQ0FBQztJQUVuQixzQ0FBZSxHQUFmO1FBQ0UsNERBQTREO1FBQzVELGlCQUFpQjtRQUNqQixvQkFBb0I7UUFDcEIsa0NBQWtDO1FBQ2xDLCtCQUErQjtRQUMvQixNQUFNO1FBQ04sZ0JBQWdCO1FBQ2hCLHVCQUF1QjtRQUN2Qix1QkFBdUI7UUFDdkIsMkJBQTJCO1FBQzNCLDJCQUEyQjtRQUMzQixjQUFjO1FBQ2QsTUFBTTtJQUNSLENBQUM7SUFDRCxjQUFjO0lBQ2Qsc0NBQXNDO0lBQ3RDLDhDQUE4QztJQUM5QywrREFBK0Q7SUFDL0QsVUFBVTtJQUVWLG1CQUFtQjtJQUNuQixvQkFBb0I7SUFDcEIsK0NBQStDO0lBQy9DLGFBQWE7SUFDYiwyQkFBMkI7SUFDM0IsdUNBQXVDO0lBQ3ZDLFlBQVk7SUFDWixRQUFRO0lBRVIscURBQXFEO0lBRXJELHNJQUFzSTtJQUV0SSwrQkFBK0I7SUFDL0IsOEJBQThCO0lBQzlCLGdDQUFnQztJQUNoQyx5Q0FBeUM7SUFDekMsNkRBQTZEO0lBQzdELHVCQUF1QjtJQUV2QixvQ0FBb0M7SUFDcEMsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQix1Q0FBdUM7SUFFdkMsa0NBQWtDO0lBQ2xDLFlBQVk7SUFDWixTQUFTO0lBQ1QscUJBQXFCO0lBQ3JCLDBDQUEwQztJQUUxQyw4QkFBOEI7SUFDOUIsYUFBYTtJQUNiLElBQUk7SUFFSixhQUFhO0lBQ2IsNEJBQTRCO0lBQzVCLGtEQUFrRDtJQUNsRCxJQUFJO0lBRUosa0NBQWtDO0lBQ2xDLCtDQUErQztJQUMvQywwREFBMEQ7SUFDMUQsNEJBQTRCO0lBQzVCLHVDQUF1QztJQUN2QyxTQUFTO0lBQ1QsdUJBQXVCO0lBQ3ZCLElBQUk7SUFFSixvQ0FBYSxHQUFiO1FBQ0UsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLE9BQU8sRUFBRTtZQUM5QixJQUFJLENBQUMsVUFBVSxHQUFHLGFBQWEsQ0FBQztZQUNoQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztTQUNyQjthQUFNO1lBQ0wsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUM7WUFDMUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDdEI7SUFDSCxDQUFDO0lBRUQscUNBQWMsR0FBZDtRQUNFLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxNQUFNLEVBQUU7WUFDOUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUM7WUFDM0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7WUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUN2QjthQUFNO1lBQ0wsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUM7WUFDMUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUN2QjtJQUNILENBQUM7SUFFRCw4QkFBTyxHQUFQLFVBQVEsSUFBMkI7UUFDakMsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsRUFBRTtZQUN2QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztTQUMxQztJQUNILENBQUM7SUFsSXVCO1FBQXZCLGdCQUFTLENBQUMsV0FBVyxDQUFDO2tDQUFXLGdDQUFjO2tEQUFDO0lBQ3ZCO1FBQXpCLGdCQUFTLENBQUMsYUFBYSxDQUFDO2tDQUFjLGlCQUFVO3FEQUFDO0lBRnZDLFlBQVk7UUFOeEIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxLQUFLO1lBQ2YsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSxzQkFBc0I7WUFDbkMsU0FBUyxFQUFFLENBQUMscUJBQXFCLENBQUM7U0FDbkMsQ0FBQzt5Q0FhMEIsdUJBQWM7WUFDZCw0Q0FBYztZQUNaLHlCQUFnQjtZQUM1QixpQkFBVTtZQUNILDBCQUFXO09BaEJ2QixZQUFZLENBc0l4QjtJQUFELG1CQUFDO0NBQUEsQUF0SUQsSUFzSUM7QUF0SVksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBDb21wb25lbnQsXG4gIE9uSW5pdCxcbiAgQWZ0ZXJWaWV3SW5pdCxcbiAgVmlld0NoaWxkLFxuICBFbGVtZW50UmVmXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBCYXJjb2RlU2Nhbm5lciB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYmFyY29kZXNjYW5uZXJcIjtcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBNb2RhbENvbXBvbmVudCB9IGZyb20gXCIuLi9tb2RhbHMvbW9kYWwuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycyB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IHJlZ2lzdGVyRWxlbWVudCB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9lbGVtZW50LXJlZ2lzdHJ5XCI7XG5pbXBvcnQgeyBBbmltYXRpb25DdXJ2ZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2VudW1zXCI7XG5pbXBvcnQgeyBTdGFja0xheW91dCB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2xheW91dHMvc3RhY2stbGF5b3V0XCI7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcbmltcG9ydCB7IFN3aXBlR2VzdHVyZUV2ZW50RGF0YSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2dlc3R1cmVzXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJTa3VcIixcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgdGVtcGxhdGVVcmw6IFwiLi9za3UuY29tcG9uZW50Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL3NrdS5jb21wb25lbnQuY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIFNrdUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB7XG4gIEBWaWV3Q2hpbGQoXCJvdHBEaWFsb2dcIikgb3RwTW9kYWw6IE1vZGFsQ29tcG9uZW50O1xuICBAVmlld0NoaWxkKFwiYmFyY29kZWxpbmVcIikgYmFyY29kZWxpbmU6IEVsZW1lbnRSZWY7XG5cbiAgaWQgPSBcIlwiO1xuICBpc0Zyb250Q2FtZXJhOiBib29sZWFuID0gZmFsc2U7XG4gIHRvcmNoT246IGJvb2xlYW4gPSBmYWxzZTtcbiAgaXNTY2FubmVkT25jZTogYm9vbGVhbiA9IGZhbHNlO1xuICBmbGFzaENsYXNzID0gXCJmbGFzaFwiO1xuICBjYW1lcmFDbGFzcyA9IFwicmFyZVwiO1xuICBhcHBvaW50bWVudElkID0gXCJcIjtcbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUsXG4gICAgcHJpdmF0ZSBiYXJjb2RlU2Nhbm5lcjogQmFyY29kZVNjYW5uZXIsXG4gICAgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLFxuICAgIHByaXZhdGUgaHR0cDogSHR0cENsaWVudCxcbiAgICBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZVxuICApIHtcbiAgICB0aGlzLmZsYXNoQ2xhc3MgPSBcImZsYXNoXCI7XG4gICAgdGhpcy5jYW1lcmFDbGFzcyA9IFwicmFyZVwiO1xuICAgIHRoaXMuaXNGcm9udENhbWVyYSA9IGZhbHNlO1xuICAgIHRoaXMudXNlclNlcnZpY2UuYWN0aW9uQmFyU3RhdGUodHJ1ZSk7XG4gICAgdGhpcy51c2VyU2VydmljZS5ib3R0b21CYXJEb3RzKDEpO1xuICAgIHRoaXMudXNlclNlcnZpY2UuYm90dG9tQmFyRG90U3RhdGUodHJ1ZSk7XG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGUodHJ1ZSk7XG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGVDaGFuZ2VzKDEpO1xuICAgIHRoaXMudXNlclNlcnZpY2Uuc3dpdGNoU3RhdGUoZmFsc2UpO1xuICAgIHRoaXMuYWN0aXZhdGVkUm91dGUucXVlcnlQYXJhbXMuc3Vic2NyaWJlKHBhcmFtcyA9PiB7XG4gICAgICB0aGlzLmFwcG9pbnRtZW50SWQgPSBwYXJhbXMuaWQ7XG4gICAgICBjb25zb2xlLmxvZyh0aGlzLmFwcG9pbnRtZW50SWQpO1xuICAgIH0pO1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7fVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcbiAgICAvLyBsZXQgdmlldyA9IHRoaXMuYmFyY29kZWxpbmUubmF0aXZlRWxlbWVudCBhcyBTdGFja0xheW91dDtcbiAgICAvLyB2aWV3LmFuaW1hdGUoe1xuICAgIC8vICAgZHVyYXRpb246IDMwMDAsXG4gICAgLy8gICBjdXJ2ZTogQW5pbWF0aW9uQ3VydmUubGluZWFyLFxuICAgIC8vICAgdHJhbnNsYXRlOiB7IHg6IDAsIHk6IDUwIH1cbiAgICAvLyB9KTtcbiAgICAvLyAudGhlbigoKSA9PiB7XG4gICAgLy8gICAvLyBSZXNldCBhbmltYXRpb25cbiAgICAvLyAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgIC8vICAgICB2aWV3LnRyYW5zbGF0ZVkgPSAwO1xuICAgIC8vICAgICB2aWV3LnRyYW5zbGF0ZVggPSAwO1xuICAgIC8vICAgfSwgMzAwMCk7XG4gICAgLy8gfSk7XG4gIH1cbiAgLy8gcHV0RGF0YSgpIHtcbiAgLy8gICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgLy8gICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcbiAgLy8gICAgICAgICBcIngtcm9sZS1rZXlcIjogXCJiMzEyZjFmOS0xZjUxLWMwNjAtMTFiNS1kM2UzZDkwMTliOGNcIlxuICAvLyAgICAgfSk7XG5cbiAgLy8gICAgIHZhciBib2R5ID0ge1xuICAvLyAgICAgICAgIFwicm9sZVwiOiB7XG4gIC8vICAgICAgICAgICAgIFwiaWRcIjogXCI1YWZiYjRiZmRmYWI5NzE2YWFiNzVmYTdcIlxuICAvLyAgICAgICAgIH0sXG4gIC8vICAgICAgICAgXCJhcHBvaW50bWVudFwiOiB7XG4gIC8vICAgICAgICAgICAgIFwiaWRcIjogdGhpcy5hcHBvaW50bWVudElkXG4gIC8vICAgICAgICAgfVxuICAvLyAgICAgfVxuXG4gIC8vICAgICBjb25zb2xlLmxvZygnUmVhY2hlZCBmdW4nLCB0aGlzLmFwcG9pbnRtZW50SWQpXG5cbiAgLy8gICAgIHRoaXMuaHR0cC5wdXQoXCJodHRwOi8vd2VsY29tZS1hcGktZGV2Lm0tc2FzLmNvbS9hcGkvc2Vzc2lvbnMvXCIgKyB0aGlzLmlkLCBib2R5LCB7IGhlYWRlcnM6IGhlYWRlcnMgfSkuc3Vic2NyaWJlKChyZXM6IGFueSkgPT4ge1xuXG4gIC8vICAgICAgICAgaWYgKHJlcy5pc1N1Y2Nlc3MpIHtcbiAgLy8gICAgICAgICAgICAgbGV0IHJlc3VsdDogYW55XG4gIC8vICAgICAgICAgICAgIHJlc3VsdCA9IHJlcy5kYXRhXG4gIC8vICAgICAgICAgICAgIHRoaXMuaXNTY2FubmVkT25jZSA9IHRydWU7XG4gIC8vICAgICAgICAgICAgIC8vIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbJy9sb2dpbiddKTtcbiAgLy8gICAgIGNvbnNvbGUubG9nKHJlcylcblxuICAvLyAgICAgICAgICAgICB0aGlzLm90cE1vZGFsLnNob3coKTtcbiAgLy8gICAgICAgICB9XG4gIC8vICAgICAgICAgZWxzZSB7XG4gIC8vICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzKVxuXG4gIC8vICAgICAgICAgICAgIC8vIGFsZXJ0KHJlcy5lcnJvcilcbiAgLy8gICAgICAgICB9XG4gIC8vICAgICB9LFxuICAvLyAgICAgICAgIGVycm9yID0+IHtcbiAgLy8gICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvciApXG5cbiAgLy8gICAgICAgICAgICAgLy8gYWxlcnQoZXJyb3IpXG4gIC8vICAgICAgICAgfSlcbiAgLy8gfVxuXG4gIC8vIG9uRG9uZSgpIHtcbiAgLy8gICAgIHRoaXMub3RwTW9kYWwuaGlkZSgpO1xuICAvLyAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFsnL2xvZ2luJ10pO1xuICAvLyB9XG5cbiAgLy8gb25CYXJjb2RlU2Nhbm5pbmdSZXN1bHQoYXJncykge1xuICAvLyAgIC8vIHRoaXMuaWQgPSBhcmdzLnZhbHVlLmJhcmNvZGVzWzBdLnZhbHVlO1xuICAvLyAgIC8vIGlmICh0aGlzLmlkICE9IHVuZGVmaW5lZCAmJiAhdGhpcy5pc1NjYW5uZWRPbmNlKSB7XG4gIC8vICAgLy8gICAvLyB0aGlzLnB1dERhdGEoKTtcbiAgLy8gICAvLyAgIGNvbnNvbGUubG9nKFwiUlNTOlwiLCB0aGlzLmlkKTtcbiAgLy8gICAvLyB9XG4gIC8vICAgY29uc29sZS5sb2coYXJncyk7XG4gIC8vIH1cblxuICBvbkZsYXNoU2VsZWN0KCkge1xuICAgIGlmICh0aGlzLmZsYXNoQ2xhc3MgPT0gXCJmbGFzaFwiKSB7XG4gICAgICB0aGlzLmZsYXNoQ2xhc3MgPSBcImZsYXNoLWZvY3VzXCI7XG4gICAgICB0aGlzLnRvcmNoT24gPSB0cnVlO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmZsYXNoQ2xhc3MgPSBcImZsYXNoXCI7XG4gICAgICB0aGlzLnRvcmNoT24gPSBmYWxzZTtcbiAgICB9XG4gIH1cblxuICBvbkNhbWVyYVNlbGVjdCgpIHtcbiAgICBpZiAodGhpcy5jYW1lcmFDbGFzcyA9PSBcInJhcmVcIikge1xuICAgICAgdGhpcy5jYW1lcmFDbGFzcyA9IFwiZnJvbnRcIjtcbiAgICAgIHRoaXMuaXNGcm9udENhbWVyYSA9IHRydWU7XG4gICAgICBjb25zb2xlLmxvZyhcIlRhcHBlZFwiKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5jYW1lcmFDbGFzcyA9IFwicmFyZVwiO1xuICAgICAgdGhpcy5pc0Zyb250Q2FtZXJhID0gZmFsc2U7XG4gICAgICBjb25zb2xlLmxvZyhcIlRhcHBlZFwiKTtcbiAgICB9XG4gIH1cblxuICBvblN3aXBlKGFyZ3M6IFN3aXBlR2VzdHVyZUV2ZW50RGF0YSkge1xuICAgIGlmIChhcmdzLmRpcmVjdGlvbiA9PSAyKSB7XG4gICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3VwY1wiXSk7XG4gICAgfVxuICB9XG5cbiAgLy8gICAoc2NhblJlc3VsdCk9XCJvbkJhcmNvZGVTY2FubmluZ1Jlc3VsdCgkZXZlbnQpXG59XG4iXX0=
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_barcodescanner_1 = require("nativescript-barcodescanner");
var router_1 = require("nativescript-angular/router");
var modal_component_1 = require("../modals/modal.component");
var http_1 = require("@angular/common/http");
var router_2 = require("@angular/router");
var user_service_1 = require("../services/user.service");
var InventoryLocationComponent = /** @class */ (function () {
    function InventoryLocationComponent(activatedRoute, barcodeScanner, routerExtensions, http, userService) {
        var _this = this;
        this.activatedRoute = activatedRoute;
        this.barcodeScanner = barcodeScanner;
        this.routerExtensions = routerExtensions;
        this.http = http;
        this.userService = userService;
        this.brandNames = [
            "Dell",
            "Samsung",
            "Apple",
            "Hp",
            "Nokia",
            "Redmi",
            "Motorola",
            "Linovo",
            "Panasonic"
        ];
        this.quantity = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];
        this.id = "";
        this.isFrontCamera = false;
        this.torchOn = false;
        this.isScannedOnce = false;
        this.flashClass = "flash";
        this.cameraClass = "rare";
        this.appointmentId = "";
        this.flashClass = "flash";
        this.cameraClass = "rare";
        this.isFrontCamera = false;
        this.userService.actionBarState(true);
        this.userService.bottomBarDotState(false);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.activatedRoute.queryParams.subscribe(function (params) {
            _this.appointmentId = params.id;
            console.log(_this.appointmentId);
        });
    }
    InventoryLocationComponent.prototype.ngOnInit = function () { };
    InventoryLocationComponent.prototype.ngAfterViewInit = function () {
        // let view = this.barcodeline.nativeElement as StackLayout;
        // view.animate({
        //   duration: 3000,
        //   curve: AnimationCurve.linear,
        //   translate: { x: 0, y: 50 }
        // });
        // .then(() => {
        //   // Reset animation
        //   setTimeout(() => {
        //     view.translateY = 0;
        //     view.translateX = 0;
        //   }, 3000);
        // });
    };
    // putData() {
    //     let headers = new HttpHeaders({
    //         "Content-Type": "application/json",
    //         "x-role-key": "b312f1f9-1f51-c060-11b5-d3e3d9019b8c"
    //     });
    //     var body = {
    //         "role": {
    //             "id": "5afbb4bfdfab9716aab75fa7"
    //         },
    //         "appointment": {
    //             "id": this.appointmentId
    //         }
    //     }
    //     console.log('Reached fun', this.appointmentId)
    //     this.http.put("http://welcome-api-dev.m-sas.com/api/sessions/" + this.id, body, { headers: headers }).subscribe((res: any) => {
    //         if (res.isSuccess) {
    //             let result: any
    //             result = res.data
    //             this.isScannedOnce = true;
    //             // this.routerExtensions.navigate(['/login']);
    //     console.log(res)
    //             this.otpModal.show();
    //         }
    //         else {
    //                     console.log(res)
    //             // alert(res.error)
    //         }
    //     },
    //         error => {
    //                     console.log(error )
    //             // alert(error)
    //         })
    // }
    // onDone() {
    //     this.otpModal.hide();
    //     this.routerExtensions.navigate(['/login']);
    // }
    // onBarcodeScanningResult(args) {
    //     this.id = args.value.barcodes[0].value;
    //     if (this.id != undefined && !this.isScannedOnce) {
    //         this.putData();
    //         console.log('RSS:', this.id)
    //     }
    // }
    InventoryLocationComponent.prototype.onFlashSelect = function () {
        if (this.flashClass == "flash") {
            this.flashClass = "flash-focus";
            this.torchOn = true;
        }
        else {
            this.flashClass = "flash";
            this.torchOn = false;
        }
    };
    InventoryLocationComponent.prototype.onCameraSelect = function () {
        if (this.cameraClass == "rare") {
            this.cameraClass = "front";
            this.isFrontCamera = true;
            console.log("Tapped");
        }
        else {
            this.cameraClass = "rare";
            this.isFrontCamera = false;
            console.log("Tapped");
        }
    };
    __decorate([
        core_1.ViewChild("otpDialog"),
        __metadata("design:type", modal_component_1.ModalComponent)
    ], InventoryLocationComponent.prototype, "otpModal", void 0);
    __decorate([
        core_1.ViewChild("barcodeline"),
        __metadata("design:type", core_1.ElementRef)
    ], InventoryLocationComponent.prototype, "barcodeline", void 0);
    InventoryLocationComponent = __decorate([
        core_1.Component({
            selector: "inventoryLocation",
            moduleId: module.id,
            templateUrl: "./inventoryLocation.component.html",
            styleUrls: ["./inventoryLocation.component.css"]
        }),
        __metadata("design:paramtypes", [router_2.ActivatedRoute,
            nativescript_barcodescanner_1.BarcodeScanner,
            router_1.RouterExtensions,
            http_1.HttpClient,
            user_service_1.UserService])
    ], InventoryLocationComponent);
    return InventoryLocationComponent;
}());
exports.InventoryLocationComponent = InventoryLocationComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW52ZW50b3J5TG9jYXRpb24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaW52ZW50b3J5TG9jYXRpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBTXVCO0FBQ3ZCLDJFQUE2RDtBQUM3RCxzREFBK0Q7QUFDL0QsNkRBQTJEO0FBQzNELDZDQUErRDtBQUMvRCwwQ0FBeUQ7QUFJekQseURBQXVEO0FBU3ZEO0lBeUJFLG9DQUNVLGNBQThCLEVBQzlCLGNBQThCLEVBQzlCLGdCQUFrQyxFQUNsQyxJQUFnQixFQUNoQixXQUF3QjtRQUxsQyxpQkFtQkM7UUFsQlMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLFNBQUksR0FBSixJQUFJLENBQVk7UUFDaEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUExQjNCLGVBQVUsR0FBRztZQUNsQixNQUFNO1lBQ04sU0FBUztZQUNULE9BQU87WUFDUCxJQUFJO1lBQ0osT0FBTztZQUNQLE9BQU87WUFDUCxVQUFVO1lBQ1YsUUFBUTtZQUNSLFdBQVc7U0FDWixDQUFDO1FBRUssYUFBUSxHQUFHLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFdEUsT0FBRSxHQUFHLEVBQUUsQ0FBQztRQUNSLGtCQUFhLEdBQVksS0FBSyxDQUFDO1FBQy9CLFlBQU8sR0FBWSxLQUFLLENBQUM7UUFDekIsa0JBQWEsR0FBWSxLQUFLLENBQUM7UUFDL0IsZUFBVSxHQUFHLE9BQU8sQ0FBQztRQUNyQixnQkFBVyxHQUFHLE1BQU0sQ0FBQztRQUNyQixrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQVFqQixJQUFJLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQztRQUMxQixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFeEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtZQUM5QyxLQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxFQUFFLENBQUM7WUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDbEMsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsNkNBQVEsR0FBUixjQUFrQixDQUFDO0lBRW5CLG9EQUFlLEdBQWY7UUFDRSw0REFBNEQ7UUFDNUQsaUJBQWlCO1FBQ2pCLG9CQUFvQjtRQUNwQixrQ0FBa0M7UUFDbEMsK0JBQStCO1FBQy9CLE1BQU07UUFDTixnQkFBZ0I7UUFDaEIsdUJBQXVCO1FBQ3ZCLHVCQUF1QjtRQUN2QiwyQkFBMkI7UUFDM0IsMkJBQTJCO1FBQzNCLGNBQWM7UUFDZCxNQUFNO0lBQ1IsQ0FBQztJQUNELGNBQWM7SUFDZCxzQ0FBc0M7SUFDdEMsOENBQThDO0lBQzlDLCtEQUErRDtJQUMvRCxVQUFVO0lBRVYsbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQiwrQ0FBK0M7SUFDL0MsYUFBYTtJQUNiLDJCQUEyQjtJQUMzQix1Q0FBdUM7SUFDdkMsWUFBWTtJQUNaLFFBQVE7SUFFUixxREFBcUQ7SUFFckQsc0lBQXNJO0lBRXRJLCtCQUErQjtJQUMvQiw4QkFBOEI7SUFDOUIsZ0NBQWdDO0lBQ2hDLHlDQUF5QztJQUN6Qyw2REFBNkQ7SUFDN0QsdUJBQXVCO0lBRXZCLG9DQUFvQztJQUNwQyxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLHVDQUF1QztJQUV2QyxrQ0FBa0M7SUFDbEMsWUFBWTtJQUNaLFNBQVM7SUFDVCxxQkFBcUI7SUFDckIsMENBQTBDO0lBRTFDLDhCQUE4QjtJQUM5QixhQUFhO0lBQ2IsSUFBSTtJQUVKLGFBQWE7SUFDYiw0QkFBNEI7SUFDNUIsa0RBQWtEO0lBQ2xELElBQUk7SUFFSixrQ0FBa0M7SUFDbEMsOENBQThDO0lBQzlDLHlEQUF5RDtJQUN6RCwwQkFBMEI7SUFDMUIsdUNBQXVDO0lBQ3ZDLFFBQVE7SUFDUixJQUFJO0lBRUosa0RBQWEsR0FBYjtRQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxPQUFPLEVBQUU7WUFDOUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxhQUFhLENBQUM7WUFDaEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7U0FDckI7YUFBTTtZQUNMLElBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDO1lBQzFCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1NBQ3RCO0lBQ0gsQ0FBQztJQUVELG1EQUFjLEdBQWQ7UUFDRSxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksTUFBTSxFQUFFO1lBQzlCLElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxDQUFDO1lBQzNCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1lBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDdkI7YUFBTTtZQUNMLElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDO1lBQzFCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1lBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDdkI7SUFDSCxDQUFDO0lBeEl1QjtRQUF2QixnQkFBUyxDQUFDLFdBQVcsQ0FBQztrQ0FBVyxnQ0FBYztnRUFBQztJQUN2QjtRQUF6QixnQkFBUyxDQUFDLGFBQWEsQ0FBQztrQ0FBYyxpQkFBVTttRUFBQztJQUZ2QywwQkFBMEI7UUFOdEMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxtQkFBbUI7WUFDN0IsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSxvQ0FBb0M7WUFDakQsU0FBUyxFQUFFLENBQUMsbUNBQW1DLENBQUM7U0FDakQsQ0FBQzt5Q0EyQjBCLHVCQUFjO1lBQ2QsNENBQWM7WUFDWix5QkFBZ0I7WUFDNUIsaUJBQVU7WUFDSCwwQkFBVztPQTlCdkIsMEJBQTBCLENBNEl0QztJQUFELGlDQUFDO0NBQUEsQUE1SUQsSUE0SUM7QUE1SVksZ0VBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgQ29tcG9uZW50LFxuICBPbkluaXQsXG4gIEFmdGVyVmlld0luaXQsXG4gIFZpZXdDaGlsZCxcbiAgRWxlbWVudFJlZlxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgQmFyY29kZVNjYW5uZXIgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWJhcmNvZGVzY2FubmVyXCI7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHsgTW9kYWxDb21wb25lbnQgfSBmcm9tIFwiLi4vbW9kYWxzL21vZGFsLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uL2h0dHBcIjtcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyByZWdpc3RlckVsZW1lbnQgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZWxlbWVudC1yZWdpc3RyeVwiO1xuaW1wb3J0IHsgQW5pbWF0aW9uQ3VydmUgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9lbnVtc1wiO1xuaW1wb3J0IHsgU3RhY2tMYXlvdXQgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9sYXlvdXRzL3N0YWNrLWxheW91dFwiO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBTd2lwZUdlc3R1cmVFdmVudERhdGEgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9nZXN0dXJlc1wiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwiaW52ZW50b3J5TG9jYXRpb25cIixcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgdGVtcGxhdGVVcmw6IFwiLi9pbnZlbnRvcnlMb2NhdGlvbi5jb21wb25lbnQuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vaW52ZW50b3J5TG9jYXRpb24uY29tcG9uZW50LmNzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBJbnZlbnRvcnlMb2NhdGlvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB7XG4gIEBWaWV3Q2hpbGQoXCJvdHBEaWFsb2dcIikgb3RwTW9kYWw6IE1vZGFsQ29tcG9uZW50O1xuICBAVmlld0NoaWxkKFwiYmFyY29kZWxpbmVcIikgYmFyY29kZWxpbmU6IEVsZW1lbnRSZWY7XG5cbiAgcHVibGljIGJyYW5kTmFtZXMgPSBbXG4gICAgXCJEZWxsXCIsXG4gICAgXCJTYW1zdW5nXCIsXG4gICAgXCJBcHBsZVwiLFxuICAgIFwiSHBcIixcbiAgICBcIk5va2lhXCIsXG4gICAgXCJSZWRtaVwiLFxuICAgIFwiTW90b3JvbGFcIixcbiAgICBcIkxpbm92b1wiLFxuICAgIFwiUGFuYXNvbmljXCJcbiAgXTtcblxuICBwdWJsaWMgcXVhbnRpdHkgPSBbXCIxXCIsIFwiMlwiLCBcIjNcIiwgXCI0XCIsIFwiNVwiLCBcIjZcIiwgXCI3XCIsIFwiOFwiLCBcIjlcIiwgXCIxMFwiXTtcblxuICBpZCA9IFwiXCI7XG4gIGlzRnJvbnRDYW1lcmE6IGJvb2xlYW4gPSBmYWxzZTtcbiAgdG9yY2hPbjogYm9vbGVhbiA9IGZhbHNlO1xuICBpc1NjYW5uZWRPbmNlOiBib29sZWFuID0gZmFsc2U7XG4gIGZsYXNoQ2xhc3MgPSBcImZsYXNoXCI7XG4gIGNhbWVyYUNsYXNzID0gXCJyYXJlXCI7XG4gIGFwcG9pbnRtZW50SWQgPSBcIlwiO1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcbiAgICBwcml2YXRlIGJhcmNvZGVTY2FubmVyOiBCYXJjb2RlU2Nhbm5lcixcbiAgICBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsXG4gICAgcHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LFxuICAgIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlXG4gICkge1xuICAgIHRoaXMuZmxhc2hDbGFzcyA9IFwiZmxhc2hcIjtcbiAgICB0aGlzLmNhbWVyYUNsYXNzID0gXCJyYXJlXCI7XG4gICAgdGhpcy5pc0Zyb250Q2FtZXJhID0gZmFsc2U7XG4gICAgdGhpcy51c2VyU2VydmljZS5hY3Rpb25CYXJTdGF0ZSh0cnVlKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdFN0YXRlKGZhbHNlKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZSh0cnVlKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZUNoYW5nZXMoMSk7XG5cbiAgICB0aGlzLmFjdGl2YXRlZFJvdXRlLnF1ZXJ5UGFyYW1zLnN1YnNjcmliZShwYXJhbXMgPT4ge1xuICAgICAgdGhpcy5hcHBvaW50bWVudElkID0gcGFyYW1zLmlkO1xuICAgICAgY29uc29sZS5sb2codGhpcy5hcHBvaW50bWVudElkKTtcbiAgICB9KTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge31cblxuICBuZ0FmdGVyVmlld0luaXQoKSB7XG4gICAgLy8gbGV0IHZpZXcgPSB0aGlzLmJhcmNvZGVsaW5lLm5hdGl2ZUVsZW1lbnQgYXMgU3RhY2tMYXlvdXQ7XG4gICAgLy8gdmlldy5hbmltYXRlKHtcbiAgICAvLyAgIGR1cmF0aW9uOiAzMDAwLFxuICAgIC8vICAgY3VydmU6IEFuaW1hdGlvbkN1cnZlLmxpbmVhcixcbiAgICAvLyAgIHRyYW5zbGF0ZTogeyB4OiAwLCB5OiA1MCB9XG4gICAgLy8gfSk7XG4gICAgLy8gLnRoZW4oKCkgPT4ge1xuICAgIC8vICAgLy8gUmVzZXQgYW5pbWF0aW9uXG4gICAgLy8gICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAvLyAgICAgdmlldy50cmFuc2xhdGVZID0gMDtcbiAgICAvLyAgICAgdmlldy50cmFuc2xhdGVYID0gMDtcbiAgICAvLyAgIH0sIDMwMDApO1xuICAgIC8vIH0pO1xuICB9XG4gIC8vIHB1dERhdGEoKSB7XG4gIC8vICAgICBsZXQgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gIC8vICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXG4gIC8vICAgICAgICAgXCJ4LXJvbGUta2V5XCI6IFwiYjMxMmYxZjktMWY1MS1jMDYwLTExYjUtZDNlM2Q5MDE5YjhjXCJcbiAgLy8gICAgIH0pO1xuXG4gIC8vICAgICB2YXIgYm9keSA9IHtcbiAgLy8gICAgICAgICBcInJvbGVcIjoge1xuICAvLyAgICAgICAgICAgICBcImlkXCI6IFwiNWFmYmI0YmZkZmFiOTcxNmFhYjc1ZmE3XCJcbiAgLy8gICAgICAgICB9LFxuICAvLyAgICAgICAgIFwiYXBwb2ludG1lbnRcIjoge1xuICAvLyAgICAgICAgICAgICBcImlkXCI6IHRoaXMuYXBwb2ludG1lbnRJZFxuICAvLyAgICAgICAgIH1cbiAgLy8gICAgIH1cblxuICAvLyAgICAgY29uc29sZS5sb2coJ1JlYWNoZWQgZnVuJywgdGhpcy5hcHBvaW50bWVudElkKVxuXG4gIC8vICAgICB0aGlzLmh0dHAucHV0KFwiaHR0cDovL3dlbGNvbWUtYXBpLWRldi5tLXNhcy5jb20vYXBpL3Nlc3Npb25zL1wiICsgdGhpcy5pZCwgYm9keSwgeyBoZWFkZXJzOiBoZWFkZXJzIH0pLnN1YnNjcmliZSgocmVzOiBhbnkpID0+IHtcblxuICAvLyAgICAgICAgIGlmIChyZXMuaXNTdWNjZXNzKSB7XG4gIC8vICAgICAgICAgICAgIGxldCByZXN1bHQ6IGFueVxuICAvLyAgICAgICAgICAgICByZXN1bHQgPSByZXMuZGF0YVxuICAvLyAgICAgICAgICAgICB0aGlzLmlzU2Nhbm5lZE9uY2UgPSB0cnVlO1xuICAvLyAgICAgICAgICAgICAvLyB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoWycvbG9naW4nXSk7XG4gIC8vICAgICBjb25zb2xlLmxvZyhyZXMpXG5cbiAgLy8gICAgICAgICAgICAgdGhpcy5vdHBNb2RhbC5zaG93KCk7XG4gIC8vICAgICAgICAgfVxuICAvLyAgICAgICAgIGVsc2Uge1xuICAvLyAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlcylcblxuICAvLyAgICAgICAgICAgICAvLyBhbGVydChyZXMuZXJyb3IpXG4gIC8vICAgICAgICAgfVxuICAvLyAgICAgfSxcbiAgLy8gICAgICAgICBlcnJvciA9PiB7XG4gIC8vICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IgKVxuXG4gIC8vICAgICAgICAgICAgIC8vIGFsZXJ0KGVycm9yKVxuICAvLyAgICAgICAgIH0pXG4gIC8vIH1cblxuICAvLyBvbkRvbmUoKSB7XG4gIC8vICAgICB0aGlzLm90cE1vZGFsLmhpZGUoKTtcbiAgLy8gICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbJy9sb2dpbiddKTtcbiAgLy8gfVxuXG4gIC8vIG9uQmFyY29kZVNjYW5uaW5nUmVzdWx0KGFyZ3MpIHtcbiAgLy8gICAgIHRoaXMuaWQgPSBhcmdzLnZhbHVlLmJhcmNvZGVzWzBdLnZhbHVlO1xuICAvLyAgICAgaWYgKHRoaXMuaWQgIT0gdW5kZWZpbmVkICYmICF0aGlzLmlzU2Nhbm5lZE9uY2UpIHtcbiAgLy8gICAgICAgICB0aGlzLnB1dERhdGEoKTtcbiAgLy8gICAgICAgICBjb25zb2xlLmxvZygnUlNTOicsIHRoaXMuaWQpXG4gIC8vICAgICB9XG4gIC8vIH1cblxuICBvbkZsYXNoU2VsZWN0KCkge1xuICAgIGlmICh0aGlzLmZsYXNoQ2xhc3MgPT0gXCJmbGFzaFwiKSB7XG4gICAgICB0aGlzLmZsYXNoQ2xhc3MgPSBcImZsYXNoLWZvY3VzXCI7XG4gICAgICB0aGlzLnRvcmNoT24gPSB0cnVlO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmZsYXNoQ2xhc3MgPSBcImZsYXNoXCI7XG4gICAgICB0aGlzLnRvcmNoT24gPSBmYWxzZTtcbiAgICB9XG4gIH1cblxuICBvbkNhbWVyYVNlbGVjdCgpIHtcbiAgICBpZiAodGhpcy5jYW1lcmFDbGFzcyA9PSBcInJhcmVcIikge1xuICAgICAgdGhpcy5jYW1lcmFDbGFzcyA9IFwiZnJvbnRcIjtcbiAgICAgIHRoaXMuaXNGcm9udENhbWVyYSA9IHRydWU7XG4gICAgICBjb25zb2xlLmxvZyhcIlRhcHBlZFwiKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5jYW1lcmFDbGFzcyA9IFwicmFyZVwiO1xuICAgICAgdGhpcy5pc0Zyb250Q2FtZXJhID0gZmFsc2U7XG4gICAgICBjb25zb2xlLmxvZyhcIlRhcHBlZFwiKTtcbiAgICB9XG4gIH1cblxuICAvLyAgIChzY2FuUmVzdWx0KT1cIm9uQmFyY29kZVNjYW5uaW5nUmVzdWx0KCRldmVudClcbn1cbiJdfQ==
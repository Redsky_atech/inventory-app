"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var user_service_1 = require("../services/user.service");
var UserAccountComponent = /** @class */ (function () {
    function UserAccountComponent(routerExtensions, userService) {
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.userService.actionBarState(true);
        this.userService.bottomBarDotState(false);
        this.userService.tabViewStateChanges(2);
        this.userService.switchState(false);
    }
    UserAccountComponent.prototype.ngOnInit = function () { };
    UserAccountComponent.prototype.onPreferences = function () {
        this.routerExtensions.navigate(["/prefSku"], {
            animated: true,
            transition: {
                name: "slide",
                duration: 200,
                curve: "ease"
            }
        });
    };
    UserAccountComponent = __decorate([
        core_1.Component({
            selector: "ns-userAccount",
            moduleId: module.id,
            templateUrl: "./userAccount.component.html",
            styleUrls: ["./userAccount.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            user_service_1.UserService])
    ], UserAccountComponent);
    return UserAccountComponent;
}());
exports.UserAccountComponent = UserAccountComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlckFjY291bnQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidXNlckFjY291bnQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlFO0FBQ3pFLHNEQUErRDtBQUMvRCx5REFBdUQ7QUFRdkQ7SUFHRSw4QkFDRSxnQkFBa0MsRUFDMUIsV0FBd0I7UUFBeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFFaEMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBQ0QsdUNBQVEsR0FBUixjQUFrQixDQUFDO0lBRVosNENBQWEsR0FBcEI7UUFDRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDM0MsUUFBUSxFQUFFLElBQUk7WUFDZCxVQUFVLEVBQUU7Z0JBQ1YsSUFBSSxFQUFFLE9BQU87Z0JBQ2IsUUFBUSxFQUFFLEdBQUc7Z0JBQ2IsS0FBSyxFQUFFLE1BQU07YUFDZDtTQUNGLENBQUMsQ0FBQztJQUNMLENBQUM7SUF4QlUsb0JBQW9CO1FBTmhDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsZ0JBQWdCO1lBQzFCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsOEJBQThCO1lBQzNDLFNBQVMsRUFBRSxDQUFDLDZCQUE2QixDQUFDO1NBQzNDLENBQUM7eUNBS29CLHlCQUFnQjtZQUNiLDBCQUFXO09BTHZCLG9CQUFvQixDQXlCaEM7SUFBRCwyQkFBQztDQUFBLEFBekJELElBeUJDO0FBekJZLG9EQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcIm5zLXVzZXJBY2NvdW50XCIsXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICB0ZW1wbGF0ZVVybDogXCIuL3VzZXJBY2NvdW50LmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL3VzZXJBY2NvdW50LmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIFVzZXJBY2NvdW50Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsXHJcbiAgICBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZVxyXG4gICkge1xyXG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zID0gcm91dGVyRXh0ZW5zaW9ucztcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYWN0aW9uQmFyU3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdFN0YXRlKGZhbHNlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UudGFiVmlld1N0YXRlQ2hhbmdlcygyKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2Uuc3dpdGNoU3RhdGUoZmFsc2UpO1xyXG4gIH1cclxuICBuZ09uSW5pdCgpOiB2b2lkIHt9XHJcblxyXG4gIHB1YmxpYyBvblByZWZlcmVuY2VzKCkge1xyXG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9wcmVmU2t1XCJdLCB7XHJcbiAgICAgIGFuaW1hdGVkOiB0cnVlLFxyXG4gICAgICB0cmFuc2l0aW9uOiB7XHJcbiAgICAgICAgbmFtZTogXCJzbGlkZVwiLFxyXG4gICAgICAgIGR1cmF0aW9uOiAyMDAsXHJcbiAgICAgICAgY3VydmU6IFwiZWFzZVwiXHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=
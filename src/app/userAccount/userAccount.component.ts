import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { UserService } from "../services/user.service";

@Component({
  selector: "ns-userAccount",
  moduleId: module.id,
  templateUrl: "./userAccount.component.html",
  styleUrls: ["./userAccount.component.css"]
})
export class UserAccountComponent implements OnInit {
  routerExtensions: RouterExtensions;

  constructor(
    routerExtensions: RouterExtensions,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDotState(false);
    this.userService.tabViewStateChanges(2);
    this.userService.switchState(false);
  }
  ngOnInit(): void {}

  public onPreferences() {
    this.routerExtensions.navigate(["/prefSku"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }
}

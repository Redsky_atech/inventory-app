import { Component, OnInit } from "@angular/core";
import { registerElement } from "nativescript-angular/element-registry";
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page/page";
import { UserService } from "../services/user.service";
import { Switch } from "tns-core-modules/ui/switch";

@Component({
  selector: "preferencesDefaults",
  moduleId: module.id,
  templateUrl: "./preferencesDefaults.component.html",
  styleUrls: ["./preferencesDefaults.component.css"]
})
export class PreferencesDefaultsComponent implements OnInit {
  routerExtensions: RouterExtensions;

  public listingTypes = [
    "Listing Type 1",
    "Listing Type 2",
    "Listing Type 3",
    "Listing Type 4",
    "Listing Type 5",
    "Listing Type 6",
    "Listing Type 7",
    "Listing Type 8",
    "Listing Type 9",
    "Listing Type 10"
  ];

  public listingDuration = [
    "5",
    "10",
    "15",
    "20",
    "25",
    "30",
    "35",
    "40",
    "45",
    "50",
    "55",
    "60"
  ];

  public paymentPolicyType = [
    "Payment Policy 1",
    "Payment Policy 2",
    "Payment Policy 3",
    "Payment Policy 4",
    "Payment Policy 5",
    "Payment Policy 6",
    "Payment Policy 7",
    "Payment Policy 8",
    "Payment Policy 9",
    "Payment Policy 10"
  ];

  public returnPolicyType = [
    "Return Policy 1",
    "Return Policy 2",
    "Return Policy 3",
    "Return Policy 4",
    "Return Policy 5",
    "Return Policy 6",
    "Return Policy 7",
    "Return Policy 8",
    "Return Policy 9",
    "Return Policy 10"
  ];

  public shippingPolicyType = [
    "Shipping Policy 1",
    "Shipping Policy 2",
    "Shipping Policy 3",
    "Shipping Policy 4",
    "Shipping Policy 5",
    "Shipping Policy 6",
    "Shipping Policy 7",
    "Shipping Policy 8",
    "Shipping Policy 9",
    "Shipping Policy 10"
  ];

  businessLabel = "Business Policies";
  eBayPremiumLabel = "eBay Premium Content";
  structuredDataLabel = "Structured Data";
  storeCategoryLabel = "Store Categories";
  makeOffersLabel = "Make Offers";
  globalShippingLabel = "Global Shipping";

  businessSwitch = "switchDisable";
  eBayPremiumSwitch = "switchDisable";
  structuredSwitch = "switchDisable";
  storeCategorySwitch = "switchDisable";
  makeOffersSwitch = "switchDisable";
  globalShippingSwitch = "switchDisable";

  constructor(
    routerExtensions: RouterExtensions,
    private activatedRoute: ActivatedRoute,
    private page: Page,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDotState(false);
    this.userService.tabViewState(true);
    this.userService.switchState(false);
    this.userService.tabViewStateChanges(3);
  }

  public onBusinessSwitch(args) {
    let checkStatus = <Switch>args.object;
    if (checkStatus.checked) {
      this.businessSwitch = "switchEnable";
    } else {
      this.businessSwitch = "switchDisable";
    }
  }

  public oneBayPremiumSwitch(args) {
    let checkStatus = <Switch>args.object;
    if (checkStatus.checked) {
      this.eBayPremiumSwitch = "switchEnable";
    } else {
      this.eBayPremiumSwitch = "switchDisable";
    }
  }

  public onStructuredDataSwitch(args) {
    let checkStatus = <Switch>args.object;
    if (checkStatus.checked) {
      this.structuredSwitch = "switchEnable";
    } else {
      this.structuredSwitch = "switchDisable";
    }
  }

  public onStoreCategorySwitch(args) {
    let checkStatus = <Switch>args.object;
    if (checkStatus.checked) {
      this.storeCategorySwitch = "switchEnable";
    } else {
      this.storeCategorySwitch = "switchDisable";
    }
  }

  public onMakeOffersSwitch(args) {
    let checkStatus = <Switch>args.object;
    if (checkStatus.checked) {
      this.makeOffersSwitch = "switchEnable";
    } else {
      this.makeOffersSwitch = "switchDisable";
    }
  }

  public onGlobalShippingSwitch(args) {
    let checkStatus = <Switch>args.object;
    if (checkStatus.checked) {
      this.globalShippingSwitch = "switchEnable";
    } else {
      this.globalShippingSwitch = "switchDisable";
    }
  }

  ngOnInit(): void {}
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var router_2 = require("@angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var user_service_1 = require("../services/user.service");
var PreferencesDefaultsComponent = /** @class */ (function () {
    function PreferencesDefaultsComponent(routerExtensions, activatedRoute, page, userService) {
        this.activatedRoute = activatedRoute;
        this.page = page;
        this.userService = userService;
        this.listingTypes = [
            "Listing Type 1",
            "Listing Type 2",
            "Listing Type 3",
            "Listing Type 4",
            "Listing Type 5",
            "Listing Type 6",
            "Listing Type 7",
            "Listing Type 8",
            "Listing Type 9",
            "Listing Type 10"
        ];
        this.listingDuration = [
            "5",
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50",
            "55",
            "60"
        ];
        this.paymentPolicyType = [
            "Payment Policy 1",
            "Payment Policy 2",
            "Payment Policy 3",
            "Payment Policy 4",
            "Payment Policy 5",
            "Payment Policy 6",
            "Payment Policy 7",
            "Payment Policy 8",
            "Payment Policy 9",
            "Payment Policy 10"
        ];
        this.returnPolicyType = [
            "Return Policy 1",
            "Return Policy 2",
            "Return Policy 3",
            "Return Policy 4",
            "Return Policy 5",
            "Return Policy 6",
            "Return Policy 7",
            "Return Policy 8",
            "Return Policy 9",
            "Return Policy 10"
        ];
        this.shippingPolicyType = [
            "Shipping Policy 1",
            "Shipping Policy 2",
            "Shipping Policy 3",
            "Shipping Policy 4",
            "Shipping Policy 5",
            "Shipping Policy 6",
            "Shipping Policy 7",
            "Shipping Policy 8",
            "Shipping Policy 9",
            "Shipping Policy 10"
        ];
        this.businessLabel = "Business Policies";
        this.eBayPremiumLabel = "eBay Premium Content";
        this.structuredDataLabel = "Structured Data";
        this.storeCategoryLabel = "Store Categories";
        this.makeOffersLabel = "Make Offers";
        this.globalShippingLabel = "Global Shipping";
        this.businessSwitch = "switchDisable";
        this.eBayPremiumSwitch = "switchDisable";
        this.structuredSwitch = "switchDisable";
        this.storeCategorySwitch = "switchDisable";
        this.makeOffersSwitch = "switchDisable";
        this.globalShippingSwitch = "switchDisable";
        this.routerExtensions = routerExtensions;
        this.userService.actionBarState(true);
        this.userService.bottomBarDotState(false);
        this.userService.tabViewState(true);
        this.userService.switchState(false);
        this.userService.tabViewStateChanges(3);
    }
    PreferencesDefaultsComponent.prototype.onBusinessSwitch = function (args) {
        var checkStatus = args.object;
        if (checkStatus.checked) {
            this.businessSwitch = "switchEnable";
        }
        else {
            this.businessSwitch = "switchDisable";
        }
    };
    PreferencesDefaultsComponent.prototype.oneBayPremiumSwitch = function (args) {
        var checkStatus = args.object;
        if (checkStatus.checked) {
            this.eBayPremiumSwitch = "switchEnable";
        }
        else {
            this.eBayPremiumSwitch = "switchDisable";
        }
    };
    PreferencesDefaultsComponent.prototype.onStructuredDataSwitch = function (args) {
        var checkStatus = args.object;
        if (checkStatus.checked) {
            this.structuredSwitch = "switchEnable";
        }
        else {
            this.structuredSwitch = "switchDisable";
        }
    };
    PreferencesDefaultsComponent.prototype.onStoreCategorySwitch = function (args) {
        var checkStatus = args.object;
        if (checkStatus.checked) {
            this.storeCategorySwitch = "switchEnable";
        }
        else {
            this.storeCategorySwitch = "switchDisable";
        }
    };
    PreferencesDefaultsComponent.prototype.onMakeOffersSwitch = function (args) {
        var checkStatus = args.object;
        if (checkStatus.checked) {
            this.makeOffersSwitch = "switchEnable";
        }
        else {
            this.makeOffersSwitch = "switchDisable";
        }
    };
    PreferencesDefaultsComponent.prototype.onGlobalShippingSwitch = function (args) {
        var checkStatus = args.object;
        if (checkStatus.checked) {
            this.globalShippingSwitch = "switchEnable";
        }
        else {
            this.globalShippingSwitch = "switchDisable";
        }
    };
    PreferencesDefaultsComponent.prototype.ngOnInit = function () { };
    PreferencesDefaultsComponent = __decorate([
        core_1.Component({
            selector: "preferencesDefaults",
            moduleId: module.id,
            templateUrl: "./preferencesDefaults.component.html",
            styleUrls: ["./preferencesDefaults.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            router_2.ActivatedRoute,
            page_1.Page,
            user_service_1.UserService])
    ], PreferencesDefaultsComponent);
    return PreferencesDefaultsComponent;
}());
exports.PreferencesDefaultsComponent = PreferencesDefaultsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJlZmVyZW5jZXNEZWZhdWx0cy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJwcmVmZXJlbmNlc0RlZmF1bHRzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUVsRCxzREFBK0Q7QUFDL0QsMENBQWlEO0FBQ2pELHNEQUFxRDtBQUNyRCx5REFBdUQ7QUFTdkQ7SUFvRkUsc0NBQ0UsZ0JBQWtDLEVBQzFCLGNBQThCLEVBQzlCLElBQVUsRUFDVixXQUF3QjtRQUZ4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUNWLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBckYzQixpQkFBWSxHQUFHO1lBQ3BCLGdCQUFnQjtZQUNoQixnQkFBZ0I7WUFDaEIsZ0JBQWdCO1lBQ2hCLGdCQUFnQjtZQUNoQixnQkFBZ0I7WUFDaEIsZ0JBQWdCO1lBQ2hCLGdCQUFnQjtZQUNoQixnQkFBZ0I7WUFDaEIsZ0JBQWdCO1lBQ2hCLGlCQUFpQjtTQUNsQixDQUFDO1FBRUssb0JBQWUsR0FBRztZQUN2QixHQUFHO1lBQ0gsSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtZQUNKLElBQUk7U0FDTCxDQUFDO1FBRUssc0JBQWlCLEdBQUc7WUFDekIsa0JBQWtCO1lBQ2xCLGtCQUFrQjtZQUNsQixrQkFBa0I7WUFDbEIsa0JBQWtCO1lBQ2xCLGtCQUFrQjtZQUNsQixrQkFBa0I7WUFDbEIsa0JBQWtCO1lBQ2xCLGtCQUFrQjtZQUNsQixrQkFBa0I7WUFDbEIsbUJBQW1CO1NBQ3BCLENBQUM7UUFFSyxxQkFBZ0IsR0FBRztZQUN4QixpQkFBaUI7WUFDakIsaUJBQWlCO1lBQ2pCLGlCQUFpQjtZQUNqQixpQkFBaUI7WUFDakIsaUJBQWlCO1lBQ2pCLGlCQUFpQjtZQUNqQixpQkFBaUI7WUFDakIsaUJBQWlCO1lBQ2pCLGlCQUFpQjtZQUNqQixrQkFBa0I7U0FDbkIsQ0FBQztRQUVLLHVCQUFrQixHQUFHO1lBQzFCLG1CQUFtQjtZQUNuQixtQkFBbUI7WUFDbkIsbUJBQW1CO1lBQ25CLG1CQUFtQjtZQUNuQixtQkFBbUI7WUFDbkIsbUJBQW1CO1lBQ25CLG1CQUFtQjtZQUNuQixtQkFBbUI7WUFDbkIsbUJBQW1CO1lBQ25CLG9CQUFvQjtTQUNyQixDQUFDO1FBRUYsa0JBQWEsR0FBRyxtQkFBbUIsQ0FBQztRQUNwQyxxQkFBZ0IsR0FBRyxzQkFBc0IsQ0FBQztRQUMxQyx3QkFBbUIsR0FBRyxpQkFBaUIsQ0FBQztRQUN4Qyx1QkFBa0IsR0FBRyxrQkFBa0IsQ0FBQztRQUN4QyxvQkFBZSxHQUFHLGFBQWEsQ0FBQztRQUNoQyx3QkFBbUIsR0FBRyxpQkFBaUIsQ0FBQztRQUV4QyxtQkFBYyxHQUFHLGVBQWUsQ0FBQztRQUNqQyxzQkFBaUIsR0FBRyxlQUFlLENBQUM7UUFDcEMscUJBQWdCLEdBQUcsZUFBZSxDQUFDO1FBQ25DLHdCQUFtQixHQUFHLGVBQWUsQ0FBQztRQUN0QyxxQkFBZ0IsR0FBRyxlQUFlLENBQUM7UUFDbkMseUJBQW9CLEdBQUcsZUFBZSxDQUFDO1FBUXJDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVNLHVEQUFnQixHQUF2QixVQUF3QixJQUFJO1FBQzFCLElBQUksV0FBVyxHQUFXLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdEMsSUFBSSxXQUFXLENBQUMsT0FBTyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDO1NBQ3RDO2FBQU07WUFDTCxJQUFJLENBQUMsY0FBYyxHQUFHLGVBQWUsQ0FBQztTQUN2QztJQUNILENBQUM7SUFFTSwwREFBbUIsR0FBMUIsVUFBMkIsSUFBSTtRQUM3QixJQUFJLFdBQVcsR0FBVyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3RDLElBQUksV0FBVyxDQUFDLE9BQU8sRUFBRTtZQUN2QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsY0FBYyxDQUFDO1NBQ3pDO2FBQU07WUFDTCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsZUFBZSxDQUFDO1NBQzFDO0lBQ0gsQ0FBQztJQUVNLDZEQUFzQixHQUE3QixVQUE4QixJQUFJO1FBQ2hDLElBQUksV0FBVyxHQUFXLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdEMsSUFBSSxXQUFXLENBQUMsT0FBTyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxjQUFjLENBQUM7U0FDeEM7YUFBTTtZQUNMLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxlQUFlLENBQUM7U0FDekM7SUFDSCxDQUFDO0lBRU0sNERBQXFCLEdBQTVCLFVBQTZCLElBQUk7UUFDL0IsSUFBSSxXQUFXLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN0QyxJQUFJLFdBQVcsQ0FBQyxPQUFPLEVBQUU7WUFDdkIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLGNBQWMsQ0FBQztTQUMzQzthQUFNO1lBQ0wsSUFBSSxDQUFDLG1CQUFtQixHQUFHLGVBQWUsQ0FBQztTQUM1QztJQUNILENBQUM7SUFFTSx5REFBa0IsR0FBekIsVUFBMEIsSUFBSTtRQUM1QixJQUFJLFdBQVcsR0FBVyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3RDLElBQUksV0FBVyxDQUFDLE9BQU8sRUFBRTtZQUN2QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsY0FBYyxDQUFDO1NBQ3hDO2FBQU07WUFDTCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZUFBZSxDQUFDO1NBQ3pDO0lBQ0gsQ0FBQztJQUVNLDZEQUFzQixHQUE3QixVQUE4QixJQUFJO1FBQ2hDLElBQUksV0FBVyxHQUFXLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdEMsSUFBSSxXQUFXLENBQUMsT0FBTyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxjQUFjLENBQUM7U0FDNUM7YUFBTTtZQUNMLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxlQUFlLENBQUM7U0FDN0M7SUFDSCxDQUFDO0lBRUQsK0NBQVEsR0FBUixjQUFrQixDQUFDO0lBeEpSLDRCQUE0QjtRQU54QyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLHFCQUFxQjtZQUMvQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLHNDQUFzQztZQUNuRCxTQUFTLEVBQUUsQ0FBQyxxQ0FBcUMsQ0FBQztTQUNuRCxDQUFDO3lDQXNGb0IseUJBQWdCO1lBQ1YsdUJBQWM7WUFDeEIsV0FBSTtZQUNHLDBCQUFXO09BeEZ2Qiw0QkFBNEIsQ0F5SnhDO0lBQUQsbUNBQUM7Q0FBQSxBQXpKRCxJQXlKQztBQXpKWSxvRUFBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IHJlZ2lzdGVyRWxlbWVudCB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9lbGVtZW50LXJlZ2lzdHJ5XCI7XHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvcGFnZS9wYWdlXCI7XHJcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL3VzZXIuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBTd2l0Y2ggfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9zd2l0Y2hcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcInByZWZlcmVuY2VzRGVmYXVsdHNcIixcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vcHJlZmVyZW5jZXNEZWZhdWx0cy5jb21wb25lbnQuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wiLi9wcmVmZXJlbmNlc0RlZmF1bHRzLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIFByZWZlcmVuY2VzRGVmYXVsdHNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnM7XHJcblxyXG4gIHB1YmxpYyBsaXN0aW5nVHlwZXMgPSBbXHJcbiAgICBcIkxpc3RpbmcgVHlwZSAxXCIsXHJcbiAgICBcIkxpc3RpbmcgVHlwZSAyXCIsXHJcbiAgICBcIkxpc3RpbmcgVHlwZSAzXCIsXHJcbiAgICBcIkxpc3RpbmcgVHlwZSA0XCIsXHJcbiAgICBcIkxpc3RpbmcgVHlwZSA1XCIsXHJcbiAgICBcIkxpc3RpbmcgVHlwZSA2XCIsXHJcbiAgICBcIkxpc3RpbmcgVHlwZSA3XCIsXHJcbiAgICBcIkxpc3RpbmcgVHlwZSA4XCIsXHJcbiAgICBcIkxpc3RpbmcgVHlwZSA5XCIsXHJcbiAgICBcIkxpc3RpbmcgVHlwZSAxMFwiXHJcbiAgXTtcclxuXHJcbiAgcHVibGljIGxpc3RpbmdEdXJhdGlvbiA9IFtcclxuICAgIFwiNVwiLFxyXG4gICAgXCIxMFwiLFxyXG4gICAgXCIxNVwiLFxyXG4gICAgXCIyMFwiLFxyXG4gICAgXCIyNVwiLFxyXG4gICAgXCIzMFwiLFxyXG4gICAgXCIzNVwiLFxyXG4gICAgXCI0MFwiLFxyXG4gICAgXCI0NVwiLFxyXG4gICAgXCI1MFwiLFxyXG4gICAgXCI1NVwiLFxyXG4gICAgXCI2MFwiXHJcbiAgXTtcclxuXHJcbiAgcHVibGljIHBheW1lbnRQb2xpY3lUeXBlID0gW1xyXG4gICAgXCJQYXltZW50IFBvbGljeSAxXCIsXHJcbiAgICBcIlBheW1lbnQgUG9saWN5IDJcIixcclxuICAgIFwiUGF5bWVudCBQb2xpY3kgM1wiLFxyXG4gICAgXCJQYXltZW50IFBvbGljeSA0XCIsXHJcbiAgICBcIlBheW1lbnQgUG9saWN5IDVcIixcclxuICAgIFwiUGF5bWVudCBQb2xpY3kgNlwiLFxyXG4gICAgXCJQYXltZW50IFBvbGljeSA3XCIsXHJcbiAgICBcIlBheW1lbnQgUG9saWN5IDhcIixcclxuICAgIFwiUGF5bWVudCBQb2xpY3kgOVwiLFxyXG4gICAgXCJQYXltZW50IFBvbGljeSAxMFwiXHJcbiAgXTtcclxuXHJcbiAgcHVibGljIHJldHVyblBvbGljeVR5cGUgPSBbXHJcbiAgICBcIlJldHVybiBQb2xpY3kgMVwiLFxyXG4gICAgXCJSZXR1cm4gUG9saWN5IDJcIixcclxuICAgIFwiUmV0dXJuIFBvbGljeSAzXCIsXHJcbiAgICBcIlJldHVybiBQb2xpY3kgNFwiLFxyXG4gICAgXCJSZXR1cm4gUG9saWN5IDVcIixcclxuICAgIFwiUmV0dXJuIFBvbGljeSA2XCIsXHJcbiAgICBcIlJldHVybiBQb2xpY3kgN1wiLFxyXG4gICAgXCJSZXR1cm4gUG9saWN5IDhcIixcclxuICAgIFwiUmV0dXJuIFBvbGljeSA5XCIsXHJcbiAgICBcIlJldHVybiBQb2xpY3kgMTBcIlxyXG4gIF07XHJcblxyXG4gIHB1YmxpYyBzaGlwcGluZ1BvbGljeVR5cGUgPSBbXHJcbiAgICBcIlNoaXBwaW5nIFBvbGljeSAxXCIsXHJcbiAgICBcIlNoaXBwaW5nIFBvbGljeSAyXCIsXHJcbiAgICBcIlNoaXBwaW5nIFBvbGljeSAzXCIsXHJcbiAgICBcIlNoaXBwaW5nIFBvbGljeSA0XCIsXHJcbiAgICBcIlNoaXBwaW5nIFBvbGljeSA1XCIsXHJcbiAgICBcIlNoaXBwaW5nIFBvbGljeSA2XCIsXHJcbiAgICBcIlNoaXBwaW5nIFBvbGljeSA3XCIsXHJcbiAgICBcIlNoaXBwaW5nIFBvbGljeSA4XCIsXHJcbiAgICBcIlNoaXBwaW5nIFBvbGljeSA5XCIsXHJcbiAgICBcIlNoaXBwaW5nIFBvbGljeSAxMFwiXHJcbiAgXTtcclxuXHJcbiAgYnVzaW5lc3NMYWJlbCA9IFwiQnVzaW5lc3MgUG9saWNpZXNcIjtcclxuICBlQmF5UHJlbWl1bUxhYmVsID0gXCJlQmF5IFByZW1pdW0gQ29udGVudFwiO1xyXG4gIHN0cnVjdHVyZWREYXRhTGFiZWwgPSBcIlN0cnVjdHVyZWQgRGF0YVwiO1xyXG4gIHN0b3JlQ2F0ZWdvcnlMYWJlbCA9IFwiU3RvcmUgQ2F0ZWdvcmllc1wiO1xyXG4gIG1ha2VPZmZlcnNMYWJlbCA9IFwiTWFrZSBPZmZlcnNcIjtcclxuICBnbG9iYWxTaGlwcGluZ0xhYmVsID0gXCJHbG9iYWwgU2hpcHBpbmdcIjtcclxuXHJcbiAgYnVzaW5lc3NTd2l0Y2ggPSBcInN3aXRjaERpc2FibGVcIjtcclxuICBlQmF5UHJlbWl1bVN3aXRjaCA9IFwic3dpdGNoRGlzYWJsZVwiO1xyXG4gIHN0cnVjdHVyZWRTd2l0Y2ggPSBcInN3aXRjaERpc2FibGVcIjtcclxuICBzdG9yZUNhdGVnb3J5U3dpdGNoID0gXCJzd2l0Y2hEaXNhYmxlXCI7XHJcbiAgbWFrZU9mZmVyc1N3aXRjaCA9IFwic3dpdGNoRGlzYWJsZVwiO1xyXG4gIGdsb2JhbFNoaXBwaW5nU3dpdGNoID0gXCJzd2l0Y2hEaXNhYmxlXCI7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcclxuICAgIHByaXZhdGUgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlLFxyXG4gICAgcHJpdmF0ZSBwYWdlOiBQYWdlLFxyXG4gICAgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2VcclxuICApIHtcclxuICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucyA9IHJvdXRlckV4dGVuc2lvbnM7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmFjdGlvbkJhclN0YXRlKHRydWUpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5ib3R0b21CYXJEb3RTdGF0ZShmYWxzZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZSh0cnVlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2Uuc3dpdGNoU3RhdGUoZmFsc2UpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGVDaGFuZ2VzKDMpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uQnVzaW5lc3NTd2l0Y2goYXJncykge1xyXG4gICAgbGV0IGNoZWNrU3RhdHVzID0gPFN3aXRjaD5hcmdzLm9iamVjdDtcclxuICAgIGlmIChjaGVja1N0YXR1cy5jaGVja2VkKSB7XHJcbiAgICAgIHRoaXMuYnVzaW5lc3NTd2l0Y2ggPSBcInN3aXRjaEVuYWJsZVwiO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5idXNpbmVzc1N3aXRjaCA9IFwic3dpdGNoRGlzYWJsZVwiO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uZUJheVByZW1pdW1Td2l0Y2goYXJncykge1xyXG4gICAgbGV0IGNoZWNrU3RhdHVzID0gPFN3aXRjaD5hcmdzLm9iamVjdDtcclxuICAgIGlmIChjaGVja1N0YXR1cy5jaGVja2VkKSB7XHJcbiAgICAgIHRoaXMuZUJheVByZW1pdW1Td2l0Y2ggPSBcInN3aXRjaEVuYWJsZVwiO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5lQmF5UHJlbWl1bVN3aXRjaCA9IFwic3dpdGNoRGlzYWJsZVwiO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uU3RydWN0dXJlZERhdGFTd2l0Y2goYXJncykge1xyXG4gICAgbGV0IGNoZWNrU3RhdHVzID0gPFN3aXRjaD5hcmdzLm9iamVjdDtcclxuICAgIGlmIChjaGVja1N0YXR1cy5jaGVja2VkKSB7XHJcbiAgICAgIHRoaXMuc3RydWN0dXJlZFN3aXRjaCA9IFwic3dpdGNoRW5hYmxlXCI7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnN0cnVjdHVyZWRTd2l0Y2ggPSBcInN3aXRjaERpc2FibGVcIjtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBvblN0b3JlQ2F0ZWdvcnlTd2l0Y2goYXJncykge1xyXG4gICAgbGV0IGNoZWNrU3RhdHVzID0gPFN3aXRjaD5hcmdzLm9iamVjdDtcclxuICAgIGlmIChjaGVja1N0YXR1cy5jaGVja2VkKSB7XHJcbiAgICAgIHRoaXMuc3RvcmVDYXRlZ29yeVN3aXRjaCA9IFwic3dpdGNoRW5hYmxlXCI7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnN0b3JlQ2F0ZWdvcnlTd2l0Y2ggPSBcInN3aXRjaERpc2FibGVcIjtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbk1ha2VPZmZlcnNTd2l0Y2goYXJncykge1xyXG4gICAgbGV0IGNoZWNrU3RhdHVzID0gPFN3aXRjaD5hcmdzLm9iamVjdDtcclxuICAgIGlmIChjaGVja1N0YXR1cy5jaGVja2VkKSB7XHJcbiAgICAgIHRoaXMubWFrZU9mZmVyc1N3aXRjaCA9IFwic3dpdGNoRW5hYmxlXCI7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLm1ha2VPZmZlcnNTd2l0Y2ggPSBcInN3aXRjaERpc2FibGVcIjtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkdsb2JhbFNoaXBwaW5nU3dpdGNoKGFyZ3MpIHtcclxuICAgIGxldCBjaGVja1N0YXR1cyA9IDxTd2l0Y2g+YXJncy5vYmplY3Q7XHJcbiAgICBpZiAoY2hlY2tTdGF0dXMuY2hlY2tlZCkge1xyXG4gICAgICB0aGlzLmdsb2JhbFNoaXBwaW5nU3dpdGNoID0gXCJzd2l0Y2hFbmFibGVcIjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuZ2xvYmFsU2hpcHBpbmdTd2l0Y2ggPSBcInN3aXRjaERpc2FibGVcIjtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge31cclxufVxyXG4iXX0=
import { Component } from "@angular/core";
import { BarcodeScanner } from "nativescript-barcodescanner";
import { RouterExtensions } from "nativescript-angular/router";
import { UserService } from "./services/user.service";
import { Switch } from "tns-core-modules/ui/switch";

@Component({
  selector: "ns-app",
  moduleId: module.id,
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  showActionBar: boolean;
  showDots: boolean;
  showTabs: boolean;
  showSwitch: boolean;

  dotImage1: string = "res://dotgrey";
  dotImage2: string = "res://dotgrey";
  dotImage3: string = "res://dotgrey";
  dotImage4: string = "res://dotgrey";
  dotImage5: string = "res://dotgrey";
  dotImage6: string = "res://dotgrey";
  dotImage7: string = "res://dotgrey";
  dotImage8: string = "res://dotgrey";
  dotImage9: string = "res://dotgrey";
  dotImage10: string = "res://dotgrey";
  dotImage11: string = "res://dotgrey";
  // dotImage12: string = "res://dotgrey";
  // dotImage13: string = "res://dotgrey";
  // dotImage14: string = "res://dotgrey";
  tabImage1: string = "res://homeblue";
  tabImage2: string = "res://personblue";
  tabImage3: string = "res://settingblue";
  colorTab1: string = "white";
  colorTab2: string = "white";
  colorTab3: String = "white";

  screenStatusLabel = "Disable *";
  screenStatusSwitch = "switchDisable";
  switchLabelColor = "black";

  constructor(
    private userService: UserService,
    private routerExtensions: RouterExtensions
  ) {
    this.userService.tabviewStateChanges.subscribe((tab: number) => {
      if (tab != undefined && tab != null && tab != -1) {
        if (tab == 1) {
          this.tabImage1 = "res://homewhite";
          this.tabImage2 = "res://personblue";
          this.tabImage3 = "res://settingblue";
          this.colorTab1 = "#2196f3";
          this.colorTab2 = "white";
          this.colorTab3 = "white";
        }
        if (tab == 2) {
          this.tabImage1 = "res://homeblue";
          this.tabImage2 = "res://personwhite";
          this.tabImage3 = "res://settingblue";
          this.colorTab1 = "white";
          this.colorTab2 = "#2196f3";
          this.colorTab3 = "white";
        }
        if (tab == 3) {
          this.tabImage1 = "res://homeblue";
          this.tabImage2 = "res://personblue";
          this.tabImage3 = "res://settingwhite";
          this.colorTab1 = "white";
          this.colorTab2 = "white";
          this.colorTab3 = "#2196f3";
        }
      }
    });
    this.userService.bottomBarDotsChanges.subscribe((dot: number) => {
      if (dot != undefined && dot != null && dot != -1) {
        if (dot == 1) {
          this.dotImage1 = "res://dotblue";
          this.dotImage2 = "res://dotgrey";
          this.dotImage3 = "res://dotgrey";
          this.dotImage4 = "res://dotgrey";
          this.dotImage5 = "res://dotgrey";
          this.dotImage6 = "res://dotgrey";
          this.dotImage7 = "res://dotgrey";
          this.dotImage8 = "res://dotgrey";
          this.dotImage9 = "res://dotgrey";
          this.dotImage10 = "res://dotgrey";
          this.dotImage11 = "res://dotgrey";
          // this.dotImage12 = "res://dotgrey";
          // this.dotImage13 = "res://dotgrey";
          // this.dotImage14 = "res://dotgrey";
        }
        if (dot == 2) {
          this.dotImage1 = "res://dotgreen";
          this.dotImage2 = "res://dotblue";
          this.dotImage3 = "res://dotgrey";
          this.dotImage4 = "res://dotgrey";
          this.dotImage5 = "res://dotgrey";
          this.dotImage6 = "res://dotgrey";
          this.dotImage7 = "res://dotgrey";
          this.dotImage8 = "res://dotgrey";
          this.dotImage9 = "res://dotgrey";
          this.dotImage10 = "res://dotgrey";
          this.dotImage11 = "res://dotgrey";
          // this.dotImage12 = "res://dotgrey";
          // this.dotImage13 = "res://dotgrey";
          // this.dotImage14 = "res://dotgrey";
        }
        if (dot == 3) {
          this.dotImage1 = "res://dotgreen";
          this.dotImage2 = "res://dotgreen";
          this.dotImage3 = "res://dotblue";
          this.dotImage4 = "res://dotgrey";
          this.dotImage5 = "res://dotgrey";
          this.dotImage6 = "res://dotgrey";
          this.dotImage7 = "res://dotgrey";
          this.dotImage8 = "res://dotgrey";
          this.dotImage9 = "res://dotgrey";
          this.dotImage10 = "res://dotgrey";
          this.dotImage11 = "res://dotgrey";
          // this.dotImage12 = "res://dotgrey";
          // this.dotImage13 = "res://dotgrey";
          // this.dotImage14 = "res://dotgrey";
        }
        if (dot == 4) {
          this.dotImage1 = "res://dotgreen";
          this.dotImage2 = "res://dotgreen";
          this.dotImage3 = "res://dotgreen";
          this.dotImage4 = "res://dotblue";
          this.dotImage5 = "res://dotgrey";
          this.dotImage6 = "res://dotgrey";
          this.dotImage7 = "res://dotgrey";
          this.dotImage8 = "res://dotgrey";
          this.dotImage9 = "res://dotgrey";
          this.dotImage10 = "res://dotgrey";
          this.dotImage11 = "res://dotgrey";
          // this.dotImage12 = "res://dotgrey";
          // this.dotImage13 = "res://dotgrey";
          // this.dotImage14 = "res://dotgrey";
        }
        if (dot == 5) {
          this.dotImage1 = "res://dotgreen";
          this.dotImage2 = "res://dotgreen";
          this.dotImage3 = "res://dotgreen";
          this.dotImage4 = "res://dotgreen";
          this.dotImage5 = "res://dotblue";
          this.dotImage6 = "res://dotgrey";
          this.dotImage7 = "res://dotgrey";
          this.dotImage8 = "res://dotgrey";
          this.dotImage9 = "res://dotgrey";
          this.dotImage10 = "res://dotgrey";
          this.dotImage11 = "res://dotgrey";
          // this.dotImage12 = "res://dotgrey";
          // this.dotImage13 = "res://dotgrey";
          // this.dotImage14 = "res://dotgrey";
        }
        if (dot == 6) {
          this.dotImage1 = "res://dotgreen";
          this.dotImage2 = "res://dotgreen";
          this.dotImage3 = "res://dotgreen";
          this.dotImage4 = "res://dotgreen";
          this.dotImage5 = "res://dotgreen";
          this.dotImage6 = "res://dotblue";
          this.dotImage7 = "res://dotgrey";
          this.dotImage8 = "res://dotgrey";
          this.dotImage9 = "res://dotgrey";
          this.dotImage10 = "res://dotgrey";
          this.dotImage11 = "res://dotgrey";
          // this.dotImage12 = "res://dotgrey";
          // this.dotImage13 = "res://dotgrey";
          // this.dotImage14 = "res://dotgrey";
        }
        if (dot == 7) {
          this.dotImage1 = "res://dotgreen";
          this.dotImage2 = "res://dotgreen";
          this.dotImage3 = "res://dotgreen";
          this.dotImage4 = "res://dotgreen";
          this.dotImage5 = "res://dotgreen";
          this.dotImage6 = "res://dotgreen";
          this.dotImage7 = "res://dotblue";
          this.dotImage8 = "res://dotgrey";
          this.dotImage9 = "res://dotgrey";
          this.dotImage10 = "res://dotgrey";
          this.dotImage11 = "res://dotgrey";
          // this.dotImage12 = "res://dotgrey";
          // this.dotImage13 = "res://dotgrey";
          // this.dotImage14 = "res://dotgrey";
        }
        if (dot == 8) {
          this.dotImage1 = "res://dotgreen";
          this.dotImage2 = "res://dotgreen";
          this.dotImage3 = "res://dotgreen";
          this.dotImage4 = "res://dotgreen";
          this.dotImage5 = "res://dotgreen";
          this.dotImage6 = "res://dotgreen";
          this.dotImage7 = "res://dotgreen";
          this.dotImage8 = "res://dotblue";
          this.dotImage9 = "res://dotgrey";
          this.dotImage10 = "res://dotgrey";
          this.dotImage11 = "res://dotgrey";
          // this.dotImage12 = "res://dotgrey";
          // this.dotImage13 = "res://dotgrey";
          // this.dotImage14 = "res://dotgrey";
        }
        if (dot == 9) {
          this.dotImage1 = "res://dotgreen";
          this.dotImage2 = "res://dotgreen";
          this.dotImage3 = "res://dotgreen";
          this.dotImage4 = "res://dotgreen";
          this.dotImage5 = "res://dotgreen";
          this.dotImage6 = "res://dotgreen";
          this.dotImage7 = "res://dotgreen";
          this.dotImage8 = "res://dotgreen";
          this.dotImage9 = "res://dotblue";
          this.dotImage10 = "res://dotgrey";
          this.dotImage11 = "res://dotgrey";
          // this.dotImage12 = "res://dotgrey";
          // this.dotImage13 = "res://dotgrey";
          // this.dotImage14 = "res://dotgrey";
        }
        if (dot == 10) {
          this.dotImage1 = "res://dotgreen";
          this.dotImage2 = "res://dotgreen";
          this.dotImage3 = "res://dotgreen";
          this.dotImage4 = "res://dotgreen";
          this.dotImage5 = "res://dotgreen";
          this.dotImage6 = "res://dotgreen";
          this.dotImage7 = "res://dotgreen";
          this.dotImage8 = "res://dotgreen";
          this.dotImage9 = "res://dotgreen";
          this.dotImage10 = "res://dotblue";
          this.dotImage11 = "res://dotgrey";
          // this.dotImage12 = "res://dotgrey";
          // this.dotImage13 = "res://dotgrey";
          // this.dotImage14 = "res://dotgrey";
        }
        if (dot == 11) {
          this.dotImage1 = "res://dotgreen";
          this.dotImage2 = "res://dotgreen";
          this.dotImage3 = "res://dotgreen";
          this.dotImage4 = "res://dotgreen";
          this.dotImage5 = "res://dotgreen";
          this.dotImage6 = "res://dotgreen";
          this.dotImage7 = "res://dotgreen";
          this.dotImage8 = "res://dotgreen";
          this.dotImage9 = "res://dotgreen";
          this.dotImage10 = "res://dotgreen";
          this.dotImage11 = "res://dotblue";
          // this.dotImage12 = "res://dotgrey";
          // this.dotImage13 = "res://dotgrey";
          // this.dotImage14 = "res://dotgrey";
        }
        // if (dot == 12) {
        //   this.dotImage1 = "res://dotgreen";
        //   this.dotImage2 = "res://dotgreen";
        //   this.dotImage3 = "res://dotgreen";
        //   this.dotImage4 = "res://dotgreen";
        //   this.dotImage5 = "res://dotgreen";
        //   this.dotImage6 = "res://dotgreen";
        //   this.dotImage7 = "res://dotgreen";
        //   this.dotImage8 = "res://dotgreen";
        //   this.dotImage9 = "res://dotgreen";
        //   this.dotImage10 = "res://dotgreen";
        //   this.dotImage11 = "res://dotgreen";
        //   this.dotImage12 = "res://dotblue";
        //   this.dotImage13 = "res://dotgrey";
        //   this.dotImage14 = "res://dotgrey";
        // }
        // if (dot == 13) {
        //   this.dotImage1 = "res://dotgreen";
        //   this.dotImage2 = "res://dotgreen";
        //   this.dotImage3 = "res://dotgreen";
        //   this.dotImage4 = "res://dotgreen";
        //   this.dotImage5 = "res://dotgreen";
        //   this.dotImage6 = "res://dotgreen";
        //   this.dotImage7 = "res://dotgreen";
        //   this.dotImage8 = "res://dotgreen";
        //   this.dotImage9 = "res://dotgreen";
        //   this.dotImage10 = "res://dotgreen";
        //   this.dotImage11 = "res://dotgreen";
        //   this.dotImage12 = "res://dotgreen";
        //   this.dotImage13 = "res://dotblue";
        //   this.dotImage14 = "res://dotgrey";
        // }
        // if (dot == 14) {
        //   this.dotImage1 = "res://dotgreen";
        //   this.dotImage2 = "res://dotgreen";
        //   this.dotImage3 = "res://dotgreen";
        //   this.dotImage4 = "res://dotgreen";
        //   this.dotImage5 = "res://dotgreen";
        //   this.dotImage6 = "res://dotgreen";
        //   this.dotImage7 = "res://dotgreen";
        //   this.dotImage8 = "res://dotgreen";
        //   this.dotImage9 = "res://dotgreen";
        //   this.dotImage10 = "res://dotgreen";
        //   this.dotImage11 = "res://dotgreen";
        //   this.dotImage12 = "res://dotgreen";
        //   this.dotImage13 = "res://dotgreen";
        //   this.dotImage14 = "res://dotblue";
        // }
      }
    });
    this.userService.actionBarChanges.subscribe((state: boolean) => {
      if (state != undefined) {
        this.showActionBar = state;
      }
    });
    this.userService.bottomBarDotsState.subscribe((state: boolean) => {
      if (state != undefined) {
        this.showDots = state;
      }
    });
    this.userService.tabviewState.subscribe((state: boolean) => {
      if (state != undefined) {
        this.showTabs = state;
      }
    });
    this.userService.switchstate.subscribe((state: boolean) => {
      if (state != undefined) {
        this.showSwitch = state;
      }
    });
  }

  public onSwitch(args) {
    let checkStatus = <Switch>args.object;
    if (checkStatus.checked) {
      this.screenStatusSwitch = "switchEnable";
      this.screenStatusLabel = "Disable *";
      this.switchLabelColor = "black";
    } else {
      this.screenStatusSwitch = "switchDisable";
      this.screenStatusLabel = "Enable";
      this.switchLabelColor = "#A9A9A9";
    }
  }

  public tabClick1() {
    this.routerExtensions.navigate(["/createListing"]);
  }
  public tabClick2() {
    this.routerExtensions.navigate(["/userAccount"]);
  }
  public tabClick3() {
    this.routerExtensions.navigate(["/preferencesDefaults"]);
  }
  public dotClick1() {
    this.routerExtensions.navigate(["/sku"]);
  }
  public dotClick2() {
    this.routerExtensions.navigate(["/upc"]);
  }
  public dotClick3() {
    this.routerExtensions.navigate(["/title"]);
  }
  public dotClick4() {
    this.routerExtensions.navigate(["/suggest"]);
  }
  public dotClick5() {
    this.routerExtensions.navigate(["/findCategory"]);
  }
  public dotClick6() {
    this.routerExtensions.navigate(["/storeCategory"]);
  }
  public dotClick7() {
    this.routerExtensions.navigate(["/condition"]);
  }
  public dotClick8() {
    this.routerExtensions.navigate(["/description"]);
  }
  public dotClick9() {
    this.routerExtensions.navigate(["/itemspecifies"]);
  }
  public dotClick10() {
    this.routerExtensions.navigate(["/businesspolicies"]);
  }
  public dotClick11() {
    this.routerExtensions.navigate(["/weightDimension"]);
  }
  // public dotClick12() {
  //   this.routerExtensions.navigate(["/returnOptions"]);
  // }
  // public dotClick13() {
  //   this.routerExtensions.navigate(["/businesspolicies"]);
  // }
  // public dotClick14() {
  //   this.routerExtensions.navigate(["/weightDimension"]);
  // }
}

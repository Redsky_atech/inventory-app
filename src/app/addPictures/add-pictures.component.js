"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var router_2 = require("@angular/router");
var imagepicker = require("nativescript-imagepicker");
var user_service_1 = require("../services/user.service");
var AddPicturesComponent = /** @class */ (function () {
    function AddPicturesComponent(routerExtensions, activatedRoute, userService) {
        this.activatedRoute = activatedRoute;
        this.userService = userService;
        this.myImage1 = "res://addphoto";
        this.myImage2 = "res://addphoto";
        this.myImage3 = "res://addphoto";
        this.myImage4 = "res://addphoto";
        this.myImage5 = "res://addphoto";
        this.myImage6 = "res://addphoto";
        this.myImage7 = "res://addphoto";
        this.myImage8 = "res://addphoto";
        this.myImage9 = "res://addphoto";
        this.myImage10 = "res://addphoto";
        this.myImage11 = "res://addphoto";
        this.myImage12 = "res://addphoto";
        this.deleteImage1 = "res://deleteicon";
        this.deleteImage2 = "res://deleteicon";
        this.deleteImage3 = "res://deleteicon";
        this.deleteImage4 = "res://deleteicon";
        this.deleteImage5 = "res://deleteicon";
        this.deleteImage6 = "res://deleteicon";
        this.deleteImage7 = "res://deleteicon";
        this.deleteImage8 = "res://deleteicon";
        this.deleteImage9 = "res://deleteicon";
        this.deleteImage10 = "res://deleteicon";
        this.deleteImage11 = "res://deleteicon";
        this.deleteImage12 = "res://deleteicon";
        this.stretch1 = "aspectFit";
        this.stretch2 = "aspectFit";
        this.stretch3 = "aspectFit";
        this.stretch4 = "aspectFit";
        this.stretch5 = "aspectFit";
        this.stretch6 = "aspectFit";
        this.stretch7 = "aspectFit";
        this.stretch8 = "aspectFit";
        this.stretch9 = "aspectFit";
        this.stretch10 = "aspectFit";
        this.stretch11 = "aspectFit";
        this.stretch12 = "aspectFit";
        this.showDelete1 = false;
        this.showDelete2 = false;
        this.showDelete3 = false;
        this.showDelete4 = false;
        this.showDelete5 = false;
        this.showDelete6 = false;
        this.showDelete7 = false;
        this.showDelete8 = false;
        this.showDelete9 = false;
        this.showDelete10 = false;
        this.showDelete11 = false;
        this.showDelete12 = false;
        this.routerExtensions = routerExtensions;
        this.userService.actionBarState(true);
        this.userService.bottomBarDotState(false);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
    }
    AddPicturesComponent.prototype.ngOnInit = function () { };
    AddPicturesComponent.prototype.onProceed = function () {
        this.routerExtensions.navigate(["/submitListings"], {
            animated: true,
            transition: {
                name: "slide",
                duration: 200,
                curve: "ease"
            }
        });
    };
    AddPicturesComponent.prototype.photoUpload1 = function () {
        // let mediafilepicker = new Mediafilepicker();
        // mediafilepicker.openImagePicker(options);
        // mediafilepicker.on("getFiles", function(res) {
        //   let results = res.object.get("results");
        //   console.log(results);
        //   });
        // });
        // mediafilepicker.on("error", function(res) {
        //   let msg = res.object.get("msg");
        //   console.log(msg);
        // });
        // mediafilepicker.on("cancel", function(res) {
        //   let msg = res.object.get("msg");
        //   console.log(msg);
        // });
        this.photoupload(1);
    };
    AddPicturesComponent.prototype.photoUpload2 = function () {
        this.photoupload(2);
    };
    AddPicturesComponent.prototype.photoUpload3 = function () {
        this.photoupload(3);
    };
    AddPicturesComponent.prototype.photoUpload4 = function () {
        this.photoupload(4);
    };
    AddPicturesComponent.prototype.photoUpload5 = function () {
        this.photoupload(5);
    };
    AddPicturesComponent.prototype.photoUpload6 = function () {
        this.photoupload(6);
    };
    AddPicturesComponent.prototype.photoUpload7 = function () {
        this.photoupload(7);
    };
    AddPicturesComponent.prototype.photoUpload8 = function () {
        this.photoupload(8);
    };
    AddPicturesComponent.prototype.photoUpload9 = function () {
        this.photoupload(9);
    };
    AddPicturesComponent.prototype.photoUpload10 = function () {
        this.photoupload(10);
    };
    AddPicturesComponent.prototype.photoUpload11 = function () {
        this.photoupload(11);
    };
    AddPicturesComponent.prototype.photoUpload12 = function () {
        this.photoupload(12);
    };
    AddPicturesComponent.prototype.photoupload = function (image) {
        var that = this;
        var context = imagepicker.create({
            mode: "single"
        });
        context
            .authorize()
            .then(function () {
            return context.present();
        })
            .then(function (selection) {
            selection.forEach(function (selected) {
                var path = selected.android.toString();
                if (image == 1) {
                    that.myImage1 = path;
                    that.stretch1 = "aspectFill";
                    that.showDelete1 = true;
                }
                if (image == 2) {
                    that.myImage2 = path;
                    that.stretch2 = "aspectFill";
                    that.showDelete2 = true;
                }
                if (image == 3) {
                    that.myImage3 = path;
                    that.stretch3 = "aspectFill";
                    that.showDelete3 = true;
                }
                if (image == 4) {
                    that.myImage4 = path;
                    that.stretch4 = "aspectFill";
                    that.showDelete4 = true;
                }
                if (image == 5) {
                    that.myImage5 = path;
                    that.stretch5 = "aspectFill";
                    that.showDelete5 = true;
                }
                if (image == 6) {
                    that.myImage6 = path;
                    that.stretch6 = "aspectFill";
                    that.showDelete6 = true;
                }
                if (image == 7) {
                    that.myImage7 = path;
                    that.stretch7 = "aspectFill";
                    that.showDelete7 = true;
                }
                if (image == 8) {
                    that.myImage8 = path;
                    that.stretch8 = "aspectFill";
                    that.showDelete8 = true;
                }
                if (image == 9) {
                    that.myImage9 = path;
                    that.stretch9 = "aspectFill";
                    that.showDelete9 = true;
                }
                if (image == 10) {
                    that.myImage10 = path;
                    that.stretch10 = "aspectFill";
                    that.showDelete10 = true;
                }
                if (image == 11) {
                    that.myImage11 = path;
                    that.stretch11 = "aspectFill";
                    that.showDelete11 = true;
                }
                if (image == 12) {
                    that.myImage12 = path;
                    that.stretch12 = "aspectFill";
                    that.showDelete12 = true;
                }
            });
        });
    };
    AddPicturesComponent.prototype.photoDelete1 = function () {
        this.photoDelete(1);
    };
    AddPicturesComponent.prototype.photoDelete2 = function () {
        this.photoDelete(2);
    };
    AddPicturesComponent.prototype.photoDelete3 = function () {
        this.photoDelete(3);
    };
    AddPicturesComponent.prototype.photoDelete4 = function () {
        this.photoDelete(4);
    };
    AddPicturesComponent.prototype.photoDelete5 = function () {
        this.photoDelete(5);
    };
    AddPicturesComponent.prototype.photoDelete6 = function () {
        this.photoDelete(6);
    };
    AddPicturesComponent.prototype.photoDelete7 = function () {
        this.photoDelete(7);
    };
    AddPicturesComponent.prototype.photoDelete8 = function () {
        this.photoDelete(8);
    };
    AddPicturesComponent.prototype.photoDelete9 = function () {
        this.photoDelete(9);
    };
    AddPicturesComponent.prototype.photoDelete10 = function () {
        this.photoDelete(10);
    };
    AddPicturesComponent.prototype.photoDelete11 = function () {
        this.photoDelete(11);
    };
    AddPicturesComponent.prototype.photoDelete12 = function () {
        this.photoDelete(12);
    };
    AddPicturesComponent.prototype.photoDelete = function (image) {
        var that = this;
        if (image == 1) {
            that.myImage1 = "res://addphoto";
            that.stretch1 = "aspectFit";
            that.showDelete1 = false;
        }
        if (image == 2) {
            that.myImage2 = "res://addphoto";
            that.stretch2 = "aspectFit";
            that.showDelete2 = false;
        }
        if (image == 3) {
            that.myImage3 = "res://addphoto";
            that.stretch3 = "aspectFit";
            that.showDelete3 = false;
        }
        if (image == 4) {
            that.myImage4 = "res://addphoto";
            that.stretch4 = "aspectFit";
            that.showDelete4 = false;
        }
        if (image == 5) {
            that.myImage5 = "res://addphoto";
            that.stretch5 = "aspectFit";
            that.showDelete5 = false;
        }
        if (image == 6) {
            that.myImage6 = "res://addphoto";
            that.stretch6 = "aspectFit";
            that.showDelete6 = false;
        }
        if (image == 7) {
            that.myImage7 = "res://addphoto";
            that.stretch7 = "aspectFit";
            that.showDelete7 = false;
        }
        if (image == 8) {
            that.myImage8 = "res://addphoto";
            that.stretch8 = "aspectFit";
            that.showDelete8 = false;
        }
        if (image == 9) {
            that.myImage9 = "res://addphoto";
            that.stretch9 = "aspectFit";
            that.showDelete9 = false;
        }
        if (image == 10) {
            that.myImage10 = "res://addphoto";
            that.stretch10 = "aspectFit";
            that.showDelete10 = false;
        }
        if (image == 11) {
            that.myImage11 = "res://addphoto";
            that.stretch11 = "aspectFit";
            that.showDelete11 = false;
        }
        if (image == 12) {
            that.myImage12 = "res://addphoto";
            that.stretch12 = "aspectFit";
            that.showDelete12 = false;
        }
    };
    AddPicturesComponent.prototype.onDeleteAll = function () {
        var that = this;
        that.myImage1 = "res://addphoto";
        that.myImage2 = "res://addphoto";
        that.myImage3 = "res://addphoto";
        that.myImage4 = "res://addphoto";
        that.myImage5 = "res://addphoto";
        that.myImage6 = "res://addphoto";
        that.myImage7 = "res://addphoto";
        that.myImage8 = "res://addphoto";
        that.myImage9 = "res://addphoto";
        that.myImage10 = "res://addphoto";
        that.myImage11 = "res://addphoto";
        that.myImage12 = "res://addphoto";
        that.stretch1 = "aspectFit";
        that.stretch2 = "aspectFit";
        that.stretch3 = "aspectFit";
        that.stretch4 = "aspectFit";
        that.stretch5 = "aspectFit";
        that.stretch6 = "aspectFit";
        that.stretch7 = "aspectFit";
        that.stretch8 = "aspectFit";
        that.stretch9 = "aspectFit";
        that.stretch10 = "aspectFit";
        that.stretch11 = "aspectFit";
        that.stretch12 = "aspectFit";
        that.showDelete1 = false;
        that.showDelete2 = false;
        that.showDelete3 = false;
        that.showDelete4 = false;
        that.showDelete5 = false;
        that.showDelete6 = false;
        that.showDelete7 = false;
        that.showDelete8 = false;
        that.showDelete9 = false;
        that.showDelete10 = false;
        that.showDelete11 = false;
        that.showDelete12 = false;
    };
    AddPicturesComponent = __decorate([
        core_1.Component({
            selector: "ns-addPictures",
            moduleId: module.id,
            templateUrl: "./add-pictures.component.html",
            styleUrls: ["./add-pictures.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            router_2.ActivatedRoute,
            user_service_1.UserService])
    ], AddPicturesComponent);
    return AddPicturesComponent;
}());
exports.AddPicturesComponent = AddPicturesComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkLXBpY3R1cmVzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFkZC1waWN0dXJlcy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFFbEQsc0RBQStEO0FBQy9ELDBDQUFpRDtBQUlqRCxzREFBd0Q7QUFDeEQseURBQXVEO0FBYXZEO0lBbURFLDhCQUNFLGdCQUFrQyxFQUMxQixjQUE4QixFQUM5QixXQUF3QjtRQUR4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFwRDNCLGFBQVEsR0FBRyxnQkFBZ0IsQ0FBQztRQUM1QixhQUFRLEdBQUcsZ0JBQWdCLENBQUM7UUFDNUIsYUFBUSxHQUFHLGdCQUFnQixDQUFDO1FBQzVCLGFBQVEsR0FBRyxnQkFBZ0IsQ0FBQztRQUM1QixhQUFRLEdBQUcsZ0JBQWdCLENBQUM7UUFDNUIsYUFBUSxHQUFHLGdCQUFnQixDQUFDO1FBQzVCLGFBQVEsR0FBRyxnQkFBZ0IsQ0FBQztRQUM1QixhQUFRLEdBQUcsZ0JBQWdCLENBQUM7UUFDNUIsYUFBUSxHQUFHLGdCQUFnQixDQUFDO1FBQzVCLGNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztRQUM3QixjQUFTLEdBQUcsZ0JBQWdCLENBQUM7UUFDN0IsY0FBUyxHQUFHLGdCQUFnQixDQUFDO1FBQzdCLGlCQUFZLEdBQUcsa0JBQWtCLENBQUM7UUFDbEMsaUJBQVksR0FBRyxrQkFBa0IsQ0FBQztRQUNsQyxpQkFBWSxHQUFHLGtCQUFrQixDQUFDO1FBQ2xDLGlCQUFZLEdBQUcsa0JBQWtCLENBQUM7UUFDbEMsaUJBQVksR0FBRyxrQkFBa0IsQ0FBQztRQUNsQyxpQkFBWSxHQUFHLGtCQUFrQixDQUFDO1FBQ2xDLGlCQUFZLEdBQUcsa0JBQWtCLENBQUM7UUFDbEMsaUJBQVksR0FBRyxrQkFBa0IsQ0FBQztRQUNsQyxpQkFBWSxHQUFHLGtCQUFrQixDQUFDO1FBQ2xDLGtCQUFhLEdBQUcsa0JBQWtCLENBQUM7UUFDbkMsa0JBQWEsR0FBRyxrQkFBa0IsQ0FBQztRQUNuQyxrQkFBYSxHQUFHLGtCQUFrQixDQUFDO1FBQ25DLGFBQVEsR0FBRyxXQUFXLENBQUM7UUFDdkIsYUFBUSxHQUFHLFdBQVcsQ0FBQztRQUN2QixhQUFRLEdBQUcsV0FBVyxDQUFDO1FBQ3ZCLGFBQVEsR0FBRyxXQUFXLENBQUM7UUFDdkIsYUFBUSxHQUFHLFdBQVcsQ0FBQztRQUN2QixhQUFRLEdBQUcsV0FBVyxDQUFDO1FBQ3ZCLGFBQVEsR0FBRyxXQUFXLENBQUM7UUFDdkIsYUFBUSxHQUFHLFdBQVcsQ0FBQztRQUN2QixhQUFRLEdBQUcsV0FBVyxDQUFDO1FBQ3ZCLGNBQVMsR0FBRyxXQUFXLENBQUM7UUFDeEIsY0FBUyxHQUFHLFdBQVcsQ0FBQztRQUN4QixjQUFTLEdBQUcsV0FBVyxDQUFDO1FBQy9CLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBQzlCLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBQzlCLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBTzVCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVELHVDQUFRLEdBQVIsY0FBa0IsQ0FBQztJQUVaLHdDQUFTLEdBQWhCO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLEVBQUU7WUFDbEQsUUFBUSxFQUFFLElBQUk7WUFDZCxVQUFVLEVBQUU7Z0JBQ1YsSUFBSSxFQUFFLE9BQU87Z0JBQ2IsUUFBUSxFQUFFLEdBQUc7Z0JBQ2IsS0FBSyxFQUFFLE1BQU07YUFDZDtTQUNGLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSwyQ0FBWSxHQUFuQjtRQUNFLCtDQUErQztRQUMvQyw0Q0FBNEM7UUFDNUMsaURBQWlEO1FBQ2pELDZDQUE2QztRQUM3QywwQkFBMEI7UUFDMUIsUUFBUTtRQUNSLE1BQU07UUFDTiw4Q0FBOEM7UUFDOUMscUNBQXFDO1FBQ3JDLHNCQUFzQjtRQUN0QixNQUFNO1FBQ04sK0NBQStDO1FBQy9DLHFDQUFxQztRQUNyQyxzQkFBc0I7UUFDdEIsTUFBTTtRQUNOLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUNNLDJDQUFZLEdBQW5CO1FBQ0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN0QixDQUFDO0lBQ00sMkNBQVksR0FBbkI7UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3RCLENBQUM7SUFDTSwyQ0FBWSxHQUFuQjtRQUNFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUNNLDJDQUFZLEdBQW5CO1FBQ0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN0QixDQUFDO0lBQ00sMkNBQVksR0FBbkI7UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3RCLENBQUM7SUFDTSwyQ0FBWSxHQUFuQjtRQUNFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUNNLDJDQUFZLEdBQW5CO1FBQ0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN0QixDQUFDO0lBQ00sMkNBQVksR0FBbkI7UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3RCLENBQUM7SUFDTSw0Q0FBYSxHQUFwQjtRQUNFLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDdkIsQ0FBQztJQUNNLDRDQUFhLEdBQXBCO1FBQ0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUN2QixDQUFDO0lBQ00sNENBQWEsR0FBcEI7UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFDRCwwQ0FBVyxHQUFYLFVBQVksS0FBSztRQUNmLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLE9BQU8sR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDO1lBQy9CLElBQUksRUFBRSxRQUFRO1NBQ2YsQ0FBQyxDQUFDO1FBQ0gsT0FBTzthQUNKLFNBQVMsRUFBRTthQUNYLElBQUksQ0FBQztZQUNKLE9BQU8sT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzNCLENBQUMsQ0FBQzthQUNELElBQUksQ0FBQyxVQUFBLFNBQVM7WUFDYixTQUFTLENBQUMsT0FBTyxDQUFDLFVBQVMsUUFBUTtnQkFDakMsSUFBSSxJQUFJLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDdkMsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFO29CQUNkLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO29CQUNyQixJQUFJLENBQUMsUUFBUSxHQUFHLFlBQVksQ0FBQztvQkFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7aUJBQ3pCO2dCQUNELElBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtvQkFDZCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztvQkFDckIsSUFBSSxDQUFDLFFBQVEsR0FBRyxZQUFZLENBQUM7b0JBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO2lCQUN6QjtnQkFDRCxJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7b0JBQ2QsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7b0JBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsWUFBWSxDQUFDO29CQUM3QixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztpQkFDekI7Z0JBQ0QsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFO29CQUNkLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO29CQUNyQixJQUFJLENBQUMsUUFBUSxHQUFHLFlBQVksQ0FBQztvQkFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7aUJBQ3pCO2dCQUNELElBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtvQkFDZCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztvQkFDckIsSUFBSSxDQUFDLFFBQVEsR0FBRyxZQUFZLENBQUM7b0JBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO2lCQUN6QjtnQkFDRCxJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7b0JBQ2QsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7b0JBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsWUFBWSxDQUFDO29CQUM3QixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztpQkFDekI7Z0JBQ0QsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFO29CQUNkLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO29CQUNyQixJQUFJLENBQUMsUUFBUSxHQUFHLFlBQVksQ0FBQztvQkFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7aUJBQ3pCO2dCQUNELElBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtvQkFDZCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztvQkFDckIsSUFBSSxDQUFDLFFBQVEsR0FBRyxZQUFZLENBQUM7b0JBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO2lCQUN6QjtnQkFDRCxJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7b0JBQ2QsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7b0JBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsWUFBWSxDQUFDO29CQUM3QixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztpQkFDekI7Z0JBQ0QsSUFBSSxLQUFLLElBQUksRUFBRSxFQUFFO29CQUNmLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO29CQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLFlBQVksQ0FBQztvQkFDOUIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7aUJBQzFCO2dCQUNELElBQUksS0FBSyxJQUFJLEVBQUUsRUFBRTtvQkFDZixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztvQkFDdEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxZQUFZLENBQUM7b0JBQzlCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2lCQUMxQjtnQkFDRCxJQUFJLEtBQUssSUFBSSxFQUFFLEVBQUU7b0JBQ2YsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7b0JBQ3RCLElBQUksQ0FBQyxTQUFTLEdBQUcsWUFBWSxDQUFDO29CQUM5QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztpQkFDMUI7WUFDSCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLDJDQUFZLEdBQW5CO1FBQ0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN0QixDQUFDO0lBQ00sMkNBQVksR0FBbkI7UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3RCLENBQUM7SUFDTSwyQ0FBWSxHQUFuQjtRQUNFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUNNLDJDQUFZLEdBQW5CO1FBQ0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN0QixDQUFDO0lBQ00sMkNBQVksR0FBbkI7UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3RCLENBQUM7SUFDTSwyQ0FBWSxHQUFuQjtRQUNFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUNNLDJDQUFZLEdBQW5CO1FBQ0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN0QixDQUFDO0lBQ00sMkNBQVksR0FBbkI7UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3RCLENBQUM7SUFDTSwyQ0FBWSxHQUFuQjtRQUNFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUNNLDRDQUFhLEdBQXBCO1FBQ0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUN2QixDQUFDO0lBQ00sNENBQWEsR0FBcEI7UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFDTSw0Q0FBYSxHQUFwQjtRQUNFLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDdkIsQ0FBQztJQUNELDBDQUFXLEdBQVgsVUFBWSxLQUFLO1FBQ2YsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtZQUNkLElBQUksQ0FBQyxRQUFRLEdBQUcsZ0JBQWdCLENBQUM7WUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxXQUFXLENBQUM7WUFDNUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7U0FDMUI7UUFDRCxJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7WUFDZCxJQUFJLENBQUMsUUFBUSxHQUFHLGdCQUFnQixDQUFDO1lBQ2pDLElBQUksQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFDO1lBQzVCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1NBQzFCO1FBQ0QsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFO1lBQ2QsSUFBSSxDQUFDLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQztZQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQztZQUM1QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztTQUMxQjtRQUNELElBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtZQUNkLElBQUksQ0FBQyxRQUFRLEdBQUcsZ0JBQWdCLENBQUM7WUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxXQUFXLENBQUM7WUFDNUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7U0FDMUI7UUFDRCxJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7WUFDZCxJQUFJLENBQUMsUUFBUSxHQUFHLGdCQUFnQixDQUFDO1lBQ2pDLElBQUksQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFDO1lBQzVCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1NBQzFCO1FBQ0QsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFO1lBQ2QsSUFBSSxDQUFDLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQztZQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQztZQUM1QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztTQUMxQjtRQUNELElBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtZQUNkLElBQUksQ0FBQyxRQUFRLEdBQUcsZ0JBQWdCLENBQUM7WUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxXQUFXLENBQUM7WUFDNUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7U0FDMUI7UUFDRCxJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7WUFDZCxJQUFJLENBQUMsUUFBUSxHQUFHLGdCQUFnQixDQUFDO1lBQ2pDLElBQUksQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFDO1lBQzVCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1NBQzFCO1FBQ0QsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFO1lBQ2QsSUFBSSxDQUFDLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQztZQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQztZQUM1QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztTQUMxQjtRQUNELElBQUksS0FBSyxJQUFJLEVBQUUsRUFBRTtZQUNmLElBQUksQ0FBQyxTQUFTLEdBQUcsZ0JBQWdCLENBQUM7WUFDbEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxXQUFXLENBQUM7WUFDN0IsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7U0FDM0I7UUFDRCxJQUFJLEtBQUssSUFBSSxFQUFFLEVBQUU7WUFDZixJQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO1lBQ2xDLElBQUksQ0FBQyxTQUFTLEdBQUcsV0FBVyxDQUFDO1lBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1NBQzNCO1FBQ0QsSUFBSSxLQUFLLElBQUksRUFBRSxFQUFFO1lBQ2YsSUFBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztZQUNsQyxJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsQ0FBQztZQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztTQUMzQjtJQUNILENBQUM7SUFFTSwwQ0FBVyxHQUFsQjtRQUNFLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLENBQUMsUUFBUSxHQUFHLGdCQUFnQixDQUFDO1FBQ2pDLElBQUksQ0FBQyxRQUFRLEdBQUcsZ0JBQWdCLENBQUM7UUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQztRQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLGdCQUFnQixDQUFDO1FBQ2pDLElBQUksQ0FBQyxRQUFRLEdBQUcsZ0JBQWdCLENBQUM7UUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQztRQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLGdCQUFnQixDQUFDO1FBQ2pDLElBQUksQ0FBQyxRQUFRLEdBQUcsZ0JBQWdCLENBQUM7UUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQztRQUNqQyxJQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO1FBQ2xDLElBQUksQ0FBQyxTQUFTLEdBQUcsZ0JBQWdCLENBQUM7UUFDbEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztRQUNsQyxJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQztRQUM1QixJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQztRQUM1QixJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQztRQUM1QixJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQztRQUM1QixJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQztRQUM1QixJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQztRQUM1QixJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQztRQUM1QixJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQztRQUM1QixJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQztRQUM1QixJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsQ0FBQztRQUM3QixJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsQ0FBQztRQUM3QixJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsQ0FBQztRQUM3QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztJQUM1QixDQUFDO0lBdFZVLG9CQUFvQjtRQU5oQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGdCQUFnQjtZQUMxQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLCtCQUErQjtZQUM1QyxTQUFTLEVBQUUsQ0FBQyw4QkFBOEIsQ0FBQztTQUM1QyxDQUFDO3lDQXFEb0IseUJBQWdCO1lBQ1YsdUJBQWM7WUFDakIsMEJBQVc7T0F0RHZCLG9CQUFvQixDQXVWaEM7SUFBRCwyQkFBQztDQUFBLEFBdlZELElBdVZDO0FBdlZZLG9EQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgU3dpcGVHZXN0dXJlRXZlbnREYXRhIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZ2VzdHVyZXNcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlL3BhZ2VcIjtcclxuaW1wb3J0IHsgSW1hZ2VTb3VyY2UsIGZyb21GaWxlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvaW1hZ2Utc291cmNlXCI7XHJcbmltcG9ydCB7IEltYWdlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvaW1hZ2VcIjtcclxuaW1wb3J0ICogYXMgaW1hZ2VwaWNrZXIgZnJvbSBcIm5hdGl2ZXNjcmlwdC1pbWFnZXBpY2tlclwiO1xyXG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcclxuXHJcbmltcG9ydCB7XHJcbiAgTWVkaWFmaWxlcGlja2VyLFxyXG4gIEltYWdlUGlja2VyT3B0aW9uc1xyXG59IGZyb20gXCJuYXRpdmVzY3JpcHQtbWVkaWFmaWxlcGlja2VyXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJucy1hZGRQaWN0dXJlc1wiLFxyXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9hZGQtcGljdHVyZXMuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vYWRkLXBpY3R1cmVzLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIEFkZFBpY3R1cmVzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zO1xyXG4gIHB1YmxpYyBteUltYWdlMSA9IFwicmVzOi8vYWRkcGhvdG9cIjtcclxuICBwdWJsaWMgbXlJbWFnZTIgPSBcInJlczovL2FkZHBob3RvXCI7XHJcbiAgcHVibGljIG15SW1hZ2UzID0gXCJyZXM6Ly9hZGRwaG90b1wiO1xyXG4gIHB1YmxpYyBteUltYWdlNCA9IFwicmVzOi8vYWRkcGhvdG9cIjtcclxuICBwdWJsaWMgbXlJbWFnZTUgPSBcInJlczovL2FkZHBob3RvXCI7XHJcbiAgcHVibGljIG15SW1hZ2U2ID0gXCJyZXM6Ly9hZGRwaG90b1wiO1xyXG4gIHB1YmxpYyBteUltYWdlNyA9IFwicmVzOi8vYWRkcGhvdG9cIjtcclxuICBwdWJsaWMgbXlJbWFnZTggPSBcInJlczovL2FkZHBob3RvXCI7XHJcbiAgcHVibGljIG15SW1hZ2U5ID0gXCJyZXM6Ly9hZGRwaG90b1wiO1xyXG4gIHB1YmxpYyBteUltYWdlMTAgPSBcInJlczovL2FkZHBob3RvXCI7XHJcbiAgcHVibGljIG15SW1hZ2UxMSA9IFwicmVzOi8vYWRkcGhvdG9cIjtcclxuICBwdWJsaWMgbXlJbWFnZTEyID0gXCJyZXM6Ly9hZGRwaG90b1wiO1xyXG4gIHB1YmxpYyBkZWxldGVJbWFnZTEgPSBcInJlczovL2RlbGV0ZWljb25cIjtcclxuICBwdWJsaWMgZGVsZXRlSW1hZ2UyID0gXCJyZXM6Ly9kZWxldGVpY29uXCI7XHJcbiAgcHVibGljIGRlbGV0ZUltYWdlMyA9IFwicmVzOi8vZGVsZXRlaWNvblwiO1xyXG4gIHB1YmxpYyBkZWxldGVJbWFnZTQgPSBcInJlczovL2RlbGV0ZWljb25cIjtcclxuICBwdWJsaWMgZGVsZXRlSW1hZ2U1ID0gXCJyZXM6Ly9kZWxldGVpY29uXCI7XHJcbiAgcHVibGljIGRlbGV0ZUltYWdlNiA9IFwicmVzOi8vZGVsZXRlaWNvblwiO1xyXG4gIHB1YmxpYyBkZWxldGVJbWFnZTcgPSBcInJlczovL2RlbGV0ZWljb25cIjtcclxuICBwdWJsaWMgZGVsZXRlSW1hZ2U4ID0gXCJyZXM6Ly9kZWxldGVpY29uXCI7XHJcbiAgcHVibGljIGRlbGV0ZUltYWdlOSA9IFwicmVzOi8vZGVsZXRlaWNvblwiO1xyXG4gIHB1YmxpYyBkZWxldGVJbWFnZTEwID0gXCJyZXM6Ly9kZWxldGVpY29uXCI7XHJcbiAgcHVibGljIGRlbGV0ZUltYWdlMTEgPSBcInJlczovL2RlbGV0ZWljb25cIjtcclxuICBwdWJsaWMgZGVsZXRlSW1hZ2UxMiA9IFwicmVzOi8vZGVsZXRlaWNvblwiO1xyXG4gIHB1YmxpYyBzdHJldGNoMSA9IFwiYXNwZWN0Rml0XCI7XHJcbiAgcHVibGljIHN0cmV0Y2gyID0gXCJhc3BlY3RGaXRcIjtcclxuICBwdWJsaWMgc3RyZXRjaDMgPSBcImFzcGVjdEZpdFwiO1xyXG4gIHB1YmxpYyBzdHJldGNoNCA9IFwiYXNwZWN0Rml0XCI7XHJcbiAgcHVibGljIHN0cmV0Y2g1ID0gXCJhc3BlY3RGaXRcIjtcclxuICBwdWJsaWMgc3RyZXRjaDYgPSBcImFzcGVjdEZpdFwiO1xyXG4gIHB1YmxpYyBzdHJldGNoNyA9IFwiYXNwZWN0Rml0XCI7XHJcbiAgcHVibGljIHN0cmV0Y2g4ID0gXCJhc3BlY3RGaXRcIjtcclxuICBwdWJsaWMgc3RyZXRjaDkgPSBcImFzcGVjdEZpdFwiO1xyXG4gIHB1YmxpYyBzdHJldGNoMTAgPSBcImFzcGVjdEZpdFwiO1xyXG4gIHB1YmxpYyBzdHJldGNoMTEgPSBcImFzcGVjdEZpdFwiO1xyXG4gIHB1YmxpYyBzdHJldGNoMTIgPSBcImFzcGVjdEZpdFwiO1xyXG4gIHNob3dEZWxldGUxOiBib29sZWFuID0gZmFsc2U7XHJcbiAgc2hvd0RlbGV0ZTI6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBzaG93RGVsZXRlMzogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHNob3dEZWxldGU0OiBib29sZWFuID0gZmFsc2U7XHJcbiAgc2hvd0RlbGV0ZTU6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBzaG93RGVsZXRlNjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHNob3dEZWxldGU3OiBib29sZWFuID0gZmFsc2U7XHJcbiAgc2hvd0RlbGV0ZTg6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBzaG93RGVsZXRlOTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHNob3dEZWxldGUxMDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHNob3dEZWxldGUxMTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHNob3dEZWxldGUxMjogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsXHJcbiAgICBwcml2YXRlIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMgPSByb3V0ZXJFeHRlbnNpb25zO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5hY3Rpb25CYXJTdGF0ZSh0cnVlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYm90dG9tQmFyRG90U3RhdGUoZmFsc2UpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZUNoYW5nZXMoMSk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHt9XHJcblxyXG4gIHB1YmxpYyBvblByb2NlZWQoKSB7XHJcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3N1Ym1pdExpc3RpbmdzXCJdLCB7XHJcbiAgICAgIGFuaW1hdGVkOiB0cnVlLFxyXG4gICAgICB0cmFuc2l0aW9uOiB7XHJcbiAgICAgICAgbmFtZTogXCJzbGlkZVwiLFxyXG4gICAgICAgIGR1cmF0aW9uOiAyMDAsXHJcbiAgICAgICAgY3VydmU6IFwiZWFzZVwiXHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHBob3RvVXBsb2FkMSgpIHtcclxuICAgIC8vIGxldCBtZWRpYWZpbGVwaWNrZXIgPSBuZXcgTWVkaWFmaWxlcGlja2VyKCk7XHJcbiAgICAvLyBtZWRpYWZpbGVwaWNrZXIub3BlbkltYWdlUGlja2VyKG9wdGlvbnMpO1xyXG4gICAgLy8gbWVkaWFmaWxlcGlja2VyLm9uKFwiZ2V0RmlsZXNcIiwgZnVuY3Rpb24ocmVzKSB7XHJcbiAgICAvLyAgIGxldCByZXN1bHRzID0gcmVzLm9iamVjdC5nZXQoXCJyZXN1bHRzXCIpO1xyXG4gICAgLy8gICBjb25zb2xlLmxvZyhyZXN1bHRzKTtcclxuICAgIC8vICAgfSk7XHJcbiAgICAvLyB9KTtcclxuICAgIC8vIG1lZGlhZmlsZXBpY2tlci5vbihcImVycm9yXCIsIGZ1bmN0aW9uKHJlcykge1xyXG4gICAgLy8gICBsZXQgbXNnID0gcmVzLm9iamVjdC5nZXQoXCJtc2dcIik7XHJcbiAgICAvLyAgIGNvbnNvbGUubG9nKG1zZyk7XHJcbiAgICAvLyB9KTtcclxuICAgIC8vIG1lZGlhZmlsZXBpY2tlci5vbihcImNhbmNlbFwiLCBmdW5jdGlvbihyZXMpIHtcclxuICAgIC8vICAgbGV0IG1zZyA9IHJlcy5vYmplY3QuZ2V0KFwibXNnXCIpO1xyXG4gICAgLy8gICBjb25zb2xlLmxvZyhtc2cpO1xyXG4gICAgLy8gfSk7XHJcbiAgICB0aGlzLnBob3RvdXBsb2FkKDEpO1xyXG4gIH1cclxuICBwdWJsaWMgcGhvdG9VcGxvYWQyKCkge1xyXG4gICAgdGhpcy5waG90b3VwbG9hZCgyKTtcclxuICB9XHJcbiAgcHVibGljIHBob3RvVXBsb2FkMygpIHtcclxuICAgIHRoaXMucGhvdG91cGxvYWQoMyk7XHJcbiAgfVxyXG4gIHB1YmxpYyBwaG90b1VwbG9hZDQoKSB7XHJcbiAgICB0aGlzLnBob3RvdXBsb2FkKDQpO1xyXG4gIH1cclxuICBwdWJsaWMgcGhvdG9VcGxvYWQ1KCkge1xyXG4gICAgdGhpcy5waG90b3VwbG9hZCg1KTtcclxuICB9XHJcbiAgcHVibGljIHBob3RvVXBsb2FkNigpIHtcclxuICAgIHRoaXMucGhvdG91cGxvYWQoNik7XHJcbiAgfVxyXG4gIHB1YmxpYyBwaG90b1VwbG9hZDcoKSB7XHJcbiAgICB0aGlzLnBob3RvdXBsb2FkKDcpO1xyXG4gIH1cclxuICBwdWJsaWMgcGhvdG9VcGxvYWQ4KCkge1xyXG4gICAgdGhpcy5waG90b3VwbG9hZCg4KTtcclxuICB9XHJcbiAgcHVibGljIHBob3RvVXBsb2FkOSgpIHtcclxuICAgIHRoaXMucGhvdG91cGxvYWQoOSk7XHJcbiAgfVxyXG4gIHB1YmxpYyBwaG90b1VwbG9hZDEwKCkge1xyXG4gICAgdGhpcy5waG90b3VwbG9hZCgxMCk7XHJcbiAgfVxyXG4gIHB1YmxpYyBwaG90b1VwbG9hZDExKCkge1xyXG4gICAgdGhpcy5waG90b3VwbG9hZCgxMSk7XHJcbiAgfVxyXG4gIHB1YmxpYyBwaG90b1VwbG9hZDEyKCkge1xyXG4gICAgdGhpcy5waG90b3VwbG9hZCgxMik7XHJcbiAgfVxyXG4gIHBob3RvdXBsb2FkKGltYWdlKSB7XHJcbiAgICB2YXIgdGhhdCA9IHRoaXM7XHJcbiAgICBsZXQgY29udGV4dCA9IGltYWdlcGlja2VyLmNyZWF0ZSh7XHJcbiAgICAgIG1vZGU6IFwic2luZ2xlXCJcclxuICAgIH0pO1xyXG4gICAgY29udGV4dFxyXG4gICAgICAuYXV0aG9yaXplKClcclxuICAgICAgLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgIHJldHVybiBjb250ZXh0LnByZXNlbnQoKTtcclxuICAgICAgfSlcclxuICAgICAgLnRoZW4oc2VsZWN0aW9uID0+IHtcclxuICAgICAgICBzZWxlY3Rpb24uZm9yRWFjaChmdW5jdGlvbihzZWxlY3RlZCkge1xyXG4gICAgICAgICAgdmFyIHBhdGggPSBzZWxlY3RlZC5hbmRyb2lkLnRvU3RyaW5nKCk7XHJcbiAgICAgICAgICBpZiAoaW1hZ2UgPT0gMSkge1xyXG4gICAgICAgICAgICB0aGF0Lm15SW1hZ2UxID0gcGF0aDtcclxuICAgICAgICAgICAgdGhhdC5zdHJldGNoMSA9IFwiYXNwZWN0RmlsbFwiO1xyXG4gICAgICAgICAgICB0aGF0LnNob3dEZWxldGUxID0gdHJ1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGlmIChpbWFnZSA9PSAyKSB7XHJcbiAgICAgICAgICAgIHRoYXQubXlJbWFnZTIgPSBwYXRoO1xyXG4gICAgICAgICAgICB0aGF0LnN0cmV0Y2gyID0gXCJhc3BlY3RGaWxsXCI7XHJcbiAgICAgICAgICAgIHRoYXQuc2hvd0RlbGV0ZTIgPSB0cnVlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYgKGltYWdlID09IDMpIHtcclxuICAgICAgICAgICAgdGhhdC5teUltYWdlMyA9IHBhdGg7XHJcbiAgICAgICAgICAgIHRoYXQuc3RyZXRjaDMgPSBcImFzcGVjdEZpbGxcIjtcclxuICAgICAgICAgICAgdGhhdC5zaG93RGVsZXRlMyA9IHRydWU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpZiAoaW1hZ2UgPT0gNCkge1xyXG4gICAgICAgICAgICB0aGF0Lm15SW1hZ2U0ID0gcGF0aDtcclxuICAgICAgICAgICAgdGhhdC5zdHJldGNoNCA9IFwiYXNwZWN0RmlsbFwiO1xyXG4gICAgICAgICAgICB0aGF0LnNob3dEZWxldGU0ID0gdHJ1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGlmIChpbWFnZSA9PSA1KSB7XHJcbiAgICAgICAgICAgIHRoYXQubXlJbWFnZTUgPSBwYXRoO1xyXG4gICAgICAgICAgICB0aGF0LnN0cmV0Y2g1ID0gXCJhc3BlY3RGaWxsXCI7XHJcbiAgICAgICAgICAgIHRoYXQuc2hvd0RlbGV0ZTUgPSB0cnVlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYgKGltYWdlID09IDYpIHtcclxuICAgICAgICAgICAgdGhhdC5teUltYWdlNiA9IHBhdGg7XHJcbiAgICAgICAgICAgIHRoYXQuc3RyZXRjaDYgPSBcImFzcGVjdEZpbGxcIjtcclxuICAgICAgICAgICAgdGhhdC5zaG93RGVsZXRlNiA9IHRydWU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpZiAoaW1hZ2UgPT0gNykge1xyXG4gICAgICAgICAgICB0aGF0Lm15SW1hZ2U3ID0gcGF0aDtcclxuICAgICAgICAgICAgdGhhdC5zdHJldGNoNyA9IFwiYXNwZWN0RmlsbFwiO1xyXG4gICAgICAgICAgICB0aGF0LnNob3dEZWxldGU3ID0gdHJ1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGlmIChpbWFnZSA9PSA4KSB7XHJcbiAgICAgICAgICAgIHRoYXQubXlJbWFnZTggPSBwYXRoO1xyXG4gICAgICAgICAgICB0aGF0LnN0cmV0Y2g4ID0gXCJhc3BlY3RGaWxsXCI7XHJcbiAgICAgICAgICAgIHRoYXQuc2hvd0RlbGV0ZTggPSB0cnVlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYgKGltYWdlID09IDkpIHtcclxuICAgICAgICAgICAgdGhhdC5teUltYWdlOSA9IHBhdGg7XHJcbiAgICAgICAgICAgIHRoYXQuc3RyZXRjaDkgPSBcImFzcGVjdEZpbGxcIjtcclxuICAgICAgICAgICAgdGhhdC5zaG93RGVsZXRlOSA9IHRydWU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpZiAoaW1hZ2UgPT0gMTApIHtcclxuICAgICAgICAgICAgdGhhdC5teUltYWdlMTAgPSBwYXRoO1xyXG4gICAgICAgICAgICB0aGF0LnN0cmV0Y2gxMCA9IFwiYXNwZWN0RmlsbFwiO1xyXG4gICAgICAgICAgICB0aGF0LnNob3dEZWxldGUxMCA9IHRydWU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpZiAoaW1hZ2UgPT0gMTEpIHtcclxuICAgICAgICAgICAgdGhhdC5teUltYWdlMTEgPSBwYXRoO1xyXG4gICAgICAgICAgICB0aGF0LnN0cmV0Y2gxMSA9IFwiYXNwZWN0RmlsbFwiO1xyXG4gICAgICAgICAgICB0aGF0LnNob3dEZWxldGUxMSA9IHRydWU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpZiAoaW1hZ2UgPT0gMTIpIHtcclxuICAgICAgICAgICAgdGhhdC5teUltYWdlMTIgPSBwYXRoO1xyXG4gICAgICAgICAgICB0aGF0LnN0cmV0Y2gxMiA9IFwiYXNwZWN0RmlsbFwiO1xyXG4gICAgICAgICAgICB0aGF0LnNob3dEZWxldGUxMiA9IHRydWU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHBob3RvRGVsZXRlMSgpIHtcclxuICAgIHRoaXMucGhvdG9EZWxldGUoMSk7XHJcbiAgfVxyXG4gIHB1YmxpYyBwaG90b0RlbGV0ZTIoKSB7XHJcbiAgICB0aGlzLnBob3RvRGVsZXRlKDIpO1xyXG4gIH1cclxuICBwdWJsaWMgcGhvdG9EZWxldGUzKCkge1xyXG4gICAgdGhpcy5waG90b0RlbGV0ZSgzKTtcclxuICB9XHJcbiAgcHVibGljIHBob3RvRGVsZXRlNCgpIHtcclxuICAgIHRoaXMucGhvdG9EZWxldGUoNCk7XHJcbiAgfVxyXG4gIHB1YmxpYyBwaG90b0RlbGV0ZTUoKSB7XHJcbiAgICB0aGlzLnBob3RvRGVsZXRlKDUpO1xyXG4gIH1cclxuICBwdWJsaWMgcGhvdG9EZWxldGU2KCkge1xyXG4gICAgdGhpcy5waG90b0RlbGV0ZSg2KTtcclxuICB9XHJcbiAgcHVibGljIHBob3RvRGVsZXRlNygpIHtcclxuICAgIHRoaXMucGhvdG9EZWxldGUoNyk7XHJcbiAgfVxyXG4gIHB1YmxpYyBwaG90b0RlbGV0ZTgoKSB7XHJcbiAgICB0aGlzLnBob3RvRGVsZXRlKDgpO1xyXG4gIH1cclxuICBwdWJsaWMgcGhvdG9EZWxldGU5KCkge1xyXG4gICAgdGhpcy5waG90b0RlbGV0ZSg5KTtcclxuICB9XHJcbiAgcHVibGljIHBob3RvRGVsZXRlMTAoKSB7XHJcbiAgICB0aGlzLnBob3RvRGVsZXRlKDEwKTtcclxuICB9XHJcbiAgcHVibGljIHBob3RvRGVsZXRlMTEoKSB7XHJcbiAgICB0aGlzLnBob3RvRGVsZXRlKDExKTtcclxuICB9XHJcbiAgcHVibGljIHBob3RvRGVsZXRlMTIoKSB7XHJcbiAgICB0aGlzLnBob3RvRGVsZXRlKDEyKTtcclxuICB9XHJcbiAgcGhvdG9EZWxldGUoaW1hZ2UpIHtcclxuICAgIGxldCB0aGF0ID0gdGhpcztcclxuICAgIGlmIChpbWFnZSA9PSAxKSB7XHJcbiAgICAgIHRoYXQubXlJbWFnZTEgPSBcInJlczovL2FkZHBob3RvXCI7XHJcbiAgICAgIHRoYXQuc3RyZXRjaDEgPSBcImFzcGVjdEZpdFwiO1xyXG4gICAgICB0aGF0LnNob3dEZWxldGUxID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAoaW1hZ2UgPT0gMikge1xyXG4gICAgICB0aGF0Lm15SW1hZ2UyID0gXCJyZXM6Ly9hZGRwaG90b1wiO1xyXG4gICAgICB0aGF0LnN0cmV0Y2gyID0gXCJhc3BlY3RGaXRcIjtcclxuICAgICAgdGhhdC5zaG93RGVsZXRlMiA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgaWYgKGltYWdlID09IDMpIHtcclxuICAgICAgdGhhdC5teUltYWdlMyA9IFwicmVzOi8vYWRkcGhvdG9cIjtcclxuICAgICAgdGhhdC5zdHJldGNoMyA9IFwiYXNwZWN0Rml0XCI7XHJcbiAgICAgIHRoYXQuc2hvd0RlbGV0ZTMgPSBmYWxzZTtcclxuICAgIH1cclxuICAgIGlmIChpbWFnZSA9PSA0KSB7XHJcbiAgICAgIHRoYXQubXlJbWFnZTQgPSBcInJlczovL2FkZHBob3RvXCI7XHJcbiAgICAgIHRoYXQuc3RyZXRjaDQgPSBcImFzcGVjdEZpdFwiO1xyXG4gICAgICB0aGF0LnNob3dEZWxldGU0ID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAoaW1hZ2UgPT0gNSkge1xyXG4gICAgICB0aGF0Lm15SW1hZ2U1ID0gXCJyZXM6Ly9hZGRwaG90b1wiO1xyXG4gICAgICB0aGF0LnN0cmV0Y2g1ID0gXCJhc3BlY3RGaXRcIjtcclxuICAgICAgdGhhdC5zaG93RGVsZXRlNSA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgaWYgKGltYWdlID09IDYpIHtcclxuICAgICAgdGhhdC5teUltYWdlNiA9IFwicmVzOi8vYWRkcGhvdG9cIjtcclxuICAgICAgdGhhdC5zdHJldGNoNiA9IFwiYXNwZWN0Rml0XCI7XHJcbiAgICAgIHRoYXQuc2hvd0RlbGV0ZTYgPSBmYWxzZTtcclxuICAgIH1cclxuICAgIGlmIChpbWFnZSA9PSA3KSB7XHJcbiAgICAgIHRoYXQubXlJbWFnZTcgPSBcInJlczovL2FkZHBob3RvXCI7XHJcbiAgICAgIHRoYXQuc3RyZXRjaDcgPSBcImFzcGVjdEZpdFwiO1xyXG4gICAgICB0aGF0LnNob3dEZWxldGU3ID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAoaW1hZ2UgPT0gOCkge1xyXG4gICAgICB0aGF0Lm15SW1hZ2U4ID0gXCJyZXM6Ly9hZGRwaG90b1wiO1xyXG4gICAgICB0aGF0LnN0cmV0Y2g4ID0gXCJhc3BlY3RGaXRcIjtcclxuICAgICAgdGhhdC5zaG93RGVsZXRlOCA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgaWYgKGltYWdlID09IDkpIHtcclxuICAgICAgdGhhdC5teUltYWdlOSA9IFwicmVzOi8vYWRkcGhvdG9cIjtcclxuICAgICAgdGhhdC5zdHJldGNoOSA9IFwiYXNwZWN0Rml0XCI7XHJcbiAgICAgIHRoYXQuc2hvd0RlbGV0ZTkgPSBmYWxzZTtcclxuICAgIH1cclxuICAgIGlmIChpbWFnZSA9PSAxMCkge1xyXG4gICAgICB0aGF0Lm15SW1hZ2UxMCA9IFwicmVzOi8vYWRkcGhvdG9cIjtcclxuICAgICAgdGhhdC5zdHJldGNoMTAgPSBcImFzcGVjdEZpdFwiO1xyXG4gICAgICB0aGF0LnNob3dEZWxldGUxMCA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgaWYgKGltYWdlID09IDExKSB7XHJcbiAgICAgIHRoYXQubXlJbWFnZTExID0gXCJyZXM6Ly9hZGRwaG90b1wiO1xyXG4gICAgICB0aGF0LnN0cmV0Y2gxMSA9IFwiYXNwZWN0Rml0XCI7XHJcbiAgICAgIHRoYXQuc2hvd0RlbGV0ZTExID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAoaW1hZ2UgPT0gMTIpIHtcclxuICAgICAgdGhhdC5teUltYWdlMTIgPSBcInJlczovL2FkZHBob3RvXCI7XHJcbiAgICAgIHRoYXQuc3RyZXRjaDEyID0gXCJhc3BlY3RGaXRcIjtcclxuICAgICAgdGhhdC5zaG93RGVsZXRlMTIgPSBmYWxzZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkRlbGV0ZUFsbCgpIHtcclxuICAgIGxldCB0aGF0ID0gdGhpcztcclxuICAgIHRoYXQubXlJbWFnZTEgPSBcInJlczovL2FkZHBob3RvXCI7XHJcbiAgICB0aGF0Lm15SW1hZ2UyID0gXCJyZXM6Ly9hZGRwaG90b1wiO1xyXG4gICAgdGhhdC5teUltYWdlMyA9IFwicmVzOi8vYWRkcGhvdG9cIjtcclxuICAgIHRoYXQubXlJbWFnZTQgPSBcInJlczovL2FkZHBob3RvXCI7XHJcbiAgICB0aGF0Lm15SW1hZ2U1ID0gXCJyZXM6Ly9hZGRwaG90b1wiO1xyXG4gICAgdGhhdC5teUltYWdlNiA9IFwicmVzOi8vYWRkcGhvdG9cIjtcclxuICAgIHRoYXQubXlJbWFnZTcgPSBcInJlczovL2FkZHBob3RvXCI7XHJcbiAgICB0aGF0Lm15SW1hZ2U4ID0gXCJyZXM6Ly9hZGRwaG90b1wiO1xyXG4gICAgdGhhdC5teUltYWdlOSA9IFwicmVzOi8vYWRkcGhvdG9cIjtcclxuICAgIHRoYXQubXlJbWFnZTEwID0gXCJyZXM6Ly9hZGRwaG90b1wiO1xyXG4gICAgdGhhdC5teUltYWdlMTEgPSBcInJlczovL2FkZHBob3RvXCI7XHJcbiAgICB0aGF0Lm15SW1hZ2UxMiA9IFwicmVzOi8vYWRkcGhvdG9cIjtcclxuICAgIHRoYXQuc3RyZXRjaDEgPSBcImFzcGVjdEZpdFwiO1xyXG4gICAgdGhhdC5zdHJldGNoMiA9IFwiYXNwZWN0Rml0XCI7XHJcbiAgICB0aGF0LnN0cmV0Y2gzID0gXCJhc3BlY3RGaXRcIjtcclxuICAgIHRoYXQuc3RyZXRjaDQgPSBcImFzcGVjdEZpdFwiO1xyXG4gICAgdGhhdC5zdHJldGNoNSA9IFwiYXNwZWN0Rml0XCI7XHJcbiAgICB0aGF0LnN0cmV0Y2g2ID0gXCJhc3BlY3RGaXRcIjtcclxuICAgIHRoYXQuc3RyZXRjaDcgPSBcImFzcGVjdEZpdFwiO1xyXG4gICAgdGhhdC5zdHJldGNoOCA9IFwiYXNwZWN0Rml0XCI7XHJcbiAgICB0aGF0LnN0cmV0Y2g5ID0gXCJhc3BlY3RGaXRcIjtcclxuICAgIHRoYXQuc3RyZXRjaDEwID0gXCJhc3BlY3RGaXRcIjtcclxuICAgIHRoYXQuc3RyZXRjaDExID0gXCJhc3BlY3RGaXRcIjtcclxuICAgIHRoYXQuc3RyZXRjaDEyID0gXCJhc3BlY3RGaXRcIjtcclxuICAgIHRoYXQuc2hvd0RlbGV0ZTEgPSBmYWxzZTtcclxuICAgIHRoYXQuc2hvd0RlbGV0ZTIgPSBmYWxzZTtcclxuICAgIHRoYXQuc2hvd0RlbGV0ZTMgPSBmYWxzZTtcclxuICAgIHRoYXQuc2hvd0RlbGV0ZTQgPSBmYWxzZTtcclxuICAgIHRoYXQuc2hvd0RlbGV0ZTUgPSBmYWxzZTtcclxuICAgIHRoYXQuc2hvd0RlbGV0ZTYgPSBmYWxzZTtcclxuICAgIHRoYXQuc2hvd0RlbGV0ZTcgPSBmYWxzZTtcclxuICAgIHRoYXQuc2hvd0RlbGV0ZTggPSBmYWxzZTtcclxuICAgIHRoYXQuc2hvd0RlbGV0ZTkgPSBmYWxzZTtcclxuICAgIHRoYXQuc2hvd0RlbGV0ZTEwID0gZmFsc2U7XHJcbiAgICB0aGF0LnNob3dEZWxldGUxMSA9IGZhbHNlO1xyXG4gICAgdGhhdC5zaG93RGVsZXRlMTIgPSBmYWxzZTtcclxuICB9XHJcbn1cclxuIl19
import { Component, OnInit } from "@angular/core";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page/page";
import { ImageSource, fromFile } from "tns-core-modules/image-source";
import { Image } from "tns-core-modules/ui/image";
import * as imagepicker from "nativescript-imagepicker";
import { UserService } from "../services/user.service";

import {
  Mediafilepicker,
  ImagePickerOptions
} from "nativescript-mediafilepicker";

@Component({
  selector: "ns-addPictures",
  moduleId: module.id,
  templateUrl: "./add-pictures.component.html",
  styleUrls: ["./add-pictures.component.css"]
})
export class AddPicturesComponent implements OnInit {
  routerExtensions: RouterExtensions;
  public myImage1 = "res://addphoto";
  public myImage2 = "res://addphoto";
  public myImage3 = "res://addphoto";
  public myImage4 = "res://addphoto";
  public myImage5 = "res://addphoto";
  public myImage6 = "res://addphoto";
  public myImage7 = "res://addphoto";
  public myImage8 = "res://addphoto";
  public myImage9 = "res://addphoto";
  public myImage10 = "res://addphoto";
  public myImage11 = "res://addphoto";
  public myImage12 = "res://addphoto";
  public deleteImage1 = "res://deleteicon";
  public deleteImage2 = "res://deleteicon";
  public deleteImage3 = "res://deleteicon";
  public deleteImage4 = "res://deleteicon";
  public deleteImage5 = "res://deleteicon";
  public deleteImage6 = "res://deleteicon";
  public deleteImage7 = "res://deleteicon";
  public deleteImage8 = "res://deleteicon";
  public deleteImage9 = "res://deleteicon";
  public deleteImage10 = "res://deleteicon";
  public deleteImage11 = "res://deleteicon";
  public deleteImage12 = "res://deleteicon";
  public stretch1 = "aspectFit";
  public stretch2 = "aspectFit";
  public stretch3 = "aspectFit";
  public stretch4 = "aspectFit";
  public stretch5 = "aspectFit";
  public stretch6 = "aspectFit";
  public stretch7 = "aspectFit";
  public stretch8 = "aspectFit";
  public stretch9 = "aspectFit";
  public stretch10 = "aspectFit";
  public stretch11 = "aspectFit";
  public stretch12 = "aspectFit";
  showDelete1: boolean = false;
  showDelete2: boolean = false;
  showDelete3: boolean = false;
  showDelete4: boolean = false;
  showDelete5: boolean = false;
  showDelete6: boolean = false;
  showDelete7: boolean = false;
  showDelete8: boolean = false;
  showDelete9: boolean = false;
  showDelete10: boolean = false;
  showDelete11: boolean = false;
  showDelete12: boolean = false;

  constructor(
    routerExtensions: RouterExtensions,
    private activatedRoute: ActivatedRoute,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDotState(false);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
  }

  ngOnInit(): void {}

  public onProceed() {
    this.routerExtensions.navigate(["/submitListings"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }

  public photoUpload1() {
    // let mediafilepicker = new Mediafilepicker();
    // mediafilepicker.openImagePicker(options);
    // mediafilepicker.on("getFiles", function(res) {
    //   let results = res.object.get("results");
    //   console.log(results);
    //   });
    // });
    // mediafilepicker.on("error", function(res) {
    //   let msg = res.object.get("msg");
    //   console.log(msg);
    // });
    // mediafilepicker.on("cancel", function(res) {
    //   let msg = res.object.get("msg");
    //   console.log(msg);
    // });
    this.photoupload(1);
  }
  public photoUpload2() {
    this.photoupload(2);
  }
  public photoUpload3() {
    this.photoupload(3);
  }
  public photoUpload4() {
    this.photoupload(4);
  }
  public photoUpload5() {
    this.photoupload(5);
  }
  public photoUpload6() {
    this.photoupload(6);
  }
  public photoUpload7() {
    this.photoupload(7);
  }
  public photoUpload8() {
    this.photoupload(8);
  }
  public photoUpload9() {
    this.photoupload(9);
  }
  public photoUpload10() {
    this.photoupload(10);
  }
  public photoUpload11() {
    this.photoupload(11);
  }
  public photoUpload12() {
    this.photoupload(12);
  }
  photoupload(image) {
    var that = this;
    let context = imagepicker.create({
      mode: "single"
    });
    context
      .authorize()
      .then(() => {
        return context.present();
      })
      .then(selection => {
        selection.forEach(function(selected) {
          var path = selected.android.toString();
          if (image == 1) {
            that.myImage1 = path;
            that.stretch1 = "aspectFill";
            that.showDelete1 = true;
          }
          if (image == 2) {
            that.myImage2 = path;
            that.stretch2 = "aspectFill";
            that.showDelete2 = true;
          }
          if (image == 3) {
            that.myImage3 = path;
            that.stretch3 = "aspectFill";
            that.showDelete3 = true;
          }
          if (image == 4) {
            that.myImage4 = path;
            that.stretch4 = "aspectFill";
            that.showDelete4 = true;
          }
          if (image == 5) {
            that.myImage5 = path;
            that.stretch5 = "aspectFill";
            that.showDelete5 = true;
          }
          if (image == 6) {
            that.myImage6 = path;
            that.stretch6 = "aspectFill";
            that.showDelete6 = true;
          }
          if (image == 7) {
            that.myImage7 = path;
            that.stretch7 = "aspectFill";
            that.showDelete7 = true;
          }
          if (image == 8) {
            that.myImage8 = path;
            that.stretch8 = "aspectFill";
            that.showDelete8 = true;
          }
          if (image == 9) {
            that.myImage9 = path;
            that.stretch9 = "aspectFill";
            that.showDelete9 = true;
          }
          if (image == 10) {
            that.myImage10 = path;
            that.stretch10 = "aspectFill";
            that.showDelete10 = true;
          }
          if (image == 11) {
            that.myImage11 = path;
            that.stretch11 = "aspectFill";
            that.showDelete11 = true;
          }
          if (image == 12) {
            that.myImage12 = path;
            that.stretch12 = "aspectFill";
            that.showDelete12 = true;
          }
        });
      });
  }

  public photoDelete1() {
    this.photoDelete(1);
  }
  public photoDelete2() {
    this.photoDelete(2);
  }
  public photoDelete3() {
    this.photoDelete(3);
  }
  public photoDelete4() {
    this.photoDelete(4);
  }
  public photoDelete5() {
    this.photoDelete(5);
  }
  public photoDelete6() {
    this.photoDelete(6);
  }
  public photoDelete7() {
    this.photoDelete(7);
  }
  public photoDelete8() {
    this.photoDelete(8);
  }
  public photoDelete9() {
    this.photoDelete(9);
  }
  public photoDelete10() {
    this.photoDelete(10);
  }
  public photoDelete11() {
    this.photoDelete(11);
  }
  public photoDelete12() {
    this.photoDelete(12);
  }
  photoDelete(image) {
    let that = this;
    if (image == 1) {
      that.myImage1 = "res://addphoto";
      that.stretch1 = "aspectFit";
      that.showDelete1 = false;
    }
    if (image == 2) {
      that.myImage2 = "res://addphoto";
      that.stretch2 = "aspectFit";
      that.showDelete2 = false;
    }
    if (image == 3) {
      that.myImage3 = "res://addphoto";
      that.stretch3 = "aspectFit";
      that.showDelete3 = false;
    }
    if (image == 4) {
      that.myImage4 = "res://addphoto";
      that.stretch4 = "aspectFit";
      that.showDelete4 = false;
    }
    if (image == 5) {
      that.myImage5 = "res://addphoto";
      that.stretch5 = "aspectFit";
      that.showDelete5 = false;
    }
    if (image == 6) {
      that.myImage6 = "res://addphoto";
      that.stretch6 = "aspectFit";
      that.showDelete6 = false;
    }
    if (image == 7) {
      that.myImage7 = "res://addphoto";
      that.stretch7 = "aspectFit";
      that.showDelete7 = false;
    }
    if (image == 8) {
      that.myImage8 = "res://addphoto";
      that.stretch8 = "aspectFit";
      that.showDelete8 = false;
    }
    if (image == 9) {
      that.myImage9 = "res://addphoto";
      that.stretch9 = "aspectFit";
      that.showDelete9 = false;
    }
    if (image == 10) {
      that.myImage10 = "res://addphoto";
      that.stretch10 = "aspectFit";
      that.showDelete10 = false;
    }
    if (image == 11) {
      that.myImage11 = "res://addphoto";
      that.stretch11 = "aspectFit";
      that.showDelete11 = false;
    }
    if (image == 12) {
      that.myImage12 = "res://addphoto";
      that.stretch12 = "aspectFit";
      that.showDelete12 = false;
    }
  }

  public onDeleteAll() {
    let that = this;
    that.myImage1 = "res://addphoto";
    that.myImage2 = "res://addphoto";
    that.myImage3 = "res://addphoto";
    that.myImage4 = "res://addphoto";
    that.myImage5 = "res://addphoto";
    that.myImage6 = "res://addphoto";
    that.myImage7 = "res://addphoto";
    that.myImage8 = "res://addphoto";
    that.myImage9 = "res://addphoto";
    that.myImage10 = "res://addphoto";
    that.myImage11 = "res://addphoto";
    that.myImage12 = "res://addphoto";
    that.stretch1 = "aspectFit";
    that.stretch2 = "aspectFit";
    that.stretch3 = "aspectFit";
    that.stretch4 = "aspectFit";
    that.stretch5 = "aspectFit";
    that.stretch6 = "aspectFit";
    that.stretch7 = "aspectFit";
    that.stretch8 = "aspectFit";
    that.stretch9 = "aspectFit";
    that.stretch10 = "aspectFit";
    that.stretch11 = "aspectFit";
    that.stretch12 = "aspectFit";
    that.showDelete1 = false;
    that.showDelete2 = false;
    that.showDelete3 = false;
    that.showDelete4 = false;
    that.showDelete5 = false;
    that.showDelete6 = false;
    that.showDelete7 = false;
    that.showDelete8 = false;
    that.showDelete9 = false;
    that.showDelete10 = false;
    that.showDelete11 = false;
    that.showDelete12 = false;
  }
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var user_service_1 = require("../services/user.service");
var SubmitListingComponent = /** @class */ (function () {
    function SubmitListingComponent(routerExtensions, userService) {
        this.userService = userService;
        this.myCheckColor = "#2196f3";
        this.listing1 = "Listing 1";
        this.listing2 = "Listing 2";
        this.listing3 = "Listing 3";
        this.listing4 = "Listing 4";
        this.listing5 = "Listing 5";
        this.listing6 = "Listing 6";
        this.listing7 = "Listing 7";
        this.listing8 = "Listing 8";
        this.listing9 = "Listing 9";
        this.listing10 = "Listing 10";
        this.routerExtensions = routerExtensions;
        this.userService.actionBarState(true);
        this.userService.bottomBarDotState(false);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
    }
    SubmitListingComponent.prototype.ngOnInit = function () { };
    SubmitListingComponent.prototype.onProceed = function () {
        this.routerExtensions.navigate(["/listingFunctions"], {
            animated: true,
            transition: {
                name: "slide",
                duration: 200,
                curve: "ease"
            }
        });
    };
    SubmitListingComponent = __decorate([
        core_1.Component({
            selector: "ns-submitListings",
            moduleId: module.id,
            templateUrl: "./submitListings.component.html",
            styleUrls: ["./submitListings.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            user_service_1.UserService])
    ], SubmitListingComponent);
    return SubmitListingComponent;
}());
exports.SubmitListingComponent = SubmitListingComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3VibWl0TGlzdGluZ3MuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic3VibWl0TGlzdGluZ3MuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlFO0FBQ3pFLHNEQUErRDtBQUMvRCx5REFBdUQ7QUFRdkQ7SUFjRSxnQ0FDRSxnQkFBa0MsRUFDMUIsV0FBd0I7UUFBeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFkbEMsaUJBQVksR0FBRyxTQUFTLENBQUM7UUFDekIsYUFBUSxHQUFHLFdBQVcsQ0FBQztRQUN2QixhQUFRLEdBQUcsV0FBVyxDQUFDO1FBQ3ZCLGFBQVEsR0FBRyxXQUFXLENBQUM7UUFDdkIsYUFBUSxHQUFHLFdBQVcsQ0FBQztRQUN2QixhQUFRLEdBQUcsV0FBVyxDQUFDO1FBQ3ZCLGFBQVEsR0FBRyxXQUFXLENBQUM7UUFDdkIsYUFBUSxHQUFHLFdBQVcsQ0FBQztRQUN2QixhQUFRLEdBQUcsV0FBVyxDQUFDO1FBQ3ZCLGFBQVEsR0FBRyxXQUFXLENBQUM7UUFDdkIsY0FBUyxHQUFHLFlBQVksQ0FBQztRQU12QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7UUFDekMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMxQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFDRCx5Q0FBUSxHQUFSLGNBQWtCLENBQUM7SUFFWiwwQ0FBUyxHQUFoQjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFO1lBQ3BELFFBQVEsRUFBRSxJQUFJO1lBQ2QsVUFBVSxFQUFFO2dCQUNWLElBQUksRUFBRSxPQUFPO2dCQUNiLFFBQVEsRUFBRSxHQUFHO2dCQUNiLEtBQUssRUFBRSxNQUFNO2FBQ2Q7U0FDRixDQUFDLENBQUM7SUFDTCxDQUFDO0lBbkNVLHNCQUFzQjtRQU5sQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLG1CQUFtQjtZQUM3QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLGlDQUFpQztZQUM5QyxTQUFTLEVBQUUsQ0FBQyxnQ0FBZ0MsQ0FBQztTQUM5QyxDQUFDO3lDQWdCb0IseUJBQWdCO1lBQ2IsMEJBQVc7T0FoQnZCLHNCQUFzQixDQW9DbEM7SUFBRCw2QkFBQztDQUFBLEFBcENELElBb0NDO0FBcENZLHdEQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcIm5zLXN1Ym1pdExpc3RpbmdzXCIsXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICB0ZW1wbGF0ZVVybDogXCIuL3N1Ym1pdExpc3RpbmdzLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL3N1Ym1pdExpc3RpbmdzLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIFN1Ym1pdExpc3RpbmdDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnM7XHJcbiAgbXlDaGVja0NvbG9yID0gXCIjMjE5NmYzXCI7XHJcbiAgbGlzdGluZzEgPSBcIkxpc3RpbmcgMVwiO1xyXG4gIGxpc3RpbmcyID0gXCJMaXN0aW5nIDJcIjtcclxuICBsaXN0aW5nMyA9IFwiTGlzdGluZyAzXCI7XHJcbiAgbGlzdGluZzQgPSBcIkxpc3RpbmcgNFwiO1xyXG4gIGxpc3Rpbmc1ID0gXCJMaXN0aW5nIDVcIjtcclxuICBsaXN0aW5nNiA9IFwiTGlzdGluZyA2XCI7XHJcbiAgbGlzdGluZzcgPSBcIkxpc3RpbmcgN1wiO1xyXG4gIGxpc3Rpbmc4ID0gXCJMaXN0aW5nIDhcIjtcclxuICBsaXN0aW5nOSA9IFwiTGlzdGluZyA5XCI7XHJcbiAgbGlzdGluZzEwID0gXCJMaXN0aW5nIDEwXCI7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcclxuICAgIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMgPSByb3V0ZXJFeHRlbnNpb25zO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS5hY3Rpb25CYXJTdGF0ZSh0cnVlKTtcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYm90dG9tQmFyRG90U3RhdGUoZmFsc2UpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZUNoYW5nZXMoMSk7XHJcbiAgfVxyXG4gIG5nT25Jbml0KCk6IHZvaWQge31cclxuXHJcbiAgcHVibGljIG9uUHJvY2VlZCgpIHtcclxuICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvbGlzdGluZ0Z1bmN0aW9uc1wiXSwge1xyXG4gICAgICBhbmltYXRlZDogdHJ1ZSxcclxuICAgICAgdHJhbnNpdGlvbjoge1xyXG4gICAgICAgIG5hbWU6IFwic2xpZGVcIixcclxuICAgICAgICBkdXJhdGlvbjogMjAwLFxyXG4gICAgICAgIGN1cnZlOiBcImVhc2VcIlxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuIl19
import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { UserService } from "../services/user.service";

@Component({
  selector: "ns-submitListings",
  moduleId: module.id,
  templateUrl: "./submitListings.component.html",
  styleUrls: ["./submitListings.component.css"]
})
export class SubmitListingComponent implements OnInit {
  routerExtensions: RouterExtensions;
  myCheckColor = "#2196f3";
  listing1 = "Listing 1";
  listing2 = "Listing 2";
  listing3 = "Listing 3";
  listing4 = "Listing 4";
  listing5 = "Listing 5";
  listing6 = "Listing 6";
  listing7 = "Listing 7";
  listing8 = "Listing 8";
  listing9 = "Listing 9";
  listing10 = "Listing 10";

  constructor(
    routerExtensions: RouterExtensions,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDotState(false);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
  }
  ngOnInit(): void {}

  public onProceed() {
    this.routerExtensions.navigate(["/listingFunctions"], {
      animated: true,
      transition: {
        name: "slide",
        duration: 200,
        curve: "ease"
      }
    });
  }
}

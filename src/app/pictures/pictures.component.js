"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_barcodescanner_1 = require("nativescript-barcodescanner");
var router_1 = require("nativescript-angular/router");
var modal_component_1 = require("../modals/modal.component");
var http_1 = require("@angular/common/http");
var router_2 = require("@angular/router");
var enums_1 = require("tns-core-modules/ui/enums");
var user_service_1 = require("../services/user.service");
var PicturesComponent = /** @class */ (function () {
    function PicturesComponent(activatedRoute, barcodeScanner, routerExtensions, http, userService) {
        var _this = this;
        this.activatedRoute = activatedRoute;
        this.barcodeScanner = barcodeScanner;
        this.routerExtensions = routerExtensions;
        this.http = http;
        this.userService = userService;
        this.id = "";
        this.isFrontCamera = false;
        this.torchOn = false;
        this.isScannedOnce = false;
        this.flashClass = "flash";
        this.cameraClass = "rare";
        this.appointmentId = "";
        this.flashClass = "flash";
        this.cameraClass = "rare";
        this.isFrontCamera = false;
        this.userService.actionBarState(true);
        this.userService.bottomBarDotState(false);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.activatedRoute.queryParams.subscribe(function (params) {
            _this.appointmentId = params.id;
            console.log(_this.appointmentId);
        });
    }
    PicturesComponent.prototype.ngOnInit = function () {
        throw new Error("Method not implemented.");
    };
    PicturesComponent.prototype.ngAfterViewInit = function () {
        var view = this.barcodeline.nativeElement;
        view.animate({
            duration: 3000,
            curve: enums_1.AnimationCurve.linear,
            translate: { x: 0, y: 50 }
        });
        // .then(() => {
        //   // Reset animation
        //   setTimeout(() => {
        //     view.translateY = 0;
        //     view.translateX = 0;
        //   }, 3000);
        // });
    };
    // putData() {
    //     let headers = new HttpHeaders({
    //         "Content-Type": "application/json",
    //         "x-role-key": "b312f1f9-1f51-c060-11b5-d3e3d9019b8c"
    //     });
    //     var body = {
    //         "role": {
    //             "id": "5afbb4bfdfab9716aab75fa7"
    //         },
    //         "appointment": {
    //             "id": this.appointmentId
    //         }
    //     }
    //     console.log('Reached fun', this.appointmentId)
    //     this.http.put("http://welcome-api-dev.m-sas.com/api/sessions/" + this.id, body, { headers: headers }).subscribe((res: any) => {
    //         if (res.isSuccess) {
    //             let result: any
    //             result = res.data
    //             this.isScannedOnce = true;
    //             // this.routerExtensions.navigate(['/login']);
    //     console.log(res)
    //             this.otpModal.show();
    //         }
    //         else {
    //                     console.log(res)
    //             // alert(res.error)
    //         }
    //     },
    //         error => {
    //                     console.log(error )
    //             // alert(error)
    //         })
    // }
    // onDone() {
    //     this.otpModal.hide();
    //     this.routerExtensions.navigate(['/login']);
    // }
    // onBarcodeScanningResult(args) {
    //     this.id = args.value.barcodes[0].value;
    //     if (this.id != undefined && !this.isScannedOnce) {
    //         this.putData();
    //         console.log('RSS:', this.id)
    //     }
    // }
    PicturesComponent.prototype.onFlashSelect = function () {
        if (this.flashClass == "flash") {
            this.flashClass = "flash-focus";
            this.torchOn = true;
        }
        else {
            this.flashClass = "flash";
            this.torchOn = false;
        }
    };
    PicturesComponent.prototype.onCameraSelect = function () {
        if (this.cameraClass == "rare") {
            this.cameraClass = "front";
            this.isFrontCamera = true;
            console.log("Tapped");
        }
        else {
            this.cameraClass = "rare";
            this.isFrontCamera = false;
            console.log("Tapped");
        }
    };
    PicturesComponent.prototype.onProceed = function () {
        this.routerExtensions.navigate(["/addPictures"]);
    };
    __decorate([
        core_1.ViewChild("otpDialog"),
        __metadata("design:type", modal_component_1.ModalComponent)
    ], PicturesComponent.prototype, "otpModal", void 0);
    __decorate([
        core_1.ViewChild("barcodeline"),
        __metadata("design:type", core_1.ElementRef)
    ], PicturesComponent.prototype, "barcodeline", void 0);
    PicturesComponent = __decorate([
        core_1.Component({
            selector: "Pictures",
            moduleId: module.id,
            templateUrl: "./pictures.component.html",
            styleUrls: ["./pictures.component.css"]
        }),
        __metadata("design:paramtypes", [router_2.ActivatedRoute,
            nativescript_barcodescanner_1.BarcodeScanner,
            router_1.RouterExtensions,
            http_1.HttpClient,
            user_service_1.UserService])
    ], PicturesComponent);
    return PicturesComponent;
}());
exports.PicturesComponent = PicturesComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGljdHVyZXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicGljdHVyZXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBTXVCO0FBQ3ZCLDJFQUE2RDtBQUM3RCxzREFBK0Q7QUFDL0QsNkRBQTJEO0FBQzNELDZDQUErRDtBQUMvRCwwQ0FBeUQ7QUFFekQsbURBQTJEO0FBRTNELHlEQUF1RDtBQVF2RDtJQVdFLDJCQUNVLGNBQThCLEVBQzlCLGNBQThCLEVBQzlCLGdCQUFrQyxFQUNsQyxJQUFnQixFQUNoQixXQUF3QjtRQUxsQyxpQkFrQkM7UUFqQlMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLFNBQUksR0FBSixJQUFJLENBQVk7UUFDaEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFabEMsT0FBRSxHQUFHLEVBQUUsQ0FBQztRQUNSLGtCQUFhLEdBQVksS0FBSyxDQUFDO1FBQy9CLFlBQU8sR0FBWSxLQUFLLENBQUM7UUFDekIsa0JBQWEsR0FBWSxLQUFLLENBQUM7UUFDL0IsZUFBVSxHQUFHLE9BQU8sQ0FBQztRQUNyQixnQkFBVyxHQUFHLE1BQU0sQ0FBQztRQUNyQixrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQVFqQixJQUFJLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQztRQUMxQixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtZQUM5QyxLQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxFQUFFLENBQUM7WUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDbEMsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsb0NBQVEsR0FBUjtRQUNFLE1BQU0sSUFBSSxLQUFLLENBQUMseUJBQXlCLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQsMkNBQWUsR0FBZjtRQUNFLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBNEIsQ0FBQztRQUN6RCxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ1gsUUFBUSxFQUFFLElBQUk7WUFDZCxLQUFLLEVBQUUsc0JBQWMsQ0FBQyxNQUFNO1lBQzVCLFNBQVMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBRTtTQUMzQixDQUFDLENBQUM7UUFDSCxnQkFBZ0I7UUFDaEIsdUJBQXVCO1FBQ3ZCLHVCQUF1QjtRQUN2QiwyQkFBMkI7UUFDM0IsMkJBQTJCO1FBQzNCLGNBQWM7UUFDZCxNQUFNO0lBQ1IsQ0FBQztJQUNELGNBQWM7SUFDZCxzQ0FBc0M7SUFDdEMsOENBQThDO0lBQzlDLCtEQUErRDtJQUMvRCxVQUFVO0lBRVYsbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQiwrQ0FBK0M7SUFDL0MsYUFBYTtJQUNiLDJCQUEyQjtJQUMzQix1Q0FBdUM7SUFDdkMsWUFBWTtJQUNaLFFBQVE7SUFFUixxREFBcUQ7SUFFckQsc0lBQXNJO0lBRXRJLCtCQUErQjtJQUMvQiw4QkFBOEI7SUFDOUIsZ0NBQWdDO0lBQ2hDLHlDQUF5QztJQUN6Qyw2REFBNkQ7SUFDN0QsdUJBQXVCO0lBRXZCLG9DQUFvQztJQUNwQyxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLHVDQUF1QztJQUV2QyxrQ0FBa0M7SUFDbEMsWUFBWTtJQUNaLFNBQVM7SUFDVCxxQkFBcUI7SUFDckIsMENBQTBDO0lBRTFDLDhCQUE4QjtJQUM5QixhQUFhO0lBQ2IsSUFBSTtJQUVKLGFBQWE7SUFDYiw0QkFBNEI7SUFDNUIsa0RBQWtEO0lBQ2xELElBQUk7SUFFSixrQ0FBa0M7SUFDbEMsOENBQThDO0lBQzlDLHlEQUF5RDtJQUN6RCwwQkFBMEI7SUFDMUIsdUNBQXVDO0lBQ3ZDLFFBQVE7SUFDUixJQUFJO0lBRUoseUNBQWEsR0FBYjtRQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxPQUFPLEVBQUU7WUFDOUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxhQUFhLENBQUM7WUFDaEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7U0FDckI7YUFBTTtZQUNMLElBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDO1lBQzFCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1NBQ3RCO0lBQ0gsQ0FBQztJQUVELDBDQUFjLEdBQWQ7UUFDRSxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksTUFBTSxFQUFFO1lBQzlCLElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxDQUFDO1lBQzNCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1lBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDdkI7YUFBTTtZQUNMLElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDO1lBQzFCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1lBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDdkI7SUFDSCxDQUFDO0lBRU0scUNBQVMsR0FBaEI7UUFDRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBL0h1QjtRQUF2QixnQkFBUyxDQUFDLFdBQVcsQ0FBQztrQ0FBVyxnQ0FBYzt1REFBQztJQUN2QjtRQUF6QixnQkFBUyxDQUFDLGFBQWEsQ0FBQztrQ0FBYyxpQkFBVTswREFBQztJQUZ2QyxpQkFBaUI7UUFON0IsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxVQUFVO1lBQ3BCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsMkJBQTJCO1lBQ3hDLFNBQVMsRUFBRSxDQUFDLDBCQUEwQixDQUFDO1NBQ3hDLENBQUM7eUNBYTBCLHVCQUFjO1lBQ2QsNENBQWM7WUFDWix5QkFBZ0I7WUFDNUIsaUJBQVU7WUFDSCwwQkFBVztPQWhCdkIsaUJBQWlCLENBbUk3QjtJQUFELHdCQUFDO0NBQUEsQUFuSUQsSUFtSUM7QUFuSVksOENBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgQ29tcG9uZW50LFxuICBPbkluaXQsXG4gIEFmdGVyVmlld0luaXQsXG4gIFZpZXdDaGlsZCxcbiAgRWxlbWVudFJlZlxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgQmFyY29kZVNjYW5uZXIgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWJhcmNvZGVzY2FubmVyXCI7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHsgTW9kYWxDb21wb25lbnQgfSBmcm9tIFwiLi4vbW9kYWxzL21vZGFsLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uL2h0dHBcIjtcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyByZWdpc3RlckVsZW1lbnQgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZWxlbWVudC1yZWdpc3RyeVwiO1xuaW1wb3J0IHsgQW5pbWF0aW9uQ3VydmUgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9lbnVtc1wiO1xuaW1wb3J0IHsgU3RhY2tMYXlvdXQgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9sYXlvdXRzL3N0YWNrLWxheW91dFwiO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJQaWN0dXJlc1wiLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICB0ZW1wbGF0ZVVybDogXCIuL3BpY3R1cmVzLmNvbXBvbmVudC5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9waWN0dXJlcy5jb21wb25lbnQuY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIFBpY3R1cmVzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0IHtcbiAgQFZpZXdDaGlsZChcIm90cERpYWxvZ1wiKSBvdHBNb2RhbDogTW9kYWxDb21wb25lbnQ7XG4gIEBWaWV3Q2hpbGQoXCJiYXJjb2RlbGluZVwiKSBiYXJjb2RlbGluZTogRWxlbWVudFJlZjtcblxuICBpZCA9IFwiXCI7XG4gIGlzRnJvbnRDYW1lcmE6IGJvb2xlYW4gPSBmYWxzZTtcbiAgdG9yY2hPbjogYm9vbGVhbiA9IGZhbHNlO1xuICBpc1NjYW5uZWRPbmNlOiBib29sZWFuID0gZmFsc2U7XG4gIGZsYXNoQ2xhc3MgPSBcImZsYXNoXCI7XG4gIGNhbWVyYUNsYXNzID0gXCJyYXJlXCI7XG4gIGFwcG9pbnRtZW50SWQgPSBcIlwiO1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcbiAgICBwcml2YXRlIGJhcmNvZGVTY2FubmVyOiBCYXJjb2RlU2Nhbm5lcixcbiAgICBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsXG4gICAgcHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LFxuICAgIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlXG4gICkge1xuICAgIHRoaXMuZmxhc2hDbGFzcyA9IFwiZmxhc2hcIjtcbiAgICB0aGlzLmNhbWVyYUNsYXNzID0gXCJyYXJlXCI7XG4gICAgdGhpcy5pc0Zyb250Q2FtZXJhID0gZmFsc2U7XG4gICAgdGhpcy51c2VyU2VydmljZS5hY3Rpb25CYXJTdGF0ZSh0cnVlKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdFN0YXRlKGZhbHNlKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZSh0cnVlKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZUNoYW5nZXMoMSk7XG4gICAgdGhpcy5hY3RpdmF0ZWRSb3V0ZS5xdWVyeVBhcmFtcy5zdWJzY3JpYmUocGFyYW1zID0+IHtcbiAgICAgIHRoaXMuYXBwb2ludG1lbnRJZCA9IHBhcmFtcy5pZDtcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMuYXBwb2ludG1lbnRJZCk7XG4gICAgfSk7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoXCJNZXRob2Qgbm90IGltcGxlbWVudGVkLlwiKTtcbiAgfVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcbiAgICBsZXQgdmlldyA9IHRoaXMuYmFyY29kZWxpbmUubmF0aXZlRWxlbWVudCBhcyBTdGFja0xheW91dDtcbiAgICB2aWV3LmFuaW1hdGUoe1xuICAgICAgZHVyYXRpb246IDMwMDAsXG4gICAgICBjdXJ2ZTogQW5pbWF0aW9uQ3VydmUubGluZWFyLFxuICAgICAgdHJhbnNsYXRlOiB7IHg6IDAsIHk6IDUwIH1cbiAgICB9KTtcbiAgICAvLyAudGhlbigoKSA9PiB7XG4gICAgLy8gICAvLyBSZXNldCBhbmltYXRpb25cbiAgICAvLyAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgIC8vICAgICB2aWV3LnRyYW5zbGF0ZVkgPSAwO1xuICAgIC8vICAgICB2aWV3LnRyYW5zbGF0ZVggPSAwO1xuICAgIC8vICAgfSwgMzAwMCk7XG4gICAgLy8gfSk7XG4gIH1cbiAgLy8gcHV0RGF0YSgpIHtcbiAgLy8gICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgLy8gICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcbiAgLy8gICAgICAgICBcIngtcm9sZS1rZXlcIjogXCJiMzEyZjFmOS0xZjUxLWMwNjAtMTFiNS1kM2UzZDkwMTliOGNcIlxuICAvLyAgICAgfSk7XG5cbiAgLy8gICAgIHZhciBib2R5ID0ge1xuICAvLyAgICAgICAgIFwicm9sZVwiOiB7XG4gIC8vICAgICAgICAgICAgIFwiaWRcIjogXCI1YWZiYjRiZmRmYWI5NzE2YWFiNzVmYTdcIlxuICAvLyAgICAgICAgIH0sXG4gIC8vICAgICAgICAgXCJhcHBvaW50bWVudFwiOiB7XG4gIC8vICAgICAgICAgICAgIFwiaWRcIjogdGhpcy5hcHBvaW50bWVudElkXG4gIC8vICAgICAgICAgfVxuICAvLyAgICAgfVxuXG4gIC8vICAgICBjb25zb2xlLmxvZygnUmVhY2hlZCBmdW4nLCB0aGlzLmFwcG9pbnRtZW50SWQpXG5cbiAgLy8gICAgIHRoaXMuaHR0cC5wdXQoXCJodHRwOi8vd2VsY29tZS1hcGktZGV2Lm0tc2FzLmNvbS9hcGkvc2Vzc2lvbnMvXCIgKyB0aGlzLmlkLCBib2R5LCB7IGhlYWRlcnM6IGhlYWRlcnMgfSkuc3Vic2NyaWJlKChyZXM6IGFueSkgPT4ge1xuXG4gIC8vICAgICAgICAgaWYgKHJlcy5pc1N1Y2Nlc3MpIHtcbiAgLy8gICAgICAgICAgICAgbGV0IHJlc3VsdDogYW55XG4gIC8vICAgICAgICAgICAgIHJlc3VsdCA9IHJlcy5kYXRhXG4gIC8vICAgICAgICAgICAgIHRoaXMuaXNTY2FubmVkT25jZSA9IHRydWU7XG4gIC8vICAgICAgICAgICAgIC8vIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbJy9sb2dpbiddKTtcbiAgLy8gICAgIGNvbnNvbGUubG9nKHJlcylcblxuICAvLyAgICAgICAgICAgICB0aGlzLm90cE1vZGFsLnNob3coKTtcbiAgLy8gICAgICAgICB9XG4gIC8vICAgICAgICAgZWxzZSB7XG4gIC8vICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzKVxuXG4gIC8vICAgICAgICAgICAgIC8vIGFsZXJ0KHJlcy5lcnJvcilcbiAgLy8gICAgICAgICB9XG4gIC8vICAgICB9LFxuICAvLyAgICAgICAgIGVycm9yID0+IHtcbiAgLy8gICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvciApXG5cbiAgLy8gICAgICAgICAgICAgLy8gYWxlcnQoZXJyb3IpXG4gIC8vICAgICAgICAgfSlcbiAgLy8gfVxuXG4gIC8vIG9uRG9uZSgpIHtcbiAgLy8gICAgIHRoaXMub3RwTW9kYWwuaGlkZSgpO1xuICAvLyAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFsnL2xvZ2luJ10pO1xuICAvLyB9XG5cbiAgLy8gb25CYXJjb2RlU2Nhbm5pbmdSZXN1bHQoYXJncykge1xuICAvLyAgICAgdGhpcy5pZCA9IGFyZ3MudmFsdWUuYmFyY29kZXNbMF0udmFsdWU7XG4gIC8vICAgICBpZiAodGhpcy5pZCAhPSB1bmRlZmluZWQgJiYgIXRoaXMuaXNTY2FubmVkT25jZSkge1xuICAvLyAgICAgICAgIHRoaXMucHV0RGF0YSgpO1xuICAvLyAgICAgICAgIGNvbnNvbGUubG9nKCdSU1M6JywgdGhpcy5pZClcbiAgLy8gICAgIH1cbiAgLy8gfVxuXG4gIG9uRmxhc2hTZWxlY3QoKSB7XG4gICAgaWYgKHRoaXMuZmxhc2hDbGFzcyA9PSBcImZsYXNoXCIpIHtcbiAgICAgIHRoaXMuZmxhc2hDbGFzcyA9IFwiZmxhc2gtZm9jdXNcIjtcbiAgICAgIHRoaXMudG9yY2hPbiA9IHRydWU7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuZmxhc2hDbGFzcyA9IFwiZmxhc2hcIjtcbiAgICAgIHRoaXMudG9yY2hPbiA9IGZhbHNlO1xuICAgIH1cbiAgfVxuXG4gIG9uQ2FtZXJhU2VsZWN0KCkge1xuICAgIGlmICh0aGlzLmNhbWVyYUNsYXNzID09IFwicmFyZVwiKSB7XG4gICAgICB0aGlzLmNhbWVyYUNsYXNzID0gXCJmcm9udFwiO1xuICAgICAgdGhpcy5pc0Zyb250Q2FtZXJhID0gdHJ1ZTtcbiAgICAgIGNvbnNvbGUubG9nKFwiVGFwcGVkXCIpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmNhbWVyYUNsYXNzID0gXCJyYXJlXCI7XG4gICAgICB0aGlzLmlzRnJvbnRDYW1lcmEgPSBmYWxzZTtcbiAgICAgIGNvbnNvbGUubG9nKFwiVGFwcGVkXCIpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBvblByb2NlZWQoKSB7XG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9hZGRQaWN0dXJlc1wiXSk7XG4gIH1cblxuICAvLyAgIChzY2FuUmVzdWx0KT1cIm9uQmFyY29kZVNjYW5uaW5nUmVzdWx0KCRldmVudClcbn1cbiJdfQ==
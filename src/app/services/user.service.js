"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Subject_1 = require("rxjs/Subject");
// import { Values } from '../values/values';
// import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
// import { AuthService } from './auth.service';
var UserService = /** @class */ (function () {
    // actionBarSearchChanges = this._actionBarSearch.asObservable();
    // playerButtonChanges = this._playerButton.asObservable();
    // currentUser;
    // constructor(private authService: AuthService) {
    // }
    function UserService() {
        // private _userSubject = new Subject<any>();
        // private _homeSubject = new Subject<boolean>();
        // private _homeUpdationSubject = new Subject<any>();
        this._actionBarState = new Subject_1.Subject();
        this._bottomBarDots = new Subject_1.Subject();
        this._bottomBarState = new Subject_1.Subject();
        this._tabViewState = new Subject_1.Subject();
        this._tabViewStateChanges = new Subject_1.Subject();
        this._switchState = new Subject_1.Subject();
        // private _playerButton = new Subject<boolean>();
        // private _actionBarSearch = new Subject<boolean>();
        // _radRef: RadSideDrawer;
        // userChanges = this._userSubject.asObservable();
        // homeChanges = this._homeSubject.asObservable();
        // homeUpdation = this._homeUpdationSubject.asObservable();
        this.actionBarChanges = this._actionBarState.asObservable();
        this.bottomBarDotsState = this._bottomBarState.asObservable();
        this.bottomBarDotsChanges = this._bottomBarDots.asObservable();
        this.tabviewState = this._tabViewState.asObservable();
        this.tabviewStateChanges = this._tabViewStateChanges.asObservable();
        this.switchstate = this._switchState.asObservable();
    }
    // newUser(user: any) {
    //     this._userSubject.next(user);
    // }
    // setUser(user: any, xRoleKey: string) {
    //     if (xRoleKey != undefined && xRoleKey != "" && xRoleKey != null) {
    //         Values.writeString(Values.X_ROLE_KEY, xRoleKey);
    //         this.currentUser = user;
    //         this._userSubject.next(this.currentUser);
    //     }
    // }
    // homeSelector(state: boolean) {
    //     this._homeSubject.next(state);
    // }
    // updateHomeData(data: any) {
    //     this._homeUpdationSubject.next(data);
    // }
    // openDrawer() {
    //     this._radRef.showDrawer();
    // }
    UserService.prototype.actionBarState = function (state) {
        this._actionBarState.next(state);
    };
    UserService.prototype.bottomBarDots = function (dot) {
        this._bottomBarDots.next(dot);
    };
    UserService.prototype.bottomBarDotState = function (state) {
        this._bottomBarState.next(state);
    };
    UserService.prototype.tabViewState = function (state) {
        this._tabViewState.next(state);
    };
    UserService.prototype.tabViewStateChanges = function (state) {
        this._tabViewStateChanges.next(state);
    };
    UserService.prototype.switchState = function (state) {
        this._switchState.next(state);
    };
    UserService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidXNlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLHdDQUF1QztBQUN2Qyw2Q0FBNkM7QUFDN0MsOERBQThEO0FBQzlELGdEQUFnRDtBQUdoRDtJQXdCRSxpRUFBaUU7SUFDakUsMkRBQTJEO0lBRTNELGVBQWU7SUFFZixrREFBa0Q7SUFDbEQsSUFBSTtJQUNKO1FBOUJBLDZDQUE2QztRQUM3QyxpREFBaUQ7UUFDakQscURBQXFEO1FBQzdDLG9CQUFlLEdBQUcsSUFBSSxpQkFBTyxFQUFXLENBQUM7UUFDekMsbUJBQWMsR0FBRyxJQUFJLGlCQUFPLEVBQVUsQ0FBQztRQUN2QyxvQkFBZSxHQUFHLElBQUksaUJBQU8sRUFBVyxDQUFDO1FBQ3pDLGtCQUFhLEdBQUcsSUFBSSxpQkFBTyxFQUFXLENBQUM7UUFDdkMseUJBQW9CLEdBQUcsSUFBSSxpQkFBTyxFQUFVLENBQUM7UUFDN0MsaUJBQVksR0FBRyxJQUFJLGlCQUFPLEVBQVcsQ0FBQztRQUM5QyxrREFBa0Q7UUFDbEQscURBQXFEO1FBRXJELDBCQUEwQjtRQUUxQixrREFBa0Q7UUFDbEQsa0RBQWtEO1FBQ2xELDJEQUEyRDtRQUMzRCxxQkFBZ0IsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3ZELHVCQUFrQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDekQseUJBQW9CLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUMxRCxpQkFBWSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDakQsd0JBQW1CLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFlBQVksRUFBRSxDQUFDO1FBQy9ELGdCQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQVFoQyxDQUFDO0lBQ2hCLHVCQUF1QjtJQUN2QixvQ0FBb0M7SUFDcEMsSUFBSTtJQUVKLHlDQUF5QztJQUN6Qyx5RUFBeUU7SUFDekUsMkRBQTJEO0lBQzNELG1DQUFtQztJQUNuQyxvREFBb0Q7SUFDcEQsUUFBUTtJQUNSLElBQUk7SUFFSixpQ0FBaUM7SUFDakMscUNBQXFDO0lBQ3JDLElBQUk7SUFFSiw4QkFBOEI7SUFDOUIsNENBQTRDO0lBQzVDLElBQUk7SUFFSixpQkFBaUI7SUFDakIsaUNBQWlDO0lBQ2pDLElBQUk7SUFFSixvQ0FBYyxHQUFkLFVBQWUsS0FBYztRQUMzQixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsbUNBQWEsR0FBYixVQUFjLEdBQVc7UUFDdkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUVELHVDQUFpQixHQUFqQixVQUFrQixLQUFjO1FBQzlCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRCxrQ0FBWSxHQUFaLFVBQWEsS0FBYztRQUN6QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRUQseUNBQW1CLEdBQW5CLFVBQW9CLEtBQWE7UUFDL0IsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN4QyxDQUFDO0lBRUQsaUNBQVcsR0FBWCxVQUFZLEtBQWM7UUFDeEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQTlFVSxXQUFXO1FBRHZCLGlCQUFVLEVBQUU7O09BQ0EsV0FBVyxDQThGdkI7SUFBRCxrQkFBQztDQUFBLEFBOUZELElBOEZDO0FBOUZZLGtDQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tIFwicnhqcy9TdWJqZWN0XCI7XHJcbi8vIGltcG9ydCB7IFZhbHVlcyB9IGZyb20gJy4uL3ZhbHVlcy92YWx1ZXMnO1xyXG4vLyBpbXBvcnQgeyBSYWRTaWRlRHJhd2VyIH0gZnJvbSAnbmF0aXZlc2NyaXB0LXVpLXNpZGVkcmF3ZXInO1xyXG4vLyBpbXBvcnQgeyBBdXRoU2VydmljZSB9IGZyb20gJy4vYXV0aC5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFVzZXJTZXJ2aWNlIHtcclxuICAvLyBwcml2YXRlIF91c2VyU3ViamVjdCA9IG5ldyBTdWJqZWN0PGFueT4oKTtcclxuICAvLyBwcml2YXRlIF9ob21lU3ViamVjdCA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcbiAgLy8gcHJpdmF0ZSBfaG9tZVVwZGF0aW9uU3ViamVjdCA9IG5ldyBTdWJqZWN0PGFueT4oKTtcclxuICBwcml2YXRlIF9hY3Rpb25CYXJTdGF0ZSA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcbiAgcHJpdmF0ZSBfYm90dG9tQmFyRG90cyA9IG5ldyBTdWJqZWN0PG51bWJlcj4oKTtcclxuICBwcml2YXRlIF9ib3R0b21CYXJTdGF0ZSA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcbiAgcHJpdmF0ZSBfdGFiVmlld1N0YXRlID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcclxuICBwcml2YXRlIF90YWJWaWV3U3RhdGVDaGFuZ2VzID0gbmV3IFN1YmplY3Q8bnVtYmVyPigpO1xyXG4gIHByaXZhdGUgX3N3aXRjaFN0YXRlID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcclxuICAvLyBwcml2YXRlIF9wbGF5ZXJCdXR0b24gPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xyXG4gIC8vIHByaXZhdGUgX2FjdGlvbkJhclNlYXJjaCA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcblxyXG4gIC8vIF9yYWRSZWY6IFJhZFNpZGVEcmF3ZXI7XHJcblxyXG4gIC8vIHVzZXJDaGFuZ2VzID0gdGhpcy5fdXNlclN1YmplY3QuYXNPYnNlcnZhYmxlKCk7XHJcbiAgLy8gaG9tZUNoYW5nZXMgPSB0aGlzLl9ob21lU3ViamVjdC5hc09ic2VydmFibGUoKTtcclxuICAvLyBob21lVXBkYXRpb24gPSB0aGlzLl9ob21lVXBkYXRpb25TdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xyXG4gIGFjdGlvbkJhckNoYW5nZXMgPSB0aGlzLl9hY3Rpb25CYXJTdGF0ZS5hc09ic2VydmFibGUoKTtcclxuICBib3R0b21CYXJEb3RzU3RhdGUgPSB0aGlzLl9ib3R0b21CYXJTdGF0ZS5hc09ic2VydmFibGUoKTtcclxuICBib3R0b21CYXJEb3RzQ2hhbmdlcyA9IHRoaXMuX2JvdHRvbUJhckRvdHMuYXNPYnNlcnZhYmxlKCk7XHJcbiAgdGFidmlld1N0YXRlID0gdGhpcy5fdGFiVmlld1N0YXRlLmFzT2JzZXJ2YWJsZSgpO1xyXG4gIHRhYnZpZXdTdGF0ZUNoYW5nZXMgPSB0aGlzLl90YWJWaWV3U3RhdGVDaGFuZ2VzLmFzT2JzZXJ2YWJsZSgpO1xyXG4gIHN3aXRjaHN0YXRlID0gdGhpcy5fc3dpdGNoU3RhdGUuYXNPYnNlcnZhYmxlKCk7XHJcbiAgLy8gYWN0aW9uQmFyU2VhcmNoQ2hhbmdlcyA9IHRoaXMuX2FjdGlvbkJhclNlYXJjaC5hc09ic2VydmFibGUoKTtcclxuICAvLyBwbGF5ZXJCdXR0b25DaGFuZ2VzID0gdGhpcy5fcGxheWVyQnV0dG9uLmFzT2JzZXJ2YWJsZSgpO1xyXG5cclxuICAvLyBjdXJyZW50VXNlcjtcclxuXHJcbiAgLy8gY29uc3RydWN0b3IocHJpdmF0ZSBhdXRoU2VydmljZTogQXV0aFNlcnZpY2UpIHtcclxuICAvLyB9XHJcbiAgY29uc3RydWN0b3IoKSB7fVxyXG4gIC8vIG5ld1VzZXIodXNlcjogYW55KSB7XHJcbiAgLy8gICAgIHRoaXMuX3VzZXJTdWJqZWN0Lm5leHQodXNlcik7XHJcbiAgLy8gfVxyXG5cclxuICAvLyBzZXRVc2VyKHVzZXI6IGFueSwgeFJvbGVLZXk6IHN0cmluZykge1xyXG4gIC8vICAgICBpZiAoeFJvbGVLZXkgIT0gdW5kZWZpbmVkICYmIHhSb2xlS2V5ICE9IFwiXCIgJiYgeFJvbGVLZXkgIT0gbnVsbCkge1xyXG4gIC8vICAgICAgICAgVmFsdWVzLndyaXRlU3RyaW5nKFZhbHVlcy5YX1JPTEVfS0VZLCB4Um9sZUtleSk7XHJcbiAgLy8gICAgICAgICB0aGlzLmN1cnJlbnRVc2VyID0gdXNlcjtcclxuICAvLyAgICAgICAgIHRoaXMuX3VzZXJTdWJqZWN0Lm5leHQodGhpcy5jdXJyZW50VXNlcik7XHJcbiAgLy8gICAgIH1cclxuICAvLyB9XHJcblxyXG4gIC8vIGhvbWVTZWxlY3RvcihzdGF0ZTogYm9vbGVhbikge1xyXG4gIC8vICAgICB0aGlzLl9ob21lU3ViamVjdC5uZXh0KHN0YXRlKTtcclxuICAvLyB9XHJcblxyXG4gIC8vIHVwZGF0ZUhvbWVEYXRhKGRhdGE6IGFueSkge1xyXG4gIC8vICAgICB0aGlzLl9ob21lVXBkYXRpb25TdWJqZWN0Lm5leHQoZGF0YSk7XHJcbiAgLy8gfVxyXG5cclxuICAvLyBvcGVuRHJhd2VyKCkge1xyXG4gIC8vICAgICB0aGlzLl9yYWRSZWYuc2hvd0RyYXdlcigpO1xyXG4gIC8vIH1cclxuXHJcbiAgYWN0aW9uQmFyU3RhdGUoc3RhdGU6IGJvb2xlYW4pIHtcclxuICAgIHRoaXMuX2FjdGlvbkJhclN0YXRlLm5leHQoc3RhdGUpO1xyXG4gIH1cclxuXHJcbiAgYm90dG9tQmFyRG90cyhkb3Q6IG51bWJlcikge1xyXG4gICAgdGhpcy5fYm90dG9tQmFyRG90cy5uZXh0KGRvdCk7XHJcbiAgfVxyXG5cclxuICBib3R0b21CYXJEb3RTdGF0ZShzdGF0ZTogYm9vbGVhbikge1xyXG4gICAgdGhpcy5fYm90dG9tQmFyU3RhdGUubmV4dChzdGF0ZSk7XHJcbiAgfVxyXG5cclxuICB0YWJWaWV3U3RhdGUoc3RhdGU6IGJvb2xlYW4pIHtcclxuICAgIHRoaXMuX3RhYlZpZXdTdGF0ZS5uZXh0KHN0YXRlKTtcclxuICB9XHJcblxyXG4gIHRhYlZpZXdTdGF0ZUNoYW5nZXMoc3RhdGU6IG51bWJlcikge1xyXG4gICAgdGhpcy5fdGFiVmlld1N0YXRlQ2hhbmdlcy5uZXh0KHN0YXRlKTtcclxuICB9XHJcblxyXG4gIHN3aXRjaFN0YXRlKHN0YXRlOiBib29sZWFuKSB7XHJcbiAgICB0aGlzLl9zd2l0Y2hTdGF0ZS5uZXh0KHN0YXRlKTtcclxuICB9XHJcblxyXG4gIC8vIGJ1dHRvblN0YXRlKHN0YXRlOiBib29sZWFuKSB7XHJcbiAgLy8gICAgIHRoaXMuX3BsYXllckJ1dHRvbi5uZXh0KHN0YXRlKTtcclxuICAvLyB9XHJcblxyXG4gIC8vIGFjdGlvbkJhclNlYXJjaChzdGF0ZTogYm9vbGVhbikge1xyXG4gIC8vICAgICB0aGlzLl9hY3Rpb25CYXJTZWFyY2gubmV4dChzdGF0ZSk7XHJcbiAgLy8gfVxyXG5cclxuICAvLyBsb2dvdXQoKSB7XHJcbiAgLy8gICAgIFZhbHVlcy53cml0ZVN0cmluZyhWYWx1ZXMuWF9ST0xFX0tFWSwgXCJcIik7XHJcbiAgLy8gICAgIHRoaXMuY3VycmVudFVzZXIgPSBudWxsO1xyXG4gIC8vICAgICB0aGlzLl91c2VyU3ViamVjdC5uZXh0KG51bGwpO1xyXG4gIC8vICAgICB0aGlzLmF1dGhTZXJ2aWNlLnRuc09hdXRoTG9nb3V0KCk7XHJcbiAgLy8gfVxyXG59XHJcbiJdfQ==
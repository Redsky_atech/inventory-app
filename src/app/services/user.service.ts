import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
// import { Values } from '../values/values';
// import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
// import { AuthService } from './auth.service';

@Injectable()
export class UserService {
  // private _userSubject = new Subject<any>();
  // private _homeSubject = new Subject<boolean>();
  // private _homeUpdationSubject = new Subject<any>();
  private _actionBarState = new Subject<boolean>();
  private _bottomBarDots = new Subject<number>();
  private _bottomBarState = new Subject<boolean>();
  private _tabViewState = new Subject<boolean>();
  private _tabViewStateChanges = new Subject<number>();
  private _switchState = new Subject<boolean>();
  // private _playerButton = new Subject<boolean>();
  // private _actionBarSearch = new Subject<boolean>();

  // _radRef: RadSideDrawer;

  // userChanges = this._userSubject.asObservable();
  // homeChanges = this._homeSubject.asObservable();
  // homeUpdation = this._homeUpdationSubject.asObservable();
  actionBarChanges = this._actionBarState.asObservable();
  bottomBarDotsState = this._bottomBarState.asObservable();
  bottomBarDotsChanges = this._bottomBarDots.asObservable();
  tabviewState = this._tabViewState.asObservable();
  tabviewStateChanges = this._tabViewStateChanges.asObservable();
  switchstate = this._switchState.asObservable();
  // actionBarSearchChanges = this._actionBarSearch.asObservable();
  // playerButtonChanges = this._playerButton.asObservable();

  // currentUser;

  // constructor(private authService: AuthService) {
  // }
  constructor() {}
  // newUser(user: any) {
  //     this._userSubject.next(user);
  // }

  // setUser(user: any, xRoleKey: string) {
  //     if (xRoleKey != undefined && xRoleKey != "" && xRoleKey != null) {
  //         Values.writeString(Values.X_ROLE_KEY, xRoleKey);
  //         this.currentUser = user;
  //         this._userSubject.next(this.currentUser);
  //     }
  // }

  // homeSelector(state: boolean) {
  //     this._homeSubject.next(state);
  // }

  // updateHomeData(data: any) {
  //     this._homeUpdationSubject.next(data);
  // }

  // openDrawer() {
  //     this._radRef.showDrawer();
  // }

  actionBarState(state: boolean) {
    this._actionBarState.next(state);
  }

  bottomBarDots(dot: number) {
    this._bottomBarDots.next(dot);
  }

  bottomBarDotState(state: boolean) {
    this._bottomBarState.next(state);
  }

  tabViewState(state: boolean) {
    this._tabViewState.next(state);
  }

  tabViewStateChanges(state: number) {
    this._tabViewStateChanges.next(state);
  }

  switchState(state: boolean) {
    this._switchState.next(state);
  }

  // buttonState(state: boolean) {
  //     this._playerButton.next(state);
  // }

  // actionBarSearch(state: boolean) {
  //     this._actionBarSearch.next(state);
  // }

  // logout() {
  //     Values.writeString(Values.X_ROLE_KEY, "");
  //     this.currentUser = null;
  //     this._userSubject.next(null);
  //     this.authService.tnsOauthLogout();
  // }
}

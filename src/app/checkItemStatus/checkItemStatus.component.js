"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_barcodescanner_1 = require("nativescript-barcodescanner");
var router_1 = require("nativescript-angular/router");
var modal_component_1 = require("../modals/modal.component");
var http_1 = require("@angular/common/http");
var router_2 = require("@angular/router");
var user_service_1 = require("../services/user.service");
var CheckItemStatusComponent = /** @class */ (function () {
    function CheckItemStatusComponent(activatedRoute, barcodeScanner, routerExtensions, http, userService) {
        var _this = this;
        this.activatedRoute = activatedRoute;
        this.barcodeScanner = barcodeScanner;
        this.routerExtensions = routerExtensions;
        this.http = http;
        this.userService = userService;
        this.brandNames = [
            "Dell",
            "Samsung",
            "Apple",
            "Hp",
            "Nokia",
            "Redmi",
            "Motorola",
            "Linovo",
            "Panasonic"
        ];
        this.quantity = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];
        this.id = "";
        this.isFrontCamera = false;
        this.torchOn = false;
        this.isScannedOnce = false;
        this.flashClass = "flash";
        this.cameraClass = "rare";
        this.appointmentId = "";
        this.flashClass = "flash";
        this.cameraClass = "rare";
        this.isFrontCamera = false;
        this.userService.actionBarState(true);
        this.userService.bottomBarDotState(false);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.activatedRoute.queryParams.subscribe(function (params) {
            _this.appointmentId = params.id;
            console.log(_this.appointmentId);
        });
    }
    CheckItemStatusComponent.prototype.ngOnInit = function () { };
    CheckItemStatusComponent.prototype.ngAfterViewInit = function () {
        // let view = this.barcodeline.nativeElement as StackLayout;
        // view.animate({
        //   duration: 3000,
        //   curve: AnimationCurve.linear,
        //   translate: { x: 0, y: 50 }
        // });
        // .then(() => {
        //   // Reset animation
        //   setTimeout(() => {
        //     view.translateY = 0;
        //     view.translateX = 0;
        //   }, 3000);
        // });
    };
    // putData() {
    //     let headers = new HttpHeaders({
    //         "Content-Type": "application/json",
    //         "x-role-key": "b312f1f9-1f51-c060-11b5-d3e3d9019b8c"
    //     });
    //     var body = {
    //         "role": {
    //             "id": "5afbb4bfdfab9716aab75fa7"
    //         },
    //         "appointment": {
    //             "id": this.appointmentId
    //         }
    //     }
    //     console.log('Reached fun', this.appointmentId)
    //     this.http.put("http://welcome-api-dev.m-sas.com/api/sessions/" + this.id, body, { headers: headers }).subscribe((res: any) => {
    //         if (res.isSuccess) {
    //             let result: any
    //             result = res.data
    //             this.isScannedOnce = true;
    //             // this.routerExtensions.navigate(['/login']);
    //     console.log(res)
    //             this.otpModal.show();
    //         }
    //         else {
    //                     console.log(res)
    //             // alert(res.error)
    //         }
    //     },
    //         error => {
    //                     console.log(error )
    //             // alert(error)
    //         })
    // }
    // onDone() {
    //     this.otpModal.hide();
    //     this.routerExtensions.navigate(['/login']);
    // }
    // onBarcodeScanningResult(args) {
    //     this.id = args.value.barcodes[0].value;
    //     if (this.id != undefined && !this.isScannedOnce) {
    //         this.putData();
    //         console.log('RSS:', this.id)
    //     }
    // }
    CheckItemStatusComponent.prototype.onFlashSelect = function () {
        if (this.flashClass == "flash") {
            this.flashClass = "flash-focus";
            this.torchOn = true;
        }
        else {
            this.flashClass = "flash";
            this.torchOn = false;
        }
    };
    CheckItemStatusComponent.prototype.onCameraSelect = function () {
        if (this.cameraClass == "rare") {
            this.cameraClass = "front";
            this.isFrontCamera = true;
            console.log("Tapped");
        }
        else {
            this.cameraClass = "rare";
            this.isFrontCamera = false;
            console.log("Tapped");
        }
    };
    __decorate([
        core_1.ViewChild("otpDialog"),
        __metadata("design:type", modal_component_1.ModalComponent)
    ], CheckItemStatusComponent.prototype, "otpModal", void 0);
    __decorate([
        core_1.ViewChild("barcodeline"),
        __metadata("design:type", core_1.ElementRef)
    ], CheckItemStatusComponent.prototype, "barcodeline", void 0);
    CheckItemStatusComponent = __decorate([
        core_1.Component({
            selector: "checkItemStatus",
            moduleId: module.id,
            templateUrl: "./checkItemStatus.component.html",
            styleUrls: ["./checkItemStatus.component.css"]
        }),
        __metadata("design:paramtypes", [router_2.ActivatedRoute,
            nativescript_barcodescanner_1.BarcodeScanner,
            router_1.RouterExtensions,
            http_1.HttpClient,
            user_service_1.UserService])
    ], CheckItemStatusComponent);
    return CheckItemStatusComponent;
}());
exports.CheckItemStatusComponent = CheckItemStatusComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tJdGVtU3RhdHVzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNoZWNrSXRlbVN0YXR1cy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FNdUI7QUFDdkIsMkVBQTZEO0FBQzdELHNEQUErRDtBQUMvRCw2REFBMkQ7QUFDM0QsNkNBQStEO0FBQy9ELDBDQUF5RDtBQUl6RCx5REFBdUQ7QUFTdkQ7SUF5QkUsa0NBQ1UsY0FBOEIsRUFDOUIsY0FBOEIsRUFDOUIsZ0JBQWtDLEVBQ2xDLElBQWdCLEVBQ2hCLFdBQXdCO1FBTGxDLGlCQW1CQztRQWxCUyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUNoQixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQTFCM0IsZUFBVSxHQUFHO1lBQ2xCLE1BQU07WUFDTixTQUFTO1lBQ1QsT0FBTztZQUNQLElBQUk7WUFDSixPQUFPO1lBQ1AsT0FBTztZQUNQLFVBQVU7WUFDVixRQUFRO1lBQ1IsV0FBVztTQUNaLENBQUM7UUFFSyxhQUFRLEdBQUcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUV0RSxPQUFFLEdBQUcsRUFBRSxDQUFDO1FBQ1Isa0JBQWEsR0FBWSxLQUFLLENBQUM7UUFDL0IsWUFBTyxHQUFZLEtBQUssQ0FBQztRQUN6QixrQkFBYSxHQUFZLEtBQUssQ0FBQztRQUMvQixlQUFVLEdBQUcsT0FBTyxDQUFDO1FBQ3JCLGdCQUFXLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLGtCQUFhLEdBQUcsRUFBRSxDQUFDO1FBUWpCLElBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDO1FBQzFCLElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDO1FBQzFCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUV4QyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsVUFBQSxNQUFNO1lBQzlDLEtBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLEVBQUUsQ0FBQztZQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNsQyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCwyQ0FBUSxHQUFSLGNBQWtCLENBQUM7SUFFbkIsa0RBQWUsR0FBZjtRQUNFLDREQUE0RDtRQUM1RCxpQkFBaUI7UUFDakIsb0JBQW9CO1FBQ3BCLGtDQUFrQztRQUNsQywrQkFBK0I7UUFDL0IsTUFBTTtRQUNOLGdCQUFnQjtRQUNoQix1QkFBdUI7UUFDdkIsdUJBQXVCO1FBQ3ZCLDJCQUEyQjtRQUMzQiwyQkFBMkI7UUFDM0IsY0FBYztRQUNkLE1BQU07SUFDUixDQUFDO0lBQ0QsY0FBYztJQUNkLHNDQUFzQztJQUN0Qyw4Q0FBOEM7SUFDOUMsK0RBQStEO0lBQy9ELFVBQVU7SUFFVixtQkFBbUI7SUFDbkIsb0JBQW9CO0lBQ3BCLCtDQUErQztJQUMvQyxhQUFhO0lBQ2IsMkJBQTJCO0lBQzNCLHVDQUF1QztJQUN2QyxZQUFZO0lBQ1osUUFBUTtJQUVSLHFEQUFxRDtJQUVyRCxzSUFBc0k7SUFFdEksK0JBQStCO0lBQy9CLDhCQUE4QjtJQUM5QixnQ0FBZ0M7SUFDaEMseUNBQXlDO0lBQ3pDLDZEQUE2RDtJQUM3RCx1QkFBdUI7SUFFdkIsb0NBQW9DO0lBQ3BDLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsdUNBQXVDO0lBRXZDLGtDQUFrQztJQUNsQyxZQUFZO0lBQ1osU0FBUztJQUNULHFCQUFxQjtJQUNyQiwwQ0FBMEM7SUFFMUMsOEJBQThCO0lBQzlCLGFBQWE7SUFDYixJQUFJO0lBRUosYUFBYTtJQUNiLDRCQUE0QjtJQUM1QixrREFBa0Q7SUFDbEQsSUFBSTtJQUVKLGtDQUFrQztJQUNsQyw4Q0FBOEM7SUFDOUMseURBQXlEO0lBQ3pELDBCQUEwQjtJQUMxQix1Q0FBdUM7SUFDdkMsUUFBUTtJQUNSLElBQUk7SUFFSixnREFBYSxHQUFiO1FBQ0UsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLE9BQU8sRUFBRTtZQUM5QixJQUFJLENBQUMsVUFBVSxHQUFHLGFBQWEsQ0FBQztZQUNoQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztTQUNyQjthQUFNO1lBQ0wsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUM7WUFDMUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDdEI7SUFDSCxDQUFDO0lBRUQsaURBQWMsR0FBZDtRQUNFLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxNQUFNLEVBQUU7WUFDOUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUM7WUFDM0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7WUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUN2QjthQUFNO1lBQ0wsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUM7WUFDMUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUN2QjtJQUNILENBQUM7SUF4SXVCO1FBQXZCLGdCQUFTLENBQUMsV0FBVyxDQUFDO2tDQUFXLGdDQUFjOzhEQUFDO0lBQ3ZCO1FBQXpCLGdCQUFTLENBQUMsYUFBYSxDQUFDO2tDQUFjLGlCQUFVO2lFQUFDO0lBRnZDLHdCQUF3QjtRQU5wQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLGtDQUFrQztZQUMvQyxTQUFTLEVBQUUsQ0FBQyxpQ0FBaUMsQ0FBQztTQUMvQyxDQUFDO3lDQTJCMEIsdUJBQWM7WUFDZCw0Q0FBYztZQUNaLHlCQUFnQjtZQUM1QixpQkFBVTtZQUNILDBCQUFXO09BOUJ2Qix3QkFBd0IsQ0E0SXBDO0lBQUQsK0JBQUM7Q0FBQSxBQTVJRCxJQTRJQztBQTVJWSw0REFBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBDb21wb25lbnQsXG4gIE9uSW5pdCxcbiAgQWZ0ZXJWaWV3SW5pdCxcbiAgVmlld0NoaWxkLFxuICBFbGVtZW50UmVmXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBCYXJjb2RlU2Nhbm5lciB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYmFyY29kZXNjYW5uZXJcIjtcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBNb2RhbENvbXBvbmVudCB9IGZyb20gXCIuLi9tb2RhbHMvbW9kYWwuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycyB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IHJlZ2lzdGVyRWxlbWVudCB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9lbGVtZW50LXJlZ2lzdHJ5XCI7XG5pbXBvcnQgeyBBbmltYXRpb25DdXJ2ZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2VudW1zXCI7XG5pbXBvcnQgeyBTdGFja0xheW91dCB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2xheW91dHMvc3RhY2stbGF5b3V0XCI7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcbmltcG9ydCB7IFN3aXBlR2VzdHVyZUV2ZW50RGF0YSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2dlc3R1cmVzXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJjaGVja0l0ZW1TdGF0dXNcIixcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgdGVtcGxhdGVVcmw6IFwiLi9jaGVja0l0ZW1TdGF0dXMuY29tcG9uZW50Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL2NoZWNrSXRlbVN0YXR1cy5jb21wb25lbnQuY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIENoZWNrSXRlbVN0YXR1c0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB7XG4gIEBWaWV3Q2hpbGQoXCJvdHBEaWFsb2dcIikgb3RwTW9kYWw6IE1vZGFsQ29tcG9uZW50O1xuICBAVmlld0NoaWxkKFwiYmFyY29kZWxpbmVcIikgYmFyY29kZWxpbmU6IEVsZW1lbnRSZWY7XG5cbiAgcHVibGljIGJyYW5kTmFtZXMgPSBbXG4gICAgXCJEZWxsXCIsXG4gICAgXCJTYW1zdW5nXCIsXG4gICAgXCJBcHBsZVwiLFxuICAgIFwiSHBcIixcbiAgICBcIk5va2lhXCIsXG4gICAgXCJSZWRtaVwiLFxuICAgIFwiTW90b3JvbGFcIixcbiAgICBcIkxpbm92b1wiLFxuICAgIFwiUGFuYXNvbmljXCJcbiAgXTtcblxuICBwdWJsaWMgcXVhbnRpdHkgPSBbXCIxXCIsIFwiMlwiLCBcIjNcIiwgXCI0XCIsIFwiNVwiLCBcIjZcIiwgXCI3XCIsIFwiOFwiLCBcIjlcIiwgXCIxMFwiXTtcblxuICBpZCA9IFwiXCI7XG4gIGlzRnJvbnRDYW1lcmE6IGJvb2xlYW4gPSBmYWxzZTtcbiAgdG9yY2hPbjogYm9vbGVhbiA9IGZhbHNlO1xuICBpc1NjYW5uZWRPbmNlOiBib29sZWFuID0gZmFsc2U7XG4gIGZsYXNoQ2xhc3MgPSBcImZsYXNoXCI7XG4gIGNhbWVyYUNsYXNzID0gXCJyYXJlXCI7XG4gIGFwcG9pbnRtZW50SWQgPSBcIlwiO1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcbiAgICBwcml2YXRlIGJhcmNvZGVTY2FubmVyOiBCYXJjb2RlU2Nhbm5lcixcbiAgICBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsXG4gICAgcHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LFxuICAgIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlXG4gICkge1xuICAgIHRoaXMuZmxhc2hDbGFzcyA9IFwiZmxhc2hcIjtcbiAgICB0aGlzLmNhbWVyYUNsYXNzID0gXCJyYXJlXCI7XG4gICAgdGhpcy5pc0Zyb250Q2FtZXJhID0gZmFsc2U7XG4gICAgdGhpcy51c2VyU2VydmljZS5hY3Rpb25CYXJTdGF0ZSh0cnVlKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdFN0YXRlKGZhbHNlKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZSh0cnVlKTtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZUNoYW5nZXMoMSk7XG5cbiAgICB0aGlzLmFjdGl2YXRlZFJvdXRlLnF1ZXJ5UGFyYW1zLnN1YnNjcmliZShwYXJhbXMgPT4ge1xuICAgICAgdGhpcy5hcHBvaW50bWVudElkID0gcGFyYW1zLmlkO1xuICAgICAgY29uc29sZS5sb2codGhpcy5hcHBvaW50bWVudElkKTtcbiAgICB9KTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge31cblxuICBuZ0FmdGVyVmlld0luaXQoKSB7XG4gICAgLy8gbGV0IHZpZXcgPSB0aGlzLmJhcmNvZGVsaW5lLm5hdGl2ZUVsZW1lbnQgYXMgU3RhY2tMYXlvdXQ7XG4gICAgLy8gdmlldy5hbmltYXRlKHtcbiAgICAvLyAgIGR1cmF0aW9uOiAzMDAwLFxuICAgIC8vICAgY3VydmU6IEFuaW1hdGlvbkN1cnZlLmxpbmVhcixcbiAgICAvLyAgIHRyYW5zbGF0ZTogeyB4OiAwLCB5OiA1MCB9XG4gICAgLy8gfSk7XG4gICAgLy8gLnRoZW4oKCkgPT4ge1xuICAgIC8vICAgLy8gUmVzZXQgYW5pbWF0aW9uXG4gICAgLy8gICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAvLyAgICAgdmlldy50cmFuc2xhdGVZID0gMDtcbiAgICAvLyAgICAgdmlldy50cmFuc2xhdGVYID0gMDtcbiAgICAvLyAgIH0sIDMwMDApO1xuICAgIC8vIH0pO1xuICB9XG4gIC8vIHB1dERhdGEoKSB7XG4gIC8vICAgICBsZXQgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gIC8vICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXG4gIC8vICAgICAgICAgXCJ4LXJvbGUta2V5XCI6IFwiYjMxMmYxZjktMWY1MS1jMDYwLTExYjUtZDNlM2Q5MDE5YjhjXCJcbiAgLy8gICAgIH0pO1xuXG4gIC8vICAgICB2YXIgYm9keSA9IHtcbiAgLy8gICAgICAgICBcInJvbGVcIjoge1xuICAvLyAgICAgICAgICAgICBcImlkXCI6IFwiNWFmYmI0YmZkZmFiOTcxNmFhYjc1ZmE3XCJcbiAgLy8gICAgICAgICB9LFxuICAvLyAgICAgICAgIFwiYXBwb2ludG1lbnRcIjoge1xuICAvLyAgICAgICAgICAgICBcImlkXCI6IHRoaXMuYXBwb2ludG1lbnRJZFxuICAvLyAgICAgICAgIH1cbiAgLy8gICAgIH1cblxuICAvLyAgICAgY29uc29sZS5sb2coJ1JlYWNoZWQgZnVuJywgdGhpcy5hcHBvaW50bWVudElkKVxuXG4gIC8vICAgICB0aGlzLmh0dHAucHV0KFwiaHR0cDovL3dlbGNvbWUtYXBpLWRldi5tLXNhcy5jb20vYXBpL3Nlc3Npb25zL1wiICsgdGhpcy5pZCwgYm9keSwgeyBoZWFkZXJzOiBoZWFkZXJzIH0pLnN1YnNjcmliZSgocmVzOiBhbnkpID0+IHtcblxuICAvLyAgICAgICAgIGlmIChyZXMuaXNTdWNjZXNzKSB7XG4gIC8vICAgICAgICAgICAgIGxldCByZXN1bHQ6IGFueVxuICAvLyAgICAgICAgICAgICByZXN1bHQgPSByZXMuZGF0YVxuICAvLyAgICAgICAgICAgICB0aGlzLmlzU2Nhbm5lZE9uY2UgPSB0cnVlO1xuICAvLyAgICAgICAgICAgICAvLyB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoWycvbG9naW4nXSk7XG4gIC8vICAgICBjb25zb2xlLmxvZyhyZXMpXG5cbiAgLy8gICAgICAgICAgICAgdGhpcy5vdHBNb2RhbC5zaG93KCk7XG4gIC8vICAgICAgICAgfVxuICAvLyAgICAgICAgIGVsc2Uge1xuICAvLyAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlcylcblxuICAvLyAgICAgICAgICAgICAvLyBhbGVydChyZXMuZXJyb3IpXG4gIC8vICAgICAgICAgfVxuICAvLyAgICAgfSxcbiAgLy8gICAgICAgICBlcnJvciA9PiB7XG4gIC8vICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IgKVxuXG4gIC8vICAgICAgICAgICAgIC8vIGFsZXJ0KGVycm9yKVxuICAvLyAgICAgICAgIH0pXG4gIC8vIH1cblxuICAvLyBvbkRvbmUoKSB7XG4gIC8vICAgICB0aGlzLm90cE1vZGFsLmhpZGUoKTtcbiAgLy8gICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbJy9sb2dpbiddKTtcbiAgLy8gfVxuXG4gIC8vIG9uQmFyY29kZVNjYW5uaW5nUmVzdWx0KGFyZ3MpIHtcbiAgLy8gICAgIHRoaXMuaWQgPSBhcmdzLnZhbHVlLmJhcmNvZGVzWzBdLnZhbHVlO1xuICAvLyAgICAgaWYgKHRoaXMuaWQgIT0gdW5kZWZpbmVkICYmICF0aGlzLmlzU2Nhbm5lZE9uY2UpIHtcbiAgLy8gICAgICAgICB0aGlzLnB1dERhdGEoKTtcbiAgLy8gICAgICAgICBjb25zb2xlLmxvZygnUlNTOicsIHRoaXMuaWQpXG4gIC8vICAgICB9XG4gIC8vIH1cblxuICBvbkZsYXNoU2VsZWN0KCkge1xuICAgIGlmICh0aGlzLmZsYXNoQ2xhc3MgPT0gXCJmbGFzaFwiKSB7XG4gICAgICB0aGlzLmZsYXNoQ2xhc3MgPSBcImZsYXNoLWZvY3VzXCI7XG4gICAgICB0aGlzLnRvcmNoT24gPSB0cnVlO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmZsYXNoQ2xhc3MgPSBcImZsYXNoXCI7XG4gICAgICB0aGlzLnRvcmNoT24gPSBmYWxzZTtcbiAgICB9XG4gIH1cblxuICBvbkNhbWVyYVNlbGVjdCgpIHtcbiAgICBpZiAodGhpcy5jYW1lcmFDbGFzcyA9PSBcInJhcmVcIikge1xuICAgICAgdGhpcy5jYW1lcmFDbGFzcyA9IFwiZnJvbnRcIjtcbiAgICAgIHRoaXMuaXNGcm9udENhbWVyYSA9IHRydWU7XG4gICAgICBjb25zb2xlLmxvZyhcIlRhcHBlZFwiKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5jYW1lcmFDbGFzcyA9IFwicmFyZVwiO1xuICAgICAgdGhpcy5pc0Zyb250Q2FtZXJhID0gZmFsc2U7XG4gICAgICBjb25zb2xlLmxvZyhcIlRhcHBlZFwiKTtcbiAgICB9XG4gIH1cblxuICAvLyAgIChzY2FuUmVzdWx0KT1cIm9uQmFyY29kZVNjYW5uaW5nUmVzdWx0KCRldmVudClcbn1cbiJdfQ==
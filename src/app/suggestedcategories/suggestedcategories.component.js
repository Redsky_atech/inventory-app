"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var angular_1 = require("nativescript-drop-down/angular");
var router_1 = require("nativescript-angular/router");
var user_service_1 = require("../services/user.service");
var SuggestedCategoriesComponent = /** @class */ (function () {
    function SuggestedCategoriesComponent(dropDownmodule, routerExtensions, userService) {
        this.dropDownmodule = dropDownmodule;
        this.userService = userService;
        this.categoriesArray = ["Home", "Mindful", "Kitchen"];
        this.categories = "";
        this.category = "";
        this.routerExtensions = routerExtensions;
        this.userService.actionBarState(true);
        this.userService.bottomBarDots(4);
        this.userService.bottomBarDotState(true);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.userService.switchState(false);
        var str = this.categories;
        var str1 = " > ";
        var arrLength = this.categoriesArray.length;
        for (var i = 0; i < arrLength; i++) {
            str = str.concat(this.categoriesArray[i]);
            if (i < arrLength - 1) {
                str = str.concat(str1);
            }
            this.categories = str;
        }
        this.category = this.categories;
    }
    SuggestedCategoriesComponent.prototype.ngOnInit = function () { };
    SuggestedCategoriesComponent.prototype.ngAfterViewInit = function () { };
    SuggestedCategoriesComponent.prototype.onSwipe = function (args) {
        if (args.direction == 1) {
            this.routerExtensions.navigate(["/title"]);
        }
        if (args.direction == 2) {
            this.routerExtensions.navigate(["/findCategory"]);
        }
    };
    SuggestedCategoriesComponent = __decorate([
        core_1.Component({
            selector: "Suggest",
            moduleId: module.id,
            templateUrl: "./suggestedcategories.component.html",
            styleUrls: ["./suggestedcategories.component.css"]
        }),
        __metadata("design:paramtypes", [angular_1.DropDownModule,
            router_1.RouterExtensions,
            user_service_1.UserService])
    ], SuggestedCategoriesComponent);
    return SuggestedCategoriesComponent;
}());
exports.SuggestedCategoriesComponent = SuggestedCategoriesComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3VnZ2VzdGVkY2F0ZWdvcmllcy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdWdnZXN0ZWRjYXRlZ29yaWVzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFpRTtBQUVqRSwwREFBZ0U7QUFFaEUsc0RBQStEO0FBQy9ELHlEQUF1RDtBQVF2RDtJQVFFLHNDQUNVLGNBQThCLEVBQ3RDLGdCQUFrQyxFQUMxQixXQUF3QjtRQUZ4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFFOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFWbEMsb0JBQWUsR0FBRyxDQUFDLE1BQU0sRUFBRSxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFFakQsZUFBVSxHQUFXLEVBQUUsQ0FBQztRQUV4QixhQUFRLEdBQUcsRUFBRSxDQUFDO1FBUVosSUFBSSxDQUFDLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQzFCLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQztRQUNqQixJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQztRQUM1QyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ2xDLEdBQUcsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsR0FBRyxTQUFTLEdBQUcsQ0FBQyxFQUFFO2dCQUNyQixHQUFHLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN4QjtZQUNELElBQUksQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDO1NBQ3ZCO1FBRUQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ2xDLENBQUM7SUFFRCwrQ0FBUSxHQUFSLGNBQWtCLENBQUM7SUFFbkIsc0RBQWUsR0FBZixjQUFtQixDQUFDO0lBRXBCLDhDQUFPLEdBQVAsVUFBUSxJQUEyQjtRQUNqQyxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1NBQzVDO1FBQ0QsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsRUFBRTtZQUN2QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztTQUNuRDtJQUNILENBQUM7SUE3Q1UsNEJBQTRCO1FBTnhDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsU0FBUztZQUNuQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLHNDQUFzQztZQUNuRCxTQUFTLEVBQUUsQ0FBQyxxQ0FBcUMsQ0FBQztTQUNuRCxDQUFDO3lDQVUwQix3QkFBYztZQUNwQix5QkFBZ0I7WUFDYiwwQkFBVztPQVh2Qiw0QkFBNEIsQ0E4Q3hDO0lBQUQsbUNBQUM7Q0FBQSxBQTlDRCxJQThDQztBQTlDWSxvRUFBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFN3aXBlR2VzdHVyZUV2ZW50RGF0YSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2dlc3R1cmVzXCI7XHJcbmltcG9ydCB7IERyb3BEb3duTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1kcm9wLWRvd24vYW5ndWxhclwiO1xyXG5pbXBvcnQgeyBTZWxlY3RlZEluZGV4Q2hhbmdlZEV2ZW50RGF0YSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtZHJvcC1kb3duXCI7XHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL3VzZXIuc2VydmljZVwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwiU3VnZ2VzdFwiLFxyXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9zdWdnZXN0ZWRjYXRlZ29yaWVzLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL3N1Z2dlc3RlZGNhdGVnb3JpZXMuY29tcG9uZW50LmNzc1wiXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU3VnZ2VzdGVkQ2F0ZWdvcmllc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB7XHJcbiAgY2F0ZWdvcmllc0FycmF5ID0gW1wiSG9tZVwiLCBcIk1pbmRmdWxcIiwgXCJLaXRjaGVuXCJdO1xyXG5cclxuICBjYXRlZ29yaWVzOiBzdHJpbmcgPSBcIlwiO1xyXG5cclxuICBjYXRlZ29yeSA9IFwiXCI7XHJcbiAgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucztcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGRyb3BEb3dubW9kdWxlOiBEcm9wRG93bk1vZHVsZSxcclxuICAgIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsXHJcbiAgICBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZVxyXG4gICkge1xyXG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zID0gcm91dGVyRXh0ZW5zaW9ucztcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYWN0aW9uQmFyU3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdHMoNCk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdFN0YXRlKHRydWUpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZUNoYW5nZXMoMSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnN3aXRjaFN0YXRlKGZhbHNlKTtcclxuICAgIHZhciBzdHIgPSB0aGlzLmNhdGVnb3JpZXM7XHJcbiAgICB2YXIgc3RyMSA9IFwiID4gXCI7XHJcbiAgICB2YXIgYXJyTGVuZ3RoID0gdGhpcy5jYXRlZ29yaWVzQXJyYXkubGVuZ3RoO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBhcnJMZW5ndGg7IGkrKykge1xyXG4gICAgICBzdHIgPSBzdHIuY29uY2F0KHRoaXMuY2F0ZWdvcmllc0FycmF5W2ldKTtcclxuICAgICAgaWYgKGkgPCBhcnJMZW5ndGggLSAxKSB7XHJcbiAgICAgICAgc3RyID0gc3RyLmNvbmNhdChzdHIxKTtcclxuICAgICAgfVxyXG4gICAgICB0aGlzLmNhdGVnb3JpZXMgPSBzdHI7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5jYXRlZ29yeSA9IHRoaXMuY2F0ZWdvcmllcztcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge31cclxuXHJcbiAgbmdBZnRlclZpZXdJbml0KCkge31cclxuXHJcbiAgb25Td2lwZShhcmdzOiBTd2lwZUdlc3R1cmVFdmVudERhdGEpIHtcclxuICAgIGlmIChhcmdzLmRpcmVjdGlvbiA9PSAxKSB7XHJcbiAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvdGl0bGVcIl0pO1xyXG4gICAgfVxyXG4gICAgaWYgKGFyZ3MuZGlyZWN0aW9uID09IDIpIHtcclxuICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9maW5kQ2F0ZWdvcnlcIl0pO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=
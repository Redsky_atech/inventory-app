import { Component, OnInit, AfterViewInit } from "@angular/core";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { DropDownModule } from "nativescript-drop-down/angular";
import { SelectedIndexChangedEventData } from "nativescript-drop-down";
import { RouterExtensions } from "nativescript-angular/router";
import { UserService } from "../services/user.service";

@Component({
  selector: "Suggest",
  moduleId: module.id,
  templateUrl: "./suggestedcategories.component.html",
  styleUrls: ["./suggestedcategories.component.css"]
})
export class SuggestedCategoriesComponent implements OnInit, AfterViewInit {
  categoriesArray = ["Home", "Mindful", "Kitchen"];

  categories: string = "";

  category = "";
  routerExtensions: RouterExtensions;

  constructor(
    private dropDownmodule: DropDownModule,
    routerExtensions: RouterExtensions,
    private userService: UserService
  ) {
    this.routerExtensions = routerExtensions;
    this.userService.actionBarState(true);
    this.userService.bottomBarDots(4);
    this.userService.bottomBarDotState(true);
    this.userService.tabViewState(true);
    this.userService.tabViewStateChanges(1);
    this.userService.switchState(false);
    var str = this.categories;
    var str1 = " > ";
    var arrLength = this.categoriesArray.length;
    for (var i = 0; i < arrLength; i++) {
      str = str.concat(this.categoriesArray[i]);
      if (i < arrLength - 1) {
        str = str.concat(str1);
      }
      this.categories = str;
    }

    this.category = this.categories;
  }

  ngOnInit(): void {}

  ngAfterViewInit() {}

  onSwipe(args: SwipeGestureEventData) {
    if (args.direction == 1) {
      this.routerExtensions.navigate(["/title"]);
    }
    if (args.direction == 2) {
      this.routerExtensions.navigate(["/findCategory"]);
    }
  }
}

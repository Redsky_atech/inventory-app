"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var angular_1 = require("nativescript-drop-down/angular");
var user_service_1 = require("../services/user.service");
var ItemSpecifiesComponent = /** @class */ (function () {
    function ItemSpecifiesComponent(routerExtensions, dropDownmodule, userService) {
        this.dropDownmodule = dropDownmodule;
        this.userService = userService;
        // public selectedIndex = 0;
        this.brandNames = [
            "Dell",
            "Samsung",
            "Apple",
            "Hp",
            "Nokia",
            "Redmi",
            "Motorola",
            "Linovo",
            "Panasonic"
        ];
        this.mpn = [
            "23421313123",
            "3242312132",
            "123134123",
            "44234234",
            "3242332423",
            "32434232",
            "342343432",
            "4323423423"
        ];
        this.type = [
            "dsghdsd",
            "sdmnfgd",
            "dshgjhd",
            "sadjghja",
            "dsfjgashfdh",
            "gjsagdja",
            "sdfdsaad",
            "sadasdsad",
            "sadaass"
        ];
        this.system = [
            "sdsaa",
            "sadasd",
            "dsasdas",
            "sdfdas",
            "sdfadsdd",
            "sdasdsad",
            "dSasdsad",
            "daasdsad",
            "dsasdasd"
        ];
        this.model = [
            "dsghdsd",
            "sdmnfgd",
            "dshgjhd",
            "sadjghja",
            "dsfjgashfdh",
            "gjsagdja",
            "sdfdsaad",
            "sadasdsad",
            "sadaass"
        ];
        this.displayType = [
            "sdsaa",
            "sadasd",
            "dsasdas",
            "sdfdas",
            "sdfadsdd",
            "sdasdsad",
            "dSasdsad",
            "daasdsad",
            "dsasdasd"
        ];
        this.feature1 = "1 TB storage";
        this.feature2 = "Metal body";
        this.feature3 = "500 MB SSD";
        this.myCheckColor = "#2196f3";
        this.routerExtensions = routerExtensions;
        this.userService.actionBarState(true);
        this.userService.bottomBarDots(9);
        this.userService.bottomBarDotState(true);
        this.userService.tabViewState(true);
        this.userService.tabViewStateChanges(1);
        this.userService.switchState(false);
    }
    ItemSpecifiesComponent.prototype.ngOnInit = function () { };
    // public onchange(args: SelectedIndexChangedEventData) {
    //   console.log(
    //     `Drop Down selected index changed from ${args.oldIndex} to ${
    //       args.newIndex
    //     }`
    //   );
    // }
    ItemSpecifiesComponent.prototype.onopen = function () {
        console.log("Drop Down opened.");
    };
    ItemSpecifiesComponent.prototype.onclose = function () {
        console.log("Drop Down closed.");
    };
    ItemSpecifiesComponent.prototype.onSwipe = function (args) {
        if (args.direction == 1) {
            this.routerExtensions.navigate(["/description"]);
        }
        if (args.direction == 2) {
            this.routerExtensions.navigate(["/businesspolicies"]);
        }
    };
    __decorate([
        core_1.ViewChild("dd"),
        __metadata("design:type", core_1.ElementRef)
    ], ItemSpecifiesComponent.prototype, "dropDown", void 0);
    ItemSpecifiesComponent = __decorate([
        core_1.Component({
            selector: "ns-itemspecifies",
            moduleId: module.id,
            templateUrl: "./item-specifies.component.html",
            styleUrls: ["./item-specifies.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            angular_1.DropDownModule,
            user_service_1.UserService])
    ], ItemSpecifiesComponent);
    return ItemSpecifiesComponent;
}());
exports.ItemSpecifiesComponent = ItemSpecifiesComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbS1zcGVjaWZpZXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaXRlbS1zcGVjaWZpZXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlFO0FBQ3pFLHNEQUErRDtBQUMvRCwwREFBZ0U7QUFHaEUseURBQXVEO0FBUXZEO0lBMEVFLGdDQUNFLGdCQUFrQyxFQUMxQixjQUE4QixFQUM5QixXQUF3QjtRQUR4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUF6RWxDLDRCQUE0QjtRQUNyQixlQUFVLEdBQUc7WUFDbEIsTUFBTTtZQUNOLFNBQVM7WUFDVCxPQUFPO1lBQ1AsSUFBSTtZQUNKLE9BQU87WUFDUCxPQUFPO1lBQ1AsVUFBVTtZQUNWLFFBQVE7WUFDUixXQUFXO1NBQ1osQ0FBQztRQUNLLFFBQUcsR0FBRztZQUNYLGFBQWE7WUFDYixZQUFZO1lBQ1osV0FBVztZQUNYLFVBQVU7WUFDVixZQUFZO1lBQ1osVUFBVTtZQUNWLFdBQVc7WUFDWCxZQUFZO1NBQ2IsQ0FBQztRQUNLLFNBQUksR0FBRztZQUNaLFNBQVM7WUFDVCxTQUFTO1lBQ1QsU0FBUztZQUNULFVBQVU7WUFDVixhQUFhO1lBQ2IsVUFBVTtZQUNWLFVBQVU7WUFDVixXQUFXO1lBQ1gsU0FBUztTQUNWLENBQUM7UUFDSyxXQUFNLEdBQUc7WUFDZCxPQUFPO1lBQ1AsUUFBUTtZQUNSLFNBQVM7WUFDVCxRQUFRO1lBQ1IsVUFBVTtZQUNWLFVBQVU7WUFDVixVQUFVO1lBQ1YsVUFBVTtZQUNWLFVBQVU7U0FDWCxDQUFDO1FBQ0ssVUFBSyxHQUFHO1lBQ2IsU0FBUztZQUNULFNBQVM7WUFDVCxTQUFTO1lBQ1QsVUFBVTtZQUNWLGFBQWE7WUFDYixVQUFVO1lBQ1YsVUFBVTtZQUNWLFdBQVc7WUFDWCxTQUFTO1NBQ1YsQ0FBQztRQUNLLGdCQUFXLEdBQUc7WUFDbkIsT0FBTztZQUNQLFFBQVE7WUFDUixTQUFTO1lBQ1QsUUFBUTtZQUNSLFVBQVU7WUFDVixVQUFVO1lBQ1YsVUFBVTtZQUNWLFVBQVU7WUFDVixVQUFVO1NBQ1gsQ0FBQztRQUNGLGFBQVEsR0FBRyxjQUFjLENBQUM7UUFDMUIsYUFBUSxHQUFHLFlBQVksQ0FBQztRQUN4QixhQUFRLEdBQUcsWUFBWSxDQUFDO1FBQ3hCLGlCQUFZLEdBQUcsU0FBUyxDQUFDO1FBTXZCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNsQyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVELHlDQUFRLEdBQVIsY0FBa0IsQ0FBQztJQUVuQix5REFBeUQ7SUFDekQsaUJBQWlCO0lBQ2pCLG9FQUFvRTtJQUNwRSxzQkFBc0I7SUFDdEIsU0FBUztJQUNULE9BQU87SUFDUCxJQUFJO0lBRUcsdUNBQU0sR0FBYjtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRU0sd0NBQU8sR0FBZDtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsd0NBQU8sR0FBUCxVQUFRLElBQTJCO1FBQ2pDLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7U0FDbEQ7UUFDRCxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7U0FDdkQ7SUFDSCxDQUFDO0lBaEhnQjtRQUFoQixnQkFBUyxDQUFDLElBQUksQ0FBQztrQ0FBVyxpQkFBVTs0REFBQztJQUQzQixzQkFBc0I7UUFObEMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSxpQ0FBaUM7WUFDOUMsU0FBUyxFQUFFLENBQUMsZ0NBQWdDLENBQUM7U0FDOUMsQ0FBQzt5Q0E0RW9CLHlCQUFnQjtZQUNWLHdCQUFjO1lBQ2pCLDBCQUFXO09BN0V2QixzQkFBc0IsQ0FrSGxDO0lBQUQsNkJBQUM7Q0FBQSxBQWxIRCxJQWtIQztBQWxIWSx3REFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBFbGVtZW50UmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgRHJvcERvd25Nb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWRyb3AtZG93bi9hbmd1bGFyXCI7XHJcbmltcG9ydCB7IFNlbGVjdGVkSW5kZXhDaGFuZ2VkRXZlbnREYXRhIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1kcm9wLWRvd25cIjtcclxuaW1wb3J0IHsgU3dpcGVHZXN0dXJlRXZlbnREYXRhIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZ2VzdHVyZXNcIjtcclxuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJucy1pdGVtc3BlY2lmaWVzXCIsXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICB0ZW1wbGF0ZVVybDogXCIuL2l0ZW0tc3BlY2lmaWVzLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL2l0ZW0tc3BlY2lmaWVzLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIEl0ZW1TcGVjaWZpZXNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBWaWV3Q2hpbGQoXCJkZFwiKSBkcm9wRG93bjogRWxlbWVudFJlZjtcclxuXHJcbiAgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucztcclxuICAvLyBwdWJsaWMgc2VsZWN0ZWRJbmRleCA9IDA7XHJcbiAgcHVibGljIGJyYW5kTmFtZXMgPSBbXHJcbiAgICBcIkRlbGxcIixcclxuICAgIFwiU2Ftc3VuZ1wiLFxyXG4gICAgXCJBcHBsZVwiLFxyXG4gICAgXCJIcFwiLFxyXG4gICAgXCJOb2tpYVwiLFxyXG4gICAgXCJSZWRtaVwiLFxyXG4gICAgXCJNb3Rvcm9sYVwiLFxyXG4gICAgXCJMaW5vdm9cIixcclxuICAgIFwiUGFuYXNvbmljXCJcclxuICBdO1xyXG4gIHB1YmxpYyBtcG4gPSBbXHJcbiAgICBcIjIzNDIxMzEzMTIzXCIsXHJcbiAgICBcIjMyNDIzMTIxMzJcIixcclxuICAgIFwiMTIzMTM0MTIzXCIsXHJcbiAgICBcIjQ0MjM0MjM0XCIsXHJcbiAgICBcIjMyNDIzMzI0MjNcIixcclxuICAgIFwiMzI0MzQyMzJcIixcclxuICAgIFwiMzQyMzQzNDMyXCIsXHJcbiAgICBcIjQzMjM0MjM0MjNcIlxyXG4gIF07XHJcbiAgcHVibGljIHR5cGUgPSBbXHJcbiAgICBcImRzZ2hkc2RcIixcclxuICAgIFwic2RtbmZnZFwiLFxyXG4gICAgXCJkc2hnamhkXCIsXHJcbiAgICBcInNhZGpnaGphXCIsXHJcbiAgICBcImRzZmpnYXNoZmRoXCIsXHJcbiAgICBcImdqc2FnZGphXCIsXHJcbiAgICBcInNkZmRzYWFkXCIsXHJcbiAgICBcInNhZGFzZHNhZFwiLFxyXG4gICAgXCJzYWRhYXNzXCJcclxuICBdO1xyXG4gIHB1YmxpYyBzeXN0ZW0gPSBbXHJcbiAgICBcInNkc2FhXCIsXHJcbiAgICBcInNhZGFzZFwiLFxyXG4gICAgXCJkc2FzZGFzXCIsXHJcbiAgICBcInNkZmRhc1wiLFxyXG4gICAgXCJzZGZhZHNkZFwiLFxyXG4gICAgXCJzZGFzZHNhZFwiLFxyXG4gICAgXCJkU2FzZHNhZFwiLFxyXG4gICAgXCJkYWFzZHNhZFwiLFxyXG4gICAgXCJkc2FzZGFzZFwiXHJcbiAgXTtcclxuICBwdWJsaWMgbW9kZWwgPSBbXHJcbiAgICBcImRzZ2hkc2RcIixcclxuICAgIFwic2RtbmZnZFwiLFxyXG4gICAgXCJkc2hnamhkXCIsXHJcbiAgICBcInNhZGpnaGphXCIsXHJcbiAgICBcImRzZmpnYXNoZmRoXCIsXHJcbiAgICBcImdqc2FnZGphXCIsXHJcbiAgICBcInNkZmRzYWFkXCIsXHJcbiAgICBcInNhZGFzZHNhZFwiLFxyXG4gICAgXCJzYWRhYXNzXCJcclxuICBdO1xyXG4gIHB1YmxpYyBkaXNwbGF5VHlwZSA9IFtcclxuICAgIFwic2RzYWFcIixcclxuICAgIFwic2FkYXNkXCIsXHJcbiAgICBcImRzYXNkYXNcIixcclxuICAgIFwic2RmZGFzXCIsXHJcbiAgICBcInNkZmFkc2RkXCIsXHJcbiAgICBcInNkYXNkc2FkXCIsXHJcbiAgICBcImRTYXNkc2FkXCIsXHJcbiAgICBcImRhYXNkc2FkXCIsXHJcbiAgICBcImRzYXNkYXNkXCJcclxuICBdO1xyXG4gIGZlYXR1cmUxID0gXCIxIFRCIHN0b3JhZ2VcIjtcclxuICBmZWF0dXJlMiA9IFwiTWV0YWwgYm9keVwiO1xyXG4gIGZlYXR1cmUzID0gXCI1MDAgTUIgU1NEXCI7XHJcbiAgbXlDaGVja0NvbG9yID0gXCIjMjE5NmYzXCI7XHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLFxyXG4gICAgcHJpdmF0ZSBkcm9wRG93bm1vZHVsZTogRHJvcERvd25Nb2R1bGUsXHJcbiAgICBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZVxyXG4gICkge1xyXG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zID0gcm91dGVyRXh0ZW5zaW9ucztcclxuICAgIHRoaXMudXNlclNlcnZpY2UuYWN0aW9uQmFyU3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdHMoOSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmJvdHRvbUJhckRvdFN0YXRlKHRydWUpO1xyXG4gICAgdGhpcy51c2VyU2VydmljZS50YWJWaWV3U3RhdGUodHJ1ZSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnRhYlZpZXdTdGF0ZUNoYW5nZXMoMSk7XHJcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLnN3aXRjaFN0YXRlKGZhbHNlKTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge31cclxuXHJcbiAgLy8gcHVibGljIG9uY2hhbmdlKGFyZ3M6IFNlbGVjdGVkSW5kZXhDaGFuZ2VkRXZlbnREYXRhKSB7XHJcbiAgLy8gICBjb25zb2xlLmxvZyhcclxuICAvLyAgICAgYERyb3AgRG93biBzZWxlY3RlZCBpbmRleCBjaGFuZ2VkIGZyb20gJHthcmdzLm9sZEluZGV4fSB0byAke1xyXG4gIC8vICAgICAgIGFyZ3MubmV3SW5kZXhcclxuICAvLyAgICAgfWBcclxuICAvLyAgICk7XHJcbiAgLy8gfVxyXG5cclxuICBwdWJsaWMgb25vcGVuKCkge1xyXG4gICAgY29uc29sZS5sb2coXCJEcm9wIERvd24gb3BlbmVkLlwiKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbmNsb3NlKCkge1xyXG4gICAgY29uc29sZS5sb2coXCJEcm9wIERvd24gY2xvc2VkLlwiKTtcclxuICB9XHJcblxyXG4gIG9uU3dpcGUoYXJnczogU3dpcGVHZXN0dXJlRXZlbnREYXRhKSB7XHJcbiAgICBpZiAoYXJncy5kaXJlY3Rpb24gPT0gMSkge1xyXG4gICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2Rlc2NyaXB0aW9uXCJdKTtcclxuICAgIH1cclxuICAgIGlmIChhcmdzLmRpcmVjdGlvbiA9PSAyKSB7XHJcbiAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvYnVzaW5lc3Nwb2xpY2llc1wiXSk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==
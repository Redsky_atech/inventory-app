NativeScript is an open-source framework to develop apps on the Apple iOS and Android platforms.You can learn how to build a version of this app from scratch using either our JavaScript getting started guide, or our TypeScript and Angular getting started guide.
 
 
 ##  Screenshots
![image](./src/1-SKU.jpeg)
![image](./src/2-UPC.jpeg)
![image](./src/10-Item-Specifies.jpeg) 
![image](./src/17-Add-Pictures.jpeg)
![image](./src/14-Size-Weight.jpeg)


* ## Prerequisites
1. Globally installed Nativecript  - `npm install -g nativescript`
2. Globally installed Angular CLI - `npm install -g angular-cli`
3. Mac OS to build iOS app.

## Installation
This app is built with the NativeScript CLI. Once you have the [CLI installed](https://docs.nativescript.org/start/quick-setup), start by cloning the repo:
1. `git clone https://github.com/RedskyAtech/inventory-app.git`
2. `cd inventory-app`
3. `npm install` 

## App Functions
1. Add inventory to you store
2. Remove inventory from your warehouse
3. Move inventory between storage locations
4. Real-time global inventory visibility for all your users
5. Count and correct your inventory
6. Share your inventory
7. Manage your stock transactions
8. Locate your stock

## Run iOS Application
`tns run ios --bundle --env.uglify --env.snapshot --env.aot` 
## Build IPA Using
`tns build ios --bundle --env.uglify --env.snapshot --env.aot`

## Run Android Application
`tns run android --bundle --env.uglify --env.snapshot --env.aot`
## Build APK Using
`tns build android --bundle --env.uglify --env.snapshot --env.aot`


## NOTE:

Snapshot flag is only supported by MacOS. It will stripped off in windows OS.
    







